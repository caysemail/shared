﻿using UnityEngine;

namespace Shared
{
    public static class BaseFind
    {
        public static void ClearConsole()
        {
            var logEntries = System.Type.GetType("UnityEditor.LogEntries, UnityEditor.dll");

            var clearMethod = logEntries.GetMethod("Clear",
                System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);

            clearMethod.Invoke(null, null);
        }

        public static void DebugBreak(string why, Transform t = null)
        {
            UnityEngine.Debug.LogError("Break: " + why, t);
            UnityEngine.Debug.Break();
            throw new UnityException("BREAK");
        }
    }
}