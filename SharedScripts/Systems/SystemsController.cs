﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public class SystemsController
    {
        private List<ISystem> _systems;
        
        public SystemsController CreateSystems()
        {
            _systems = new List<ISystem>();
            var systemTypes = ClassUtility.GetAllTypes(typeof(ISystem));
            string log = "";
            foreach (var type in systemTypes)
            {
                log += type.ToString() + " ";
                var obj = (ISystem)Activator.CreateInstance(type);
                _systems.Add(obj);
            }
            Debug.Log("SystemsController.StartSystems: systems added: "+log);
            return this;
        }
    }
}