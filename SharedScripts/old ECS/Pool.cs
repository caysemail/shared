﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.Ecs
{
    interface IComponentPool
    {
        //object GetExistItemById(int idx);
        //void RecycleById(int id);
        //int GetComponentTypeIndex();
    }

    public sealed class ComponentPool<T> : IComponentPool where T : struct
    {
        public static readonly ComponentPool<T> Instance = new ComponentPool<T>();

        const int MinSize = 0;

        public T[] Items = new T[MinSize];
        private int _itemsCount;

        //private int[] _reservedItems = new int[MinSize];
        //private int _reservedItemsCount;

        public int RequestNewId()
        {
            int id = 0;
//            if (_reservedItemsCount > 0)
//            {
//                id = _reservedItems[--_reservedItemsCount];
//            }
//            else
//            {
//                id = _itemsCount;
//                if (_itemsCount == Items.Length)
//                {
//                    Array.Resize(ref Items, _itemsCount << 1);
//                }
//                Items[_itemsCount++] = _creator != null ? _creator() : (T)Activator.CreateInstance(typeof(T));
//            }
            return id;
        }
    }
}