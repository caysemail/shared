﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//
//namespace Shared.Ecs
//{
//    public class Systems
//    {
//        private List<ISystem> _systems = new List<ISystem>();
//
//        public Systems AddSystem<T>() where T : ISystem, new()
//        {
//            _systems.Add(new T());
//            return this;
//        }
//
//        public void Execute(World world)
//        {
//            foreach (var system in _systems)
//            {
//                system.Execute(world);
//            }
//        }
//    }
//}