﻿using System;

namespace Shared.Ecs
{
    [Flags]
    public enum Mask
    {
        None = 0,
        CPosition = 1,
        CExplosion = 2,
    }
}