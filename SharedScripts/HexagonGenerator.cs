using UnityEngine;
using System.Collections.Generic;

namespace Shared
{
	public class HexagonGenerator
	{
		public struct Province
		{
			public Vector2 center;

			public List<Vector2> edges;

			//public bool
			public Province(Vector2 center, Vector2[] edgesPoints)
			{
				this.center = center;
				this.edges = new List<Vector2>();
				this.edges.AddRange(edgesPoints);
			}

			//public Province( Vector2 center ) { this.center = center; }
		}

		public static List<Vector2> Generate(Vector2 center, int mapRadius, float edgeLength)//, Vector2 maxSize)
		{
			List<Vector2> m_points = new List<Vector2>();
			//float ls32 = edgeLength * Mathf.Sqrt(3f) / 2f;

			//Vector2 center = new Vector2(0,0);
			m_points.Add(center);

			int start = 0;
			for (int i = 0; i < mapRadius; i++)
			{
				int count = m_points.Count;
				for (int p = start; p < count; p++)
				{
					center = m_points[p];
					//if ( Vector2.Distance(Vector2.zero, center) < 2f*ls32*(i) ) continue;
					Vector2[] centers = CreateHexagonNeighbors(center, edgeLength);
//				Vector2[] centers = new Vector2[] {
//					new Vector2(center.x, center.y + 2f*ls32),
//					new Vector2(center.x + 1.5f*edgeLength , center.y + ls32),
//					new Vector2(center.x + 1.5f*edgeLength , center.y - ls32),
//					new Vector2(center.x, center.y -2f*ls32),
//					new Vector2(center.x - 1.5f*edgeLength , center.y - ls32),
//					new Vector2(center.x - 1.5f*edgeLength , center.y + ls32),
//				};
					for (int n = 0; n < centers.Length; n++)
					{
						bool foundSame = false;
						for (int pt = 0; pt < m_points.Count; pt++)
						{
							if (m_points[pt] == centers[n])
							{
								foundSame = true;
								break;
							}
						}

						if (!foundSame)
						{
							//if (centers[n].x >= 0 && centers[n].y >= 0 &&
							//    centers[n].x <= maxSize.x && centers[n].y <= maxSize.y)
							{
								m_points.Add(centers[n]);
							}
						}
					}
				}

				start = count;
			}

			return m_points;
		}

		public static Vector2[] EdgesForCenter(Vector2 center, float edgeLength, float randomizeEdges)
		{
			Vector2[] points = CreateHexagon(center, edgeLength);
			float delta = randomizeEdges * edgeLength;
			for (int p = 0; p < points.Length; p++)
			{
				Vector2 p0 = points[p];
				Random.InitState((int) (p0.x * 10f) + (int) (10f * p0.y));
				p0 += new Vector2(Random.Range(-delta, +delta), Random.Range(-delta, +delta));
				points[p] = p0;
			}

			return points;
		}

		public static Vector2[] CreateHexagon(Vector2 center, float length)
		{
			float xDisp = center.x;
			float yDisp = center.y;
			//var length = 10;
			float ls32 = length * Mathf.Sqrt(3f) / 2f;
			float half = length / 2f;
			Vector2[] points = new Vector2[]
			{
				new Vector2(xDisp + half, yDisp + ls32),
				new Vector2(xDisp + length, yDisp),
				new Vector2(xDisp + half, yDisp - ls32),
				new Vector2(xDisp - half, yDisp - ls32),
				new Vector2(xDisp - length, yDisp),
				new Vector2(xDisp - half, yDisp + ls32),
			};
			return points;
		}

		public static Vector2[] CreateHexagonRound3(Vector2 center, float length)
		{
			var points = CreateHexagon(center, length);
			for (int i = 0; i < points.Length; i++)
				points[i] = new Vector2(points[i].x.Cut3(), points[i].y.Cut3());
			return points;
		}

		public static Vector2[] CreateHexagonNeighbors(Vector2 center, float edgeLength)
		{
			float ls32 = edgeLength * Mathf.Sqrt(3f) / 2f;
			Vector2[] centers = new Vector2[]
			{
				new Vector2(center.x, center.y + 2f * ls32),
				new Vector2(center.x + 1.5f * edgeLength, center.y + ls32),
				new Vector2(center.x + 1.5f * edgeLength, center.y - ls32),
				new Vector2(center.x, center.y - 2f * ls32),
				new Vector2(center.x - 1.5f * edgeLength, center.y - ls32),
				new Vector2(center.x - 1.5f * edgeLength, center.y + ls32),
			};
			return centers;
		}

//	static public List<Province> Generate() {
//		List<Province> provinces = new List<Province>();
//		return provinces;
//	}
	}

}