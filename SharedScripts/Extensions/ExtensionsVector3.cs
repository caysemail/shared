using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;

namespace Shared
{
    public static class ExtensionVector3
    {
        public static unsafe int FloatToInt32Bits( float f )
        {
            return *( (int*)&f );
        }
        
        public static string ToStringFull(this Vector3 source)
        {
            return "("+source.x.ToString(CultureInfo.InvariantCulture)+", "
                   +source.y.ToString(CultureInfo.InvariantCulture)
                   +", "+source.z.ToString(CultureInfo.InvariantCulture)+")"
                   +" (x="+FloatToInt32Bits(source.x)+", z="+FloatToInt32Bits(source.z)+")"
                   ;
        }
        
        //[__DynamicallyInvokable]
        public static Vector3 Sum<TSource>(
            this IEnumerable<TSource> source,
            Func<TSource, Vector3> selector)
        {
            return source.Select<TSource, Vector3>(selector).Sum();
        }
        
        public static Vector3 Sum(this IEnumerable<Vector3> source)
        {
            if (source == null)
                throw new ArgumentException(nameof (source));
            Vector3 sum = default;
            foreach (var value in source)
                sum += value;
            return sum;
        }
        
        public static Vector3 Average<TSource>(
            this IEnumerable<TSource> source,
            Func<TSource, Vector3> selector)
        {
            return source.Select<TSource, Vector3>(selector).Average();
        }
        
        public static Vector3 Average(this IEnumerable<Vector3> source)
        {
            if (source == null)
                throw new ArgumentException(nameof (source));
            Vector3 sum = default;
            int count = 0;
            foreach (var value in source)
            {
                sum += value;
                count++;
            }
            sum /= count;
            return sum;
        }
    }
}