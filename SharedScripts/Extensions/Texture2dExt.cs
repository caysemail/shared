using UnityEngine;

namespace Shared
{
    public static class Texture2DExt
    {
        public static Texture2D TextureScaling(Texture2D texture, float superSize)
        {
            int newWidth = (int)(texture.width * superSize);
            int newHeight = (int)(texture.height * superSize);

            RenderTexture rt = new RenderTexture(newWidth, newHeight, 24);
            RenderTexture.active = rt;
            Graphics.Blit(texture, rt);
            Texture2D result = new Texture2D(newWidth, newHeight);
            result.ReadPixels(new Rect(0, 0, newWidth, newHeight), 0, 0);
            result.Apply();
     
            Debug.Log(result.width + "x" + result.height);
            return result;
        }

        public static Texture2D Copy(Texture2D original)
        {
            Texture2D copyTexture = new Texture2D(original.width, original.height, original.format, original.mipmapCount > 0);
            Graphics.CopyTexture(original, copyTexture);
            return copyTexture;
        }
        
        public static void UpdateTextureByMaterial(this Texture2D texture2d, Material material)
        {
            var texSize = texture2d.width;
            // create objects
            var renderTexture = new RenderTexture(texSize, texSize, 0);
            // set state
            var previousRT = RenderTexture.active;
            RenderTexture.active = renderTexture;
            // modify
            Graphics.Blit(texture2d, renderTexture, material);
            // копируем активную рендер текстуру в текстуру (в оперативную память)
            texture2d.ReadPixels(new Rect(0, 0, texSize, texSize), 0, 0);
            // завершаем копирование
            texture2d.Apply();
            // release state
            RenderTexture.active = previousRT; 
            renderTexture.Release();
        }
        
        public static void SaveAsPNG (this Texture2D origTex)
        {
            //savePath = savePath.Replace(Application.dataPath, "Assets");
            var path = @"Assets\texture_"+Random.Range(1000, 99999)+".png";
            System.IO.File.WriteAllBytes(path, origTex.EncodeToPNG());

#if UNITY_EDITOR
            UnityEditor.AssetDatabase.Refresh();
#endif
        }

    }
}