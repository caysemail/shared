﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace Shared
{
    public static class TaskEx
    {
        public static IEnumerator AsIEnumerator(this Task task)
        {
            while (!task.IsCompleted)
            {
                yield return null;
            }

            if (task.IsFaulted)
            {
                throw task.Exception;
            }
        }

        /// <summary>
        /// Blocks while condition is true or timeout occurs.
        /// </summary>
        /// <param name="condition">The condition that will perpetuate the block.</param>
        /// <param name="frequency">The frequency at which the condition will be check, in milliseconds.</param>
        /// <param name="timeout">Timeout in milliseconds.</param>
        /// <exception cref="TimeoutException"></exception>
        /// <returns></returns>
        public static async Task WaitWhile(Func<bool> condition, int frequency = 25, int timeout = -1)
        {
            var waitTask = Task.Run(async () =>
            {
                while (condition() && Application.isPlaying) await Task.Delay(frequency);
            });

            if(waitTask != await Task.WhenAny(waitTask, Task.Delay(timeout)))
                throw new TimeoutException();
        }

        /// <summary>
        /// Blocks until condition is true or timeout occurs.
        /// </summary>
        /// <param name="condition">The break condition.</param>
        /// <param name="timeout">The timeout in milliseconds.</param>
        /// <param name="frequency">The frequency at which the condition will be checked.</param>
        /// <returns></returns>
        public static async Task WaitUntil(Func<bool> condition, int timeout = -1, int frequency = 25)
        {
            var waitTask = Task.Run(async () =>
            {
                while (!condition())// && Application.isPlaying)
                {
                    //Debug.LogError("WaitUntil in progress: condition()="+condition());
                    await Task.Delay(frequency);
                }
                //Debug.LogError("WaitUntil completed1: condition()="+condition());
            });

            var completedTask = await Task.WhenAny(waitTask, Task.Delay(timeout));
            
            if (waitTask != completedTask) 
                throw new TimeoutException();

            await completedTask;

            //Debug.LogError("WaitUntil completed2: condition()="+condition());
        }
    }
    
    //[__DynamicallyInvokable]
    public static class Extensions
    {
        
        public static void SetLayerOnAllRecursive(this GameObject obj, int layer) {
            obj.layer = layer;
            foreach (Transform child in obj.transform) {
                SetLayerOnAllRecursive(child.gameObject, layer);
            }
        }

        public static bool Approximately(this Quaternion quatA, Quaternion value, float acceptableRange)
        {
            return 1 - Mathf.Abs(Quaternion.Dot(quatA, value)) < acceptableRange;
        }   
        
        public static float norm(this Quaternion q) {
            return q.w * q.w + q.x * q.x + q.y * q.y + q.z * q.z;
        }

        public static int ToInt(this bool v)
        {
            return v ? 1 : 0;
        }
        



        
        [MethodImpl((MethodImplOptions) 256)]
        public static Vector2 Mult(this Vector2 a, Vector2 b)
        {
            return new Vector2(a.x*b.x, a.y*b.y);
        }
        
        private static readonly Vector2 FIX01_MINMAX = new Vector2(0.05f, 0.95f);
        public static Vector3 Fix01(this Vector3 a)
        {
            //a.x = ((int)(a.x * 1000f)) / 1000f;
            //a.y = ((int)(a.y * 1000f)) / 1000f;
            //a.z = ((int)(a.z * 1000f)) / 1000f;
            //a.x = (float)System.Math.Round(a.x, 3);
            //a.y = (float)System.Math.Round(a.y, 3);
            //a.z = (float)System.Math.Round(a.z, 3);
            if (a.x <= FIX01_MINMAX.x) a.x = 0;
            if (a.y <= FIX01_MINMAX.x) a.y = 0;
            if (a.z <= FIX01_MINMAX.x) a.z = 0;
            if (a.x >= FIX01_MINMAX.y) a.x = 1;
            if (a.y >= FIX01_MINMAX.y) a.y = 1;
            if (a.z >= FIX01_MINMAX.y) a.z = 1;
            return a;
        }

        public static Vector2 Fix01(this Vector2 a)
        {
            if (a.x <= FIX01_MINMAX.x) a.x = 0;
            if (a.y <= FIX01_MINMAX.x) a.y = 0;
            if (a.x >= FIX01_MINMAX.y) a.x = 1;
            if (a.y >= FIX01_MINMAX.y) a.y = 1;
            return a;
        }

     

        public static float GetRadius(this Bounds b)
        {
            var e = b.extents;
            return Mathf.Sqrt(e.x * e.x + e.y * e.y + e.z * e.z);
        }

        public static void Show(this GameObject t) { t.SetActive(true); }
        public static void Hide(this GameObject t) { t.SetActive(false); }
        public static void Show(this Transform t) { t.gameObject.SetActive(true); }
        public static void Hide(this Transform t) { t.gameObject.SetActive(false); }
        public static void Show(this Button b) { b.gameObject.SetActive(true); }
        public static void Hide(this Button b) { b.gameObject.SetActive(false); }
        public static void Show(this Text t) { t.gameObject.SetActive(true); }
        public static void Hide(this Text t) { t.gameObject.SetActive(false); }
        public static void Show(this Image t) { t.gameObject.SetActive(true); }
        public static void Hide(this Image t) { t.gameObject.SetActive(false); }

        public static string ToDebug(this List<string> list)
        {
            if (list == null)
                return "null";
            string total = "";
            foreach (var item in list)
            {
                total += item + ", ";
            }

            if (list.Count > 0)
                return total.Remove(total.Length - 2, 2);
            return "empty";
        }

        public static void SetColorAlpha(this Image image, float a)
        {
            image.color = image.color.SetA(a);
        }

        public static Vector2 Clamp(this Vector2 a, float min, float max)
        {
            return new Vector2(Mathf.Clamp(a.x, min, max), Mathf.Clamp(a.y, min, max));
        }
        //public static Vector3 ToVector3(this Vector3Int a) { return new Vector3(a.x, a.y, a.z); }


        public static string ToStringEx(this List<Vector3> list)
        {
            var text = "[";
            foreach (var v3 in list)
            {
                text += v3 + ",";
            }
            return text + "]";
        }
        



        public static List<Vector3> ElementsAdd(this List<Vector3> collection, Vector3 v)
        {
            for (var index = 0; index < collection.Count; index++)
                collection[index] += v;
            return collection;
        }
      
        public static List<Vector3> ElementsMult(this List<Vector3> collection, Vector3 v)
        {
            for (var index = 0; index < collection.Count; index++)
                collection[index] = collection[index].Mult(v);
            return collection;
        }
        
        public static List<Vector2> ElementsMult(this List<Vector2> collection, Vector2 v)
        {
            for (var index = 0; index < collection.Count; index++)
                collection[index] = collection[index].Mult(v);
            return collection;
        }
        
        public static List<Vector3> ElementsMult(this List<Vector3> collection, List<Vector3> collectionToMult)
        {
            Assert.IsTrue(collection.Count == collectionToMult.Count);
            for (var index = 0; index < collection.Count; index++)
                collection[index] = collection[index].Mult(collectionToMult[index]);
            return collection;
        }



        public static void Destroy(this GameObject go)
        {
            if (Application.isPlaying)
                GameObject.Destroy(go);
            else
                GameObject.DestroyImmediate(go);
        }
        
        public static void Destroy(this List<GameObject> list)
        {
            foreach (var element in list) GameObject.Destroy(element);
            list.Clear();
        }
        public static void DestroyImmediate(this List<GameObject> list)
        {
            foreach (var element in list) GameObject.DestroyImmediate(element);
            list.Clear();
        }

        public static void Destroy(this List<Transform> list)
        {
            if (Application.isPlaying)
            {
                foreach (var element in list)
                    if (element != null)
                        GameObject.Destroy(element.gameObject);
            }
            else
            {
                foreach (var element in list) if (element != null) GameObject.DestroyImmediate(element.gameObject);
            }

            list.Clear();
        }


    }
}