﻿using UnityEngine;

namespace Shared
{
    public static class MeshFilterExt
    {
        public static void SetMesh(this MeshFilter mf, Mesh mesh)
        {
            if (Application.isPlaying) mf.mesh = mesh;
            else mf.sharedMesh = mesh;
        }
    }
}