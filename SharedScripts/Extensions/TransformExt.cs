using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace Shared
{
    public static class TransformExt
    {
        public static void UpdateLocalChildren(this Transform parent)
        {
            foreach (Transform child in parent)
            {
                var component = child.GetComponent<IUpdate>();
                if (component != null)
                    component.OnUpdate();
            }
        }

        public static void ResetChildren(this Transform parent)
        {
            var components = parent.GetComponentsInChildren<IReset>(true);
            foreach (var child in components)
            {
                if (child == null) Debug.LogError("found null for child of type IReset");
                else
                {
                    //Debug.LogWarning("ResetChildren: "+((Component)child).name);
                    child.OnReset();
                }
            }
        }
        
        public static T GetComponentInLocalChildren<T>(this Transform parent) where T : MonoBehaviour
        {
            foreach (Transform child in parent)
            {
                var component = child.GetComponent<T>();
                if (component != null)
                    return component;
            }

            return null;
        }
        
        public static Transform FindDeepChild(this Transform aParent, string aName)
        {
            var result = aParent.Find(aName);
            if (result != null)
                return result;
            foreach (Transform child in aParent)
            {
                result = child.FindDeepChild(aName);
                if (result != null)
                    return result;
            }
            return null;
        }

        public static T GetOrAddComponent<T>(this Transform t) where T : Component
        {
            var c = t.GetComponent<T>();
            if (c == null)
                c = t.gameObject.AddComponent<T>();
            return c;
        }
        
        public static Transform InstantiateChild(this Transform t, string childName)
        {
            var transform = new GameObject(childName).transform;
            transform.SetParent(t, false);
            return transform;
        }

        public static T InstantiateChild<T>(this Transform t, string childName) where T : MonoBehaviour
        {
            var transform = new GameObject(childName).transform;
            transform.SetParent(t, false);
            return transform.GetOrAddComponent<T>();
        }

        public static Transform ReInstantiateChild(this Transform t, string childName)
        {
            var child = t.Find(childName);
            if (child != null) child.DestroyToNull();
            var transform = new GameObject(childName).transform;
            transform.SetParent(t, false);
            return transform;
        }

        public static Transform DestroyToNull(this Transform t)
        {
            t.gameObject.SetActive(false);
            if (t != null)
            {
                if (Application.isPlaying) Object.Destroy(t.gameObject);
                else Object.DestroyImmediate(t.gameObject);
            }
            return null;
        }
        
        public static void DestroyChildren(this Transform transform)
        {
            for (int i = transform.childCount-1; i >= 0; i--)
                transform.GetChild(i).DestroyToNull();
        }

        public static void DisableChildren(this Transform transform)
        {
            for (int i = transform.childCount-1; i >= 0; i--)
                transform.GetChild(i).gameObject.SetActive(false);
        }

        public static Transform FindOrCreateChild(this Transform transform, string childName)
        {
            var child = transform.Find(childName);
            if (!child)
            {
                var childGo = new GameObject(childName);
                childGo.transform.SetParent(transform, false);
                child = childGo.transform;
            }
            Assert.IsNotNull(child);
            return child;
        }
        
        

    }
}