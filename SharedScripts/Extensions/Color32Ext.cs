using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Shared
{
    public static class Color32Ext
    {
        public static Color32 COLOR32_ZERO = new Color32(0, 0, 0, 0);
        public static Color32 zero = new Color32(0, 0, 0, 0);
        public static Color32 black = new Color32(0, 0, 0, 255);
        public static Color32 white = new Color32(255, 255, 255, 255);
        public static Color32 whiteTransparent = new Color32(255, 255, 255, 0);
        public static Color32 one = new Color32(255, 255, 255, 255);
        public static Color32 gray = new Color32(127, 127, 127, 255);
        public static Color32 grayTransparent = new Color32(127, 127, 127, 0);
        public static Color32 magenta = new Color32(255, 0, 255, 255);
        
                
        public static Color[] ToColorArray(this List<Color32> collection)
        {
            Color[] colorArray = new Color[collection.Count];
            for (var index = 0; index < collection.Count; index++)
            {
                colorArray[index] = collection[index];
            }

            return colorArray;
        }
        
        public static Color32 SetA(this Color32 color, int a)
        {
            color.a = (byte)a;
            return color;
        }
        
        public static Color32 MultRGB(this Color32 color, float value)
        {
            color.r = (byte)((float)color.r * value);
            color.g = (byte)((float)color.g * value);
            color.b = (byte)((float)color.b * value);
            return color;
        }

        public static Color32 AddRGB(this Color32 color, Color32 add)
        {
            color.r = (byte)(color.r + add.r);
            color.g = (byte)(color.g + add.g);
            color.b = (byte)(color.b + add.b);
            return color;
        }

        public static Color32 AddRGB(this Color32 color, float value)
        {
            color.r = (byte)((float)color.r + value * 255f);
            color.g = (byte)((float)color.g + value * 255f);
            color.b = (byte)((float)color.b + value * 255f);
            return color;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)] // 256
        public static bool EqualsRgb(this Color32 color, Color32 value)
        {
            return color.r == value.r && color.g == value.g && color.b == value.b;
        }

        public static Color32 SetRgb(this Color32 color, Color32 value)
        {
            color.r = value.r;
            color.g = value.g;
            color.b = value.b;
            return color;
        }

    }
}