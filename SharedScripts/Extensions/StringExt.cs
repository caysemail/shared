using System;
using UnityEngine;

namespace Shared
{
    public static class StringExt
    {
        public static string Colored(this string text, Color color)
        {
            return "<color=#" + ColorUtility.ToHtmlStringRGBA(color) + ">" + text + "</color>";
        }
        
        public static string SubstringBefore(this string source, string before)
        {
            var index = source.IndexOf(before, StringComparison.InvariantCultureIgnoreCase);
            var result = source.Substring(0, index);
            return result;
        }
    }
}