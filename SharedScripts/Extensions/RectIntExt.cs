using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public static class RectIntExt
    {
        public static List<Vector2Int> ToList(this RectInt source)
        {
            var result = new List<Vector2Int>();
            for (int y = source.yMin; y < source.yMax; y++)
            {
                for (int x = source.xMin; x < source.xMax; x++)
                {
                    result.Add(new Vector2Int(x,y));
                }
            }
            return result;
        }

        public static bool Contains(this RectInt source, RectInt other)
        {
            if (!source.Contains(other.min)) return false;
            if (!source.Contains(other.max)) return false;
            return true;
        }
    }
}