using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Shared
{
    public static class ListExt
    {
        public static int IndexOf<TSource>(this List<TSource> source, Func<TSource, bool> predicate) //where TSource : struct
        {
     
            var index = 0;
            foreach (var item in source) {
                if (predicate.Invoke(item)) {
                    return index;
                }
                index++;
            }

            return -1;
        }
        
        // public static T GetNext<T>(this List<T> list, int index)
        // {
        //     index = index == list.Count - 1 ? 
        //     var item = list[list.Count-1];
        //     list.RemoveAt(list.Count-1);
        //     return item;
        // }
        
        public static void ForEach<T>(
            this List<T> source,
            Action<T, T> action) {
            if (source == null) throw new ArgumentNullException("source");
            if (action == null) throw new ArgumentNullException("action");

            for (var index = 0; index + 1 < source.Count; index++)
            {
                var item0 = source[index];
                var item1 = source[index+1];
                action(item0, item1);
            }
        }

        public static T PopFirstOrDefault<T>(this List<T> list)
        {
            if (list.Count == 0) return default(T);
            var item = list[0];
            list.RemoveAt(0);
            return item;
        }
        
        public static bool TryPopFirst<T>(this List<T> list, out T item)
        {
            item = default(T);
            if (list.Count == 0) return false;
            item = list[0];
            list.RemoveAt(0);
            return true;
        }

        public static T PopLast<T>(this List<T> list)
        {
            var item = list[list.Count-1];
            list.RemoveAt(list.Count-1);
            return item;
        }
        
        public static T Pop<T>(this List<T> list)
        {
            var item = list[0];
            list.RemoveAt(0);
            return item;
        }


        public static T Last<T>(this List<T> list)
        {
            return list.Count == 0 ? default(T) : list[list.Count-1];
        }
        public static T PreLast<T>(this List<T> list)
        {
            return list.Count <= 1 ? default(T) : list[list.Count-2];
        }
        public static void RemoveLast<T>(this List<T> list)
        {
            list.RemoveAt(list.Count-1);
        }
        public static int RandomIndex<T>(this List<T> list)
        {
            return UnityEngine.Random.Range(0, list.Count);
        }
        public static T Random<T>(this List<T> list)
        {
            Assert.IsNotNull(list);
            Assert.IsTrue(list.Count > 0);
            return list[UnityEngine.Random.Range(0, list.Count)];
        }

//        public static List<T> Randomize<T>(this List<T> list)
//        {
//            Assert.IsNotNull(list);
//
//            return list[UnityEngine.Random.Range(0, list.Count)];
//        }

        public static void Randomize<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = UnityEngine.Random.Range(0, n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static object Random(this System.Collections.ArrayList list)
        {
            return list[UnityEngine.Random.Range(0, list.Count)];
        }

        public static int GetFromMinMax(this Array array, float value)
        {
            Assert.IsNotNull(array);
            int[] intArray = array as int[];
            int min = intArray.Length == 0 ? 0 : intArray[0];
            int max = intArray.Length < 2 ? min : intArray[1];
            if (min == max) return min;
            return min + Mathf.RoundToInt(value * (max-min));
        }

        public static int GetRandomFromMinMax(this Array array)
        {
            Assert.IsNotNull(array);
            int[] intArray = array as int[];
            int min = intArray.Length == 0 ? 0 : intArray[0];
            int max = intArray.Length < 2 ? min : intArray[1];
            if (min == max) return min;
            return UnityEngine.Random.Range(min, max+1);
        }
        
        public static object Random(this Array array)
        {
            return array.GetValue(UnityEngine.Random.Range(0,array.Length));
        }
        
        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = UnityEngine.Random.Range(0, n + 1 +1); //    int k = rng.Next(n + 1);  
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
        
        public static string ToStringList<T>(this IList<T> list)
        {
            string info = "";
            for (var index = 0; index < list.Count; index++)
            {
                var item = list[index];
                info += item.ToString();
                if (index + 1 < list.Count)
                    info += ", ";
            }

            return info;
        }
    }
}