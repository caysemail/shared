using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public static class ColorExt
    {
        
        public static Color greenYellow = new Color(0.7f, 1.0f, 0f);
        public static Color blueLight = new Color(0.0f, 0.7f, 1f);
        public static Color orangeLight = new Color(1.0f, 0.75f, 0f);
        public static Color redOrange = new Color(1.0f, 0.33f, 0f);
        public static Color greenDark = new Color(0.0f, 0.50f, 0f);
        public static Color redDark = new Color(0.60f, 0.00f, 0f);
        public static Color brown = new Color(0.40f, 0.15f, 0f);
        public static Color greenSemiDark = new Color(0.0f, 0.750f, 0f);
        public static Color zero = new Color(0, 0, 0, 0);
        public static Color darkGray = new Color(0.25f, 0.25f, 0.25f);
        
        
        public static Color SetA(this Color color, float a) { color.a = a; return color; }
        public static Color SetB(this Color color, float b) { color.b = b; return color; }

        public static float GetH(this Color color)
        {
            Color.RGBToHSV(color, out var h, out var s, out var v);
            return h;
        }
        
        public static Color Max(this Color a, Color b)
        {
            return new Color(Mathf.Max(a.r, b.r), Mathf.Max(a.g, b.g), Mathf.Max(a.b, b.b), 1f);
        }

        public static Color SetH(this Color color, float H)
        {
            Color.RGBToHSV(color, out var h, out var s, out var v);
            h = H;
            return Color.HSVToRGB(h,s,v);
        }

        public static Color SetV(this Color color, float V)
        {
            Color.RGBToHSV(color, out var h, out var s, out var v);
            v = V;
            return Color.HSVToRGB(h,s,v);
        }
        
        public static Color GetHSV(this Color color)
        {
            Color.RGBToHSV(color, out var h, out var s, out var v);
            return new Color(h,s,v,color.a);
        }

        public static Color ToVisible(this Color color)
        {
            if (color.r < 0.5f) color.r = 1f - color.r;
            if (color.g < 0.5f) color.g = 1f - color.g;
            if (color.b < 0.5f) color.b = 1f - color.b;
            return color;
        }

        public static Color MakeBrighter(this Color color)
        {
            var maxValue = Mathf.Max(color.r, color.g, color.b);
            var delta = 1f / maxValue;
            return (color * delta);
        }
        
        public static Color MakeDarker(this Color color)
        {
            // 0.5, 0.1, 0.3 or 0.5,0.5,0.5
            var maxValue = Mathf.Max(color.r, color.g, color.b);
            var delta = maxValue*10;
            return (color / delta);
        }

        public static Color IdToColor(int id)
        {
            Random.InitState(id);
            var max = 0.95f;
            var min = 0.4f;
            
            
            var color = new Color(Random.Range(min, max), Random.Range(min, max), Random.Range(min, max), 1);
            //var t = 0.3f;
            //if (color.r < t && color.g < t && color.b < t)
            //    color = new Color(Random.Range(t, 1f), Random.Range(t, 1f), Random.Range(t, 1f));
            
            return color;
        }
        
        public static Color IdToColorNotGray(int id)
        {
            Random.InitState(id);
            var max = 0.6f;
            var min = 0.4f;
            bool side = Random.value > 0.5f;
            float r, g, b;
            r = side ? Random.Range(max, 0.9f) : Random.Range(0.1f, min);  
            g = side ? Random.Range(max, 0.9f) : Random.Range(0.1f, min);  
            b = side ? Random.Range(max, 0.9f) : Random.Range(0.1f, min);
            var color = new Color(r,g,b,1);
            return color;
        }

    }
}