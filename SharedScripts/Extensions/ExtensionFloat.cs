namespace Shared
{
    public static class ExtensionFloat
    {
        public static float Cut3(this float val)
        {
            return (float)((int)(val*1000f))/1000f;
        }
        
    }
}