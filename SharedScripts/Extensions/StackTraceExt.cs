using System.Diagnostics;
using System.Text;

namespace Shared
{
    public static class StackTraceExt
    {
        public static string GetSinge()
        {
            var st = new StackTrace(1, true);
            var sf = st.GetFrame(0);

            StringBuilder sb = new StringBuilder("");
            sb.AppendFormat("{4} (at <a href=\"{2}\" line=\"{3}\">{0}:{1}</a>)", 
                sf.GetMethod().DeclaringType?.FullName ?? "No Method Name", 
                sf.GetMethod().Name+":"+sf.GetFileLineNumber(), 
                sf.GetFileName()?.Replace('\\','/') ?? "No File Name", 
                sf.GetFileLineNumber(),
                sf.GetMethod().Name);

            return sb.ToString()+"\n";
        }
    }
}