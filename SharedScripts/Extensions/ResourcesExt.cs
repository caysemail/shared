using UnityEngine;
using UnityEngine.Assertions;

namespace Shared
{
    public static class ResourcesExt
    {
        public static void LoadConfig<T>(ref T obj) where T : ScriptableObject
        {
            if (obj != null) return;
            obj = LoadConfig<T>();
        }
        
        public static T LoadConfig<T>() where T : ScriptableObject
        {
            T obj = default;

            var type = typeof(T);
            string name = type.Name.Replace("Config", "");
            var path = @"Configs/" + name;
            obj = (T)Resources.Load<ScriptableObject>(path);
            Assert.IsNotNull(obj, "LoadConfig<"+type+"> - resource not found at path="+path);
            
            //Debug.LogError("T="+type+", name="+name);
            
            return obj;
        }
    }
}