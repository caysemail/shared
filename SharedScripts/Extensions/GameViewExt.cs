#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace Shared
{
    public static class GameViewExt
    {
        // bad idea
        // just use var mousePos = Event.current.mousePosition; in void OnGUI(){
        
        public static bool TryGetGameViewWorldPositionInEditor(out Vector3 worldPosition)
        {
            worldPosition = default;
            bool isOk = TryGetGameViewNormalizedMousePositionInEditor(out var normalized);
            if (!isOk) return isOk;
            worldPosition = Camera.main.ViewportToWorldPoint(normalized);
            return true;
        }

        public static bool TryGetGameViewNormalizedMousePositionInEditor(out Vector2 mousePosition)
        {
            mousePosition = default;
        
            EditorWindow mouseOverWindow = EditorWindow.mouseOverWindow;
            if (mouseOverWindow == null) return false;
            var windowName = mouseOverWindow.GetType().ToString();
            if (windowName != "UnityEditor.GameView") return false;
            var propertyValue = mouseOverWindow.GetPrivateFieldValue<Rect>("m_GameViewRect");
            //var maxRect = mouseOverWindow.rootVisualElement.contentRect;
            var mousePos = Event.current.mousePosition;
            mousePos = GUIUtility.GUIToScreenPoint(mousePos);
            mousePos -= new Vector2(propertyValue.x, propertyValue.y);
            //mousePos /= new Vector2(maxRect.width, maxRect.height);
            mousePos /= new Vector2(propertyValue.width, propertyValue.height);
            mousePos.y = 1 - mousePos.y;
            mousePosition = mousePos;

            return true;
        }
    }
}
#endif
