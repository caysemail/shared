using System;
using System.Collections.Generic;
using System.Linq;

namespace Shared
{
    public static class EnumExt
    {
    }
    
    /// <summary> Enum Extension Methods </summary>
    /// <typeparam name="T"> type of Enum </typeparam>
    public class Enum<T> where T : struct, IConvertible
    {
        public static int Count
        {
            get
            {
                if (!typeof(T).IsEnum)
                    throw new ArgumentException("T must be an enumerated type");

                return Enum.GetNames(typeof(T)).Length;
            }
        }
        
        public static List<T> Values
        {
            get
            {
                if (!typeof(T).IsEnum)
                    throw new ArgumentException("T must be an enumerated type");

                var values = Enum.GetValues(typeof(T));
                var list = new List<T>();
                for (int i = 0; i < values.Length; i++)
                {
                    list.Add((T)values.GetValue(i));
                }
                return list;
            }
        }

    }
}