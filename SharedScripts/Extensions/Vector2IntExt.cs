﻿using System.Runtime.CompilerServices;
using UnityEngine;

namespace Shared
{
    public static class Vector2IntExt
    {
        public static Vector2 ToVector2(this Vector2Int position)
        {
            return new Vector2(position.x, position.y);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 ToVector3(this Vector2Int position)
        {
            return new Vector3(position.x, position.y, 0);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 ToVector3(this Vector2Int position, float addX, float addY)
        {
            return new Vector3(addX+position.x, addY+position.y, 0);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 ToVector3XyZ(this Vector2Int position)
        {
            return new Vector3(position.x, 0, position.y);
        }

        public static Vector2Int[] dirs8 = new[]
        {
            new Vector2Int(0, 1), 
            new Vector2Int(1, 1), new Vector2Int(1, 0), new Vector2Int(1, -1), 
            new Vector2Int(0, -1), 
            new Vector2Int(-1, -1), new Vector2Int(-1, 0), new Vector2Int(-1, 1)
        };
    }
}