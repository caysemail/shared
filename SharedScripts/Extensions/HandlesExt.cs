#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace Shared
{
    public static class HandlesExt
    {
        /// <summary>
        ///   <para>Draw a dotted square (XZ) with center and size.</para>
        /// </summary>
        /// <param name="center"></param>
        /// <param name="size"></param>
        public static void DrawDottedSquare(Vector2 center, Vector2 size)
        {
            // p1 p2
            // p0 p3
            var p0 = center.ToXyZ() - size.ToXyZ() / 2f;
            var p2 = center.ToXyZ() + size.ToXyZ() / 2f;
            var p1 = p0 + new Vector3(0, 0, size.y);
            var p3 = p0 + new Vector3(size.x, 0, 0);
                
            Handles.DrawDottedLine(p0, p1, 1f);
            Handles.DrawDottedLine(p0, p3, 1f);
            Handles.DrawDottedLine(p2, p1, 1f);
            Handles.DrawDottedLine(p2, p3, 1f);
        }

    }
}
#endif