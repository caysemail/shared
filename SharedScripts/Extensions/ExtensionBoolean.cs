﻿namespace Shared
{
    public static class ExtensionBoolean
    {
        public static bool Invert(this bool val)
        {
            return !val;
        }
        
        public static void Subscribe(this bool val, System.Action<bool> callback)
        {
            return;
        }

    }
}