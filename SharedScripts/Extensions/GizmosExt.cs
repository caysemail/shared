#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;

namespace Shared
{
    public static class GizmosExt
    {
        //public GizmosSorted Sorted => GizmosSorted;
        
        // 678
        // 345
        // 012
        private static List<Vector2> DigitalPoints0 = new()
        {
            new(-1, -2), new(-1, +2),
            new(+1, +2), new(+1, -2), new(-1, -2)
        };
        private static List<Vector2> DigitalPoints1 = new ()
        {
            new (-1, 0), new (0, +2), new (0, -2),
        };
        private static List<Vector2> DigitalPoints2 = new ()
        {
            new (-1, +2), new (+1, +2), new (+1, +0), new (-1, +0),
            new (-1, -2), new (+1, -2),
        };
        private static List<Vector2> DigitalPoints3 = new ()
        {
            new (-1, +2), new (+1, +2), new (+1, 0),
            new (-1, 0),new (+1, 0),
            new (+1, -2),new (-1, -2),
        };
        private static List<Vector2> DigitalPoints4 = new ()
        {
            new (-1, +2), new (-1, 0), new (+1, 0),
            new (+1, 2),new (+1, -2),
        };
        private static List<Vector2> DigitalPoints5 = new ()
        {
            new (+1, +2), new (-1, +2), new (-1, +0), new (+1, +0),
            new (+1, -2), new (-1, -2),
        };
        private static List<Vector2> DigitalPoints6 = new ()
        {
            new (+1, +2), new (-1, +2), new (-1, -2), new (+1, -2),
            new (+1, 0), new (-1, 0),
        };
        private static List<Vector2> DigitalPoints7 = new ()
        {
            new (-1, +2), new (+1, +2), new (-1, -2)
        };
        private static List<Vector2> DigitalPoints8 = new ()
        {
            new (-1, -2), new (-1, +2),
            new (+1, +2), new (+1, -2), new (-1, -2),
            new (-1, 0), new (+1, 0)
        };
        private static List<Vector2> DigitalPoints9 = new ()
        {
            new (-1, -2), new (+1, 0),
            new (+1, +2), new (-1, +2), new (-1, 0),
            new (+1, 0)
        };

        public static void DrawNumber(Vector3 worldPos, string number, float scale)
        {
            var stepRight = Vector3.right * scale * 1.3f;
            
            for (var index = 0; index < number.Length; index++)
            {
                var digit = number[index];
                switch (digit)
                {
                    case '0': DrawDigital(worldPos+stepRight*index, 0, scale); break;
                    case '1': DrawDigital(worldPos+stepRight*index, 1, scale); break;
                    case '2': DrawDigital(worldPos+stepRight*index, 2, scale); break;
                    case '3': DrawDigital(worldPos+stepRight*index, 3, scale); break;
                    case '4': DrawDigital(worldPos+stepRight*index, 4, scale); break;
                    case '5': DrawDigital(worldPos+stepRight*index, 5, scale); break;
                    case '6': DrawDigital(worldPos+stepRight*index, 6, scale); break;
                    case '7': DrawDigital(worldPos+stepRight*index, 7, scale); break;
                    case '8': DrawDigital(worldPos+stepRight*index, 8, scale); break;
                    case '9': DrawDigital(worldPos+stepRight*index, 9, scale); break;
                }
            }
        }

        public static void DrawNumber(Vector3 worldPos, int number, float scale)
        {
            int n = number;
            int d = 1;
    
            while (d <= n / 10) d *= 10;

            var stepRight = Vector3.right * scale * 1.3f;

            int index = 0;
            while (d>0) {
                int digit = (n / d) % 10;

                DrawDigital(worldPos+stepRight*index, digit, scale);
                
                d /= 10;
                index++;
            }
        }
        
        public static void DrawDigital(Vector3 worldPos, int digital, float scale)
        {
            scale /= 2f;
            switch (digital)
            {
                case 0: DrawPoints(DigitalPoints0); break;
                case 1: DrawPoints(DigitalPoints1); break;
                case 2: DrawPoints(DigitalPoints2); break;
                case 3: DrawPoints(DigitalPoints3); break;
                case 4: DrawPoints(DigitalPoints4); break;
                case 5: DrawPoints(DigitalPoints5); break;
                case 6: DrawPoints(DigitalPoints6); break;
                case 7: DrawPoints(DigitalPoints7); break;
                case 8: DrawPoints(DigitalPoints8); break;
                case 9: DrawPoints(DigitalPoints9); break;
            }

            void DrawPoints(List<Vector2> digitalPoints)
            {
                for (var index = 0; index < digitalPoints.Count-1; index++)
                {
                    var point0 = digitalPoints[index] * scale;
                    var point1 = digitalPoints[index+1] * scale;
                    Gizmos.DrawLine(worldPos + (Vector3)point0, worldPos + (Vector3)point1);
                }
            }
        }
        
        public static void DrawSquare(float x, float y, float sizeX, float sizeY, float z = 0)
        {
            Gizmos.DrawLine(new Vector3(x,y,z), new Vector3(x,y+sizeY,z));
            Gizmos.DrawLine(new Vector3(x,y+sizeY,z), new Vector3(x+sizeX,y+sizeY,z));

            Gizmos.DrawLine(new Vector3(x+sizeX,y+sizeY,z), new Vector3(x+sizeX,y,z));
            Gizmos.DrawLine(new Vector3(x+sizeX,y,z), new Vector3(x,y,z));
        }
        
        public static void DrawSquare(Vector2Int pos, int size)
        {
            DrawSquare(pos.x, pos.y, size, size, 0);
        }
        
        public static void DrawSquare(Vector2 start, Vector2 size)
        {
            DrawSquare(start.x, start.y, size.x, size.y, 0);
        }
        
        public static void DrawSquareFromCenter(Vector2 center, Vector2 size)
        {
            DrawSquare(center.x-size.x/2f, center.y-size.y/2f, size.x, size.y, 0);
        }
        
        public static void DrawSquareMinMax(Vector2 min, Vector2 max)
        {
            Gizmos.DrawLine(new Vector3(min.x, min.y), new Vector3(max.x,min.y));
            Gizmos.DrawLine(new Vector3(min.x, min.y), new Vector3(min.x,max.y));

            Gizmos.DrawLine(new Vector3(max.x, max.y), new Vector3(max.x,min.y));
            Gizmos.DrawLine(new Vector3(max.x, max.y), new Vector3(min.x,max.y));
        }

        
        /// <summary>
        ///   <para>Draw a square (XZ) with center and size.</para>
        /// </summary>
        /// <param name="center"></param>
        /// <param name="size"></param>
        public static void DrawSquareXZ(Vector2 center, Vector2 size)
        {
            // p1 p2
            // p0 p3
            var p0 = center.ToXyZ() - size.ToXyZ() / 2f;
            var p2 = center.ToXyZ() + size.ToXyZ() / 2f;
            var p1 = p0 + new Vector3(0, 0, size.y);
            var p3 = p0 + new Vector3(size.x, 0, 0);
                
            Gizmos.DrawLine(p0, p1);
            Gizmos.DrawLine(p0, p3);
            Gizmos.DrawLine(p2, p1);
            Gizmos.DrawLine(p2, p3);
        }

        public static void DrawSquareXZ(int x, int y, int sizeX, int sizeY, int z)
        {
            Gizmos.DrawLine(new Vector3(x,y,z), new Vector3(x,y+sizeY,z));
            Gizmos.DrawLine(new Vector3(x,y+sizeY,z), new Vector3(x+sizeX,y+sizeY,z));

            Gizmos.DrawLine(new Vector3(x+sizeX,y+sizeY,z), new Vector3(x+sizeX,y,z));
            Gizmos.DrawLine(new Vector3(x+sizeX,y,z), new Vector3(x,y,z));
        }

        
        public static void DrawRomboid(Vector3 center, float radius)
        {
            Vector3 top = center + Vector3.up * radius;
            Vector3 bottom = center - Vector3.up * radius;
            Vector3 forward = center + Vector3.forward * radius;
            Vector3 back = center + Vector3.back * radius;
            Vector3 right = center + Vector3.right * radius;
            Vector3 left = center + Vector3.left * radius;
            Gizmos.DrawLine(top, forward);
            Gizmos.DrawLine(forward, bottom);
            Gizmos.DrawLine(top, back);
            Gizmos.DrawLine(back, bottom);
            Gizmos.DrawLine(top, right);
            Gizmos.DrawLine(right, bottom);
            Gizmos.DrawLine(top, left);
            Gizmos.DrawLine(left, bottom);
            Gizmos.DrawLine(forward, right);
            Gizmos.DrawLine(right, back);
            Gizmos.DrawLine(forward, left);
            Gizmos.DrawLine(left, back);
        }
        // size is larger(by half cone) than drawline 
        public static void DrawArrow(Vector3 from, Vector3 to, Color color)
        {
            Handles.color = color;
            float size = Vector3.Distance(from, to);
            Handles.ArrowHandleCap(0, from,  Quaternion.LookRotation(to-from), size,  EventType.Repaint);
        }
        
        public static void DrawArrowXZ(Vector3 from, Vector3 to, float length = 10f)
        {
            Gizmos.DrawLine(from, to);
            //var rot1 = Quaternion.Euler(0, 30, 0);
            var originalVector = (to - from).normalized;
            Vector3 rotatedVector = Quaternion.AngleAxis(160, Vector3.up) * originalVector;
            Vector3 rotatedVector2 = Quaternion.AngleAxis(-160, Vector3.up) * originalVector;
            Gizmos.color = Color.red;
            Gizmos.DrawLine(to, to+rotatedVector*length);
            Gizmos.DrawLine(to, to+rotatedVector2*length);
        }

        public static void DrawDiamond(Vector3 v1, Vector3 v2) {
            float size = Vector3.Distance(v2, v1);
            Gizmos.matrix = Matrix4x4.TRS(v1, Quaternion.FromToRotation(Vector3.right, v2 - v1), new Vector3(size, size, size));
            GismosDiamond();
            Gizmos.matrix = Matrix4x4.identity;
        }
        
        public static void DrawDiamond(Vector3 v1, Quaternion rot, float r) {
            float norm = rot.norm();
            if (norm > 1.1 || norm < 0.9f) return;
            Gizmos.matrix = Matrix4x4.TRS(v1, rot, new Vector3(r, r, r));
            GismosDiamond();
            Gizmos.matrix = Matrix4x4.identity;
        }

        private static void GismosDiamond()
        {
            float x1 = 0.2f;
            float d1 = 0.1f;

            Gizmos.DrawLine(new Vector3(0, 0, 0), new Vector3(1, 0, 0));
            int m = 5;
            float fm = 5f;
            for (int i = 0; i < m; i++)
            {
                Vector3 pos = new Vector3(x1, (float) Math.Cos(i / fm * 2 * Math.PI) * d1,
                    (float) Math.Sin(i / fm * 2 * Math.PI) * d1);
                Vector3 pos2 = new Vector3(x1, (float) Math.Cos((i + 1) / fm * 2 * Math.PI) * d1,
                    (float) Math.Sin((i + 1) / fm * 2 * Math.PI) * d1);
                Gizmos.DrawLine(new Vector3(0, 0, 0), pos);
                Gizmos.DrawLine(pos, new Vector3(1, 0, 0));
                Gizmos.DrawLine(pos, pos2);
            }
        }

        public static void DrawString(string text, Vector3 worldPos, Color? color = null)
        {
            var view = UnityEditor.SceneView.currentDrawingSceneView;
            Camera camera = view == null ? Camera.main : view.camera;
            //Assert.IsNotNull(camera);

            Vector3 screenPos = camera.WorldToScreenPoint(worldPos);
            if (screenPos.y < 0 || screenPos.y > Screen.height || screenPos.x < 0 || screenPos.x > Screen.width || screenPos.z < 0)
                return;

            var isColor = color.HasValue;

            //UnityEditor.Handles.BeginGUI();
            
            var restoreColor = GUI.color;
            if (isColor) GUI.color = color.Value;

            UnityEditor.Handles.Label(worldPos, text);

            GUI.color = restoreColor;
            //UnityEditor.Handles.EndGUI();
            
            Handles.DrawWireCube(worldPos, Vector3.one*2);
        }

        public static void DrawString(string text, Vector3 worldPos, float oX, float oY, Color? color = null, bool isCentered = false)
        {
            UnityEditor.Handles.BeginGUI();
            var restoreColor = GUI.color;

            if (color.HasValue) GUI.color = color.Value;
            var view = UnityEditor.SceneView.currentDrawingSceneView;
            Camera camera = view == null ? Camera.main : view.camera;
            Assert.IsNotNull(camera);
            
            Vector3 screenPos = camera.WorldToScreenPoint(worldPos);
            if (screenPos.y < 0 || screenPos.y > Screen.height || screenPos.x < 0 || screenPos.x > Screen.width || screenPos.z < 0)
            {
                GUI.color = restoreColor;
                UnityEditor.Handles.EndGUI();
                return;
            }

            if (isCentered)
                oX += text.Length * -6;

            UnityEditor.Handles.Label(TransformByPixel(worldPos, oX, oY, camera), text);

            GUI.color = restoreColor;
            UnityEditor.Handles.EndGUI();
        }

        static Vector3 TransformByPixel(Vector3 position, float x, float y, Camera camera)
        {
            return TransformByPixel(position, new Vector3(x, y), camera);
        }

        static Vector3 TransformByPixel(Vector3 position, Vector3 translateBy, Camera camera)
        {
            if (camera)
                return camera.ScreenToWorldPoint(camera.WorldToScreenPoint(position) + translateBy);
            else
                return position;
        }

    }
}

#endif