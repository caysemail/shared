using UnityEngine;

namespace Shared
{
    public static class ExtensionVector2Int
    {
        public static Vector2Int InvertX(this Vector2Int val)
        {
            return new Vector2Int(-val.x, val.y);
        }
        
    }
}