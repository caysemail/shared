namespace Shared
{
    public static class ProcessExt
    {
        public static void ShowExplorer(string itemPath)
        {
            itemPath = itemPath.Replace(@"/", @"\");   // explorer doesn't like front slashes
            System.Diagnostics.Process.Start("explorer.exe", "/select,"+itemPath);
        }
    }
}