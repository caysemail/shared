using System;
using System.Collections.Generic;
using System.Linq;

namespace Shared
{
    public static class EnumerableExt
    {
        public static IEnumerable<T> DistinctBy<T, TKey>(this IEnumerable<T> enumerable, Func<T, TKey> keySelector)
        {
            return enumerable.GroupBy(keySelector).Select(grp => grp.First());
        }
        
        public static int IndexOf<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate) {
     
            var index = 0;
            foreach (var item in source) {
                if (predicate.Invoke(item)) {
                    return index;
                }
                index++;
            }

            return -1;
        }
    }
}
