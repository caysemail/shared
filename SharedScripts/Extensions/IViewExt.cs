using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Shared
{
    public static class IViewExt
    {
        public static void InitializeChildren(Transform transform)
        {
            var views = new List<IView>();
            foreach (Transform t in transform)
            {
                var iview = t.GetComponent<IView>();
                if (iview != null)
                    views.Add(iview);
            }
            //var views = transform.GetComponentsInChildren<IView>().ToList();
            //var modes = Shared.InterfaceExt.Load<IMode>(out var logs);
            string info = "";
            views.ForEach(t => info += t.GetType().Name + ", " );
            Debug.Log(transform.name+ ".IViewExt.InitializeChildren: " + views.Count + " views initialized: "+info);
            views.ForEach(t => t.Initialize());
        }
    }
}

