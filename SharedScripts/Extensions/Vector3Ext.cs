using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Shared
{
    public static class Vector3Ext
    {
        
        [MethodImpl((MethodImplOptions) 256)]
        public static bool IsNotEquals(this Vector3 a, Vector3 b) => (a.x != b.x || a.y != b.y || a.z != b.z);
        [MethodImpl((MethodImplOptions) 256)]
        public static bool IsEquals(this Vector3 a, Vector3 b) => (a.x == b.x && a.y == b.y && a.z == b.z);
        [MethodImpl((MethodImplOptions) 256)]
        public static bool IsZero(this Vector3 a) => (a.x == 0 && a.y == 0 && a.z == 0);

        [MethodImpl((MethodImplOptions) 256)]
        public static Vector3 Mult(this Vector3 a, Vector3 b)
        {
            return new Vector3(a.x*b.x, a.y*b.y, a.z*b.z);
        }
        
        [MethodImpl((MethodImplOptions) 256)]
        public static Vector3 Div(this Vector3 a, Vector3 b)
        {
            return new Vector3(a.x/b.x, a.y/b.y, a.z/b.z);
        }
        
        //[MethodImpl((MethodImplOptions) 256)]
        //public static Vector3 operator /(Vector3 a, Vector3 d) => new Vector3(a.x / d.x, a.y / d.y, a.z / d.z);
        
        public static Vector3 ToRound2(this Vector3 a)
        {
            a.x = Mathf.RoundToInt(a.x * 100f) / 100f;
            a.y = Mathf.RoundToInt(a.y * 100f) / 100f;
            a.z = Mathf.RoundToInt(a.z * 100f) / 100f;
            return a;
        }

        public static IntVector3 RoundToIntVector3(this Vector3 a)
        {
            return new IntVector3(Mathf.RoundToInt(a.x), Mathf.RoundToInt(a.y), Mathf.RoundToInt(a.z));
        }

        public static Vector3 SetX(this Vector3 a, float x) { return new Vector3(x, a.y, a.z); }
        public static Vector3 SetY(this Vector3 a, float y) { return new Vector3(a.x, y, a.z); }
        public static Vector3 SetZ(this Vector3 a, float z) { return new Vector3(a.x, a.y, z); }
        public static Vector2 GetXY(this Vector3 a) { return new Vector2(a.x, a.y); }
        public static Vector2 GetZY(this Vector3 a) { return new Vector2(a.z, a.y); }
        public static Vector2 GetXZ(this Vector3 a) { return new Vector2(a.x, a.z); }

        public static Vector3 InvertSignX(this Vector3 a) { a.x = -a.x; return a; }

        public static Vector3 InvertSignY(this Vector3 a) { a.y = -a.y; return a; }

        public static Vector3 InvertSignZ(this Vector3 a) { a.z = -a.z; return a; }

        public static Vector3Int RoundToVector3Int(this Vector3 a)
        {
            return new Vector3Int(Mathf.RoundToInt(a.x), Mathf.RoundToInt(a.y), Mathf.RoundToInt(a.z));
        }

        public static float DistanceFast(this Vector3 a, Vector3 b)
        {
            a.x = a.x - b.x; a.y = a.y - b.y; a.z = a.z - b.z;
            a.x = a.x >= 0 ? a.x : -a.x;
            a.y = a.y >= 0 ? a.y : -a.y;
            a.z = a.z >= 0 ? a.z : -a.z;
            return a.x + a.y + a.z;
        }
        
        public static float DistanceFastXZ(this Vector3 a, Vector3 b)
        {
            a.x = a.x - b.x; a.z = a.z - b.z;
            a.x = a.x >= 0 ? a.x : -a.x;
            a.z = a.z >= 0 ? a.z : -a.z;
            return a.x + a.z;
        }

        [MethodImpl((MethodImplOptions) 256)]
        public static float DistanceXZ(this Vector3 a, Vector3 b)
        {
            float num1 = a.x - b.x;
            float num2 = a.z - b.z;
            return (float) Math.Sqrt((double) num1 * (double) num1 + (double) num2 * (double) num2);
        }

        public static Vector3 Scale(Vector3 a, Vector3 b)
        {
            return new Vector3(a.x * b.x, a.y*b.y, a.z*b.z);
        }


        public static Vector2 ToXZ(this Vector3 position)
        {
            return new Vector2(position.x, position.z);
        }
        
        /// <summary>
        /// Converts Vector3 to Vector2 using specified projection plane.
        /// </summary>
        public static Vector3Int ToVector3Int(this Vector3 v)
        {
            return new Vector3Int((int)v.x, (int)v.y, (int)v.z);
        }

        public static Vector2Int ToVector2Int(this Vector3 position)
        {
            return new Vector2Int((int)position.x, (int)position.y);
        }
        
        public static Vector2Int ToVector2IntXZ(this Vector3 position)
        {
            return new Vector2Int((int)position.x, (int)position.z);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float DotXZ(Vector3 lhs, Vector3 rhs) => (float) ((double) lhs.x * (double) rhs.x + (double) lhs.z * (double) rhs.z);
    }
}