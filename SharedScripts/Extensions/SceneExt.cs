using UnityEngine.SceneManagement;

namespace Shared
{
    public static class SceneExt
    {
        public static bool TryGetComponentInAllScenes<T> (out T component)
        {
            component = default;
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                var scene = SceneManager.GetSceneAt(i);
                var roots = scene.GetRootGameObjects();
                foreach (var root in roots)
                {
                    component = root.GetComponentInChildren<T>();
                    if (component != null)
                        return true;
                }
            }

            return false;
        }
        
        public static bool TryGetComponentInAllScenes<T> (string goName, out T component)
        {
            component = default;
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                var scene = SceneManager.GetSceneAt(i);
                var roots = scene.GetRootGameObjects();
                foreach (var root in roots)
                {
                    if (root.name != goName) continue;
                    component = root.GetComponentInChildren<T>();
                    if (component != null)
                        return true;
                }
            }

            return false;
        }

        public static bool TryGetActiveComponentInAllScenes<T> (out T component)
        {
            component = default;
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                var scene = SceneManager.GetSceneAt(i);
                var roots = scene.GetRootGameObjects();
                foreach (var root in roots)
                {
                    if (!root.activeSelf) continue;
                    component = root.GetComponentInChildren<T>();
                    if (component != null)
                        return true;
                }
            }

            return false;
        }

    }
}