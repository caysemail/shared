using System;
using System.Linq;

namespace Shared
{
    public static class ArrayExt
    {
        public static string[] Append(ref string[] array, string value)
        {
            // easy variant
            array = array.Append(value).ToArray();
            // optimized variant
            // todo
            
            return array;
        }

        public static void Copy2dArray<T>(T[] source, int sourceWidth, T[] destination, int destinationWidth, int destPosX, int destPosY) where T : struct
        {
            int sourceHeight = source.Length / sourceWidth;
            for (int y = 0; y < sourceHeight; y++)
            {
                for (int x = 0; x < sourceWidth; x++)
                {
                    var sourceElement = source[x + y * sourceWidth];
                    var destIndex = (destPosX + x) + (destPosY + y) * destinationWidth;
                    destination[destIndex] = sourceElement;
                }
            }
        }
        
        // public static void RemoveAt<T>(ref Array array, int index)
        // {
        //     // list variant
        //     if (index >= array.Length || index < 0)
        //         throw new ArgumentOutOfRangeException("index=" + index + ", array=" + array.Length);
        //         
        //    // --this._size;
        //     //if (index < this._size)
        //         Array.Copy((Array) array, index + 1, array, index, array.Length - index);
        //         
        //     //this._items[this._size] = default (T);
        //     //++this._version;
        //
        //     // easy variant
        //     var list = array.ToList();
        //     list.RemoveAt(index);
        //     array = list.ToArray();
        //     // optimized variant
        //     // todo
        //
        // }
        
    }
}