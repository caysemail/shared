using System.Diagnostics;
using UnityEngine;

namespace Shared
{
    public static class Assert2
    {
        [Conditional("UNITY_ASSERTIONS")]
        public static void IsNotNull<T>(T value) where T : class
        {
            if (value != null)
            {
                return;
            }

            throw new UnityException("AssertExt.IsNotNull: value=" + value);
        }
        
        [Conditional("UNITY_ASSERTIONS")]
        public static void IsNotNull<T>(T value, string info) where T : class
        {
            if (value != null)
            {
                return;
            }

            throw new UnityException("AssertExt.IsNotNull: value=" + value+" "+info);
        }

        [Conditional("UNITY_ASSERTIONS")]
        public static void IsNull<T>(T value) where T : class
        {
            if (value == null)
            {
                return;
            }
            
            if ((object) value as UnityEngine.Object == (UnityEngine.Object) null)
            {
                return;
            }

            throw new UnityException("AssertExt.IsNull: value=" + value);
        }
        
        [Conditional("UNITY_ASSERTIONS")]
        public static void IsTrue(bool value)
        {
            if (value)
            {
                return;
            }
            throw new UnityException("AssertExt.IsTrue: value=" + value);
        }
        
        [Conditional("UNITY_ASSERTIONS")]
        public static void IsFalse(bool value)
        {
            if (!value)
            {
                return;
            }
            throw new UnityException("AssertExt.IsFalse: value=" + value);
        }
    }
}