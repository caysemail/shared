using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Assertions;

namespace Shared
{
    public static class InterfaceExt
    {
        public static List<T> Load<T>(out string log)
        {
            log = "";
            var systemTypes = ClassUtility.GetAllTypes(typeof(T));
            var systems = new List<T>();
            foreach (var type in systemTypes)
            {
                log += type.Name.ToString() + " ";
                var obj = (T) Activator.CreateInstance(type);
                systems.Add(obj);
            }

            return systems;
        }
        
        public static T LoadSingle<T>()
        {
            //log = "";
            var systemTypes = ClassUtility.GetAllTypes(typeof(T));
            Assert.IsTrue(systemTypes.Count == 1, "found "+systemTypes.Count+" classes of type "+typeof(T));
            var type = systemTypes.First();
            //log += type.Name.ToString() + " ";
            var obj = (T)Activator.CreateInstance(type);

            return obj;
        }

    }
}