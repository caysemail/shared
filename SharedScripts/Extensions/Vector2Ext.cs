using System.Runtime.CompilerServices;
using UnityEngine;

namespace Shared
{
    public static class Vector2Ext
    {
        public static Vector2Int RoundToInt(this Vector2 v)
        {
            return new Vector2Int(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y));
        }
        
        [MethodImpl((MethodImplOptions) 256)]
        public static bool IsZero(this Vector2 a) => (a.x == 0 && a.y == 0);
        [MethodImpl((MethodImplOptions) 256)]
        public static bool IsNotZero(this Vector2 a) => (a.x != 0 || a.y != 0);

        
        [MethodImpl((MethodImplOptions) 256)]
        public static bool IsNotEquals(this Vector2 a, Vector2 b) => (a.x != b.x || a.y != b.y);
        [MethodImpl((MethodImplOptions) 256)]
        public static bool IsEquals(this Vector2 a, Vector2 b) => (a.x == b.x && a.y == b.y);
        
        [MethodImpl((MethodImplOptions) 256)]
        public static bool IsLowerOrEquals(this Vector2 a, float b) => (a.x <= b && a.y <= b);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static IntVector2 ToIntVector2(this Vector2 source)
        {
            return new IntVector2((int)source.x, (int)source.y);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 ToVector3AsInt(this Vector2 source)
        {
            return new Vector3((int)source.x, (int)source.y, 0);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2Int ToVector2Int(this Vector2 position)
        {
            return new Vector2Int((int)position.x, (int)position.y);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2Int ToVector2Int(this Vector2 position, float addX, float addY)
        {
            return new Vector2Int((int)(position.x+addX), (int)(position.y+addY));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 ToVector2XZ(this Vector3 position)
        {
            return new Vector2(position.x, position.z);
        }

        public static string ToString3(this Vector2 a)
        {
            a = a * 1000f;
            a.x = (int) a.x;
            a.y = (int) a.y;
            a /= 1000;
            return "("+a.x+", "+a.y+")";
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 ToVector3XZ(this Vector2 a) { return new Vector3(a.x, 0, a.y); }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 ToVector3XY(this Vector2 a) { return new Vector3(a.x, a.y, 0); }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 ToXyZ(this Vector2 a) { return new Vector3(a.x, 0, a.y); }
        public static Vector3 ToVector3ZY(this Vector2 a) { return new Vector3(0, a.y, a.x); }

        public static Vector2 InvertSignX(this Vector2 a) { a.x = -a.x; return a; }

        [MethodImpl((MethodImplOptions) 256)]
        public static float DistanceFast(this Vector2 a, Vector2 b)
        {
            a.x -= b.x; a.y -= b.y;
            a.x = a.x >= 0 ? a.x : -a.x;
            a.y = a.y >= 0 ? a.y : -a.y;
            return a.x + a.y;
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float DistanceFast2(Vector2 a, Vector2 b)
        {
            a.x -= b.x; a.y -= b.y;
            a.x = a.x >= 0 ? a.x : -a.x;
            a.y = a.y >= 0 ? a.y : -a.y;
            return a.x + a.y;
        }


        
    }
}