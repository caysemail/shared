﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Assertions;
using Object = System.Object;

namespace Shared
{
    [Serializable]
    public class Defs
    {
        private Dictionary<Type, Object> _defs = new Dictionary<Type, Object>();
        private Dictionary<Type, List<Object>> _defsList = new Dictionary<Type, List<Object>>();

        public void Clear()
        {
            _defs = new Dictionary<Type, object>();
            _defsList = new Dictionary<Type, List<object>>();
        }

        public void Test()
        {
            //var jsonTest = JsonUtility.ToJson(new MechDef2(){ Pelvis = new Pelvis()});
            // Debug.LogWarning("DefTest: jsonTest=" + jsonTest + ", from json type="+ JsonUtility.FromJson<MechDef2>(jsonTest).Type);
        }

        public T Get<T>()
        {
            var payload = _defs[typeof(T)];
            return (T)payload;
        }

        public IEnumerable<T> GetList<T>()
        {
            if (_defsList.TryGetValue(typeof(T), out var payload))
                return payload.Cast<T>();
            //var payload = _defsList[typeof(T)];
            //return payload.Cast<T>();
            throw new UnityException("not found " + typeof(T) + " from deflist=" + _defsList.Count);
            //return default;
        }

        public void Load<T>()
        {
            var resource = typeof(T).Name;
            var payload = LoadSingle<T>(@"Defs\" + resource);
            _defs.Add(typeof(T), payload);

            if (payload is DefsBase @base)
                @base.PostProcess();
        }

        public void Load<T>(string subFolder)
        {
            //SDebug.Log("Defs.Load: " + typeof(T)+" sub="+subFolder);
            var texts = Resources.LoadAll(@"Defs\" + subFolder, typeof(TextAsset));
            //Debug.Log("loaded: "+texts.Length);
            List<Object> addList = new List<Object>();
            foreach (var text in texts)
            {
                // MechDefPart[] test = new MechDefPart[2];
                // test[0] = new MechDefPart();
                // test[1] = new MechDefPart();
                // var testString = JsonConvert.SerializeObject(test);
                // Debug.LogError("testString="+testString);
                //List<T> instance = null;
                T instance = default;
                var textAsset = (TextAsset)text;
                try
                {
                    instance = JsonConvert.DeserializeObject<T>(textAsset.text);
                    //instance = JsonConvert.DeserializeObject<List<T>>(textAsset.text);
                }
                catch (Exception e)
                {
                    Debug.LogError("Deserialize: " + textAsset.name + " " + typeof(T) + " " + subFolder + "\n" + e.ToString());
                    //throw new UnityException("Deserialize: "+textAsset.name+" "+typeof(T)+" "+subFolder+"\n"+e.ToString());
                    return;
                }

                Assert.IsNotNull((object)instance);
                // var collection = instance.Cast<Object>();
                // foreach (var element in collection)
                // {
                //     if (element is DefBase)
                //         ((DefBase) element).FileName = textAsset.name;
                // }
                // addList.AddRange(instance.Cast<Object>());

                // if (instance is DefBase @base)
                //     @base.FileName = textAsset.name;

                if (instance is DefBase @base)
                    @base.PostProcess();

                addList.Add(instance);
            }

            if (!_defsList.TryGetValue(typeof(T), out var list))
            {
                _defsList.Add(typeof(T), addList);
                list = _defsList[typeof(T)];
            }
            else list.AddRange(addList);

            Assert.IsTrue(list != null && list.Any(), "List<" + typeof(T) + ">.Any=" + list.Any());
        }

        public void LoadList<T>(string subFolder)
        {
            Assert.IsTrue(!string.IsNullOrWhiteSpace(subFolder));
            //SDebug.Log("Defs.Load: " + typeof(T)+" sub="+subFolder);
            var texts = Resources.LoadAll(@"Defs\" + subFolder, typeof(TextAsset));
            Assert.IsTrue(texts.Length > 0, @"Not found any TextAsset files in Defs\" + subFolder);
            //Debug.Log("loaded: "+texts.Length);
            List<Object> addList = new List<Object>();
            foreach (var text in texts)
            {
                // MechDefPart[] test = new MechDefPart[2];
                // test[0] = new MechDefPart();
                // test[1] = new MechDefPart();
                // var testString = JsonConvert.SerializeObject(test);
                // Debug.LogError("testString="+testString);
                List<T> instance = null;
                //T instance = default;
                var textAsset = (TextAsset)text;
                try
                {
                    //instance = JsonConvert.DeserializeObject<T>(textAsset.text);
                    instance = JsonConvert.DeserializeObject<List<T>>(textAsset.text);
                }
                catch (Exception e)
                {
                    //Debug.LogError("Deserialize: "+textAsset.name+" "+typeof(T)+" "+subFolder+"\n"+e.ToString());
                    throw new UnityException("Deserialize: " + textAsset.name + " " + typeof(T) + " " + subFolder + "\n" + e.ToString());
                    //return;
                }

                Assert.IsNotNull((object)instance);
                var collection = instance.Cast<Object>();
                // foreach (var element in collection)
                // {
                //     if (element is DefBase)
                //         ((DefBase) element).FileName = textAsset.name;
                // }
                addList.AddRange(instance.Cast<Object>());
            }

            if (!_defsList.TryGetValue(typeof(T), out var list))
            {
                _defsList.Add(typeof(T), addList);
                list = _defsList[typeof(T)];
            }
            else list.AddRange(addList);

            Assert.IsTrue(list != null && list.Any(), "List<" + typeof(T) + ">.Any=" + list.Any());
        }

        private T LoadSingle<T>(string resource)
        {
            //Debug.Log("Load type "+typeof(T));
            string text = "";
            try
            {
                text = Resources.Load<TextAsset>(resource).text;
            }
            catch (Exception e)
            {
                Debug.LogError("Cannot load " + resource + " of type " + typeof(T) + " from resources\n" + e);
                return default(T);
            }

            try
            {
                return JsonConvert.DeserializeObject<T>(text);
                //return JsonUtility.FromJson<T>(text);
            }
            catch (Exception e)
            {
                Debug.LogError("Cannot parse to object " + resource + " of type " + typeof(T) + "\n" + e);
                return default(T);
            }
        }

        public abstract class DefBase
        {
            //public string FileName;
            [NonSerialized] public int Id;

            public override string ToString()
            {
                return JsonUtility.ToJson(this);
            }

            public virtual void PostProcess()
            {
            }
        }

        public abstract class DefsBase
        {
            public virtual void PostProcess()
            {
            }
        }
    }
}