using System;
using UnityEngine;

namespace Shared
{
    //[AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    //[AttributeUsage(AttributeTargets.Field|AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class ShowInEditor : Attribute
    {
        public ButtonSpacing Spacing { get; private set; } = ButtonSpacing.None;
        
        public ShowInEditor()
        {
        }
    }
}