﻿using System;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Shared
{
    public static class PrivateEditorExtensions
    {
        public static void DrawPrivate(this Editor editor)
        {
            // Loop through all methods with no parameters
            FieldInfo[] infos = editor.target.GetType()
                    .GetFields(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
                //.GetMethods(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
                //.Where(m => m.GetParameters().Length == 0);
                ;

            //Debug.LogError("DrawPrivate: methods="+infos.Count());

            foreach (FieldInfo info in infos)
            {
                string debugName = ObjectNames.NicifyVariableName(info.Name) + " = " + info.GetValue(editor.target)+" ; ";
                //Debug.Log(ObjectNames.NicifyVariableName(info.Name)+" value: " + info.GetValue(editor.target));
                // Get the ButtonAttribute on the method (if any)
                var attribute = (ShowInEditor)Attribute.GetCustomAttribute(info, typeof(ShowInEditor));

                if (attribute != null)
                {
                    object realValue = info.GetValue(editor.target);

                    var realName = ObjectNames.NicifyVariableName(info.Name);
                    
                    DrawProperty(info.FieldType, realValue, realName);

                    // Determine whether the button should be enabled based on its mode
                    var wasEnabled = GUI.enabled;
                    //GUI.enabled = attribute.Mode == ButtonMode.AlwaysEnabled
                    //|| (EditorApplication.isPlaying ? attribute.Mode == ButtonMode.EnabledInPlayMode : attribute.Mode == ButtonMode.DisabledInPlayMode);
                    GUI.enabled = true;

                    if (((int)attribute.Spacing & (int)ButtonSpacing.Before) != 0) GUILayout.Space(10);

                    //EditorGUILayout.PropertyField(iterator, true);

                    // Draw a button which invokes the method
                    var buttonName = ObjectNames.NicifyVariableName(info.Name);

                    //Editor.CreateEditor(editor.target, typeof(Editor).Assembly.GetType("UnityEditor.RectTransformEditor")).OnInspectorGUI();
                    //var customEditor = Editor.CreateEditor(editor.target);
                    //if (customEditor != null) customEditor.OnInspectorGUI();

                    //var serializeObject = new SerializedObject(realValue);
                    if (realValue is UnityEngine.Object unityValue)
                    {
                        //SerializedObject serializedObject = new UnityEditor.SerializedObject(unityValue);
                        debugName += " u.Object";

                        SerializedProperty serializedProperty = null;
                        if (serializedProperty != null)
                            EditorGUILayout.PropertyField(serializedProperty, true);
                    }

                    buttonName = debugName;


                    if (GUILayout.Button(buttonName))
                    {
                        foreach (var t in editor.targets)
                        {
                            //info.Invoke(t, null);
                        }
                    }

                    if (((int)attribute.Spacing & (int)ButtonSpacing.After) != 0) GUILayout.Space(10);

                    GUI.enabled = wasEnabled;
                }
            }

            void DrawProperty(Type type, object value, string label)
            {
                //Debug.LogError("type="+type+", value="+value+", u.Object="+(value is UnityEngine.Object));
                if (type == typeof(Single))
                {
                    EditorGUILayout.FloatField(label, (float)value);
                    //EditorGUI.FloatField(position, label, (float)value);
                }
                else if (value is UnityEngine.Object)
                {
                    var unityValue = (UnityEngine.Object)value;
                    EditorGUILayout.ObjectField(label, unityValue, unityValue.GetType());
                }

                // switch (type)
                // {
                //     case typeof(System.Single):
                //         break;
                //     
                // }
                // switch (propertyType)
                // {
                //     case SerializedPropertyType.Integer:
                //         return EditorGUI.IntField(position, label, (int)value);
                //     case SerializedPropertyType.Boolean:
                //         return EditorGUI.Toggle(position, label, (bool)value);
                //     case SerializedPropertyType.Float:
                //         return EditorGUI.FloatField(position, label, (float)value);
                //     case SerializedPropertyType.String:
                //         return EditorGUI.TextField(position, label, (string)value);
                //     case SerializedPropertyType.Color:
                //         return EditorGUI.ColorField(position, label, (Color)value);
                //     case SerializedPropertyType.ObjectReference:
                //         return EditorGUI.ObjectField(position, label, (UnityEngine.Object)value, type, true);
                //     case SerializedPropertyType.ExposedReference:
                //         return EditorGUI.ObjectField(position, label, (UnityEngine.Object)value, type, true);
                //     case SerializedPropertyType.LayerMask:
                //         return EditorGUI.LayerField(position, label, (int)value);
                //     case SerializedPropertyType.Enum:
                //         return EditorGUI.EnumPopup(position, label, (Enum)value);
                //     case SerializedPropertyType.Vector2:
                //         return EditorGUI.Vector2Field(position, label, (Vector2)value);
                //     case SerializedPropertyType.Vector3:
                //         return EditorGUI.Vector3Field(position, label, (Vector3)value);
                //     case SerializedPropertyType.Vector4:
                //         return EditorGUI.Vector4Field(position, label, (Vector4)value);
                //     case SerializedPropertyType.Rect:
                //         return EditorGUI.RectField(position, label, (Rect)value);
                //     case SerializedPropertyType.AnimationCurve:
                //         return EditorGUI.CurveField(position, label, (AnimationCurve)value);
                //     case SerializedPropertyType.Bounds:
                //         return EditorGUI.BoundsField(position, label, (Bounds)value);
                //     default:
                //         throw new NotImplementedException("Unimplemented propertyType " + propertyType + ".");
                // }
            }
        }
    }
}