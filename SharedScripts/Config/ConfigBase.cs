﻿#if Newtonsoft

using System.IO;
using Newtonsoft.Json;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Shared
{
    public class ConfigBase : ScriptableObject
    {
        public static T Load<T>() where T : UnityEngine.Object
        {
            var type = typeof(T);
            var path = "Configs/" + type;
            var resource = Resources.Load<T>(path);
            if (resource == null)
                Debug.LogError("Cannot load resource type=" + type + " on path=" + path);
            return resource;
        }
    }

    public class ConfigJsonBase : ConfigBase
    {
        
#if UNITY_EDITOR
        [Button]
        protected void ToJson()
        {
            string output = "";
            output = JsonConvert.SerializeObject(this, Formatting.Indented);
            var isAsset = AssetDatabase.Contains(this);
            var assetPath = AssetDatabase.GetAssetPath(this);
            var jsonPath = assetPath.Replace(".asset", ".json");
            var isDeleted = AssetDatabase.DeleteAsset(jsonPath);
            //var jsonTextAsset = new TextAsset(output);
            //AssetDatabase.CreateAsset(jsonTextAsset.text, jsonPath);
            File.WriteAllText(Application.dataPath + jsonPath.Replace("Assets",""), output);
            AssetDatabase.Refresh();
            Debug.Log("ConfigJsonBase.ToJson: "+"isAsset="+isAsset+", assetPath="+assetPath+", isDeleted="+isDeleted
                           +", dataPath="+Application.dataPath
                           +"\n"+output);
        }

        [Button]
        protected void FromJson()
        {
            var type = this.GetType();
            var assetPath = AssetDatabase.GetAssetPath(this);
            var jsonPath = assetPath.Replace(".asset", ".json");
            var asset = AssetDatabase.LoadAssetAtPath(jsonPath, typeof(TextAsset));
            bool isAsset = asset != null;
            if (isAsset)
            {
                var config = (ConfigJsonBase)JsonConvert.DeserializeObject(((TextAsset) asset).text, type);
                AssetDatabase.CreateAsset(config, assetPath);
                //AssetDatabase.Refresh();
                //var refreshedConfig = AssetDatabase.LoadAssetAtPath<ConfigJsonBase>(assetPath);
                EditorGUIUtility.PingObject( config );
                Selection.activeObject = config;
            }
            Debug.Log("ConfigJsonBase.FromJson: "+"type="+type+", assetPath="+assetPath
                      +", dataPath="+Application.dataPath+", jsonPath="+jsonPath
                      +", isAsset="+isAsset
                      );
        }
#endif        
    }
}

#endif