﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Shared
{
    public class TouchGraphic : Graphic, IPointerDownHandler, IDragHandler, IPointerUpHandler
    {
        public class DragEvent : UnityEvent<PointerEventData> { public DragEvent() { } }

        public DragEvent OnDragEvent = new DragEvent();
        public DragEvent OnEndDragEvent = new DragEvent();
        public DragEvent OnClick = new DragEvent();
        public DragEvent OnPress = new DragEvent();
        public DragEvent OnUnpress = new DragEvent();

        //        //[SerializeField]
        //        //[ReadOnly]
        //#pragma warning disable IDE0044 // Add readonly modifier
        //#pragma warning disable 414
        //        // ReSharper disable once InconsistentNaming
        //        // ReSharper disable once IdentifierTypo
        //        private bool m_RaycastTarget = true;
        //#pragma warning restore 414
        //#pragma warning restore IDE0044 // Add readonly modifier

        public void OnDrag(PointerEventData eventData)
        {
            //Debug.LogWarning("TouchGraphic.OnDrag: pressPosition=" + eventData.pressPosition+
           //     ", scrollDelta=" + eventData.scrollDelta+", delta="+eventData.delta+
            //    ", position="+eventData.position+", calc delta="+(eventData.position- eventData.pressPosition));
            OnDragEvent.Invoke(eventData);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            OnPress.Invoke(eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            OnEndDragEvent.Invoke(eventData);
            OnClick.Invoke(eventData);
            OnUnpress.Invoke(eventData);
        }

        public override void SetMaterialDirty() { return; }
        public override void SetVerticesDirty() { return; }

        /// Probably not necessary since the chain of calls `Rebuild()`->`UpdateGeometry()`->`DoMeshGeneration()`->`OnPopulateMesh()` won't happen; so here really just as a fail-safe.
        protected override void OnPopulateMesh(VertexHelper vh)
        {
            vh.Clear();
            return;
        }
    }
}