﻿#if UNITY_EDITOR

using System.IO;
using System.Linq;
using System.Globalization;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Linq;

using UnityEngine;
using UnityEditor;
using System;


// Adds a nice editor to edit JSON files as well as a simple text editor incase
// the editor doesn't support the types you need. It works with strings, floats
// ints and bools at the moment.
// 
// * Requires the latest version of JSON.net compatible with Unity


//If you want to edit a JSON file in the "StreammingAssets" Folder change this to DefaultAsset.
//Hacky solution to a weird problem :/
[CustomEditor(typeof(TextAsset), true)]
public class JSONEditor : Editor
{
    private string Path => AssetDatabase.GetAssetPath(target);
    private bool isCompatible => Path.EndsWith(".json");
    private bool unableToParse => false;// !NewtonsoftExtensions.IsValidJson(rawText);

    private bool isTextMode, wasTextMode;

    private string rawText;
    //private JObject jsonObject;
    //private JProperty propertyToRename;
    private string propertyRename;


    private void OnEnable()
    {
        if (isCompatible)
            LoadFromJson();
    }

    private void OnDisable()
    {
        //if (isCompatible)
        //    WriteToJson();
    }

    public override void OnInspectorGUI()
    {
        if (isCompatible)
        {
            JsonInspectorGUI();
            return;
        }
        base.OnInspectorGUI();
    }

    Vector2 scrollPosition;

    private void JsonInspectorGUI()
    {
//        bool enabled = GUI.enabled;
//        GUI.enabled = true;
//
//        Rect subHeaderRect = EditorGUILayout.GetControlRect(false, EditorGUIUtility.singleLineHeight * 2.5f);
//        Rect helpBoxRect = new Rect(subHeaderRect.x, subHeaderRect.y, subHeaderRect.width - subHeaderRect.width / 6 - 5f, subHeaderRect.height);
//        Rect rawTextModeButtonRect = new Rect(subHeaderRect.x + subHeaderRect.width / 6 * 5, subHeaderRect.y, subHeaderRect.width / 6, subHeaderRect.height);
// //       EditorGUI.HelpBox(helpBoxRect, "You edit raw text if the JSON editor isn't enough by clicking the button to the right", MessageType.Info);
//
//
//        GUIStyle wrappedButton = new GUIStyle("Button");
//        wrappedButton.wordWrap = true;
        //EditorGUI.BeginChangeCheck();
        //GUI.enabled = !unableToParse;
//        isTextMode = GUI.Toggle(rawTextModeButtonRect, isTextMode, "Edit Text", wrappedButton);
//        if (EditorGUI.EndChangeCheck())
//        {
//            WriteToJson();
//            GUI.FocusControl("");
//            LoadFromJson();
//        }
        GUI.enabled = true;
        
        //{
            //float textFieldHeight = GUI.skin.label.CalcSize(new GUIContent(rawText)).y;
            //Rect textFieldRect = new Rect(subHeaderRect.x, subHeaderRect.y + subHeaderRect.height + EditorGUIUtility.singleLineHeight, subHeaderRect.width, textFieldHeight);

            //rawText = EditorGUI.TextArea(textFieldRect, rawText);
            //EditorGUI.LabelField(textFieldRect, rawText);

            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, 
                GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

            EditorGUILayout.LabelField(rawText, GUI.skin.textArea);
        //EditorGUILayout.TextField(rawText, GUI.skin.textArea);
            GUILayout.EndScrollView();

            //Rect errorBoxRect = new Rect(textFieldRect.x, textFieldRect.y + textFieldRect.height + EditorGUIUtility.singleLineHeight, textFieldRect.width, EditorGUIUtility.singleLineHeight * 2.5f);
            //GUIStyle helpBoxRichText = new GUIStyle(EditorStyles.helpBox);
            //Texture errorIcon = EditorGUIUtility.Load("icons/console.erroricon.png") as Texture2D;

            //helpBoxRichText.richText = true;

            //if (unableToParse)
            //    GUI.Label(errorBoxRect, new GUIContent("Unable to parse text into JSON. Make sure there are no mistakes! Are you missing a <b>{</b>, <b>{</b> or <b>,</b>?", errorIcon), helpBoxRichText);
        //}

        //wasTextMode = isTextMode;
        //GUI.enabled = enabled;
    }

   
    private void LoadFromJson()
    {
        if (!string.IsNullOrWhiteSpace(Path) && File.Exists(Path))
        {
            rawText = File.ReadAllText(Path);
            rawText = rawText.Replace("\t", "   ");
            //jsonObject = JsonConvert.DeserializeObject<JObject>(rawText);
        }
    }

//    private void WriteToJson()
//    {
//        if (jsonObject != null)
//        {
//            if (!wasTextMode)
//                rawText = jsonObject.ToString();
//
//            File.WriteAllText(Path, rawText);
//        }
//    }

//   [MenuItem("Assets/Create/JSON File", priority = 81)]
//    public static void CreateNewJsonFile()
//    {
//        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
//        if (path == "")
//            path = "Assets";
//        else if (System.IO.Path.GetExtension(path) != "")
//            path = path.Replace(System.IO.Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
//
//        path = System.IO.Path.Combine(path, "New JSON File.json");
//
//        JObject jObject = new JObject();
//        File.WriteAllText(path, jObject.ToString());
//        AssetDatabase.Refresh();
//    }
}

#endif