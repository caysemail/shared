﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace Shared
{
    [CustomEditor(typeof(EditorRaycastBeh))]
    public class EditorEditorRaycastBeh : Editor
    {
        void OnSceneGUI()
        {
            //Debug.LogError("OnSceneGUI: keyCode="+Event.current.keyCode);
            // usually detect right and middle click if script is selected
            if (Event.current.type == EventType.MouseUp)
            {
                Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
                //Debug.LogWarning("EditorEditorRaycastBeh: MouseUp, mousePos="+Event.current.mousePosition+", ray="+ray);
                ((EditorRaycastBeh)target).OnEditorRaycast(ray);
            }
            // usually detect right and middle click if script is selected
            if (Event.current.type == EventType.MouseMove)
            {
                Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
                //Debug.LogWarning("EditorEditorRaycastBeh: MouseUp, mousePos="+Event.current.mousePosition+", ray="+ray);
                ((EditorRaycastBeh)target).OnEditorMouseMove(ray);
            }
            
            ((EditorRaycastBeh)target).OnSceneGui();
        }
    }
}
#endif