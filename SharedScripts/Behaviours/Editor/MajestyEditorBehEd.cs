﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace Shared
{
    [CustomEditor(typeof(ClickEditorBeh))]
    public class ClickEditorBehEd : Editor
    {
        void OnSceneGUI() // work only if MajestyEditorBeh selected in editor
        {
            //Debug.LogWarning("OnSceneGUI type="+Event.current.type);
            if (Event.current.type == EventType.MouseUp)
            {
                //Debug.LogWarning("OnSceneGUI MouseUp");
                Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
                ((ClickEditorBeh) target).OnEditorRaycast(ray);
            }
        }
    }
}

#endif