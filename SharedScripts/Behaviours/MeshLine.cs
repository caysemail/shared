using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public class MeshLine : MonoBehaviour, IPoolable
    {
        public int PoolCopyCount = 1;
        public float width = 1;

        private const int density = 1;

        public int GetPoolCopyCount() { return PoolCopyCount; }

        public void SetColor(Color color)
        {
            var lr = GetComponent<LineRenderer>();
            lr.startColor = color;
            lr.endColor = color;
        }
        
        public void Set(List<Vector3> points)
        {
            var lr = GetComponent<LineRenderer>();
            lr.positionCount = points.Count;
            lr.SetPositions(points.ToArray());
        }

        public void SetAutoHeight(List<Vector3> points)
        {
#pragma warning disable CS0162            
            if (density == 2)
            {
                List<Vector3> newPoints = new List<Vector3>();
                for (var index = 0; index < points.Count; index++)
                {
                    var point = points[index];
                    newPoints.Add(point);
                    if (index + 1 == points.Count)
                        break;
                    newPoints.Add((point + points[index+1])/2);
                }

                points = newPoints;
            }
#pragma warning restore CS0162            
            
            var lr = GetComponent<LineRenderer>();
            lr.positionCount = points.Count;
            lr.SetPositions(points.ToArray());
            SetHeight();
        }

        public void SetAutoSquareSegments(Vector3 p0, Vector3 p1)
        {
            var lr = GetComponent<LineRenderer>();
            //var p0 = lr.GetPosition(0);
            //var p1 = lr.GetPosition(lr.positionCount-1);
            var length = Vector3.Distance(p0, p1);
            var segments = (int)(length / lr.startWidth);
            List<Vector3> newPoints = new List<Vector3>();
            float lerp = 0;
            for (int i = 0; i <= segments; i++)
            {
                lerp = (float)i / segments;
                var pT = Vector3.Lerp(p0, p1, lerp);
                newPoints.Add(pT);
            }
            lr.positionCount = newPoints.Count;
            lr.SetPositions(newPoints.ToArray());
        }

        [Button]
        private void SetWidth()
        {
            var lr = GetComponent<LineRenderer>();
            lr.startWidth = width;
            lr.endWidth = width;
        }

        [Button]
        private void Test()
        {
            var points = new List<Vector3>()
            {
                Vector3.zero, 10 * Vector3.one / 2, 10 * (Vector3.one + Vector3.forward*2) 
            };
            SetAutoHeight(points);
        }

        [Button]
        private void SetHeight()
        {
            var lr = GetComponent<LineRenderer>();
            for (int i = 0; i < lr.positionCount; i++)
            {
                var pos = lr.GetPosition(i);
                RaycastHit hit;
                bool isHit = PhysUtility.TryRaycastGround(pos, out hit);
                if (!isHit)
                {
                    Debug.LogError("cannot find ground at "+pos);
                    continue;
                }
                lr.SetPosition(i, hit.point);
            }
        }
    }
}