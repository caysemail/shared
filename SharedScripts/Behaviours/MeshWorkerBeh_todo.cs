using System;
using System.Collections;
using System.Collections.Generic;
using MyBox;
using UnityEngine;
using UnityEngine.Rendering;

namespace Shared
{
    //[ExecuteInEditMode]
    public class MeshWorkerBeh : MonoBehaviour
    {
        public enum Types
        {
            None,
            Smooth,
            Depth
        }
        
        [SerializeReferenceDropdown]
        [SerializeReference]
        private IMeshStep[] _steps;

        [Serializable]
        public class MeshStep
        {
            public Types type = Types.None;
            [ConditionalField(nameof(type), false, Types.Smooth)]
            public SmoothData smoothData;
            [ConditionalField(nameof(type), false, Types.Depth)]
            public DepthData depthData;
        }

        [Serializable]
        public struct SmoothData : IMeshStep
        {
            public int range;
        }
        
        [Serializable]
        public class DepthData : IMeshStep
        {
            public enum Mode
            {
                Mult,
                Add
            }
            public Texture2D depthTexture;
            public Mode mode = Mode.Mult;
            public float depthMult = 1f;
        }
        
        //public List<MeshStep> commands = new List<MeshStep>();
        
        public interface IMeshStep { }
        
        // [Serializable]
        // public class MeshStepBase
        // {
        //      public int range;
        // }
        //
        // [Serializable]
        // public class MeshStepSmooth : MeshStepBase
        // {
        //      public float value;
        //      public int range2;
        // }

        
        
        //[SerializeField]
        //private List<MeshStep> saved = new List<MeshStep>();

        //[SerializeField] private int updateCount = 0;

        [Button]
        private void Execute()
        {
            //updateCount = 0;
            // var child = transform.FindOrCreateChild("Child");
            // var childMF = child.GetOrAddComponent<MeshFilter>();
            // var childMR = child.GetOrAddComponent<MeshRenderer>();
            // childMR.InitAsSimple();

            var mesh = GetComponent<MeshFilter>().sharedMesh;
            var vertices = mesh.vertices;

            MeshSection section = new MeshSection();

            foreach (var cmd in _steps)
            {
                switch (cmd)
                {
                    case SmoothData data:
                        MakeSmooth(data.range);
                        break;
                    case DepthData data:
                        MakeDepth(data);
                        break;
                }
            }
        }

        private void MakeDepth(DepthData data)
        {
            var mesh = GetComponent<MeshFilter>().sharedMesh;
            var vertices = mesh.vertices;
            var size = (int)Math.Sqrt(vertices.Length);
            var depthPixels = data.depthTexture.GetPixels();
            float depthTextureWidth = data.depthTexture.width;
            var bounds = mesh.bounds;
            var minVertex = mesh.bounds.min;
            var sizeVertex = mesh.bounds.size;

            Debug.LogWarning("MakeDepth: size="+size+", depth="+data.depthTexture.width+"x"+data.depthTexture.height+", depthTextureWidth="+depthTextureWidth+", bounds="+bounds+", minVertex="+minVertex+", sizeVertex="+sizeVertex);

            for (var index = 0; index < vertices.Length; index++)
            {
                var vertex = vertices[index];
                // to 0-1
                var v01 = (vertex - minVertex).Div(sizeVertex);
                var dVertex = (v01 * (depthTextureWidth-1)).ToVector3Int();
                if (dVertex.x < 0 || dVertex.x >= depthTextureWidth || dVertex.z < 0 || dVertex.z >= depthTextureWidth)
                    throw new UnityException("vertex=" + vertex + ", v01=" + v01 + ", dVertex=" + dVertex);

                int depthIndex = dVertex.x +dVertex.z * data.depthTexture.width;
                var depthPixel = depthPixels[depthIndex];

                if (data.mode == DepthData.Mode.Mult)
                    vertex.y *= depthPixel.r * data.depthMult;
                else if (data.mode == DepthData.Mode.Add)
                    vertex.y += depthPixel.r * data.depthMult;

                
                vertices[index] = vertex;
            }

            mesh.vertices = vertices;
            mesh.RecalculateNormals();
            mesh.RecalculateTangents();
            mesh.RecalculateBounds();
        }

        private void MakeSmooth(int count)
        {
            var mesh = GetComponent<MeshFilter>().sharedMesh;
            var vertices = mesh.vertices;
            var size = (int)Math.Sqrt(vertices.Length);

            for (int i = 0; i < count; i++)
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    if (x <= 0 || y <= 0) continue;
                    if (x+1 >= size || y+1 >= size) continue;
                    var v0 = GetVertex(x, y);
                    var v1 = GetVertex(x, y + 1);
                    var v2 = GetVertex(x+1, y);
                    var v3 = GetVertex(x, y -1);
                    var v4 = GetVertex(x-1, y);
                    v0.y = (v1.y + v2.y + v3.y + v4.y + v0.y) / 5f;
                    vertices[x + y * size] = v0;
                }
            }

            mesh.vertices = vertices;
            
            mesh.RecalculateNormals();
            mesh.RecalculateTangents();
            mesh.RecalculateBounds();

            Vector3 GetVertex(int x, int y)
            {
                int index = x + y * size;
                var vertex = vertices[index];
                return vertex;
            }
        }

        // private void Update()
        // {
        //     updateCount++;
        //     UpdateList();
        // }
        //
        // private void UpdateList()
        // {
        //     for (var index = 0; index < commands.Count; index++)
        //     {
        //         var c0 = commands[index];
        //         if (c0.type == Types.Smooth && (c0.step == null || c0.step.GetType() != typeof(MeshStepSmooth)))
        //         {
        //             c0.step = new MeshStepSmooth();
        //         }
        //     }
        // }

        // private bool IsChanged()
        // {
        //     if (commands.Count != saved.Count)
        //         return true;
        //     for (var index = 0; index < commands.Count; index++)
        //     {
        //         var c0 = commands[index];
        //         var cS = saved[index];
        //         if (c0.type != cS.type)
        //         {
        //             if (c0.type == Types.Smooth)
        //                 c0.step = new MeshStepSmooth();
        //         }
        //     }
        //
        //     return false;
        // }

        // private void OnDrawGizmosSelected2()
        // {
        //     Update();
        // }
    }
}