﻿//using System.Threading.Tasks;
//using UnityEngine;
//
//namespace Shared
//{
//    public class ManagerMainDefault : Manager
//    {
//        [SerializeField] private int _targetFramerate = -1;
//        //public UnityEvent OnRound { get; set; }
//
//        [ReadOnly] [SerializeField] private long _updateCounter;
//        [SerializeField] private State _debugState;
//        //private bool _isStarted;
//        [SerializeField] private bool IsFixedUpdated = false;
//        [SerializeField] private bool IsUpdated = false;
//        [SerializeField] private bool IsLateUpdated = false;
//
//        protected override void PostInitialize()
//        {
//            base.PostInitialize();
//            //Find.ConfigMain.Notify();
//        }
//
//        private async void Awake()
//        {
//            Application.targetFrameRate = _targetFramerate;
//            Debug.Log("Camera: " + Camera.main.scaledPixelWidth + "x" + Camera.main.scaledPixelHeight);
//            Camera.main.orthographicSize = Camera.main.scaledPixelHeight / 2f;
//
//            Find.Main = this;
//            Find.Initialize(this.transform);
//            //Find.Register(this);
//
//            _debugState = Find.State;
//
//            this.InitializeMaster();
//            this.InitializeChilds();
//            this.PostInitializeChilds();
//
//            Find.Events.Restart.AddListener(async () =>
//            {
//                IsUpdated = false;
//                IsFixedUpdated = false;
//                IsLateUpdated = false;
//                await RestartAsync();
//            });
//
//            Find.Events.Restart.Invoke();
//        }
//
//        private async Task RestartAsync()
//        {
//            // play
//            Debug.Log("RestartAsync: play");
//
//            IsUpdated = true;
//            IsFixedUpdated = true;
//            IsLateUpdated = true;
//
//        }
//
//        private void FixedUpdate()
//        {
//            if (!Application.isPlaying) return;
//            if (!IsFixedUpdated) return;
//
//            Find.Events.FixedUpdate.Invoke();
//        }
//
//        private void Update()
//        {
//            if (!Application.isPlaying) return;
//            if (!IsUpdated) return;
//
//            Find.Events.Update.Invoke();
//        }
//
//        private void LateUpdate()
//        {
//            if (!Application.isPlaying) return;
//            if (!IsLateUpdated) return;
//
//            Find.Events.LateUpdate.Invoke();
//        }
//
//    }
//}