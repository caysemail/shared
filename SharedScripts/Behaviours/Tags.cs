﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Shared
{
    [Flags]
    public enum Tag
    {
        None = 0,
        Light = 1,
        Flicker = 2,
        Player = 4,
    }

    public class Tags : MonoBehaviour
    {
        public bool Light;
        public bool Flicker;
        public bool Player;
        [HideInInspector]
        public Tag Tag;

        private void OnValidate()
        {
            if (!gameObject.activeInHierarchy) return;
            Debug.LogWarning("OnValidate");

            Tag = Tag.None;
            if (Light) Tag |= Tag.Light;
            if (Flicker) Tag |= Tag.Flicker;
            if (Player) Tag |= Tag.Player;
        }
    }

    public static class TagsExtensions
    {
        public static List<Tags> GetTagsInChildren(this Component component, bool isInDisabled)
        {
            return component.GetComponentsInChildren<Tags>().ToList();
        }
        public static List<Tags> GetTagsInChildren(this Component component, Tag compare)
        {
            return component.GetComponentsInChildren<Tags>(true).Where(t=>t.Tag == compare).ToList();
        }
    }
}