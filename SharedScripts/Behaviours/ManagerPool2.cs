using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Shared
{
    public interface IPoolable
    {
        public int GetPoolCopyCount();
    }
    
    public class ManagerPool2 : Manager
    {
        private GameObject container;

        private List<IPoolable> items;
        private List<IPoolable> sources;

        private List<GameObject> goItems;
        private List<GameObject> goSources;

        [Button]
        private void Clear()
        {
            var container = transform.Find("container");
            container.DestroyToNull();
        }
        
        protected override void Initialize()
        {
            base.Initialize();
            
            Debug.Log("ManagerPool2.Initialize: disabled");
            
            Clear();

            return;
            
            // container = new GameObject("container");
            // container.transform.SetParent(transform, false);
            // var containerTransform = container.transform;
            //
            // items = new List<IPoolable>();
            // sources = new List<IPoolable>();
            // goItems = new List<GameObject>();
            // goSources = new List<GameObject>();
            //
            // foreach (Transform child in transform)
            // {
            //     if (child == containerTransform) continue;
            //     var item = child.GetComponent<IPoolable>();
            //     
            //     //if (item == null)
            //     {
            //         goSources.Add(child.gameObject);
            //
            //         var copyCount = 1;
            //         for (int i = 0; i < copyCount; i++)
            //         {
            //             var copy = CreatePoolItem(child.gameObject);
            //             goItems.Add(copy);
            //         }
            //     }
            //     if (item != null)
            //     {
            //         sources.Add(item);
            //
            //         var copyCount = item.GetPoolCopyCount();
            //         for (int i = 0; i < copyCount; i++)
            //         {
            //             var copy = CreatePoolItem(item);
            //             items.Add(copy);
            //         }
            //     }
            //     
            // }
        }

        private IPoolable CreatePoolItem(IPoolable original)
        {
            MonoBehaviour beh = (MonoBehaviour)original;
            var instance = GameObject.Instantiate(beh, container.transform, false);
            return instance.GetComponent<IPoolable>();
        }
        
        private GameObject CreatePoolItem(GameObject original)
        {
            var instance = GameObject.Instantiate(original, container.transform, false);
            instance.name = original.name;
            return instance;
        }

        public void Return(GameObject itemGo)
        {
            itemGo.SetActive(false);
            goItems.Add(itemGo);
            itemGo.transform.SetParent(container.transform, false);
            itemGo.transform.localPosition = Vector3.zero;
            itemGo.transform.localRotation = Quaternion.identity;
        }

        public GameObject Get(string itemName, Transform parent)
        {
            for (var index = goItems.Count - 1; index >= 0; index--)
            {
                var item = goItems[index];
                if (item.name == itemName)
                {
                    goItems.RemoveAt(index);
                    item.transform.SetParent(parent, false);
                    item.gameObject.SetActive(true);

                    return item;
                }
            }
            
            var source = goSources.FirstOrDefault(t => t.name == itemName);
            if (source == null)
            {
                Debug.LogError("ManagerPool2.Get: dont have poolitem.name="+itemName);
                return null;
            }
            var newItem = CreatePoolItem(source);
            newItem.transform.SetParent(parent, false);
            newItem.gameObject.SetActive(true);
            return newItem;
        }

        public T Get<T>(Transform parent)
        {
            for (var index = items.Count - 1; index >= 0; index--)
            {
                var item = items[index];
                if (item.GetType() == typeof(T))
                {
                    items.RemoveAt(index);
                    var itemT = ((MonoBehaviour)item).transform;
                    itemT.SetParent(parent, false);
                    //itemT.localPosition = Vector3.zero;
                    //itemT.localRotation = Quaternion.identity;

                    return (T)item;
                }
            }

            var source = sources.First(t => t.GetType() == typeof(T));
            var newItem = CreatePoolItem(source);
            var newItemT = ((MonoBehaviour)newItem).transform;
            newItemT.SetParent(parent, false);
            return (T)newItem;
        }
    }
}