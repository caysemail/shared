﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Shared
{
    public class CustomButton : MonoBehaviour, 
        IPointerDownHandler, IPointerUpHandler,
        IPointerEnterHandler, IPointerExitHandler, 
        IEndDragHandler, IBeginDragHandler, IDragHandler
    {
        public class PointerEvent : UnityEvent<PointerEventData> { public PointerEvent() { } }

        public PointerEvent OnDragEvent = new PointerEvent();
        public PointerEvent OnEndDragEvent = new PointerEvent();

        //        public void Invoke()
        //        {
        //            Up.Invoke(new PointerEventData(EventSystem.current));
        //        }
        //        public void Subscribe(System.Action onSubscribe)
        //        {
        //            Up.Subscribe(data=>onSubscribe());
        //        }
        //        public void Clear()
        //        {
        //            Up.Clear();
        //        }

        public void OnBeginDrag(PointerEventData data)
        {
        }

        public void OnEndDrag(PointerEventData data)
        {
//            Vector2 pos1 = Camera.main.ScreenToViewportPoint(data.pressPosition);
//            Vector2 pos2 = Camera.main.ScreenToViewportPoint(data.position);
//            float delta = Mathf.Abs(pos1.x - pos2.x) + Mathf.Abs(pos1.y - pos2.y);
//            if (delta < ViewDistanceForClick)
//            {
//                Debug.LogWarning("OnEndDrag click");
//                //Click.Invoke(data);
//            }
//            _state = State.EndDrag;
//            //Debug.LogWarning("State=" + _state);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
        }

        public void OnPointerExit(PointerEventData eventData)
        {
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            OnEndDragEvent.Invoke(eventData);
        }

        public void OnDrag(PointerEventData eventData)
        {
            OnDragEvent.Invoke(eventData);
        }
    }
}