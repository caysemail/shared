using UnityEngine;

namespace Shared
{
    [ExecuteAlways]
    public class TransformAniBeh : MonoBehaviour
    {
        [SerializeField] private Vector3 xyz = Vector3.zero;

        // private void Awake()
        // {
        //     OnEnable();
        // }
        //
        // private void Start()
        // {
        //     
        // }

        void OnEnable()
        {
            //Debug.Log("TransformAniBeh.OnEnable");
            MainFind.events.fixedUpdate -= OnFixedUpdate;
            MainFind.events.fixedUpdate += OnFixedUpdate;
        }
        
        void OnDisable()
        {
            MainFind.events.fixedUpdate -= OnFixedUpdate;
        }
        
        void OnDestroy()
        {
            MainFind.events.fixedUpdate -= OnFixedUpdate;
        }

        private void OnFixedUpdate()
        {
            transform.localRotation *= Quaternion.Euler(xyz);
        }
    }
}