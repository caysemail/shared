﻿#if UNITY_EDITOR

using System;
using UnityEditor;
using UnityEngine;

namespace Shared
{
    public class ClickEditorBeh : MonoBehaviour
    {
        public Action<Ray> OnRaycast;
        public Action<RaycastHit> OnClick;

        private int MaxTimer = 100;
        private int _isClicked;
        private Vector3 _clickPos;

        [NonSerialized] private bool _isInited;

        //[NonSerialized] 
        private bool _isEditorInited;
        [SerializeField] private bool _needSubscribeToSceneView = false;

        private void EditorInitialize()
        {
            SceneView.duringSceneGui -= EditorOnSceneGUI;
            if (_needSubscribeToSceneView) return;
            SceneView.duringSceneGui += EditorOnSceneGUI;
            _isEditorInited = true;
        }

        private void EditorOnSceneGUI(SceneView sceneView)
        {
            EditorUpdate();
            //int controlId = GUIUtility.GetControlID (FocusType.Passive);
            //_windowRect = GUILayout.Window (controlId, _windowRect, CloningWindowContent, _name, _style);
        }

        private void Initialize()
        {
            Debug.LogWarning("Initialize");
            if (!Application.isPlaying)
            {
                EditorApplication.update = EditorUpdate;
            }
        }

        private void EditorUpdate()
        {
            //if (Event.current != null) Debug.LogWarning("EditorUpdate type="+Event.current.type);
            if (Event.current?.type == EventType.MouseUp)
            {
                //Debug.LogWarning("OnSceneGUI MouseUp");
                Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
                OnEditorRaycast(ray);
            }
        }

        // void OnGUI()
        // {
        //     Debug.Log("Current event detected: " + Event.current?.type);
        // }

        public void OnEditorRaycast(Ray ray)
        {
            //if (!this.enabled) return;

            if (Physics.Raycast(ray, out RaycastHit hitInfo))
            {
                OnClick?.Invoke(hitInfo);
                var raycastGo = hitInfo.transform.gameObject;
                _clickPos = hitInfo.point;
                _isClicked = MaxTimer;
                //bool isHit = PhysUtility.TryRaycastGround(_clickPos, out Vector3 hPos);
                //Debug.LogWarning("OnEditorRaycast ray=" + ray + ", pos=" + _clickPos + ", go=" + raycastGo.name);
            }

            OnRaycast?.Invoke(ray);
            //var mvl = GetComponent<MeshVolumeLamellar>();
            //if (mvl) mvl.OnEditorRaycast(ray);
        }

        private void OnDrawGizmos()
        {
            //Debug.LogError("OnDrawGizmos");
            if (!_isEditorInited) EditorInitialize();
            //if (!_isInited) Initialize();
            if (_isClicked <= 0) return;
            _isClicked--;
            float percent = (float) _isClicked / MaxTimer;
            Gizmos.color = Color.Lerp(Color.black, Color.white, percent);
            Gizmos.DrawWireSphere(_clickPos, 5f * percent);
            GizmosExt.DrawString(_clickPos.ToString(), _clickPos, 0, 0, Gizmos.color);
        }
    }
}

#endif