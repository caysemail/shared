using System;
using UnityEngine;

namespace Shared
{
    public class EditorRaycastBeh : MonoBehaviour
    {
        public Action<Ray> OnRaycast;

        [SerializeField][ReadOnly] private int countRaycast = 0;
        [SerializeField][ReadOnly] private int countScene = 0;
        [SerializeField][ReadOnly] private int countMouseMove = 0;

        private void Start()
        {
        }

        public void OnEditorRaycast(Ray ray)
        {
            if (!this.enabled || !gameObject.activeSelf) return;
            OnRaycast?.Invoke(ray);
            countRaycast++;
            var beh = GetComponent<IEditorRaycast>();
            if (beh != null) beh.OnEditorRaycast(ray);
        }
        
        public void OnSceneGui()
        {
            if (!this.enabled || !gameObject.activeSelf) return;
            countScene++;
            var beh = GetComponent<IEditorSceneGui>();
            if (beh != null) beh.OnEditorSceneGui();
        }

        public void OnEditorMouseMove(Ray ray)
        {
            if (!this.enabled || !gameObject.activeSelf) return;
            OnRaycast?.Invoke(ray);
            countMouseMove++;
            var beh = GetComponent<IEditorMouseMove>();
            if (beh != null) beh.OnEditorMouseMove(ray);
        }
    }
}