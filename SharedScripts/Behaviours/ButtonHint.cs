﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Shared
{
    public class ButtonHint : Graphic,
        IPointerDownHandler, IPointerUpHandler,
        IPointerEnterHandler, IPointerExitHandler, 
        IEndDragHandler, IBeginDragHandler, IDragHandler
    {
        // graphic
       
        public override void SetMaterialDirty() { return; }
        public override void SetVerticesDirty() { return; }
        
        /// Probably not necessary since the chain of calls `Rebuild()`->`UpdateGeometry()`->`DoMeshGeneration()`->`OnPopulateMesh()` won't happen; so here really just as a fail-safe.
        protected override void OnPopulateMesh(VertexHelper vh)
        {
            vh.Clear();
            return;
        }
        
        // button
        public class PointerEvent : UnityEvent<PointerEventData> { public PointerEvent() { } }
        public class BoolEvent : UnityEvent<bool> { public BoolEvent() { } }
        public class SimpleEvent : UnityEvent { public SimpleEvent() { } }
        
        public PointerEvent OnDragEvent = new PointerEvent();
        public PointerEvent OnEndDragEvent = new PointerEvent();

        public BoolEvent OnHint = new BoolEvent();
        public SimpleEvent OnClick = new SimpleEvent();
        //public UnityAction OnClickAction;// = new UnityAction();
#pragma warning disable
#pragma warning disable 0219
        // ReSharper disable once NotAccessedField.Local
        [ReadOnly][SerializeField] private bool _onDrag;
        // ReSharper disable once NotAccessedField.Local
        [ReadOnly][SerializeField] private int _dragCount;
        // ReSharper disable once NotAccessedField.Local
        [ReadOnly][SerializeField] private bool _onPointerInside;
        // ReSharper disable once NotAccessedField.Local
        [ReadOnly][SerializeField] private bool _onPress;
#pragma warning restore 0219        
#pragma warning restore
        //        public void Invoke()
        //        {
        //            Up.Invoke(new PointerEventData(EventSystem.current));
        //        }
        //        public void Subscribe(System.Action onSubscribe)
        //        {
        //            Up.Subscribe(data=>onSubscribe());
        //        }
        //        public void Clear()
        //        {
        //            Up.Clear();
        //        }

        public void OnBeginDrag(PointerEventData data)
        {
            _onDrag = true;
            _dragCount = 0;
        }

        public void OnEndDrag(PointerEventData data)
        {
            _onDrag = false;
//            Vector2 pos1 = Camera.main.ScreenToViewportPoint(data.pressPosition);
//            Vector2 pos2 = Camera.main.ScreenToViewportPoint(data.position);
//            float delta = Mathf.Abs(pos1.x - pos2.x) + Mathf.Abs(pos1.y - pos2.y);
//            if (delta < ViewDistanceForClick)
//            {
//                Debug.LogWarning("OnEndDrag click");
//                //Click.Invoke(data);
//            }
//            _state = State.EndDrag;
//            //Debug.LogWarning("State=" + _state);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _onPress = true;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _onPointerInside = true;
            OnHint.Invoke(true);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _onPointerInside = false;
            OnHint.Invoke(false);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            OnEndDragEvent.Invoke(eventData);
            _onPress = false;
            OnClick.Invoke();
        }

        public void OnDrag(PointerEventData eventData)
        {
            OnDragEvent.Invoke(eventData);
            _dragCount++;
        }

       
    }
}