using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public class ColliderToMeshBeh : MonoBehaviour
    {
        public bool gizmo = false;
        public float gizmoLength = 0.02f;
        public int size;
        public float minY = -100;
        public MeshCollider meshCollider;
        
        [Button]
        void Execute()
        {
            var child = transform.FindOrCreateChild("Child");
            var childMF = child.GetOrAddComponent<MeshFilter>();
            var childMR = child.GetOrAddComponent<MeshRenderer>();
            childMR.InitAsSimple();
            childMF.sharedMesh = new Mesh();
            
            //var collider = GetComponent<MeshCollider>();
            var bounds = meshCollider.bounds;

            Vector3 step = bounds.size / (float)size;
            
            Vector2Int arraySize = Vector2Int.zero;

            Debug.Log("ColliderToMeshBeh: bounds="+bounds+", min="+bounds.min+", max="+bounds.max+", step="+step+", arraySize="+arraySize);

            MeshSection section = new MeshSection();

            for (float z = bounds.min.z; z <= bounds.max.z; z+=step.z)
            {
                if (arraySize.y >= size) continue;
                arraySize.y++;
                arraySize.x = 0;
                for (float x = bounds.min.x; x <= bounds.max.x; x+=step.x)
                {
                    if (arraySize.x >= size) continue;
                    arraySize.x++;
                    var pos = new Vector3(x, 0, z);
                    var ray = new Ray(pos.SetY(1000), Vector3.down);
                    bool isHit = meshCollider.Raycast(ray, out var hit, float.MaxValue);
                    if (!isHit)
                        hit.point = pos;
                    //if (isHit && hit.point.y >= minY)
                    {
                        //Gizmos.DrawLine(hit.point, hit.point + Vector3.up * 0.02f);
                        AddPoint(hit.point);
                    }
                    //Physics.Raycast()
                }
            }

            section.MakeIndicesByArray(size);
            
            section.Apply(childMF.sharedMesh, new MeshSection.ApplyMask() { vertices = true, quads = true, indices = true, normalsAndTangents = true, uv0 = true});

            void AddPoint(Vector3 point)
            {
                section.Vertices.Add(point);
            }
        }

        private void OnDrawGizmosSelected()
        {
            if (!gizmo) return;
            
            //var collider = GetComponent<MeshCollider>();
            var bounds = meshCollider.bounds;

            Vector3 step = bounds.size / (float)size;

            Gizmos.color = Color.green;
            for (float z = bounds.min.z; z <= bounds.max.z; z+=step.z)
            {
                for (float x = bounds.min.x; x <= bounds.max.x; x+=step.x)
                {
                    var pos = new Vector3(x, 0, z);
                    var ray = new Ray(pos.SetY(1000), Vector3.down);
                    bool isHit = meshCollider.Raycast(ray, out var hit, float.MaxValue);
                    //if (isHit && hit.point.y >= minY)
                        Gizmos.DrawLine(hit.point, hit.point+Vector3.up*gizmoLength);
                }
            }
        }
    }
}