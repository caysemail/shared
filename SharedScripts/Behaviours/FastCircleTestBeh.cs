using System;
using System.Collections.Generic;
using Shared;
using UnityEditor;
using UnityEngine;

public class FastCircleTestBeh : MonoBehaviour
{
    [Button]
    private void Test()
    {
        int tryes = 2000000;
        FindRandom random = new FindRandom();
        var points = new List<Vector2>();
        float radius = tryes;
        for (int i = 0; i < tryes; i++)
        {
            var randomPoint = new Vector2(random.Range(-radius, +radius), random.Range(-radius, +radius));
            points.Add(randomPoint);
        }

        var point0t = new Vector2(10, 20);
        var point1t = new Vector2(40, 80);
        
        Shared.Timer timer2 = new Timer().Start();
        float summ2 = 0f;
        var a = point1t;
        var b = point0t;
        for (int i = 0; i < tryes; i++)
        {
            //var point0 = points[i];
            //var point1 = points[tryes - i - 1];
            //var distance = point0.DistanceFast(point1);
            //var distance = Vector2Ext.DistanceFast2(point0t, point1t);
            //var distance2 = Vector2Ext.DistanceFast2(point1t, point0t);
            
            a.x -= b.x; a.y -= b.y;
            a.x = a.x >= 0 ? a.x : -a.x;
            a.y = a.y >= 0 ? a.y : -a.y;
            summ2 += a.x + a.y;

            
            //summ2 += distance;
        }
        Debug.LogWarning("point0.DistanceFast time="+timer2.Stop());

        Shared.Timer timer = new Timer().Start();
        timer.Start();
        float summ = 0f;
        for (int i = 0; i < tryes; i++)
        {
            //var point0 = points[i];
            //var point1 = points[tryes - i - 1];
            //var distance = Vector2.Distance(point0t, point1t);
            
            float num1 = a.x - b.x;
            float num2 = a.y - b.y;
            summ += (float) Math.Sqrt((double) num1 * (double) num1 + (double) num2 * (double) num2);

            
            //var distance2 = Vector2.Distance(point1t, point0t);
            //summ += distance;
        }
        Debug.LogWarning("Vector2.Distance time="+timer.Stop());

    }
    
    private void OnDrawGizmosSelected()
    {
        Vector3 center = Vector3.zero;
        float radius = 1f;
        Handles.color = ColorExt.greenSemiDark;
        //Handles.CircleHandleCap(0, center, Quaternion.identity, 1f, EventType.Repaint);
        Handles.DrawWireDisc(center, Vector3.up, radius);

        float wSize = radius;
        Handles.DrawLine(center+new Vector3(-wSize,0,-wSize), center+new Vector3(wSize,0, -wSize), 1f);
        Handles.DrawLine(center+new Vector3(-wSize,0,-wSize), center+new Vector3(-wSize,0, +wSize), 1f);
        Handles.DrawLine(center+new Vector3(+wSize,0,+wSize), center+new Vector3(wSize,0, -wSize), 1f);
        Handles.DrawLine(center+new Vector3(+wSize,0,+wSize), center+new Vector3(-wSize,0, +wSize), 1f);

        var tryesMax = 1000;
        FindRandom random = new FindRandom(0);
        for (int i = 0; i < tryesMax; i++)
        {
            var randomPoint = new Vector3(random.Range(-radius, +radius), 0f, random.Range(-radius, +radius));
            var isInsideFast = center.DistanceFastXZ(randomPoint) < radius;
            if (isInsideFast) Handles.color = Color.red;
            else Handles.color = Color.green;
            Handles.DrawWireDisc(randomPoint, Vector3.up, 0.02f);
        }
    }
}
