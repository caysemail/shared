using UnityEngine;

namespace Shared
{
    public class DebugBeh : MonoBehaviour
    {
        void Awake()
        {
            Debug.LogError("DebugBeh.Awake: enabled="+this.enabled+", playing="+Application.isPlaying);
        }

        void Start()
        {
            Debug.LogError("DebugBeh.Start: enabled="+this.enabled+", playing="+Application.isPlaying);
        }

        void OnEnable()
        {
            Debug.LogError("DebugBeh.OnEnable: enabled="+this.enabled+", playing="+Application.isPlaying);;
        }
        
        void OnDisable()
        {
            Debug.LogError("DebugBeh.OnDisable: enabled="+this.enabled+", playing="+Application.isPlaying);
        }
    }
}