using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Shared
{
    public class Texture2DTestBeh : MonoBehaviour
    {
        [Button]
        private void TestStart()
        {
            int width = 256;
            int height = 256;
            
            var texture = new Texture2D(width, height, TextureFormat.RGBA32, false);
            texture.filterMode = FilterMode.Point;
            texture.wrapMode = TextureWrapMode.Clamp;

            Timer timerRawData = new Timer();
            var rawData = texture.GetRawTextureData<Color32>();
            Timer timerRawDataRead = new Timer();
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    var index = x + y * width;
                    var color32 = rawData[index];
                }
            }
            timerRawDataRead.Stop();
            Timer timerRawDataWrite = new Timer();
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    var index = x + y * width;
                    rawData[index] = Color32Ext.gray;
                }
            }
            timerRawDataWrite.Stop();
            texture.Apply();
            timerRawData.Stop();
            
            Timer timerGetPixels32 = new Timer();
            var colorsArray = texture.GetPixels32();
            Timer timerGetPixels32Read = new Timer();
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    var index = x + y * width;
                    var color32 = colorsArray[index];
                }
            }
            timerGetPixels32Read.Stop();
            Timer timerGetPixels32Write = new Timer();
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    var index = x + y * width;
                    colorsArray[index] = Color32Ext.gray;
                }
            }
            timerGetPixels32Write.Stop();
            texture.Apply();
            timerGetPixels32.Stop();
            
            Debug.Log("TestStart completed successfully: "
                      +"timerRawData="+timerRawData.elapsed+", timerRawDataRead="+timerRawDataRead.elapsed+", timerRawDataWrite="+timerRawDataWrite.elapsed
                      +", timerGetPixels32="+timerGetPixels32.elapsed+", timerGetPixels32Read="+timerGetPixels32Read.elapsed+", timerGetPixels32Write="+timerGetPixels32Write.elapsed);
        }
    }
}