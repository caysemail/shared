using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public class Assert2TestBeh : MonoBehaviour
    {
        [Button]
        private void TestStart()
        {
            List<int> a = new List<int>();
            Assert2.IsNotNull(a);
            Assert2.IsNotNull(gameObject);
            Assert2.IsNotNull(transform);
            //int b = 0;
            //Assert2.IsNotNull(b);
            var test = transform.FindOrCreateChild("test");
            Assert2.IsNotNull(test);
            transform.DestroyChildren();
            Assert2.IsNull(test);
            a = null;
            Assert2.IsNull(a);
            
            Debug.Log("Assert2Test completed successfully");
        }
    }
}