using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public class DictionaryTestBeh : MonoBehaviour
    {
        [Button]
        private void TestStart()
        {
            Dictionary<(int, int), DictionaryTestBeh> tupleDic = new Dictionary<(int, int), DictionaryTestBeh>();
            Dictionary<int, DictionaryTestBeh> intDic = new Dictionary<int, DictionaryTestBeh>();

            int width = 256;
            int height = 256;
            
            Timer timerTupleDic = new Timer();
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    tupleDic.Add((x,y), this);
                }
            }
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    var temp = tupleDic[(x, y)];
                }
            }
            timerTupleDic.Stop();
            
            Timer timerIntDic = new Timer();
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    var index = x + y * width;
                    intDic.Add(index, this);
                }
            }
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    var index = x + y * width;
                    var temp = intDic[index];
                }
            }
            timerIntDic.Stop();
            
            Debug.Log("Assert2Test completed successfully, timerTupleDic="+timerTupleDic.elapsed+", timerIntDic="+timerIntDic.elapsed);
        }
    }
}