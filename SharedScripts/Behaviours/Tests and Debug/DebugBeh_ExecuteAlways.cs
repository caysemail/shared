using UnityEngine;

namespace Shared
{
    [ExecuteAlways]
    public class DebugBeh_ExecuteAlways : MonoBehaviour
    {
        void Awake()
        {
            Debug.LogError("DebugBeh_ExecuteAlways.Awake: enabled="+this.enabled+", playing="+Application.isPlaying);
        }
        
        void Start()
        {
            Debug.LogError("DebugBeh_ExecuteAlways.Start: enabled="+this.enabled+", playing="+Application.isPlaying);
        }

        void OnEnable()
        {
            Debug.LogError("DebugBeh_ExecuteAlways.OnEnable: enabled="+this.enabled+", playing="+Application.isPlaying);;
        }
        
        void OnDisable()
        {
            Debug.LogError("DebugBeh_ExecuteAlways.OnDisable: enabled="+this.enabled+", playing="+Application.isPlaying);
        }
    }
}