using UnityEngine;

namespace Shared
{
    [ExecuteInEditMode]
    public class DebugBeh_ExecuteInEditMode : MonoBehaviour
    {
        void Awake()
        {
            Debug.LogError("DebugBeh_ExecuteInEditMode.Awake: enabled="+this.enabled+", playing="+Application.isPlaying);
        }

        void Start()
        {
            Debug.LogError("DebugBeh_ExecuteInEditMode.Start: enabled="+this.enabled+", playing="+Application.isPlaying);
        }

        void OnEnable()
        {
            Debug.LogError("DebugBeh_ExecuteInEditMode.OnEnable: enabled="+this.enabled+", playing="+Application.isPlaying);;
        }
        
        void OnDisable()
        {
            Debug.LogError("DebugBeh_ExecuteInEditMode.OnDisable: enabled="+this.enabled+", playing="+Application.isPlaying);
        }
    }
}