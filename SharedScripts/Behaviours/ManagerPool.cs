﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Shared
{
    public class ManagerPool : Manager
    {
        [Serializable]
        public class Element
        {
            public string prefabName;
            public GameObject prefab;
            public int count;
            public int debugRequestedCount;
            [ReadOnly]
            public int freeCount;
            [ReadOnly]
            public int busyCount;
            [HideInInspector]
            public List<Transform> free = new List<Transform>();
            [HideInInspector]
            public List<Transform> busy = new List<Transform>();
        }

        [SerializeField]
        private List<Element> _elements = new List<Element>();
        
        public Transform Get(string prefabName)
        {
            var element = _elements.First(t => t.prefabName == prefabName);
            element.debugRequestedCount++;
            Transform instance;
            if (element.free.Count > 0)
            {
                instance = element.free.PopLast();
                element.busy.Add(instance);
                element.freeCount = element.free.Count;
                element.busyCount = element.busy.Count;
            }
            else
            {
                instance = Instantiate(element.prefab).transform;
                element.busy.Add(instance);
                element.busyCount = element.busy.Count;
            }
            return instance;
        }

        [Button]
        public void EditorPreparePool()
        {
            transform.hierarchyCapacity = 200000;
            foreach (var element in _elements)
            {
                var need = element.count - element.free.Count;
                for (int i = 0; i < need; i++)
                {
                    var instance = Instantiate(element.prefab, transform, false).transform;
                    element.free.Add(instance);
                }
                element.freeCount = element.free.Count;
            }
        }

        [Button]
        public void Clear()
        {
            foreach (var element in _elements)
            {
                foreach (var instance in element.free)
                {
                    GameObject.DestroyImmediate(instance.gameObject);
                }
                element.free.Clear();
            }
        }
    }
}