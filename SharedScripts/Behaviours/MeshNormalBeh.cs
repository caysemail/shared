using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Shared
{
    public class MeshNormalBeh : MonoBehaviour
    {
        [SerializeField][ReadOnly]
        private Mesh _savedMesh = null;
        
        public bool gizmoNormals = false;
        public bool gizmoTriangles = false;
        public bool gizmoTrianglesDefault = false;
        
        [Range(0, 1f)] public float smoothStrength = 0.5f;
        
        [Button]
        private void SaveMesh()
        {
            var mesh = GetComponent<MeshFilter>().sharedMesh;
            _savedMesh = mesh;
        }
        [Button]
        private void RestoreMesh()
        {
            if (_savedMesh == null) return;
            
            GetComponent<MeshFilter>().sharedMesh = _savedMesh;
            Debug.LogWarning("vertices="+_savedMesh.vertexCount+", tangets="+_savedMesh.tangents.Length);
        }

        private void OnDrawGizmosSelected()
        {
            //return;
            if (gizmoNormals)
            {
                var mesh = GetComponent<MeshFilter>().sharedMesh;
                bool isTangents = mesh.tangents.Length > 0;
                for(var i = 0; i < mesh.vertexCount; i++)
                {
                    var vertex = mesh.vertices[i];
                    var normal = mesh.normals[i];
                    
                    Gizmos.color = Color.white;
                    var pos = transform.TransformPoint(vertex);
                    Gizmos.DrawLine(pos, pos+normal*0.1f);

                    if (isTangents && false)
                    {
                        var tanget = mesh.tangents[i];
                        Gizmos.color = tanget.z == 0 ? Color.black : (tanget.z > 0 ? Color.red : Color.blue);
                        Gizmos.DrawLine(pos, pos + (Vector3) tanget * 0.2f);
                    }
                }
            }

            if (gizmoTriangles)
            {
                var mesh = GetComponent<MeshFilter>().sharedMesh;
                for (var index = 0; index < mesh.triangles.Length; index+=3)
                {
                    var v0 = mesh.vertices[mesh.triangles[index]];
                    var v1 = mesh.vertices[mesh.triangles[index+1]];
                    var v2 = mesh.vertices[mesh.triangles[index+2]];
                    var average = (v0+v1+v2)/3f;
                    var n0 = mesh.normals[mesh.triangles[index]];
                    var n1 = mesh.normals[mesh.triangles[index+1]];
                    var n2 = mesh.normals[mesh.triangles[index+2]];
                    var normalAverage = (n0+n1+n2)/3f;
                    var worldPoint = transform.TransformPoint(average);
                    Gizmos.color = Color.white;
                    Gizmos.DrawLine(worldPoint, worldPoint + normalAverage*0.1f);
                }
            }

            if (gizmoTrianglesDefault)
            {
                var mesh = GetComponent<MeshFilter>().sharedMesh;
                var vertices = mesh.vertices;
                var triangles = mesh.triangles;
                for (var index = 0; index < triangles.Length; index+=3)
                {
                    var v0 = vertices[triangles[index]];
                    var v1 = vertices[triangles[index+1]];
                    var v2 = vertices[triangles[index+2]];
                    var average = (v0+v1+v2)/3f;
                    // var n0 = mesh.normals[mesh.triangles[index]];
                    // var n1 = mesh.normals[mesh.triangles[index+1]];
                    // var n2 = mesh.normals[mesh.triangles[index+2]];
                    // var normalAverage = (n0+n1+n2)/3f;
                    var worldPoint = transform.TransformPoint(average);
                    
                    Vector3 p1 = v1 - v0;
                    Vector3 p2 = v2 - v0;
                    // By not normalizing the cross product,
                    // the face area is pre-multiplied onto the normal for free.
                    Vector3 normal = Vector3.Cross (p1, p2).normalized;
                    Gizmos.color = Color.white;
                    Gizmos.DrawLine(worldPoint, worldPoint + normal * 0.1f);
                    
                    // Gizmos.color = Color.red;
                    // Gizmos.DrawLine(worldPoint, transform.TransformPoint(v0));
                    // Gizmos.DrawLine(worldPoint, transform.TransformPoint(v1));
                    // Gizmos.DrawLine(worldPoint, transform.TransformPoint(v2));
                }
            }
        }

        [Button]
        private void RecreateMeshAsIndependentTriangles()
        {
            var mesh = (Application.isPlaying) ? GetComponent<MeshFilter>().mesh : GetComponent<MeshFilter>().sharedMesh;
            
            mesh = new Mesh()
            {
                vertices = mesh.vertices, 
                triangles = mesh.triangles, 
                normals = mesh.normals, 
                tangents = mesh.tangents, 
                bounds = mesh.bounds,
                uv = mesh.uv
            };
            
            if (Application.isPlaying) GetComponent<MeshFilter>().mesh = mesh;
            else GetComponent<MeshFilter>().sharedMesh = mesh;
            
            var vertices = mesh.vertices;
            var normals = mesh.normals;
            var triangles = mesh.triangles;
            List<Vector3> newVertices = new List<Vector3>();
            List<int> newTriangles = new List<int>();
            int triangleIndex = 0;
            List<Vector2> uv1 = new List<Vector2>();
            List<Color32> colors32 = new List<Color32>();
            for (var index = 0; index < triangles.Length; index += 3)
            {
                var v0 = vertices[triangles[index]];
                var v1 = vertices[triangles[index + 1]];
                var v2 = vertices[triangles[index + 2]];

                newVertices.Add(v0);
                newVertices.Add(v1);
                newVertices.Add(v2);
                
                newTriangles.Add(triangleIndex); triangleIndex++;
                newTriangles.Add(triangleIndex); triangleIndex++;
                newTriangles.Add(triangleIndex); triangleIndex++;
                
                uv1.Add(new Vector2(0,0));
                uv1.Add(new Vector2(0,1));
                uv1.Add(new Vector2(1,1));
                
                colors32.Add(new Color32(255,0,0,0));
                colors32.Add(new Color32(0, 255,0,0));
                colors32.Add(new Color32(0,0, 255,0));
            }
            mesh.SetColors(colors32);
            mesh.SetVertices(newVertices);
            mesh.SetIndices(newTriangles, MeshTopology.Triangles, 0);
            mesh.SetUVs(0, uv1);
            mesh.SetUVs(1, uv1);
            mesh.RecalculateNormals();
            mesh.RecalculateTangents();
            mesh.UploadMeshData(false);
            
            
        }

        [Button]
        private void AverageNormalByTriangles()
        {
            var mesh = (Application.isPlaying) ? GetComponent<MeshFilter>().mesh : GetComponent<MeshFilter>().sharedMesh;
            
            var vertices = mesh.vertices;
            var normals = mesh.normals;
            var triangles = mesh.triangles;
            // assume what all normals is lost and must be recalculated by triangles
            //Debug.Log("AverageNormalByTriangles: vertices="+vertices.Length);
            for (var index = 0; index < vertices.Length; index++)
            {
                var normalSumm = Vector3.zero;
                int normalCount = 0;
                for (var i = 0; i < triangles.Length; i+=3)
                {
                    var t0 = triangles[i];
                    var t1 = triangles[i+1];
                    var t2 = triangles[i+2];
                    
                    if (t0 == index || t1 == index || t2 == index)
                    {
                        //Calculate the normal of the triangle
                        Vector3 p1 = vertices[t1] - vertices[t0];
                        Vector3 p2 = vertices[t2] - vertices[t0];
                        // By not normalizing the cross product,
                        // the face area is pre-multiplied onto the normal for free.
                        Vector3 normal = Vector3.Cross (p1, p2).normalized;
                        normalCount++;
                        normalSumm += normal;
                    }
                }

                var newNormal = (normalSumm / (float) normalCount).normalized;
                Debug.Log("AverageNormalByTriangles: index="+index+", normal="+normals[index]+" <- "+newNormal+", normalCount="+normalCount);
                normals[index] = newNormal;
            }
            mesh.SetNormals(normals);
            mesh.UploadMeshData(false);
        }
            
        
        [Button]
        private void SmoothNormal2()
        {
            var mesh = GetComponent<MeshFilter>().sharedMesh;
            var newMesh = new Mesh()
            {
                vertices = mesh.vertices, 
                triangles = mesh.triangles, 
                normals = mesh.normals, 
                tangents = mesh.tangents, 
                bounds = mesh.bounds,
                uv = mesh.uv,
                uv2 = mesh.uv2,
                colors32 = mesh.colors32,
            };
            
            var vertices = newMesh.vertices;
            var normals = newMesh.normals.ToList();
            var normals2 = normals.ToList();
            bool[] set = new bool[normals.Count()];
            for (var index = 0; index < vertices.Length; index++)
            {
                if (set[index]) continue;
                set[index] = true;
                var vertex = vertices[index];
                List<int> indices = new List<int>() { index };
                Vector3 normalsSumm = normals[index];
                int normalsCount = 1;
                for (var index2 = 0; index2 < vertices.Length; index2++)
                {
                    if (index == index2) continue;
                    var vertex2 = vertices[index2];
                    if (vertex2.IsEquals(vertex))
                    {
                        indices.Add(index2);
                        normalsCount++;
                        normalsSumm += normals[index2];
                        set[index2] = true;
                    }
                }

                normalsSumm = (normalsSumm / (float)normalsCount).normalized;
                //Debug.LogError("index="+index+", normal="+normals[index]+", normalsCount="+normalsCount+", normalsSumm="+normalsSumm+", indices="+indices.Count);

                // Vector3 summ = Vector3.zero;
                // foreach (var index3 in indices)
                // {
                //     summ += normals[index3];
                // }
                // summ /= indices.Count;
                
                foreach (var index3 in indices)
                {
                    normals2[index3] = Vector3.Lerp(normals[index3],normalsSumm, smoothStrength).normalized;
                    //normals2[index3] = normalsSumm.normalized;
                    //set[index3] = true;
                }
            }
            //newMesh.RecalculateNormals();
            newMesh.SetNormals(normals2);
            //newMesh.RecalculateNormals();
            newMesh.UploadMeshData(false);
            GetComponent<MeshFilter>().sharedMesh = newMesh;
        }
        
        [Button]
        private void SmoothNormals()
        {
            var mesh = GetComponent<MeshFilter>().sharedMesh;
            var newMesh = new Mesh()
            {
                vertices = mesh.vertices, 
                triangles = mesh.triangles, 
                normals = mesh.normals, 
                tangents = mesh.tangents, 
                bounds = mesh.bounds,
                uv = mesh.uv
            };
            //var mesh = new Mesh();
            //var mesh = GetComponent<MeshFilter>().sharedMesh;
            RecalculateNormalsSeamless(newMesh);
            //mesh.RecalculateTangents();
            newMesh.UploadMeshData(false);
            GetComponent<MeshFilter>().sharedMesh = newMesh;
        }

        [Button]
        private void RecalcTangents()
        {
            var mesh = GetComponent<MeshFilter>().sharedMesh;
            mesh.RecalculateTangents();
            mesh.UploadMeshData(false);
        }
        
        [Button]
        private void RecalcNormals()
        {
            var mesh = GetComponent<MeshFilter>().sharedMesh;
            mesh.RecalculateNormals();
            mesh.UploadMeshData(false);
        }


        void RecalculateNormalsSeamless(Mesh mesh) 
        {
            var trianglesOriginal = mesh.triangles;
            var triangles = trianglesOriginal.ToArray();
       
            var vertices = mesh.vertices;
       
            var mergeIndices = new Dictionary<int, int>();
     
            for (int i = 0; i < vertices.Length; i++) {
                var vertexHash = vertices[i].GetHashCode();                  
           
                if (mergeIndices.TryGetValue(vertexHash, out var index)) {
                    for (int j = 0; j < triangles.Length; j++)
                        if (triangles[j] == i)
                            triangles[j] = index;
                } else
                    mergeIndices.Add(vertexHash, i);
            }
     
            mesh.triangles = triangles;
       
            var normals = new Vector3[vertices.Length];
       
            mesh.RecalculateNormals();
            var newNormals = mesh.normals;
       
            for (int i = 0; i < vertices.Length; i++)
                if (mergeIndices.TryGetValue(vertices[i].GetHashCode(), out var index))
                    normals[i] = newNormals[index];
     
            mesh.triangles = trianglesOriginal;
            mesh.normals = normals;
        }
    }
}