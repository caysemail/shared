﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Component = UnityEngine.Component;

namespace Shared
{
    public class Manager : MonoBehaviour
    {
        private const bool LOG_INITIALIZE = false;
        private const bool LOG_DEBUG = false;
        
        [Serializable]
        public struct ManagerInfo
        {
            public ManagerType Type;
            //public bool IsPreInitialized;
            public bool IsInitialized;
            public bool IsMaster;
            public bool IsScreenMaster;
            public bool IsScreen;
            public bool IsPanel;
            public bool IsManager;
            public bool GetMounts;
            public Manager ParentManager;
            public int PanelCount;
            public int MountCount;
            public int ManagerCount;
        }
        //private const bool LOG_INIT = false;
        public enum ManagerType { None = 0, Master, UIManager, Manager, Screen, Panel }

        [ReadOnly][SerializeField]
        private ManagerInfo Info;

        //[SerializeField] [ReadOnly] private ManagerType _type = ManagerType.None;
        //[SerializeField] [ReadOnly] protected bool _isManagerInitialized;
        //[SerializeField] [ReadOnly] protected bool _isManagerPostInitialized;

        private const string MOUNT_PREFIX   = "Mount - ";
        private const string MANAGER_PREFIX = "Manager - ";
        private const string SCREEN_PREFIX  = "Screen - ";
        private const string PANEL_PREFIX   = "Panel - ";
        private const string LABEL_PREFIX   = "Label - ";
        private const string BUTTON_PREFIX = "Button - ";
        private const string TOGGLE_PREFIX = "Toggle - ";
        private const string IMAGE_PREFIX   = "Image - ";

        // manager components
        private readonly Dictionary<string, Transform> _mounts   = new Dictionary<string, Transform>();
        private readonly Dictionary<string, Manager>   _managers = new Dictionary<string, Manager>();
        private readonly Dictionary<string, Manager>   _screens  = new Dictionary<string, Manager>();
        //private readonly Dictionary<string, Manager>   _managerPanels = new Dictionary<string, Manager>();
        // panel components
        private readonly Dictionary<string, Transform> _panels  = new Dictionary<string, Transform>();
        private Dictionary<string, TextMeshProUGUI> _labels = new Dictionary<string, TextMeshProUGUI>();
        //private Dictionary<string, Text> _labels = new Dictionary<string, Text>();
        private Dictionary<string, Button>             _buttons = new Dictionary<string, Button>();
        private Dictionary<string, Toggle>             _toggles = new Dictionary<string, Toggle>();
        private Dictionary<string, Image>              _images  = new Dictionary<string, Image>();

        //protected bool _isUIManager;

        private int LabelCount;

        public static Manager PanelInstantiateShowInitialize(GameObject panelDef)
        {
            var go = UIUtility.InstantiateAndShow(panelDef);
            return go.AddComponent<Manager>().InitializeAsPanel();
        }

        public void AddShowInitialize<T>(GameObject panel) where T:Manager
        {
            var script = (Manager)panel.AddComponent(typeof(T));
            script.Show();
            script.Initialize();
        }

        public List<Manager> GetManagers()
        {
            return _managers.Values.ToList();
        }
        
        public ManagerInfo DebugGetManagerInfo()
        {
            return Info;
        }

        protected Manager GetMaster()
        {
            int maxCount = 100;
            Manager parent = Info.ParentManager;
            if (parent == null)
            {
                Debug.LogError(this.name+".Info.ParentManager is null", transform);
                throw new UnityException("Info.ParentManager is null");
            }

            while(maxCount > 0)
            {
                var nextParent = parent.Info.ParentManager;
                if (nextParent == null)
                {
                    return parent;
                }
                parent = nextParent;
                maxCount--;
            }
            throw new UnityException("recursive");
        }
        
        // private void Awake()
        // {
        //     Info = new ManagerInfo();
        //     if (LOG_DEBUG)
        //         Debug.LogWarning("Manager."+name+".Awake: Info new");
        // }

        [ContextMenu("RemoveRaycasts")]
        protected void RemoveRaycasts()
        {
            var images = transform.GetComponentsInChildren<Image>();
            foreach (var image in images)
            {
                image.raycastTarget = false;
            }
            var tmp_texts = transform.GetComponentsInChildren<TMP_Text>();
            foreach (var text in tmp_texts)
            {
                text.raycastTarget = false;
            }
        }

        private void CheckIsMasterSetParent ()
        {
            Info.IsMaster = false;
            var parent = transform.parent;
            if (parent == null)
                Info.IsMaster = true;
            else
            {
                Info.ParentManager = parent.GetComponentInParent<Manager>();
                if (Info.ParentManager == null)
                    Info.IsMaster = true;
            }
        }

        private void GetMounts()
        {
            CollectDeepComponents (_managers, MOUNT_PREFIX);
            Info.MountCount = CollectDeepComponents(_mounts, MOUNT_PREFIX, false, false);
            Info.GetMounts = true;
        }

        private void CollectComponentsBase()
        {
            if (name.Contains(SCREEN_PREFIX))
                Info.IsScreen = true;
            if (name.Contains(MANAGER_PREFIX))
                Info.IsManager = true;
            if (name.Contains(PANEL_PREFIX))
                Info.IsPanel = true;

            if (Info.IsScreen)
            {
                _panels.Clear();
                _managers.Clear();
                CollectChildComponents(_panels, PANEL_PREFIX);
                CollectDeepComponents(_managers, PANEL_PREFIX, true);
            }
            else
            {
                _screens.Clear();
                _managers.Clear();
                _mounts.Clear();
                CollectChildComponents(_managers, SCREEN_PREFIX);
                CollectChildComponents(_managers, MANAGER_PREFIX);
                //if (!Info.IsMaster)
                //    CollectDeepComponents (_managers, MOUNT_PREFIX);

                CollectChildComponents(_screens, SCREEN_PREFIX);
                //if (!Info.IsMaster)
                    //CollectDeepComponents(_mounts, MOUNT_PREFIX, false, false);
            }

            Info.IsScreenMaster = _screens.Count > 0;
            // if (_isScreenMaster)
            // {
            //
            // }

            //            CollectChildComponents(_mounts, MOUNT_PREFIX);
            //            if (_isScreen)
            //                CollectDeepComponents(_managers, MANAGER_PREFIX);
            //            else if (!_isScreen)
            //                CollectChildComponents(_managers, MANAGER_PREFIX);
            //            
            //CollectDeepComponents(_managerPanels, PANEL_PREFIX);

            if (Info.ParentManager == null && !Info.IsMaster)
                Debug.LogError("No parent manager but not Master", this);

            if (!Info.IsPanel && (Info.ParentManager != null && Info.ParentManager.Info.IsScreen))
                Debug.LogError("_isPanel="+ Info.IsPanel+ ", _parentManager="+ Info.ParentManager.name+ ", _parentManager._isScreen="+ Info.ParentManager.Info.IsScreen, this);

            //if (_mounts.Count + _managers.Count > 0 && _screens.Count + _managerPanels.Count > 0)
            //    Debug.LogWarning("Manager is mixed!");

            if (Info.IsPanel) Info.Type = ManagerType.Panel;
            else if (Info.IsManager) Info.Type = ManagerType.Manager;
            else if (Info.IsScreen) Info.Type = ManagerType.Screen;
            else if (Info.IsScreenMaster) Info.Type = ManagerType.UIManager;
            else if (Info.IsMaster) Info.Type = ManagerType.Master;
            //else if (_isUIManager) _type = ManagerType.UIManager;
        }

        private void CollectChildComponents<T>(Dictionary<string, T> dictionary, string prefix) where T : Component
        {
            string itemName = "";
            foreach (Transform child in transform)
            {
#pragma warning disable CS0162                
                if (LOG_DEBUG)
                    Debug.LogWarning("child="+child.name, child);
#pragma warning restore CS0162                
                if (!child.name.Contains(prefix)) continue;
                var component = child.GetComponent<T>();
                if (component == null) continue;
                itemName = child.name.Replace(prefix, string.Empty);
                if (dictionary.ContainsKey(itemName))
                    Debug.LogError($"Component \""+typeof(T)+"\" {itemName} already indexed for object {name}!", component);
                else
                    dictionary.Add(child.name.Replace(prefix, string.Empty), component);
            }
        }


        public void InitializeMaster()
        {
#pragma warning disable 162
            if (LOG_INITIALIZE)
                Debug.Log("Manager.InitializeMaster: " + name, this);
#pragma warning restore 162
            
            CheckIsMasterSetParent();
            if (Info.IsMaster)
            {
                CollectComponentsBase();

                Info.IsInitialized = true;
            } else Debug.LogError(name+" is not a master", this);
        }

        public void EditorInitializeChilds()
        {
            InitializeChilds();
        }
        
        protected void InitializeChilds()
        {
            if (Info.IsMaster)
            {
                Info.ManagerCount = _managers.Count;
                foreach (var keyValue in _managers)
                {
                    //if (keyValue.Value.Info.IsInitialized) continue;
                    keyValue.Value.Info = new ManagerInfo();
                    keyValue.Value.Initialize();
                }
            }
            else Debug.LogError(name + " is not a master", this);
        }
        


        // protected void PostInitializeChilds()
        // {
        //     if (Info.IsMaster)
        //     {
        //         foreach (var keyValue in _managers)
        //         {
        //             if (keyValue.Value.Info.IsPostInitialized) continue;
        //             keyValue.Value.PostInitialize();
        //         }
        //     }
        //     else Debug.LogError(name + " is not a master", this);
        // }

       
        private void BaseInitialize()
        {
            //InitializeMaster();
            //if (_isMaster) return;// throw new UnityException("Master manager must not be initialized");
            CheckIsMasterSetParent();
            CollectComponentsBase();
            CollectUiElements();
            // init childs
            foreach (var keyValue in _managers)
            {
                if (keyValue.Value == this)
                {
                    Debug.LogError("self as child", this);
                    throw new UnityException("self as child");
                }
                if (keyValue.Value.Info.IsInitialized) continue;
                keyValue.Value.Info = new ManagerInfo();
                keyValue.Value.Initialize();
            }
            //foreach (var keyValue in _screens)
            //    keyValue.Value.Initialize();
            // foreach (var keyValue in _managerPanels)
            // {
            //     keyValue.Value.Initialize();
            // }

//            if (_isScreenMaster)
//                foreach (var keyValue in _screens)
//                    keyValue.Value.Hide();

            Info.PanelCount = _panels.Count;
            if (Application.isPlaying)
                Info.IsInitialized = true;
            
#pragma warning disable 162
            if (LOG_INITIALIZE)
                Debug.Log("Manager(tr.id="+transform.GetInstanceID()+").Initialized: " + name+", mounts="+_mounts.Count
                          +", isPlaying="+Application.isPlaying+", isInit="+Info.IsInitialized+"("+transform.GetComponent<Manager>().Info.IsInitialized+")"+", iId="+GetInstanceID(), transform);//+", panels="+_managerPanels.Count);
#pragma warning restore 162
        }

        public Manager InitializeAsPanel()
        {
            //BaseInitialize();
            //if (!_isPanel)
            {
                Info.IsPanel = true;
                CollectUiElements();
            }

            return this;
        }

        private void CollectUiElements(bool isForceCollect = false)
        {
            if (!Info.IsPanel && !isForceCollect) return;
            _panels.Clear();
            _images.Clear();
            _labels.Clear();
            _buttons.Clear();
            _toggles.Clear();
            CollectDeepComponents(_panels, PANEL_PREFIX);
            CollectDeepComponents(_images, IMAGE_PREFIX);
            CollectDeepComponents(_labels, LABEL_PREFIX);
            CollectDeepComponents(_buttons, BUTTON_PREFIX);
            CollectDeepComponents(_toggles, TOGGLE_PREFIX);
            LabelCount = _labels.Count;
        }

        public void CollectImages()
        {
            _images.Clear();
            CollectDeepComponents(_images, IMAGE_PREFIX);
        }

        private int CollectDeepComponents<T>(Dictionary<string, T> dictionary, string prefix, bool isNeedManager=false, bool logDoubled=true) where T : Component
        {
            int count = 0;
            var components = GetComponentsInChildren<T>(true)
                .Where(c => c.name.Contains(prefix) && c.gameObject != gameObject);
            foreach (var component in components)
            {
                if (component.gameObject == this.gameObject)
                    Debug.LogError("found self");
                if (isNeedManager && component.GetComponent<Manager>() == null)
                    continue;
                var componentName = component.name.Replace(prefix, string.Empty);
                if (dictionary.ContainsKey(componentName))
                {
                    if (logDoubled)
                    Debug.LogError(
                        "prefix=\"" + prefix + "\": " + name + $": Component \"" + typeof(T) +
                        $"\" {componentName} already indexed for object {name}!", component);
                }
                else
                {
                    dictionary.Add(componentName, component);
                    //Debug.Log("CollectDeepComponents add "+componentName+" "+component, component);
                    count++;
                }
            }

            return count;
            //Debug.Log("CollectDeepComponents: "+this.name+", type="+typeof(T)+", prefix="+prefix+", added "+count+" from "+dictionary.Count, this);
        }


        protected virtual void Initialize()
        {
            if (Info.IsInitialized)
                Debug.LogError("Initialize: IsInitialized already set", transform);
            //Assert.IsFalse(Info.IsInitialized);
            
#pragma warning disable 162
            if (LOG_INITIALIZE)
                Debug.Log("Manager("+this.GetInstanceID()+").Initialize: " + name, gameObject);
#pragma warning restore 162
            
            BaseInitialize();
        }

        // protected void AddManagerAndInitialize<T>(string panelName) where T : Manager
        // {
        //     Assert.IsFalse(string.IsNullOrEmpty(panelName));
        //     var panel = Panel(panelName);
        //     Assert.IsNotNull(panel);
        //     _managerPanels.Add(panelName, panel.AddComponent<T>());
        //     _managerPanels[panelName].Initialize();
        // }

        private void OnEnable() { if (Info.IsMaster) Show(); }

        private void OnDisable()
        {
            //Debug.LogError("OnDisable "+name, transform);
            if (Info.IsMaster) Hide();
        }

        public virtual void Show()
        {
            gameObject.SetActive(true);
        }
        public virtual void Hide()
        {
            Debug.Log("Hide: "+name,this);
            gameObject.SetActive(false);
        }

        public T GetManager<T>() where T : Manager
        {
            if (!Info.IsMaster)
                return GetMaster().GetManager<T>();
            
            var manager = _managers.Values.FirstOrDefault(m => m is T);
            if (manager != null) return (T)manager;
            string total = ""; foreach (var item in _managers) total += item.Key + ", ";
            Debug.LogError("Manager type("+typeof(T)+") \"" + manager + "\" not found (" + total + ") in "+name, gameObject);
            return default(T);
        }

        public T GetScreen<T>() where T : Manager
        {
            var manager = _screens.Values.FirstOrDefault(m => m is T);
            if (manager != null) return (T)manager;
            string total = ""; foreach (var item in _screens) total += item.Key + ", ";
            Debug.LogError("Manager \"" + manager + "\" not found (" + total + ") in " + name, gameObject);
            return default(T);
        }

        public T GetManager<T>(string manager) where T : Manager
        {
            if (_managers.ContainsKey(manager)) return (T)_managers[manager];
            string total = ""; foreach (var item in _managers) total += item.Key + ", ";
            Debug.LogError("Manager "+typeof(T)+" \"" + manager + "\" not found (" + total + ") (panels="+_panels.Count+") in " + name, gameObject);
            return default(T);
        }

        public Manager GetManager(string manager)
        {
            if (_managers.ContainsKey(manager)) return (Manager)_managers[manager];
            string total = ""; foreach (var item in _managers) total += item.Key + ", ";
            Debug.LogError("UIManager \"" + manager + "\" not found (" + total + ")");
            return default(Manager);
        }

        public Transform GetMount(string mount)
        {
            if (!Application.isPlaying) return transform.FindDeepChild("Mount - " + mount);
            if (!Info.GetMounts) GetMounts();
            if (_mounts.ContainsKey(mount)) return (Transform)_mounts[mount];
            string total = ""; foreach (var item in _mounts) total += item.Key + ", ";
            Debug.LogError("Transform \"" + MOUNT_PREFIX + mount + "\" not found (" + total + ")", this);
            return default(Transform);
        }
        
        public T GetMount<T>(string mount) where T : Component
        {
            if (!Application.isPlaying) return transform.FindDeepChild("Mount - " + mount).GetComponent<T>();
            if (!Info.GetMounts) GetMounts();
            if (_mounts.ContainsKey(mount)) return _mounts[mount].GetComponent<T>();
            string total = ""; foreach (var item in _mounts) total += item.Key + ", ";
            Debug.LogError("Transform \"" + MOUNT_PREFIX + mount + "\" not found (" + total + ")", this);
            return default(T);
        }

        public Button Button(string button)
        {
            if (_buttons.ContainsKey(button)) return _buttons[button];
            string total = ""; foreach (var item in _buttons) total += item.Key + ", ";
            Debug.LogError("Transform \"" + BUTTON_PREFIX + button + "\" not found (" + total + ")", this);
            return default(Button);
        }

        public Toggle Toggle(string componentName)
        {
            if (_toggles.ContainsKey(componentName)) return _toggles[componentName];
            string total = ""; foreach (var item in _toggles) total += item.Key + ", ";
            Debug.LogError("Transform \"" + TOGGLE_PREFIX + componentName + "\" not found (" + total + ")", this);
            return default(Toggle);
        }

        public T Panel<T>(string panel)
        {
            if (_panels.ContainsKey(panel)) return (_panels[panel]).GetComponent<T>();
            string total = ""; foreach (var item in _panels) total += item.Key + ", ";
            Debug.LogError("Panel type "+typeof(T)+" \"" + panel + "\" not found (" + total + ") in "+name, gameObject);
            return default(T);
        }

        public GameObject Panel(string panel)
        {
            if (_panels.ContainsKey(panel)) return _panels[panel].gameObject;
            string total = ""; foreach (var item in _panels) total += item.Key + ", ";
            Debug.LogError("Transform \"" + panel + "\" not found ("+total+") in "+name, this);
            return gameObject;
        }

        public Image Image(string image)
        {
            if (_images.ContainsKey(image)) return _images[image];
            string total = ""; foreach (var item in _images) total += item.Key + ", ";
            Debug.LogError("Image \"" + image + "\" not found (" + total + ") in " + name, this);
            return null;
        }

        //public Text Label(string label)
        //{
        //    if (_labels.ContainsKey(label)) return _labels[label];
        //    string total = ""; foreach (var item in _images) total += item.Key + ", ";
        //    Debug.LogError("Label \"" + label + "\" not found (" + total + ") in " + name, this);
        //    return null;
        //}

        public TextMeshProUGUI Label(string label)
        {
            if (_labels.ContainsKey(label)) return _labels[label];
            string total = ""; foreach (var item in _images) total += item.Key + ", ";
            Debug.LogError("Label \"" + label + "\" not found (" + total + ") in " + name, this);
            return null;
        }



        //        public UIManager GetUIManager(string manager)
        //        {
        //            if (_managers.ContainsKey(manager)) return (UIManager)_managers[manager];
        //            string total = ""; foreach (var item in _managers) total += item.Key + ", ";
        //            Debug.LogError("UIManager \"" + manager + "\" not found (" + total + ")");
        //            return default(UIManager);
        //        }

        //        private void UIAddObjects<T>(Transform targetTransform, ref Dictionary<string, object> dic, string prefix)
        //        {
        //            foreach (Transform child in targetTransform)
        //            {
        //                var component = child.GetComponent<T>();
        //                if (component != null && child.name.Contains(prefix))
        //                {
        //                    var index = child.name.IndexOf(prefix) + prefix.Length;
        //                    var key = child.name.Substring(index, child.name.Length - index);
        //                    if (dic.ContainsKey(key)) Debug.LogError(child.name+" already in list for manager "+name, child);
        //                    else dic.Add(key, (T)component);
        //                    continue;
        //                }
        //                if (null != child.GetComponent<Manager>()) continue;
        //                UIAddObjects<T>(child, ref dic, prefix);
        //            }
        //        }
    }
}