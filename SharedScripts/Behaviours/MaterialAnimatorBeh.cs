using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Shared
{
    [ExecuteInEditMode]
    public class MaterialAnimatorBeh : MonoBehaviour
    {
        public bool isSpriteAnimation = false;
        public bool fixOneRandom = false;
        public int TilesX = 6;
        public int TilesY = 6;
        public int FPS = 60;


        [SerializeField] private int _debugIndex;
        [SerializeField] private Material material;
        private Vector2 size;
        public string textureInShaderName = "_MainTex";

        public float AnimateOffsetYSpeed = 0f;

        private void OnEnable()
        {
            size = new Vector2(1f / TilesX, 1f / TilesY);
            var renderer = GetComponentInChildren<Renderer>();
            if (renderer == null) return;
                
            material = renderer.sharedMaterial;
            
            if (fixOneRandom && isSpriteAnimation)
            {
                SetSpriteAnimation();
            }

            MainFind.events.fixedUpdate += OnUpdate;
        }

        private void OnUpdate()
        {
            if (fixOneRandom) return;
            if (material == null) return;
            
            if (isSpriteAnimation)
                SetSpriteAnimation();

            if (AnimateOffsetYSpeed != 0)
            {
                var offset = material.GetTextureOffset(textureInShaderName);
                offset.y += AnimateOffsetYSpeed * Time.fixedDeltaTime;
                material.SetTextureOffset(textureInShaderName, offset);
            }
        }
        
        void SetSpriteAnimation()
        {
            var animationStartTime = 0f;
            
            int index = (int)((Time.time - animationStartTime) * FPS);
            if (fixOneRandom) index = (int) (Random.value * TilesX * TilesY);
            index = index % (TilesX*TilesY);

            _debugIndex = index;
            // if (!IsLoop && index < previousIndex)
            // {
            //     canUpdate = false;
            //     return;
            // }
            //
            // if (IsInterpolateFrames && index != previousIndex)
            // {
            //     currentInterpolatedTime = 0;
            // }
            // previousIndex = index;
            //
            // if (IsReverse)
            //     index = totalFrames - index - 1;

            var uIndex = index % TilesX;
            var vIndex = index / TilesX;

            float offsetX = uIndex * size.x;
            float offsetY = (1.0f - size.y) - vIndex * size.y;
            var offset = new Vector2(offsetX, offsetY);

            if (material != null)
            {
                {
                    material.SetTextureScale(textureInShaderName, size);
                    material.SetTextureOffset(textureInShaderName, offset);

                    var block = new MaterialPropertyBlock();
                    block.SetFloat("_Dx", offsetX);
                    block.SetFloat("_Dy", offsetY);
                    GetComponent<Renderer>().SetPropertyBlock(block);
                    
                    //material.SetFloat("_Dx", offsetX);
                    //material.SetFloat("_Dy", offsetY);
                }
            }
        }
    }
}