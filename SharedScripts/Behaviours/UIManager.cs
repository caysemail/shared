﻿//
//using System.Collections.Generic;
//
//using UnityEngine;
//using UnityEngine.Assertions;
//using UnityEngine.UI;
//
//namespace Shared
//{
//    public class UIManager : Manager
//    {
//        private const string LABEL_PREFIX = "Label - ";
//        private const string BUTTON_PREFIX = "Button - ";
//        private const string IMAGE_PREFIX = "Image - ";
//        private const string PANEL_PREFIX = "Panel - ";
//
//        private bool _isInited = false;
//
//        public static UIManager InstantiateInitializeShow(Transform panel, Transform parent)
//        {
//            Assert.IsNotNull(panel);
//            Assert.IsNotNull(parent);
//            panel.gameObject.SetActive(false);
//            var t = Instantiate(panel, parent, false);
//            var ui = t.gameObject.AddComponent<UIManager>();
//            ui.Initialize();
//            ui.Show();
//            return ui;
//        }
//
////        protected override void Awake()
////        {
////            base.Awake();
////            if (!_isInited) Initialize();
////        }
//
//        protected override void Initialize()
//        {
//            base.Initialize();
//
//            if (!_isInited)
//            {
//                UIAddObjects<Text>(transform, ref _labels, LABEL_PREFIX);
//                UIAddObjects<Button>(transform, ref _buttons, BUTTON_PREFIX);
//                UIAddObjects<Transform>(transform, ref _panels, PANEL_PREFIX);
//                UIAddObjects<Image>(transform, ref _images, IMAGE_PREFIX);
//                _isInited = true;
//            }
//            Debug.Log("UIManager("+name+").Initialize: labels=" + _labels.Count+", buttons="+_buttons.Count+", panels="+_panels.Count);
//        }
//
//        protected override void PostInitialize()
//        {
//            base.PostInitialize();
//        }
//
//
//        public Text Label(string label)
//        {
//            if (_labels.ContainsKey(label)) return (Text)_labels[label];
//            Debug.LogError("Label \"" + label + "\" not found");
//            return null;
//        }
//
//        public Button Button(string button)
//        {
//            if (_buttons.ContainsKey(button)) return (Button)_buttons[button];
//            Debug.LogError("Button \"" + button + "\" not found in "+name, this);
//            return null;
//        }
//
//        public Transform Panel(string panel)
//        {
//            if (_panels.ContainsKey(panel)) return (Transform)_panels[panel];
//            string total = ""; foreach (var item in _panels) total += item.Key + ", ";
//            Debug.LogError("Panel \"" + panel + "\" not found ("+total+")", this);
//            return null;
//        }
//
//        public T Panel<T>(string panel)
//        {
//            if (_panels.ContainsKey(panel)) return ((Transform)_panels[panel]).GetComponent<T>();
//            string total = ""; foreach (var item in _panels) total += item.Key + ", ";
//            Debug.LogError("Manager \"" + panel + "\" not found (" + total + ")");
//            return default(T);
//        }
//
//        public Image Image(string image)
//        {
//            if (_images.ContainsKey(image)) return (Image)_images[image];
//            string total = ""; foreach (var item in _images) total += item.Key + ", ";
//            Debug.LogError("Image \"" + image + "\" not found (" + total + "), inited="+_isInited, this);
//            return null;
//        }
//
//        private void UIAddObjects<T>(Transform targetTransform, ref Dictionary<string, object> dic, string prefix)
//        {
//            foreach (Transform child in targetTransform)
//            {
//                var component = child.GetComponent<T>();
//                if (component != null && child.name.Contains(prefix))
//                {
//                    var key = child.name.Replace(prefix, "");
//                    if (dic.ContainsKey(key)) Debug.LogError(child.name + " already in list for manager " + name, child);
//                    else dic.Add(key, (T)component);
//                }
//                if (child.GetComponent<UIManager>() != null) continue;
//                UIAddObjects<T>(child, ref dic, prefix);
//            }
//        }
//
//
//    }
//}