using System;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public class SchedulerManager : Manager
    {
        private static SchedulerManager _instance;

        private List<Action<TaskInfo>> _tasks;
        
        [Range(0.005f, 1f)]
        [SerializeField] private float _allowedTime = 0.02f; 
        [SerializeField] [ReadOnly] private int _taskCount; 
        [SerializeField] [ReadOnly] private int _updateCount; 
        [SerializeField] [ReadOnly] private float _fixedTime; 
        [SerializeField] [ReadOnly] private float _totalFixedTime; 
        
        public class TaskInfo
        {
            public bool isCompleted;
            public float allowedTimeSec = 0.020f;
        }
        
        protected override void Initialize()
        {
            base.Initialize();

            _instance = this;
            _tasks = new List<Action<TaskInfo>>();
            _updateCount = 0;
            _fixedTime = 0;
            _totalFixedTime = 0;

            MainFind.events.fixedUpdate -= OnFixedUpdate;
            MainFind.events.fixedUpdate += OnFixedUpdate;
        }

        public static void AddTask(Action<TaskInfo> action)
        {
            //Debug.LogError("SchedulerManager.AddTask: ");
            _instance._tasks.Add(action);
            _instance._taskCount = _instance._tasks.Count;
        }

        private void OnFixedUpdate()
        {
            if (_tasks.Count > 0)
            {
                Shared.Timer timer = new Timer();
                for (var index = _tasks.Count - 1; index >= 0; index--)
                {
                    var task = _tasks[index];
                    var info = new TaskInfo() { allowedTimeSec = _allowedTime };
                    try
                    {
                        task.Invoke(info);
                    }
                    catch (Exception e)
                    {
                        MainFind.events.fixedUpdate -= OnFixedUpdate;
                        Debug.LogException(e);
                    }
                    if (info.isCompleted)
                    {
                        _tasks.RemoveAt(index);
                        _instance._taskCount = _instance._tasks.Count;
                    }
                }

                _fixedTime = timer.Stop();
                _totalFixedTime += _fixedTime;
                _updateCount++;
            }
        }
    }
}