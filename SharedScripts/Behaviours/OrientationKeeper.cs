﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    [RequireComponent(typeof(RectTransform))]
    public class OrientationKeeper : MonoBehaviour
    {
        public enum Orientation { Portrait, Landscape }
        [SerializeField]private Orientation Keep = default;
        private void OnRectTransformDimensionsChange()
        {
            switch (Screen.orientation)
            {
                case ScreenOrientation.PortraitUpsideDown:
                case ScreenOrientation.Portrait:
                    if (Keep == Orientation.Landscape)
                        Screen.orientation = ScreenOrientation.LandscapeLeft;
                    break;
                case ScreenOrientation.LandscapeLeft:
                case ScreenOrientation.LandscapeRight:
                    if (Keep == Orientation.Portrait)
                        Screen.orientation = ScreenOrientation.Portrait;
                    break;
                default:
                    Debug.LogError("ScreenOrientation=" + Screen.orientation+ ", sizeDelta=" + ((RectTransform)this.transform).sizeDelta);
                    break;
            }
        }
    }
}