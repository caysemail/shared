﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraOrbit : MonoBehaviour
{

    private float distance = 10.0f;
    private float xSpeed = 5.0f;
    private float ySpeed = 20.0f;

    private float yMinLimit = -80f;
    private float yMaxLimit = 80f;

    private float distanceMin = .5f;
    private float distanceMax = 11f;

    //private Rigidbody rigidbody;

    private float x = 0.0f;
    private float y = 0.0f;

    [NonSerialized]
    public Transform Target;
    [NonSerialized]
    public Camera Camera;

    [SerializeField] private Vector3 angles;

    public void Rotate(Vector2 shift)
    {
        x += shift.x * xSpeed * distance * 0.01f;
        y -= shift.y * ySpeed * 0.01f;

        y = ClampAngle(y, yMinLimit, yMaxLimit);

        Quaternion rotation = Quaternion.Euler(y, x, 0);

        //distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);

//        RaycastHit hit;
//        if (Physics.Linecast(Target.position, Camera.transform.position, out hit))
//        {
//            distance -= hit.distance;
//        }
        Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
        Vector3 position = rotation * negDistance + Target.position;

        Camera.transform.rotation = rotation;
        Camera.transform.position = position;
        angles = Camera.transform.localEulerAngles;
    }

    public void Zoom(float zoom)
    {
        Quaternion rotation = Quaternion.Euler(y, x, 0);
        distance = Mathf.Clamp(distanceMax-zoom, distanceMin, distanceMax);

//        RaycastHit hit;
//        if (Physics.Linecast(Target.position, Camera.transform.position, out hit))
//        {
//            distance -= hit.distance;
//        }
        Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
        Vector3 position = rotation * negDistance + Target.position;

        //Camera.transform.rotation = rotation;
        Camera.transform.position = position;
    }

    void Start2()
    {
        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;

//        rigidbody = GetComponent<Rigidbody>();
//
//        // Make the rigid body not change rotation
//        if (rigidbody != null)
//        {
//            rigidbody.freezeRotation = true;
//        }
    }

    void LateUpdate2()
    {
        if (Target)
        {
            x += Input.GetAxis("Mouse X") * xSpeed * distance * 0.02f;
            y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;

            y = ClampAngle(y, yMinLimit, yMaxLimit);

            Quaternion rotation = Quaternion.Euler(y, x, 0);

            distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);

            RaycastHit hit;
            if (Physics.Linecast(Target.position, Camera.transform.position, out hit))
            {
                distance -= hit.distance;
            }
            Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
            Vector3 position = rotation * negDistance + Target.position;

            Camera.transform.rotation = rotation;
            Camera.transform.position = position;
        }
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }
}

