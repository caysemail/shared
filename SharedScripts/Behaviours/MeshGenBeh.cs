using System;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    [ExecuteAlways]
    [RequireComponent(typeof(MeshFilter))]
    public class MeshGenBeh : MonoBehaviour
    {
        public Vector3 PositionShift = Vector3.zero;
        public float Height = 0.5f;

        private void OnEnable()
        {
            GenPyramid();
        }

        [Button]
        public void GenPyramid()
        {
            var mf = GetComponent<MeshFilter>();
            Mesh mesh = new Mesh();
            if (Application.isPlaying) 
                mf.mesh = mesh;
            else mf.sharedMesh = mesh;
                
            MeshSection section = new MeshSection();
            section.Vertices = new List<Vector3>()
            {
                new Vector3(0, 0, 0),
                new Vector3(0, 1, 0),
                new Vector3(1, 1, 0),
                new Vector3(1, 0, 0),
                new Vector3(0.5f, 0.5f, -Height),
            };
            for (var index = 0; index < section.Vertices.Count; index++)
                section.Vertices[index] += new Vector3(-0.5f,-0.5f,0f) +  PositionShift;

            section.Indices = new List<int>()
            {
                0,1,4, 1,2,4, 2,3,4, 3,0,4
            };
            section.Uv0 = new List<Vector2>()
            {
                new Vector2(0, 0),
                new Vector2(1, 0),
                new Vector2(1, 1),
                new Vector2(0, 1),
                new Vector2(0.5f, 0.5f),
            };
            section.Apply(mesh, new MeshSection.ApplyMask(){ clear = true, log = true, vertices = true, indices = true, uv0 = true, normalsAndTangents = true});
        }
    }
}