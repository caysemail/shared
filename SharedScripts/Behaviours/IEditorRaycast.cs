using UnityEngine;

namespace Shared
{
    public interface IEditorRaycast
    {
        public void OnEditorRaycast(Ray ray);
    }
    public interface IEditorMouseMove
    {
        public void OnEditorMouseMove(Ray ray);
    }
    public interface IEditorSceneGui
    {
        public void OnEditorSceneGui();
    }
}