using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Shared
{
    [InitializeOnLoad]
    public class PreventChildsSerializationBeh : MonoBehaviour
    {
        public PreventChildsSerializationBeh()
        {
            EditorSceneManager.sceneSaving -= EditorSceneManagerOnsceneSaving;
            EditorSceneManager.sceneSaving += EditorSceneManagerOnsceneSaving;
        }

        private void EditorSceneManagerOnsceneSaving(Scene scene, string path)
        {
            if (this != null && gameObject != null)
            {
                transform.DestroyChildren();
            }
        }
    }
}