using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Shared
{
    public interface IOnSaveScene
    {
        void OnSaveScene();
    }
    
#if UNITY_EDITOR
    [UnityEditor.InitializeOnLoad]
#endif
    public class PreventSerializationBeh : MonoBehaviour
    {
#if UNITY_EDITOR
        public PreventSerializationBeh()
        {
            //Debug.LogError("PreventSerializationBeh: constructor");
            UnityEditor.SceneManagement.EditorSceneManager.sceneSaving -= EditorSceneManagerOnsceneSaving;
            UnityEditor.SceneManagement.EditorSceneManager.sceneSaving += EditorSceneManagerOnsceneSaving;
        }
#endif
        
        private void EditorSceneManagerOnsceneSaving(Scene scene, string path)
        {
            //bool meshToNull = false;
            if (this != null && gameObject != null)
            {
                var mf = gameObject.GetComponent<MeshFilter>();
                if (mf != null)
                {
                    mf.sharedMesh = null;
                    mf.mesh = null;
                    //meshToNull = true;
                }

                var mc = gameObject.GetComponent<MeshCollider>();
                if (mc != null)
                {
                    mc.sharedMesh = null;
                }


                var componentsArray = GetComponents<IOnSaveScene>();
                if (componentsArray != null && componentsArray.Length > 0)
                {
                    componentsArray.ToList().ForEach(t => t.OnSaveScene());
                }

            }

            //Debug.LogError("EditorSceneManagerOnsceneSaving: path="+path+", meshToNull="+meshToNull);

        }
    }
}
