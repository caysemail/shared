﻿#if Newtonsoft

using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

public class Vector2IntConverter : JsonConverter
{
    private static readonly System.Type Vector2IntType = typeof (Vector2Int);
    
    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        var item = (Vector2Int)value;
        writer.WriteRawValue("["+item.x+", "+item.y+"]");
    }

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        var array = serializer.Deserialize<List<int>>(reader);
        return new Vector2Int(array[0], array[1]);
    }

    public override bool CanConvert(Type objectType)
    {
        return objectType == Vector2IntType;
    }
}

#endif