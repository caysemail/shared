﻿#if Newtonsoft

using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using UnityEngine;

namespace Shared
{
    public class ColorConverter : JsonConverter
    {
        private static readonly System.Type Type = typeof(Color);

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var item = (Color) value;
            writer.WriteRawValue(
                string.Format(CultureInfo.InvariantCulture, "[{0:0.00}, {1:0.00}, {2:0.00}, {3:0.00}]", 
                    item.r, item.g, item.b, item.a));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            var array = serializer.Deserialize<List<float>>(reader);
            return new Color(array[0], array[1], array[2], array[3]);
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == Type;
        }
    }
}

#endif