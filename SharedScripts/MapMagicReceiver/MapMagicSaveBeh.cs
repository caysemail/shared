using UnityEngine;

namespace Shared
{
    public class MapMagicSaveBeh : MonoBehaviour
    {
        [ReadOnly] public int WorldSize = default;
        [ReadOnly] public int Resolution = default;
        //[SerializeField] public float[,] HeightArray = default;
        [HideInInspector]
        [SerializeField] private float[] _heightArray = default;
        [SerializeField] public bool IsInited = false;
        [SerializeField] public bool ForceUpdate = false;
        [SerializeField] public bool IsDisableTerrain = false;

        public void Set(float[,] heightArray)
        {
            _heightArray = new float[heightArray.Length];
            var size = heightArray.GetLength(0);
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    _heightArray[x + y * size] = heightArray[y, x];
                }
            }

            if (!ForceUpdate)
                IsInited = true;
            
            DisableTerrain();
        }

        public float[,] Get()
        {
            var size = (int)Mathf.Sqrt(_heightArray.Length);
            float[,] heightArray = new float[size,size];
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    heightArray[y, x] = _heightArray[x + y * size];
                }
            }

            DisableTerrain();
            
            return heightArray;
        }

        public void DisableTerrain()
        {
            if (IsDisableTerrain)
            {
                var terrain = transform.GetComponentInChildren<Terrain>(true);
                if (terrain == null) Debug.LogError("DisableTerrain: terrain not found");    
                else terrain.gameObject.SetActive(false);
            }
        }

        [Button]
        private void Reset()
        {
            WorldSize = default;
            Resolution = default;
            IsInited = false;
            _heightArray = default;
        }
    }
}