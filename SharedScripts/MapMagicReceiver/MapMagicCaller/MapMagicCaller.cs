using System;
using Shared;
using UnityEngine;
using UnityEngine.Assertions;

#if MapMagic

using Den.Tools;
using MapMagic.Core;
using MapMagic.Nodes;

namespace MapMagicCaller
{
    public static class MapMagicCaller
    {
        public static void Refresh2(Action callback = null, int tileSize=-1, int tileResolution=-1, string graphName = null)
        {
            MapMagicReceiver.SaveBeh = default;
            MapMagicReceiver.OnUpdate = Callback;

            if (SceneExt.TryGetActiveComponentInAllScenes<MapMagicSaveBeh>(out MapMagicReceiver.SaveBeh))
            {
                if (MapMagicReceiver.SaveBeh.IsInited)
                {
                    MapMagicReceiver.HeightArray = MapMagicReceiver.SaveBeh.Get();
                    MapMagicReceiver.WorldSize = MapMagicReceiver.SaveBeh.WorldSize;
                    MapMagicReceiver.Resolution = MapMagicReceiver.SaveBeh.Resolution;
                    callback?.Invoke();
                    Debug.Log("MapMagicCaller.Refresh: load from MapMagicReceiver.SaveBeh");
                    return;
                }

                if (!MapMagicReceiver.SaveBeh.IsInited)
                {
                    var mapMagic = MapMagicReceiver.SaveBeh.GetComponent<MapMagicObject>();
                    if (mapMagic == null)
                        throw new UnityException("Not found MapMagicObject in MapMagicNoTerrain");

                    if (!string.IsNullOrEmpty(graphName))
                    {
                        var path = "Graphs\\" + graphName;
                        var graph = Resources.Load<Graph>(path);
                        if (graph == null)
                            throw new UnityException("Cannot load graph resource at path=" + path);
                        mapMagic.graph = graph;
                    }

                    if (tileSize != -1)
                        mapMagic.tileSize = new Vector2D(tileSize, tileSize);
                    if (tileResolution != -1)
                        mapMagic.tileResolution = (MapMagicObject.Resolution)tileResolution;

                    Assert.IsTrue(mapMagic.tileResolution > 0, "mapMagic.tileResolution=" + mapMagic.tileResolution);
                    MapMagicReceiver.WorldSize = (int)mapMagic.tileSize.x;
                    MapMagicReceiver.Resolution = (int)mapMagic.tileResolution;
                    Debug.Log("  MapMagicCaller.Refresh: call mapMagic.Refresh, WorldSize=" + MapMagicReceiver.WorldSize + ", Resolution=" + MapMagicReceiver.Resolution+", tileSize="+tileSize+", tileResolution="+tileResolution+", graphName="+graphName);
                    mapMagic.Refresh(true);
                    mapMagic.Update();
                    //Debug.LogError("Refresh completed");
                }
            }
            else Debug.LogError("Not found MapMagicSaveBeh in all scenes");
            
            return;

            void Callback()
            {
                Debug.Log("  MapMagicCaller.Refresh: Callback");
                if (MapMagicReceiver.SaveBeh != default)
                {
                    MapMagicReceiver.SaveBeh.WorldSize = MapMagicReceiver.WorldSize;
                    MapMagicReceiver.SaveBeh.Resolution = MapMagicReceiver.Resolution;
                    MapMagicReceiver.SaveBeh.Set(MapMagicReceiver.HeightArray);
                }
                callback?.Invoke();
                
                MapMagicReceiver.SaveBeh.DisableTerrain();
            }
        }
    }
}

#endif