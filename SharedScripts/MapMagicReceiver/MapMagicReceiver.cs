using System;
using UnityEngine;

namespace Shared
{
    public static class MapMagicReceiver
    {
        [NonSerialized] public static int WorldSize = default;
        [NonSerialized] public static int Resolution = default;
        [NonSerialized] public static float[,] HeightArray = default;
        [NonSerialized] public static Action OnUpdate;
        [NonSerialized] public static MapMagicSaveBeh SaveBeh;

        // Set Split Frame same as Main Resolution (257)
        // use is MapMagic, class HeightOutput200, class ApplySplitData, ApplyRoutine(307) if (terrain == null) { Shared.MapMagicReceiver.SetArray(heights2DSplits[0]); yield break; } 
        public static void SetArray(float[,] heightArray)
        {
            HeightArray = heightArray;
            Debug.LogWarning("MapMagicReceiver.SetArray: y=" + heightArray.GetLength(0) + ", x=" + heightArray.GetLength(1));
            OnUpdate?.Invoke();
        }
    }
}