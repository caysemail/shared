﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace Shared
{
    //[CustomNodeGraphEditor(typeof(XNode.NodeGraph))]
    [CustomEditor(typeof(CustomNodeGraph))]
    public class NodeGraphEditor : Editor
    {
    }
}

#endif