﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

namespace Shared
{
    [InitializeOnLoad]
    public class NodeEditorWindow : EditorWindow
    {
        public static NodeEditorWindow Current;

        [OnOpenAsset(0)]
        public static bool OnOpen(int instanceID, int line)
        {
            CustomNodeGraph customNodeGraph = EditorUtility.InstanceIDToObject(instanceID) as CustomNodeGraph;
            if (customNodeGraph != null)
            {
                Open(customNodeGraph);
                return true;
            }
            return false;
        }

        private void OnDisable()
        {
            Debug.Log("NodeEditorWindow.OnDisable: ");

        }

        private void OnEnable()
        {
            Debug.Log("NodeEditorWindow.OnEnable: ");
        }

        private void OnFocus()
        {
            Debug.Log("NodeEditorWindow.OnFocus: ");
        }

        [InitializeOnLoadMethod]
        private static void OnLoad()
        {
            Selection.selectionChanged -= OnSelectionChanged;
            Selection.selectionChanged += OnSelectionChanged;
        }

        private static void OnSelectionChanged()
        {
            CustomNodeGraph customNodeGraph = Selection.activeObject as CustomNodeGraph;
            if (customNodeGraph && !AssetDatabase.Contains(customNodeGraph))
            {
                Open(customNodeGraph);
            }
        }

        public static void Open(CustomNodeGraph graph)
        {
            if (!graph) return;
            Debug.Log("NodeEditorWindow.Open: ");
            NodeEditorWindow w = GetWindow(typeof(NodeEditorWindow), false, "NodeEditor", true) as NodeEditorWindow;
            w.wantsMouseMove = true;
            //w.graph = graph;
        }
    }
}

#endif