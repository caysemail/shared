﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Shared
{
    public class NodeDataCache
    {
        private static readonly Type BASE_TYPE = typeof(Manager);

        public static void Update()
        {
            // Debug.LogWarning("NodeDataCache.Update: ");
            // try
            // {
            //     BuildCache();
            // }
            // catch (Exception e)
            // {
            //     Console.WriteLine(e);
            //     throw;
            // }
        }

        private static void BuildCache()
        {
            //portDataCache = new PortDataCache();
            //System.Type baseType = typeof(Manager);
            List<System.Type> nodeTypes = new List<System.Type>();
            System.Reflection.Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
            Assembly selfAssembly = Assembly.GetAssembly(BASE_TYPE);
            if (selfAssembly.FullName.StartsWith("Assembly-CSharp") && !selfAssembly.FullName.Contains("-firstpass"))
            {
                // If xNode is not used as a DLL, check only CSharp (fast)
                nodeTypes.AddRange(selfAssembly.GetTypes().Where(t => !t.IsAbstract && BASE_TYPE.IsAssignableFrom(t)));
            }

//        else
//        {
//            // Else, check all relevant DDLs (slower)
//            // ignore all unity related assemblies
//            foreach (Assembly assembly in assemblies)
//            {
//                if (assembly.FullName.StartsWith("Unity")) continue;
//                // unity created assemblies always have version 0.0.0
//                if (!assembly.FullName.Contains("Version=0.0.0")) continue;
//                nodeTypes.AddRange(assembly.GetTypes().Where(t => !t.IsAbstract && baseType.IsAssignableFrom(t)).ToArray());
//            }
//        }
            for (int i = 0; i < nodeTypes.Count; i++)
            {
                CachePorts(nodeTypes[i]);
            }
        }

        private static void CachePorts(System.Type nodeType)
        {
            //return;
            //Debug.LogWarning("NodeDataCache.CachePorts: " + nodeType);
            List<System.Reflection.FieldInfo> fieldInfo = GetNodeFields(nodeType);
            //Debug.LogWarning("NodeDataCache.CachePorts: fields=" + fieldInfo.Count);
            string info = "";
            for (int i = 0; i < fieldInfo.Count; i++)
            {
                //Get InputAttribute and OutputAttribute
                object[] attribs = fieldInfo[i].GetCustomAttributes(true);
                string attribsInfo = "\n"+ fieldInfo[i].Name+":";
                foreach (var attrib in attribs)
                {
                    attribsInfo += attrib.ToString() +", ";
                    //Debug.LogError("aInfo="+attribsInfo);
                }
//                Node.InputAttribute inputAttrib = attribs.FirstOrDefault(x => x is Node.InputAttribute) as Node.InputAttribute;
//                Node.OutputAttribute outputAttrib = attribs.FirstOrDefault(x => x is Node.OutputAttribute) as Node.OutputAttribute;
//
//                if (inputAttrib == null && outputAttrib == null) continue;
//
//                if (inputAttrib != null && outputAttrib != null) Debug.LogError("Field " + fieldInfo[i].Name + " of type " + nodeType.FullName + " cannot be both input and output.");
//                else
//                {
//                    if (!portDataCache.ContainsKey(nodeType)) portDataCache.Add(nodeType, new List<NodePort>());
//                    portDataCache[nodeType].Add(new NodePort(fieldInfo[i]));
//                }
                info += attribsInfo + ", ";
            }
            Debug.LogWarning("NodeDataCache.CachePorts: " + nodeType+info);
        }

        public static List<FieldInfo> GetNodeFields(System.Type nodeType)
        {
            List<System.Reflection.FieldInfo> fieldInfo = new List<System.Reflection.FieldInfo>(nodeType.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance));

            // GetFields doesnt return inherited private fields, so walk through base types and pick those up
            System.Type tempType = nodeType;
            var monobehType = typeof(MonoBehaviour);
            while ((tempType = tempType.BaseType) != BASE_TYPE && tempType != monobehType)
            {
                var fields = tempType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
                fieldInfo.AddRange(fields);
                //Debug.LogError("fieldInfo="+fieldInfo.Count);
            }
            return fieldInfo;
        }
    }
}