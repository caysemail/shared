﻿// Upgrade NOTE: replaced 'SeperateSpecular' with 'SeparateSpecular'

Shader "shared/vertexColored noLight" {
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_SpecColor("Spec Color", Color) = (1,1,1,1)
		_Emission("Emmisive Color", Color) = (0,0,0,0)
		_Shininess("Shininess", Range(0.01, 1)) = 0.7
		_MainTex("Base (RGB)", 2D) = "white" {}
	}

		SubShader{
			Pass {
				Material {
					Shininess[_Shininess]
					Specular[_SpecColor]
					Emission[_Emission]
				}
				//ColorMaterial AmbientAndDiffuse
				Lighting Off
				SeparateSpecular Off
				//Lighting On
				//SeparateSpecular On
//				SetTexture[_MainTex] {
//					Combine texture * primary, texture * primary
//				}
				SetTexture[_MainTex] {
					constantColor[_Color]
					Combine primary * constant, primary * constant
				}
			}
	}

	//Fallback " VertexLit", 1
}