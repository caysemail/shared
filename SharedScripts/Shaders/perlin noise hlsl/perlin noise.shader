Shader "shared/perlin noise"
{
    CGINCLUDE

    #pragma multi_compile CNOISE PNOISE SNOISE SNOISE_AGRAD SNOISE_NGRAD
    #pragma multi_compile _ THREED
    #pragma multi_compile _ FRACTAL

    #include "UnityCG.cginc"

    #define PNOISE

	#if defined(PNOISE)
		#include "ClassicNoise2D.hlsl"
    #elif !defined(CNOISE)
        #if defined(THREED)
            #include "SimplexNoise3D.hlsl"
        #else
            #include "SimplexNoise2D.hlsl"
        #endif
    #else
        #if defined(THREED)
            #include "ClassicNoise3D.hlsl"
        #else
            #include "ClassicNoise2D.hlsl"
        #endif
    #endif

//	half _UvMult;
	half _UvMultX;
	half _UvMultY;
	half _PositionX;
	half _PositionY;
	half _TotalMult;
	//half _Period;
	half _PeriodX;
	half _PeriodY;

    v2f_img vert(appdata_base v)
    {
        v2f_img o;
        o.pos = UnityObjectToClipPos(v.vertex);
        o.uv = v.texcoord.xy;
        return o;
    }

    float4 frag(v2f_img i) : SV_Target
    {
        const float epsilon = 0.0001;

        //float2 uv = float2(_PositionX, _PositionY) + i.uv * _UvMult;// * _Time.y;
        float2 uv = float2(_PositionX, _PositionY) + i.uv * float2(_UvMultX,_UvMultY);// * _Time.y;
        float o = 0.5;
        float2 coord = uv;// * s;
        float2 period = float2(_PeriodX,_PeriodY);// * s * 1.0;
		o += pnoise(coord, period) * _TotalMult;
        return float4(o, o, o, 1);
    }

    ENDCG
	Properties
	{
		//_UvMult ("UV multiplicator", Range(0,128)) = 4
		_UvMultX ("UV multiplicator", Range(0,128)) = 4
		_UvMultY ("UV multiplicator", Range(0,128)) = 4
		_PositionX ("Noise Postion X", Range(0,127)) = 0
		_PositionY ("Noise Postion Y", Range(0,127)) = 0
		_TotalMult ("Total multiplicator", Range(0,31)) = 0.5
		//_Period ("Noise Period", Range(0,128)) = 4		
		_PeriodX ("Noise Period X", Range(0,64)) = 4
		_PeriodY ("Noise Period Y", Range(0,64)) = 4
	}
    SubShader
    {
        Pass
        {
            CGPROGRAM
            #pragma target 3.0
            #pragma vertex vert
            #pragma fragment frag
            ENDCG
        }
    }
}
