Shader "shared/unlit tex transp color vcolor distortion"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color("Color", Color) = (1,1,1,1)
        _DistortionTex ("Texture", 2D) = "white" {}
        _DistortionStrength ("Distortion Strength", Float) = 1
        _Speed ("Speed", Float) = 1
        _SpeedU ("SpeedU", Float) = 1
        _SpeedV ("SpeedV", Float) = 1
        _BrightnessChange ("Brightness change 0 to 1", Range(0.0, 2.0)) = 0.0
    }
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" "IgnoreProjector"="True" }
        //LOD 100

        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha 
        
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
            };

            sampler2D _MainTex;
            sampler2D _DistortionTex;
            float4 _MainTex_ST;
            fixed4 _Color;
            float _DistortionStrength, _Speed, _SpeedU, _SpeedV;
            fixed _BrightnessChange;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.color = v.color;
                return o;
            }

            float2 FlowUV (float2 uv, float2 flowVector, float time)
            {
            	return uv - flowVector;// * time;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                float3 distortion = tex2D(_DistortionTex, i.uv+(_Time.y * _SpeedU, _Time.y*_SpeedV));
                float3 distSource = distortion;
                distortion.xy = distortion.xy * 2 - 1;
			    distortion *= _DistortionStrength;

                float2 uv = i.uv - distortion.xy;
                fixed4 col = tex2D(_MainTex, uv) * _Color * i.color;
                col = lerp(col, col*((distSource.x+distSource.y)*0.5+0.5), _BrightnessChange);
                
                return col;
            }
            ENDCG
        }
    }
}
