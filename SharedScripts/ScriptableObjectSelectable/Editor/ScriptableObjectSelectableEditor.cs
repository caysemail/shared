using UnityEditor;

namespace Shared
{
    [CustomEditor(typeof(ScriptableObjectSelectable), true)]
    public class ScriptableObjectSelectableEditor : Editor
    {
        private void OnEnable()
        {
            var targetConfig = (ScriptableObjectSelectable)target;
            targetConfig.OnSelect();
        }
    }
}
