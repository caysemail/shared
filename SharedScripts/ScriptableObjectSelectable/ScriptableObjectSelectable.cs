using UnityEngine;

namespace Shared
{
    public class ScriptableObjectSelectable : ScriptableObject
    {
        public virtual void OnSelect() {}
    }
}