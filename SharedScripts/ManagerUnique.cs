﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;

namespace Shared
{
    public enum Uniq
    {
        None = 0,
        Cards,
        Turn,
    }
    [Serializable]
    public class UniqItem
    {
        public int seedDefault;
        private int _id = 0;

        public int Seed => seedDefault + Id;

        public int Id
        {
            get
            {
                _id++;
                return _id;
            }
        }

        public int Percent => Mathf.RoundToInt(Value * 100f);

        public T Element<T>(List<T> array)
        {
            Assert.IsNotNull(array);
            Assert.IsTrue(array.Count > 0);
            return array[Range(0,array.Count-1)];
        }
        
        public int Index<T>(List<T> array)
        {
            Assert.IsNotNull(array);
            Assert.IsTrue(array.Count > 0);
            return Range(0,array.Count-1);
        }
        
        public float Value
        {
            get
            {
                Random.InitState(_id + seedDefault);
                var seed2 = Random.Range(int.MinValue, int.MaxValue);
                Random.InitState(seed2);
                _id++;
                return Random.value;
            }
        }

        public int Range(int minIncluded, int maxNotIncluded)
        {
            Random.InitState(_id+ seedDefault);
            var seed2 = Random.Range(int.MinValue, int.MaxValue);
            Random.InitState(seed2);
            _id++;
            return Random.Range(minIncluded, maxNotIncluded);
        }
    }
    
    public class ManagerUnique// : MonoBehaviour
    {
        private class UniqPair
        {
            public Uniq Uniq;
            [SerializeField]
            public UniqItem Item;
        }
       
        //[ReadOnly][SerializeField] 
        private List<UniqPair> _uniqPairs;
        
        public ManagerUnique Initialize(int seed)
        {
            _uniqPairs = new List<UniqPair>();
            return this;
        }

        public UniqItem Get(Uniq uniq)
        {
            int index = _uniqPairs.FindIndex(p => p.Uniq == uniq);
            if (index < 0)
            {
                _uniqPairs.Add(new UniqPair() {Uniq = uniq, Item = new UniqItem()});
                index = _uniqPairs.Count - 1;
            }

            return _uniqPairs[index].Item;
        }
        
    }
}