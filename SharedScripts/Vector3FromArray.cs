﻿#if Newtonsoft

using System;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Shared
{
    public class Vector3FromArray : JsonConverter
    {

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException("Not implemented yet");
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
            {
                return Vector3.zero;
            }

            if (reader.TokenType == JsonToken.String)
            {
                return serializer.Deserialize(reader, objectType);
            }

            if (reader.TokenType != JsonToken.StartArray)
                return serializer.Deserialize(reader, objectType);

            //Debug.LogError("BoolVector3FromArray.ReadJson: TokenType="+reader.TokenType+", reader="+reader.ToString());
            var obj = JArray.Load(reader);
            //JObject obj = JObject.Load(reader);
            if (obj.Count != 3)
            {
                Debug.LogError("Deserialize Vector3, item count="+obj.Count);
                return Vector3.zero;
            }
            Vector3 v3 = Vector3.zero;
            float f = 0;
            if (!float.TryParse((string) obj[0], NumberStyles.Float, CultureInfo.InvariantCulture, out f))
            {
                Debug.LogError("cannot parse x?="+obj[0]+", y="+obj[1]+", z="+obj[2]);
                return Vector3.zero;
            }
            v3.x = f;
            
            if (!float.TryParse((string) obj[1], NumberStyles.Float, CultureInfo.InvariantCulture, out f))
            {
                Debug.LogError("cannot parse x="+obj[0]+", y?="+obj[1]+", z="+obj[2]);
                return Vector3.zero;
            }
            v3.y = f;
            
            if (!float.TryParse((string) obj[2], NumberStyles.Float, CultureInfo.InvariantCulture, out f))
            {
                Debug.LogError("cannot parse x="+obj[0]+", y="+obj[1]+", z?="+obj[2]);
                return Vector3.zero;
            }
            v3.z = f;
            return v3;
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override bool CanConvert(Type objectType)
        {
            return false;
        }
    }
}

#endif