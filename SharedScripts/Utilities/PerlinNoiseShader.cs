using UnityEngine;

namespace Shared
{
    public class PerlinNoiseShader
    {
        public Texture2D texture2d;
        
        public PerlinNoiseShader()
        {
            
        }

        public void CreateRepeatingSmooth(int texWidth, int texHeight, float period)
        {
            var shader = Shader.Find("shared/perlin noise");
            var material = new Material(shader);
            material.hideFlags = HideFlags.DontSave;
            material.shaderKeywords = null;
            material.EnableKeyword("PNOISE");
            material.SetFloat("_PeriodX", period);
            material.SetFloat("_PeriodY", period);
            material.SetFloat("_UvMultX", period);
            material.SetFloat("_UvMultY", period);
            material.SetFloat("_TotalMult", 0.75f);
            // create objects
            texture2d = new Texture2D(texWidth, texHeight, TextureFormat.RGBA32, false);
            texture2d.filterMode = FilterMode.Point;
            texture2d.wrapMode = TextureWrapMode.Repeat;
            
            texture2d.UpdateTextureByMaterial(material);
            //
            // var renderTexture = new RenderTexture(texSize, texSize, 0);
            // // set state
            // var previousRT = RenderTexture.active;
            // RenderTexture.active = renderTexture;
            // // modify
            // Graphics.Blit(texture2d, renderTexture, material);
            // // копируем активную рендер текстуру в текстуру (в оперативную память)
            // texture2d.ReadPixels(new Rect(0, 0, texSize, texSize), 0, 0);
            // // завершаем копирование
            // texture2d.Apply();
            // // release state
            // RenderTexture.active = previousRT; 
            // renderTexture.Release();
            // // get data
            // //var pixels32 = texture2d.GetPixels32();
        }
    }
}