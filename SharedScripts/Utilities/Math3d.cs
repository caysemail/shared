using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Shared
{
    public static class Math3d
    {
        [Serializable]
        public struct Vector
        {
            public Vector3 position;
            public Vector3 normal;
        }
        
        /**
         * Determines the point of intersection between a plane defined by a point and a normal vector and a line defined by a point and a direction vector.
         *
         * @param planePoint    A point on the plane.
         * @param planeNormal   The normal vector of the plane.
         * @param linePoint     A point on the line.
         * @param lineDirection The direction vector of the line.
         * @return The point of intersection between the line and the plane, null if the line is parallel to the plane.
         */
        public static bool TryPlaneLineIntersection(Vector plane, Vector line, out Vector3 intersection)
        {
            intersection = Vector3.zero;
            
            var dotPlaneNormalToLineDirection = Vector3.Dot(plane.normal, line.normal);
            if (dotPlaneNormalToLineDirection == 0) 
                return false;
            
            float t = (Vector3.Dot(plane.normal, plane.position) - Vector3.Dot(plane.normal, line.position)) 
                        / dotPlaneNormalToLineDirection;
            intersection = line.position + line.normal * t;
            return true;
        }
        
        public static bool TryPlanePlaneIntersection(Vector plane0, Vector plane1, out Vector3 lineNormal, out Vector3 linePoint)
        {
            Vector3 intersectPoint = Vector3.zero;
            //We can get the direction of the line of intersection of the two planes by calculating the
            //cross product of the normals of the two planes. Note that this is just a direction and the line
            //is not fixed in space yet.
            var planesCross = Vector3.Cross(plane0.normal, plane1.normal);
            
            //Next is to calculate a point on the line to fix it's position. This is done by finding a vector from
            //the plane2 location, moving parallel to it's plane, and intersecting plane1. To prevent rounding
            //errors, this vector also has to be perpendicular to lineDirection. To get this vector, calculate
            //the cross product of the normal of plane2 and the lineDirection.      
            Vector3 direction = Vector3.Cross(plane1.normal, planesCross);
            
            float numerator = Vector3.Dot(plane0.normal, direction);

            lineNormal = planesCross;
            linePoint = Vector3.zero;

            //Prevent divide by zero.
            if(Mathf.Abs(numerator) > 0.000001f)
            {
                var pos0 = plane0.position;
                var pos1 = plane1.position;
                Vector3 plane0ToPlane1 = pos0 - pos1;
                float t = Vector3.Dot(plane0.normal, plane0ToPlane1) / numerator;
                intersectPoint = pos1 + t * direction;
                linePoint = intersectPoint;
                return true;
            }
            
            return false;
        }

        
        public static bool TryLineLineIntersection(out Vector3 intersection, Vector vector1, Vector vector2){

            Vector3 lineVec3 = vector2.position - vector1.position;
            Vector3 crossVec1and2 = Vector3.Cross(vector1.normal, vector2.normal);
            Vector3 crossVec3and2 = Vector3.Cross(lineVec3, vector2.normal);

            float planarFactor = Vector3.Dot(lineVec3, crossVec1and2);

            //is coplanar, and not parallel
            if( Mathf.Abs(planarFactor) < 0.0001f 
                && crossVec1and2.sqrMagnitude > 0.0001f)
            {
                float s = Vector3.Dot(crossVec3and2, crossVec1and2) 
                          / crossVec1and2.sqrMagnitude;
                intersection = vector1.position + (vector1.normal * s);
                return true;
            }
            else
            {
                intersection = Vector3.zero;
                return false;
            }
        }
    }
}
