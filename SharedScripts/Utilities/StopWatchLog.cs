﻿using System.Diagnostics;
using System.Globalization;

public class StopWatchLog
{
    public string Message;
    public Stopwatch Watch;
    public int Count;
    public string SavedName;
    public double TotalSeconds;

//    public StopWatchLog()
//    {
//        Watch = new Stopwatch();
//        Watch.Start();
//    }

    public StopWatchLog(string name = "")
    {
        SavedName = name;
        Watch = new Stopwatch();
        Watch.Start();
    }

    public void NewLog(string name = "")
    {
        Watch.Stop();
        TotalSeconds += Watch.Elapsed.TotalSeconds;
        if (!string.IsNullOrEmpty(SavedName))
        {
            if (Count > 0)
                Message += ", ";
            Message += SavedName + "" + ToString(Watch.Elapsed.TotalSeconds);
        }
        Count++;
        SavedName = name;
        Watch.Restart();
    }

//    public void NewLog()
//    {
//        Watch.Stop();
//        TotalSeconds += Watch.Elapsed.TotalSeconds;
//        if (!string.IsNullOrEmpty(SavedName))
//        {
//            if (Count > 0)
//                Message += ", ";
//            Message += SavedName + "" + ToString(Watch.Elapsed.TotalSeconds);
//        }
//        Count++;
//        SavedName = "";
//    }

    public void Log(string message)
    {
        Watch.Stop();
        if (Count > 0)
            Message += ", ";
        Message += message + "" + ToString(Watch.Elapsed.TotalSeconds);
        Count++;
        Watch.Restart();
    }

    public override string ToString()
    {
        return "Total=" + ToString(TotalSeconds)+(string.IsNullOrEmpty(Message)? "": ", "+Message);
    }

    private string ToString(double number)
    {
        return System.Math.Round(number, 3).ToString(CultureInfo.InvariantCulture) + " sec.";
    }
}
