﻿using UnityEngine;

namespace Shared
{
    public static class UIUtility
    {
        public static GameObject InstantiateAndShow(GameObject child)
        {
            var go = Object.Instantiate(child, child.transform.parent, false);
            go.Show();
            return go;
        }

    }
}