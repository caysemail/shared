using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Shared
{
    public static class Math2d
    {
        /// <summary>
        /// Gets the coordinates of the intersection point of two lines.
        /// </summary>
        /// <param name="A1">A point on the first line.</param>
        /// <param name="A2">Another point on the first line.</param>
        /// <param name="B1">A point on the second line.</param>
        /// <param name="B2">Another point on the second line.</param>
        /// <param name="found">Is set to false of there are no solution. true otherwise.</param>
        /// <param name="epsilon">float.Epsilon</param>
        /// <returns>The intersection point coordinates. Returns Vector2.zero if there is no solution.</returns>
        public static Vector2 GetIntersectionOfLines(Vector2 A1, Vector2 A2, Vector2 B1, Vector2 B2, out bool found,
            float epsilon = float.Epsilon)
        {
            float tmp = (B2.x - B1.x) * (A2.y - A1.y) - (B2.y - B1.y) * (A2.x - A1.x);

            if (Math.Abs(tmp) < epsilon)
            {
                // No solution!
                found = false;
                return Vector2.zero;
            }

            float mu = ((A1.x - B1.x) * (A2.y - A1.y) - (A1.y - B1.y) * (A2.x - A1.x)) / tmp;

            found = true;

            return new Vector2(
                B1.x + (B2.x - B1.x) * mu,
                B1.y + (B2.y - B1.y) * mu
            );
        }

        /// <summary>
        /// Gets the coordinates of the intersection point of two lines.
        /// </summary>
        /// <param name="A1">A point on the first line.</param>
        /// <param name="A2">Another point on the first line.</param>
        /// <param name="B1">A point on the second line.</param>
        /// <param name="B2">Another point on the second line.</param>
        /// <param name="found">Is set to false of there are no solution. true otherwise.</param>
        /// <returns>The intersection point coordinates. Returns Vector2.zero if there is no solution.</returns>
        // dont doung check is out of range
        public static Vector2 GetIntersectionPointCoordinates(Vector2 A1, Vector2 A2, Vector2 B1, Vector2 B2,
            out bool found)
        {
            float tmp = (B2.x - B1.x) * (A2.y - A1.y) - (B2.y - B1.y) * (A2.x - A1.x);

            if (Math.Abs(tmp) < 0.0001f)
            {
                // No solution!
                found = false;
                return Vector2.zero;
            }

            float mu = ((A1.x - B1.x) * (A2.y - A1.y) - (A1.y - B1.y) * (A2.x - A1.x)) / tmp;

            found = true;

            return new Vector2(
                B1.x + (B2.x - B1.x) * mu,
                B1.y + (B2.y - B1.y) * mu
            );
        }

        public static bool LineIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector3 p4, out Vector2 intersection)
        {
            intersection = Vector2.zero;

            var d = (p2.x - p1.x) * (p4.y - p3.y) - (p2.y - p1.y) * (p4.x - p3.x);

            if (Math.Abs(d) < 0.0001f)
            {
                return false;
            }

            var u = ((p3.x - p1.x) * (p4.y - p3.y) - (p3.y - p1.y) * (p4.x - p3.x)) / d;
            var v = ((p3.x - p1.x) * (p2.y - p1.y) - (p3.y - p1.y) * (p2.x - p1.x)) / d;

            if (u < 0.0f || u > 1.0f || v < 0.0f || v > 1.0f)
            {
                return false;
            }

            intersection.x = p1.x + u * (p2.x - p1.x);
            intersection.y = p1.y + u * (p2.y - p1.y);

            return true;
        }

        public static bool LineIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector3 p4)
        {
            var d = (p2.x - p1.x) * (p4.y - p3.y) - (p2.y - p1.y) * (p4.x - p3.x);

            if (Math.Abs(d) < 0.0001f)
            {
                return false;
            }

            var u = ((p3.x - p1.x) * (p4.y - p3.y) - (p3.y - p1.y) * (p4.x - p3.x)) / d;
            var v = ((p3.x - p1.x) * (p2.y - p1.y) - (p3.y - p1.y) * (p2.x - p1.x)) / d;

            if (u < 0.0f || u > 1.0f || v < 0.0f || v > 1.0f)
                return false;
            return true;
        }

        // // fail if point lies on poly frontier
        // public static bool CheckPointIsInsidePolygone(Vector2 in_pos, List<Vector2> ppoints)
        // {
        //     int intrsecnum = 0;
        //     Vector3 vpos1;
        //     Vector3 vpos2;
        //     int i;
        //     for (i = 0; i < (ppoints.Count - 1); i++)
        //     {
        //         vpos1 = ppoints[i];
        //         vpos2 = ppoints[i + 1];
        //         if (fail_IsIntrsec2d(in_pos, vpos1, vpos2))
        //         {
        //             intrsecnum += 1;
        //         }
        //     }
        //
        //     vpos1 = ppoints[ppoints.Count - 1];
        //     vpos2 = ppoints[0];
        //     if (fail_IsIntrsec2d(in_pos, vpos1, vpos2))
        //     {
        //         intrsecnum += 1;
        //     }
        //
        //     if (intrsecnum % 2 != 0)
        //     {
        //         return true;
        //     }
        //     else
        //     {
        //         return false;
        //     }
        // }

        // // FAILED do not use
        // public static bool fail_IsIntrsec2d(Vector2 in_pos, Vector2 vpos1, Vector2 vpos2)
        // {
        //     if (in_pos == vpos1) return true; // my 10.06.20
        //     if (in_pos == vpos2) return true; // my 10.06.20
        //
        //     bool intrsec = false;
        //     float vangle;
        //     if ((in_pos.x > vpos1.x) && (in_pos.x > vpos2.x))
        //     {
        //         if ((in_pos.y > vpos1.y & in_pos.y < vpos2.y) || (in_pos.y < vpos1.y & in_pos.y > vpos2.y))
        //         {
        //             return true;
        //         }
        //     }
        //
        //     if (((in_pos.x > vpos1.x) && (in_pos.x < vpos2.x)) || ((in_pos.x < vpos1.x) && (in_pos.x > vpos2.x)))
        //     {
        //         if ((in_pos.y > vpos1.y && in_pos.y < vpos2.y) || (in_pos.y < vpos1.y && in_pos.y > vpos2.y))
        //         {
        //             if (vpos2.y > vpos1.y)
        //             {
        //                 vangle = Mathf.Sign(Vector3.Cross(vpos2 - vpos1, in_pos - vpos1).z);
        //                 if (vangle > 0)
        //                 {
        //                     return true;
        //                 }
        //             }
        //             else
        //             {
        //                 vangle = Mathf.Sign(Vector3.Cross(vpos1 - vpos2, in_pos - vpos2).z);
        //             }
        //
        //             if (vangle > 0)
        //             {
        //                 intrsec = true;
        //             }
        //         }
        //     }
        //
        //     return intrsec;
        // }

        // fail
        private static bool ContainsPoint(List<Vector2> polyPoints, Vector2 p)
        {
            var j = polyPoints.Count - 1;
            var inside = false;
            for (int i = 0; i < polyPoints.Count; j = i++)
            {
                var pi = polyPoints[i];
                var pj = polyPoints[j];
                if (pi.y < p.y && pj.y >= p.y || pj.y < p.y && pi.y >= p.y)
                {
                    if (pi.x + (p.y - pi.y) / (pj.y - pi.y) * (pj.x - pi.x) < p.x)
                    {
                        inside = !inside;
                    }
                }
                //if (((pi.y <= p.y && p.y < pj.y) || (pj.y <= p.y && p.y < pi.y)) &&
                //    (p.x < (pj.x - pi.x) * (p.y - pi.y) / (pj.y - pi.y) + pi.x))
                //    inside = !inside;
            }

            return inside;
        }

        // it algo can detect edge(skin, frontier) point - very good, 
        // 
        public static bool IsInPolygon(List<Vector2> polygon, Vector2 point)
        {
            bool result = false;
            var a = polygon[polygon.Count() - 1];
            var polygonCount = polygon.Count;
            for (var index = 0; index < polygonCount; index++)
            {
                var b = polygon[index];
                if (IsPointInSegment(point, a, b)) return true; // resolve when point lies in poly line
                //if (IsIntrsec2d(point, a, b)) return true;
                if ((b.x == point.x) && (b.y == point.y))
                    return true;

                if ((b.y == a.y) && (point.y == a.y) &&
                    (((a.x <= point.x) && (point.x <= b.x)) ||
                     ((a.x >= point.x) && (point.x >= b.x))))
                    return true;

                if ((b.x == a.x) && (point.x == a.x) &&
                    (((a.y <= point.y) && (point.y <= b.y)) ||
                     ((a.y >= point.y) && (point.y >= b.y))))
                    return true;

                if ((b.y < point.y) && (a.y >= point.y) || (a.y < point.y) && (b.y >= point.y))
                {
                    if (b.x + (point.y - b.y) / (a.y - b.y) * (a.x - b.x) <= point.x)
                        result = !result;
                }

                a = b;
            }

            return result;
        }

        public static bool IsPointInSegment(Vector2 pt, Vector2 from, Vector2 to, double epsilon = 0.001f)
        {
            double segmentLengthSqr = (to.x - from.x) * (to.x - from.x) + (to.y - from.y) * (to.y - from.y);
            double r = ((pt.x - from.x) * (to.x - from.x) + (pt.y - from.y) * (to.y - from.y)) / segmentLengthSqr;
            if (r < 0 || r > 1) return false;
            double sl = ((from.y - pt.y) * (to.x - from.x) - (from.x - pt.x) * (to.y - from.y)) /
                        System.Math.Sqrt(segmentLengthSqr);
            return -epsilon <= sl && sl <= epsilon;
        }

        // Return True if the point is in the polygon. NOT detect poly points - 5x times slower
        private static bool PointInPolygon(Vector2 xy, List<Vector2> Points)
        {
            // Get the angle between the point and the
            // first and last vertices.
            int max_point = Points.Count - 1;
            float total_angle = GetAngle(
                Points[max_point].x, Points[max_point].y,
                xy.x, xy.y,
                Points[0].x, Points[0].y);

            // Add the angles from the point
            // to each other pair of vertices.
            for (int i = 0; i < max_point; i++)
            {
                total_angle += GetAngle(
                    Points[i].x, Points[i].y,
                    xy.x, xy.y,
                    Points[i + 1].x, Points[i + 1].y);
            }

            // The total angle should be 2 * PI or -2 * PI if
            // the point is in the polygon and close to zero
            // if the point is outside the polygon.
            // The following statement was changed. See the comments.
            //return (Math.Abs(total_angle) > 0.000001);
            return (Math.Abs(total_angle) > 1);
        }

        // Return the angle ABC.
// Return a value between PI and -PI.
// Note that the value is the opposite of what you might
// expect because Y coordinates increase downward.
        public static float GetAngle(float Ax, float Ay,
            float Bx, float By, float Cx, float Cy)
        {
            // Get the dot product.
            float dot_product = DotProduct(Ax, Ay, Bx, By, Cx, Cy);

            // Get the cross product.
            float cross_product = CrossProductLength(Ax, Ay, Bx, By, Cx, Cy);

            // Calculate the angle.
            return (float) Math.Atan2(cross_product, dot_product);
        }

        // Return the dot product AB · BC.
// Note that AB · BC = |AB| * |BC| * Cos(theta).
        private static float DotProduct(float Ax, float Ay,
            float Bx, float By, float Cx, float Cy)
        {
            // Get the vectors' coordinates.
            float BAx = Ax - Bx;
            float BAy = Ay - By;
            float BCx = Cx - Bx;
            float BCy = Cy - By;

            // Calculate the dot product.
            return (BAx * BCx + BAy * BCy);
        }

        // Return the cross product AB x BC.
// The cross product is a vector perpendicular to AB
// and BC having length |AB| * |BC| * Sin(theta) and
// with direction given by the right-hand rule.
// For two vectors in the X-Y plane, the result is a
// vector with X and Y components 0 so the Z component
// gives the vector's length and direction.
        public static float CrossProductLength(float Ax, float Ay,
            float Bx, float By, float Cx, float Cy)
        {
            // Get the vectors' coordinates.
            float BAx = Ax - Bx;
            float BAy = Ay - By;
            float BCx = Cx - Bx;
            float BCy = Cy - By;

            // Calculate the Z coordinate of the cross product.
            return (BAx * BCy - BAy * BCx);
        }
    }
}