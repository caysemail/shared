﻿using System;
using Shared.IntVector3Extensions;
using UnityEngine;

namespace Shared.IntVector3Extensions
{
    public static class Extensions
    {
        public static IntVector3 ToIntVector3(this Vector3Int a)
        {
            return new IntVector3(a.x, a.y, a.z);
        }
        public static Vector3 ToVector3(this Vector3Int a)
        {
            return new Vector3(a.x, a.y, a.z);
        }
        
        public static float DistanceFast(IntVector3 a, IntVector3 b)
        {
            a.x = a.x - b.x; a.y = a.y - b.y; a.z = a.z - b.z;
            a.x = a.x >= 0 ? a.x : -a.x;
            a.y = a.y >= 0 ? a.y : -a.y;
            a.z = a.z >= 0 ? a.z : -a.z;
            return a.x + a.y + a.z;
        }

    }
}

namespace Shared
{ 

    [Serializable]
    public struct IntVector3 : IEquatable<IntVector3>
    {
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = x;
                hashCode = (hashCode * 397) ^ y;
                hashCode = (hashCode * 397) ^ z;
                return hashCode;
            }
        }

        public static readonly IntVector3[] Dirs = new IntVector3[6]
        {
            Vector3Int.right.ToIntVector3(), // pos X
            Vector3Int.up.ToIntVector3(), // pos Y
            new Vector3Int(0, 0, 1).ToIntVector3(), // pos Z
            Vector3Int.left.ToIntVector3(), // neg X
            Vector3Int.down.ToIntVector3(), // neg Y
            new Vector3Int(0, 0, -1).ToIntVector3() // neg Z
        };

        public static readonly IntVector3 zero = new IntVector3(0, 0, 0);
        public static readonly IntVector3 one = new IntVector3(1, 1, 1);
        public static readonly IntVector3 ff = new IntVector3(255, 255, 255);
        public static readonly IntVector3 posX = new IntVector3(1, 0, 0);
        
        public static readonly IntVector3 right = new IntVector3(1, 0, 0);
        public static readonly IntVector3 left = new IntVector3(-1, 0, 0);
        public static readonly IntVector3 up = new IntVector3(0, 1, 0);
        public static readonly IntVector3 down = new IntVector3(0, -1, 0);
        public static readonly IntVector3 forward = new IntVector3(0, 0, 1);
        public static readonly IntVector3 backward = new IntVector3(0, 0, -1);

        public int x, y, z;

        public IntVector3(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
        
        public IntVector3(Vector3 t)
        {
            this.x = (int)t.x;
            this.y = (int)t.y;
            this.z = (int)t.z;
        }

        public Vector3Int Vector3Int
        {
            get { return new Vector3Int(x, y, z); }
        }

        public Vector3 Vector3
        {
            get { return new Vector3(x, y, z); }
        }

        public override string ToString()
        {
            return x+" "+y+" "+z;
        }

        public static IntVector3 operator +(IntVector3 a, IntVector3 b)
        {
            return new IntVector3(a.x + b.x, a.y + b.y, a.z + b.z);
        }
        public static IntVector3 operator -(IntVector3 a, IntVector3 b)
        {
            return new IntVector3(a.x - b.x, a.y - b.y, a.z - b.z);
        }

        public static bool operator !=(IntVector3 a, IntVector3 b)
        { return a.x != b.x || a.y != b.y || a.z != b.z; }
        public static bool operator ==(IntVector3 a, IntVector3 b)
        { return a.x == b.x && a.y == b.y && a.z == b.z; }

        public static IntVector3 operator /(IntVector3 a, int b)
        {
            return new IntVector3(a.x / b, a.y / b, a.z / b);
        }

        /// <summary>
        ///   <para>Returns true if the objects are equal.</para>
        /// </summary>
        /// <param name="other"></param>
        public override bool Equals(object other)
        {
            if (!(other is IntVector3))
                return false;
            return this.Equals((IntVector3)other);
        }

        public bool Equals(IntVector3 other)
        {
            return this == other;
        }
        
        /// <summary>
        /// Returns the magnitude of the vector. The magnitude is the 'length' of the vector from 0,0,0 to this point. Can be used for distance calculations:
        /// <code> Debug.Log ("Distance between 3,4,5 and 6,7,8 is: "+(new Int3(3,4,5) - new Int3(6,7,8)).magnitude); </code>
        /// </summary>
        public float magnitude {
            get {
                //It turns out that using doubles is just as fast as using ints with Mathf.Sqrt. And this can also handle larger numbers (possibly with small errors when using huge numbers)!

                double _x = x;
                double _y = y;
                double _z = z;

                return (float)System.Math.Sqrt(_x*_x+_y*_y+_z*_z);
            }
        }

        /// <summary>
        /// Magnitude used for the cost between two nodes. The default cost between two nodes can be calculated like this:
        /// <code> int cost = (node1.position-node2.position).costMagnitude; </code>
        ///
        /// This is simply the magnitude, rounded to the nearest integer
        /// </summary>
        public int costMagnitude {
            get {
                return (int)System.Math.Round(magnitude);
            }
        }

        /// <summary>The squared magnitude of the vector</summary>
        public float sqrMagnitude {
            get {
                double _x = x;
                double _y = y;
                double _z = z;
                return (float)(_x*_x+_y*_y+_z*_z);
            }
        }

        /// <summary>The squared magnitude of the vector</summary>
        public long sqrMagnitudeLong {
            get {
                long _x = x;
                long _y = y;
                long _z = z;
                return (_x*_x+_y*_y+_z*_z);
            }
        }

        // intMaxValue = 2,147,483,647; that is, hexadecimal 0x7FFFFFFF.
        public int SqrDistanceInteger(IntVector3 to)
        {
            var d = this - to;
            long _x = d.x;
            long _y = d.y;
            long _z = d.z;
            // 15000*15000+15000*15000+15000*15000=675'000'000
            return (int)(d.x*d.x+d.y*d.y+d.z*d.z);
        }

    }
}