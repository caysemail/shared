﻿using System;
using UnityEngine;

namespace Shared
{
    [HideInInspector]
    [Serializable]
    public class SerializedMesh
    {
        [HideInInspector] [SerializeField] private Vector2[] uv;
        [HideInInspector] [SerializeField] private Vector3[] vertices;
        [HideInInspector] [SerializeField] private int[] triangles;
        [HideInInspector] [SerializeField] private Matrix4x4[] bindposes;
        [HideInInspector] [SerializeField] private BoneWeight[] boneWeight;

        //[HideInInspector] [SerializeField] private bool serialized = false;
        private Mesh _mesh;

        public SerializedMesh()
        {
        }

        public SerializedMesh(Mesh mesh)
        {
            //            Mesh = mesh;
            Serialize(mesh);
        }

        public Mesh Mesh
        {
            get
            {
                if (_mesh == null)
                    _mesh = Rebuild();
                return _mesh;
            }
            set => Serialize(value);
        }

        private void Serialize(Mesh mesh)
        {
            uv = mesh.uv;
            vertices = mesh.vertices;
            triangles = mesh.triangles;
            bindposes = mesh.bindposes;
            boneWeight = mesh.boneWeights;
        }

        private Mesh Rebuild()
        {
            Mesh mesh = new Mesh();
            mesh.vertices = vertices;
            mesh.triangles = triangles;
            mesh.uv = uv;
            mesh.bindposes = bindposes;
            mesh.boneWeights = boneWeight;

            mesh.RecalculateNormals();
            mesh.RecalculateBounds();

            return mesh;
        }
    }
}