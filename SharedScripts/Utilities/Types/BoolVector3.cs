﻿using System;

namespace Shared
{
    
#if Newtonsoft
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public class BoolVector3FromArray : JsonConverter
    {

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException("Not implemented yet");
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
            {
                return new BoolVector3();
            }

            if (reader.TokenType == JsonToken.String)
            {
                return serializer.Deserialize(reader, objectType);
            }
            
            if (reader.TokenType != JsonToken.StartArray)
                return serializer.Deserialize(reader, objectType);

            //Debug.LogError("BoolVector3FromArray.ReadJson: TokenType="+reader.TokenType+", reader="+reader.ToString());
            var obj = JArray.Load(reader);
            //JObject obj = JObject.Load(reader);
            if (obj.Count != 3) return new BoolVector3();
            BoolVector3 bv3 = new BoolVector3();
            bv3.x = Boolean.Parse(obj[0].ToString());
            bv3.y = Boolean.Parse(obj[1].ToString());
            bv3.z = Boolean.Parse(obj[2].ToString());
            return bv3;
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override bool CanConvert(Type objectType)
        {
            return false;
        }
    }
#endif

    [Serializable]
    public struct BoolVector3 : IEquatable<BoolVector3>
    {
        /*
        [HorizontalGroup("bool")]
        [VerticalGroup("bool/a"), LabelWidth(25)]
        public bool x;
        [VerticalGroup("bool/b"), LabelWidth(25)]
        public bool y;
        [VerticalGroup("bool/c"), LabelWidth(25)]
        public bool z;
        //*/
        
        public static BoolVector3 False => new BoolVector3();
        public static BoolVector3 True => new BoolVector3(true,true,true);
        
        public bool x, y, z;

        public BoolVector3(bool x, bool y, bool z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public bool Equals(BoolVector3 other)
        {
            return x == other.x && y == other.y && z == other.z;
        }

        public override bool Equals(object obj)
        {
            return obj is BoolVector3 other && Equals(other);
        }
        
        public static bool operator !=(BoolVector3 a, BoolVector3 b)
        { return a.x != b.x || a.y != b.y || a.z != b.z; }
        public static bool operator ==(BoolVector3 a, BoolVector3 b)
        { return a.x == b.x && a.y == b.y && a.z == b.z; }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (x ? 1 : 0);
                hashCode = (hashCode * 397) ^ (y ? 1 : 0);
                hashCode = (hashCode * 397) ^ (z ? 1 : 0);
                return hashCode;
            }
        }

        public override string ToString()
        {
            return "("+x+", "+y+", "+z+")";
        }
    }
}
