﻿using System;
using UnityEngine;
using System.Collections;

namespace Shared
{
    [Serializable]
    public class StringInt
    {
        public string Name;
        public int Value;

        public StringInt(string name, int value)
        {
            Name = name;
            Value = value;
        }

        public override string ToString()
        {
            return Name+"="+Value;
        }
    }
}