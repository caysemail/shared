﻿using System.Runtime.InteropServices;
using UnityEngine;

namespace Shared
{
    [StructLayout(LayoutKind.Explicit)] //, Pack = 8)]
    public struct RgbaBuffer
    {
        /// <summary>
        /// Number of Bytes
        /// </summary>
        [FieldOffset(0)] public int Length;

        //[FieldOffset(8)]
        //private byte[] byteBuffer;
        //[FieldOffset(8)]
        //private float[] floatBuffer;
        //[FieldOffset(8)]
        //private short[] shortBuffer;
        //[FieldOffset(8)]
        //private int[] intBuffer;
        [FieldOffset(8)] public Color32[] Color32Array;
        [FieldOffset(8)] public RGBA[] RgbaArray;

        public void SetColor32Array(Color32[] color32Array)
        {
            Length = color32Array.Length;
            Color32Array = color32Array;
        }

        public RgbaBuffer(int length)
        {
            Length = length;
            Color32Array = new Color32[0]; // init buffers
            RgbaArray = new RGBA[0]; // init buffers
            //RgbaArray = new RGBA[Length]; // all buffers is created and filled by 0
            Color32Array = new Color32[Length];
            //int aligned4Bytes = sizeToAllocateInBytes % 4;
            //sizeToAllocateInBytes = (aligned4Bytes == 0) ? sizeToAllocateInBytes : sizeToAllocateInBytes + 4 - aligned4Bytes;
            // Allocating the byteBuffer is co-allocating the floatBuffer and the intBuffer
            //byteBuffer = new byte[sizeToAllocateInBytes];
            //numberOfBytes = 0;
        }
    }
}