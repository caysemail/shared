using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using Debug = UnityEngine.Debug;
using Object = System.Object;

namespace Shared
{


            // internal interface ITypedEvent
            // {
            //     //void Subscribe(UnityAction<Object> call);
            // }
    public class TypedEvent : UnityEvent //, ITypedEvent
    {
        //public async Task Invoke() { this.Invoke(); }

        // public void Subscribe(UnityAction<object> call)
        // {
        //     AddListener(() =>
        //     {
        //         call.Invoke(null);
        //     });
        // }
        //

        
        private List<int> _listenersHash = new List<int>(); 
        private List<int> _completeHash = new List<int>();
        private List<string> _info = new List<string>();

        public new void Invoke()
        {
            base.Invoke();
        }
        
        /// <summary>
        ///   <para>Add a non persistent listener to the UnityEvent.</para>
        /// </summary>
        /// <param name="call">Callback function.</param>
        public new void AddListener(UnityAction call)
        {
            _listenersHash.Add(call.GetHashCode());
            //Debug.LogError("TypedEvent.AddListener: target="+call.Target.GetType()+", method="+call.GetMethodInfo().Name+", call="+call.GetHashCode());
            base.AddListener(call);
            _info.Add(call.Target.ToString()+" : "+call.Method.ToString());
        }
        
        /// <summary>
        ///   <para>Remove a non persistent listener from the UnityEvent.</para>
        /// </summary>
        /// <param name="call">Callback function.</param>
        public new void RemoveListener(UnityAction call)
        {
            _listenersHash.Remove(call.GetHashCode());
            base.RemoveListener(call.Target, call.Method);
        }

        /// <summary>
        ///   <para>.</para>
        /// </summary>
        /// <param name="call">Callback function.</param>
        public void OnComplete(UnityAction call)
        {
            _completeHash.Remove(call.GetHashCode());
            //Debug.LogError("TypedEvent.OnComplete: target="+call.Target.GetType()+", method="+call.GetMethodInfo().Name+", call="+call.GetHashCode());
        }

        public async Task InvokeAndWait()
        {
            //var sf0 = new StackFrame(0);
            //var sf1 = new StackFrame(1);
            //var sf2 = new StackFrame(2);
            //Debug.LogError("stacktrace 1 = " + new StackFrame(1).GetMethod().Name + "");
            //Debug.LogError("stacktrace 2 = " + new StackFrame(2).GetMethod().Name + "");
            //Debug.LogError("stacktrace 3 = " + new StackFrame(3).GetMethod().Name + "");
            _completeHash = new List<int>(_listenersHash);
            Invoke();
            int delayMax = 10000; // ms, 10 sec
            while (_completeHash.Count > 0)
            {
                await Task.Delay(25);
                if (delayMax == 0)
                {
                    Debug.LogError("We("+ new StackFrame(2).GetMethod().Name +") wait and continue wait for "+delayMax
                        +" ms., but receive no OnCompleted, no call listeners count = "+_completeHash.Count+", first="+_info.FirstOrDefault());
                }
                delayMax -= 25; // ms
                if (!Application.isPlaying) break;
            }
        }
        
        public async Task Wait(int msDelay = 25)
        {
            bool isInvoked = false;
            void Set()  {  isInvoked = true; }
            AddListener(Set);
            while (!isInvoked)
            {
                await Task.Delay(msDelay); // ms
            }
            RemoveListener(Set);
        }
    }

    public class TypedEvent<T> : UnityEvent<T>
    {
    }
    
    public class TypedEvent<T1, T2> : UnityEvent<T1, T2>
    {
    }
    public class TypedEvent<T1, T2, T3> : UnityEvent<T1, T2, T3>
    {
    }
}