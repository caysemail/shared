﻿using System;

namespace Shared
{
    [Serializable]
    public struct BoolVector4 : IEquatable<BoolVector4>
    {
        /*
        [HorizontalGroup("bool")]
        [VerticalGroup("bool/a"), LabelWidth(25)]
        public bool x;
        [VerticalGroup("bool/b"), LabelWidth(25)]
        public bool y;
        [VerticalGroup("bool/c"), LabelWidth(25)]
        public bool z;
        //*/
        
        public static BoolVector4 False => new BoolVector4();
        public static BoolVector4 True => new BoolVector4(true,true,true, true);
        
        public bool x, y, z, w;

        public BoolVector4(bool x, bool y, bool z, bool w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        public bool Equals(BoolVector4 other)
        {
            return x == other.x && y == other.y && z == other.z && w == other.w;
        }

        public override bool Equals(object obj)
        {
            return obj is BoolVector4 other && Equals(other);
        }
        
        public static bool operator !=(BoolVector4 a, BoolVector4 b)
        { return a.x != b.x || a.y != b.y || a.z != b.z || a.w != b.w; }
        public static bool operator ==(BoolVector4 a, BoolVector4 b)
        { return a.x == b.x && a.y == b.y && a.z == b.z && a.w == b.w; }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (x ? 1 : 0);
                hashCode = (hashCode * 397) ^ (y ? 1 : 0);
                hashCode = (hashCode * 397) ^ (z ? 1 : 0);
                hashCode = (hashCode * 397) ^ (w ? 1 : 0);
                return hashCode;
            }
        }

        public override string ToString()
        {
            return "("+x+", "+y+", "+z+", "+w+")";
        }
    }
}
