﻿using System;
using UnityEngine;

namespace Shared
{
    [Serializable]
    public struct ByteVector2
    {
        public byte X;
        public byte Y;

        public ByteVector2(byte x, byte y, byte z) { X = x; Y = y; }
        public ByteVector2(int x, int y) { X = (byte)x; Y = (byte)y; }

        public static ByteVector2 zero { get { return new ByteVector2(0, 0); } }

        public ByteVector2 AddX(int x) { return new ByteVector2((byte)(X + x), Y); }
        public ByteVector2 AddY(int y) { return new ByteVector2(X, (byte)(Y + y)); }
       
        public int Index { get { return X + 256 * Y; } }
        public Vector2 ToVector3 { get { return new Vector2(X, Y); } }
        //public SignedByteVector2 ToSignedByteVector2 { get { return new SignedByteVector2(X, Y, Z); } }

        public override string ToString() { return "" + X + ", " + Y; }

        public override bool Equals(object obj)
        {
            if (!(obj is ByteVector2))
            {
                return false;
            }

            var vector = (ByteVector2)obj;
            return X == vector.X &&
                   Y == vector.Y;
        }

        public override int GetHashCode()
        {
            var hashCode = -307843816;
            hashCode = hashCode * -1521134295 + base.GetHashCode();
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }

        public static bool operator ==(ByteVector2 a, ByteVector2 b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(ByteVector2 a, ByteVector2 b)
        {
            return a.X != b.X || a.Y != b.Y;
        }
        public static ByteVector2 operator -(ByteVector2 a)
        {
            return new ByteVector2(-a.X, -a.Y);
        }
        public static ByteVector2 operator /(ByteVector2 a, int del)
        {
            return new ByteVector2(a.X / del, a.Y / del);
        }
        public static ByteVector2 operator /(ByteVector2 a, float del)
        {
            return new ByteVector2((byte)(a.X / del), (byte)(a.Y / del));
        }
        public static ByteVector2 operator *(ByteVector2 a, float del)
        {
            return new ByteVector2((byte)(a.X * del), (byte)(a.Y * del));
        }
    }
}
