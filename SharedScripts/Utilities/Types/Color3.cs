﻿namespace Shared
{
    public struct Color3
    {
        public float r;
        public float g;
        public float b;

        public bool Equals(Color3 other)
        {
            return r == other.r && g == other.g && b == other.b;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is RGB other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = r.GetHashCode();
                hashCode = (hashCode * 397) ^ g.GetHashCode();
                hashCode = (hashCode * 397) ^ b.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return "("+r+" " + g + " " + b+")";
        }

        public static bool operator !=(Color3 a, Color3 b)
        { return a.r != b.r || a.g != b.g || a.b == b.b; }
        public static bool operator ==(Color3 a, Color3 b)
        { return a.r == b.r && a.g == b.g && a.b == b.b; }
    }

}