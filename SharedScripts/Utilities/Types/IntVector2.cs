﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
//using Newtonsoft.Json;
using UnityEngine;

namespace Shared
{
    [Serializable]
    public struct IntVector2
    {
        public int x;
        public int y;

        public IntVector2(Vector2Int xy)
        {
            this.x = xy.x;
            this.y = xy.y;
        }

        public IntVector2(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public IntVector2(float x, float y)
        {
            this.x = Mathf.RoundToInt(x);
            this.y = Mathf.RoundToInt(y);
        }

        public static List<IntVector2> dirs8 = new List<IntVector2>()
        {
            up, rightUp, right, rightDown, down, leftDown, left, leftUp
        };

        public static List<IntVector2> dirs4 = new List<IntVector2>()
        {
            up, right, down, left
        };

        /// <summary>
        /// 0, 1
        /// </summary>
        public static IntVector2 up
        {
            get { return new IntVector2(0, 1); }
        }
        
        public static IntVector2 p01
        {
            get { return new IntVector2(0, 1); }
        }
        public static IntVector2 m01
        {
            get { return new IntVector2(0, -1); }
        }

        public static IntVector2 rightUp
        {
            get { return new IntVector2(1, 1); }
        }

        public static IntVector2 minusOne
        {
            get { return new IntVector2(-1, -1); }
        }

        /// <summary>
        /// 1, 0
        /// </summary>
        public static IntVector2 right
        {
            get { return new IntVector2(1, 0); }
        }
        
        public static IntVector2 p10
        {
            get { return new IntVector2(1, 0); }
        }
        public static IntVector2 m10
        {
            get { return new IntVector2(-1, 0); }
        }

        public static IntVector2 rightDown
        {
            get { return new IntVector2(1, -1); }
        }

        public static IntVector2 down
        {
            get { return new IntVector2(0, -1); }
        }

        public static IntVector2 leftDown
        {
            get { return new IntVector2(-1, -1); }
        }

        public static IntVector2 left
        {
            get { return new IntVector2(-1, 0); }
        }

        public static IntVector2 leftUp
        {
            get { return new IntVector2(-1, 1); }
        }

        public static IntVector2 zero
        {
            get { return new IntVector2(0, 0); }
        }
        public static IntVector2 one
        {
            get { return new IntVector2(1, 1); }
        }

        public Vector3 XyZ => new Vector3(x, 0, y);

        public static IntVector2 max => new IntVector2(int.MaxValue, int.MaxValue);

        public IntVector2 AddX(int x)
        {
            return new IntVector2(this.x + x, y);
        }

        public IntVector2 Addy(int y)
        {
            return new IntVector2(x, this.y + y);
        }

        // public int ByteIndex { get { return x + 256 * y; } }
        //[JsonIgnore]
        public Vector3 ToVector3
        {
            get { return new Vector3(x, y, 0); }
        }

        public Vector2 ToVector2
        {
            get { return new Vector2(x, y); }
        }

        //[JsonIgnore]
        public Vector2Int ToVector2Int
        {
            get { return new Vector2Int(x, y); }
        }

        public override string ToString()
        {
            return "(" + x + ", " + y + ")";
        }

        public override bool Equals(object obj)
        {
            if (!(obj is IntVector2))
            {
                return false;
            }

            var vector = (IntVector2)obj;
            return x == vector.x &&
                   y == vector.y;
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override int GetHashCode() => this.x.GetHashCode() ^ this.y.GetHashCode() << 2;

        // public override int GetHashCode()
        // {
        //     var hashCode = -307843816;
        //     hashCode = hashCode * -1521134295 + base.GetHashCode();
        //     hashCode = hashCode * -1521134295 + x.GetHashCode();
        //     hashCode = hashCode * -1521134295 + y.GetHashCode();
        //     return hashCode;
        // }

        public static bool operator ==(IntVector2 a, IntVector2 b)
        {
            return a.x == b.x && a.y == b.y;
        }

        public static bool operator !=(IntVector2 a, IntVector2 b)
        {
            return a.x != b.x || a.y != b.y;
        }

        public static IntVector2 operator -(IntVector2 a)
        {
            return new IntVector2(-a.x, -a.y);
        }

        public static IntVector2 operator +(IntVector2 a, IntVector2 b)
        {
            return new IntVector2(a.x + b.x, a.y + b.y);
        }

        public static IntVector2 operator -(IntVector2 a, IntVector2 b)
        {
            return new IntVector2(a.x - b.x, a.y - b.y);
        }

        public static IntVector2 operator /(IntVector2 a, int del)
        {
            return new IntVector2(a.x / del, a.y / del);
        }

        public static IntVector2 operator /(IntVector2 a, float del)
        {
            return new IntVector2((int)(a.x / del), (int)(a.y / del));
        }

        public static IntVector2 operator *(IntVector2 a, float del)
        {
            return new IntVector2((int)(a.x * del), (int)(a.y * del));
        }

        public static IntVector2 operator +(IntVector2 a, Vector2 b)
        {
            return new IntVector2(a.x + b.x, a.y + b.y);
        }


        public IntVector2 GetNextAround(int count)
        {
            int circle = count / 8;
            int index = count - 8 * circle;
            int nCircle = circle + 1;
            IntVector2 pos = IntVector2.zero;

            for (int ix = -nCircle; ix <= +nCircle; ix++)
            {
                for (int iy = -nCircle; iy <= +nCircle; iy++)
                {
                    pos.x = x + ix;
                    pos.y = y + iy;
                    index--;
                    if (index < 0) return pos;
                }
            }

            throw new UnityException();
        }
    }
}