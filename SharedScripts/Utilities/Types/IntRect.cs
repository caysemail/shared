﻿using UnityEngine;

namespace Shared
{
    [System.Serializable]
    public struct IntRect
    {
        public int x;
        public int y;
        public int width;
        public int height;

        public IntRect(IntVector2 position, IntVector2 size)
        {
            x = position.x;
            y = position.y;
            width = size.x;
            height = size.y;
        }

        public IntVector2 RandomPosition
        {
            get
            {
                return new IntVector2(Random.Range(x, x + width), Random.Range(y, y + height));
            }
        }
        public IntVector2 Center
        {
            get
            {
                return new IntVector2(x + width/2, y + height/2);
            }
        }
    }
}

