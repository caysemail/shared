﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shared
{
    public class SimpleEvent
    {
        private readonly List<Action> _callbacks = new List<Action>();

        public void AddListener(Action callback)
        {
            _callbacks.Add(callback);
        }
        
        public void RemoveListener(Action callback)
        {
            _callbacks.Remove(callback);
        }
        
        public void ReAdd(Action callback)
        {
            _callbacks.Remove(callback);
            _callbacks.Add(callback);
        }

        public void Invoke()
        {
            List<Action> callbacks = new List<Action>(_callbacks);
            var maxCount = callbacks.Count;
            for (var index = 0; index < maxCount; index++)
                callbacks[index].Invoke();
        }
        
        public async Task Wait(int msDelay = 10)
        {
            bool isInvoked = false;
            void Set()  {  isInvoked = true; }
            AddListener(Set);
            while (!isInvoked)
            {
                await Task.Delay(msDelay); // ms
            }
            RemoveListener(Set);
        }
    }
    
    public class SimpleEvent<T>
    {
        private readonly List<Action<T>> _callbacks = new List<Action<T>>();

        public void AddListener(Action<T> callback)
        {
            _callbacks.Add(callback);
        }

        public void RemoveListener(Action<T> callback)
        {
            _callbacks.Remove(callback);
        }

        public void Invoke(T arg)
        {
            var callbacks = new List<Action<T>>(_callbacks);
            var maxCount = callbacks.Count;
            for (var index = 0; index < maxCount; index++)
                callbacks[index].Invoke(arg);
        }
    }
    
    public class SimpleEvent<T1,T2>
    {
        private readonly List<Action<T1, T2>> _callbacks = new List<Action<T1, T2>>();

        public void AddListener(Action<T1, T2> callback)
        {
            _callbacks.Add(callback);
        }
        
        public void RemoveListener(Action<T1, T2> callback)
        {
            _callbacks.Remove(callback);
        }

        public void Invoke(T1 arg1, T2 arg2)
        {
            var callbacks = new List<Action<T1, T2>>(_callbacks);
            var maxCount = callbacks.Count;
            for (var index = 0; index < maxCount; index++)
                callbacks[index].Invoke(arg1, arg2);
        }
    }
}