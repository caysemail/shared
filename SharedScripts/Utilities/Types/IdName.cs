using System;

namespace Shared
{
    [Serializable]
    public struct IdName
    {
        public bool Equals(IdName other)
        {
            return id == other.id && name == other.name;
        }

        public override bool Equals(object obj)
        {
            return obj is IdName other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(id, name);
        }

        public int id;
        public string name;

        public static IdName Invalid = new IdName(-1, default);

        public IdName(int id, string name)
        {
            this.id = id;
            this.name = name;
        }
        
        public IdName(string name)
        {
            this.id = -1;
            this.name = name;
        }

        public override string ToString()
        {
            return "(" + id + " " + name + ")";
        }

        public static bool operator ==(IdName a, IdName b)
        {
            return a.id == b.id;
        }

        public static bool operator !=(IdName a, IdName b)
        {
            return a.id != b.id;
        }
    }
}