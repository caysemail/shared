﻿using System;
using System.Collections;
using System.Collections.Generic;
using Shared.IntVector3Extensions;
using UnityEngine;

namespace Shared
{
    [Serializable]
    public class IntBounds : IEnumerable<IntVector3>
    {
        public IntVector3 Center;
        public IntVector3 Extents;
        //public int MinX, MaxX, MinY, MaxY, MinZ, MaxZ;
        public IntVector3 Min;
        public IntVector3 Max;

        public override string ToString()
        {
            return "("+Center + " ± "+Extents+")_["+Min+" : "+Max+"]";
        }

        public bool IsInside(IntVector3 point)
        {
            if (point.x < Min.x || point.x > Max.x) return false;
            if (point.y < Min.y || point.y > Max.y) return false;
            if (point.z < Min.z || point.z > Max.z) return false;
            return true;
        }

        public IntBounds()
        {
        }

        public IntBounds FromCenterAndExtents(IntVector3 center, IntVector3 extents)
        {
            Center = center;
            Extents = extents;
            UpdateMinMax();
            return this;
        }

        public IntBounds FromMinMax(IntVector3 min, IntVector3 max)
        {
            this.Min = min;
            this.Max = max;
            Encapsulate(this.Min);
            return this;
        }

        public void From(Bounds bounds)
        {
            Center = bounds.center.RoundToIntVector3();
            Extents = bounds.extents.RoundToIntVector3();
            UpdateMinMax();
        }

        public void UpdateMinMax()
        {
            Min.x = Center.x - Extents.x;
            Max.x = Center.x + Extents.x;
            Min.y = Center.y - Extents.y;
            Max.y = Center.y + Extents.y;
            Min.z = Center.z - Extents.z;
            Max.z = Center.z + Extents.z;
        }

        //private Vector3Int Min { get { return new Vector3Int(MinX, MinY, MinZ); } }
        //private Vector3Int Max { get { return new Vector3Int(MaxX, MaxY, MaxZ); } }

        public void Encapsulate(IntVector3 point)
        {
            Min.x = Min.x < point.x ? Min.x : point.x;
            Max.x = Max.x > point.x ? Max.x : point.x;
            Min.y = Min.y < point.y ? Min.y : point.y;
            Max.y = Max.y > point.y ? Max.y : point.y;
            Min.z = Min.z < point.z ? Min.z : point.z;
            Max.z = Max.z > point.z ? Max.z : point.z;
            Extents = (Max - Min)/2;
            Center = Min + Extents;
        }

        public IEnumerator<IntVector3> GetEnumerator()
        {
            var t = IntVector3.zero;
            for (int x = Min.x; x <= Max.x; x++)
            {
                for (int y = Min.y; y <= Max.y; y++)
                {
                    for (int z = Min.z; z <= Max.z; z++)
                    {
                        t.x = x; t.y = y; t.z = z;
                        yield return t;
                    }
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}