﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

[Serializable]
[StructLayout(LayoutKind.Explicit)]
public struct RGBA 
{
    [SerializeField] [FieldOffset(0)] public uint rgba;

    [NonSerialized] [FieldOffset(0)] public Color32 color32;
    [NonSerialized] [FieldOffset(0)] public RGB rgb;
    [NonSerialized] [FieldOffset(0)] public byte r;
    [NonSerialized] [FieldOffset(1)] public byte g;
    [NonSerialized] [FieldOffset(2)] public byte b;
    [NonSerialized] [FieldOffset(3)] public byte a;

    public static RGBA black = new RGBA(0,0,0,0);

    public RGBA(byte r, byte g, byte b, byte a) : this()
    {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }
    
    public RGBA(Color32 color32) : this()
    {
        this.color32 = color32;
    }

    public RGBA(uint rgba) : this()
    {
        this.rgba = rgba;
    }

    public override string ToString()
    {
        return ""+rgba+" "+rgb+" "+r+" "+g+" "+b+" "+a;
    }

    public static explicit operator Color32(RGBA v)
    {
        return new Color32(v.r, v.g, v.b, v.a);
    }
}

public struct RGB
{
    public bool Equals(RGB other)
    {
        return r == other.r && g == other.g && b == other.b;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is RGB other && Equals(other);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            var hashCode = r.GetHashCode();
            hashCode = (hashCode * 397) ^ g.GetHashCode();
            hashCode = (hashCode * 397) ^ b.GetHashCode();
            return hashCode;
        }
    }

    public byte r;
    public byte g;
    public byte b;
    public override string ToString()
    {
        return "("+r+" " + g + " " + b+")";
    }

    public static bool operator !=(RGB a, RGB b)
    { return a.r != b.r || a.g != b.g || a.b == b.b; }
    public static bool operator ==(RGB a, RGB b)
    { return a.r == b.r && a.g == b.g && a.b == b.b; }
}
