﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Shared
{
    public static class ClassUtility
    {
        public static List<Type> GetAllTypes(Type interfaceType)
        {
            return AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes())
                .Where(x => interfaceType.IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract)
                .Select(x => x).ToList();
        }
    }
}