﻿#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Shared
{
    [CustomPropertyDrawer(typeof(IntVector3))]
    /**
    * ReadOnlyAttributeDrawer - A class to make Read-Only inspector properties.
    **/
    public class EditorIntVector3 : PropertyDrawer
    {
        /// <summary>
        /// A dictionary lookup of field counts keyed by class type name.
        /// </summary>
        private static Dictionary<string, int> _fieldCounts = new Dictionary<string, int>();

        #region ================================ PUBLIC METHODS

        // Necessary since some properties tend to collapse smaller than their content
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            //Debug.LogWarning("EditorIntVector3: " + property.name + ", position=" + position);
            //EditorGUI.PropertyField(position, property, label, true); return;

            //if (position.height > 16) position.height = 16;

            var fieldCount = GetFieldCount(property);

            Rect contentPosition = EditorGUI.PrefixLabel(position, label);

            EditorGUIUtility.labelWidth = 14f;
            float fieldWidth = contentPosition.width / fieldCount;
            bool hideLabels = false;// contentPosition.width < 185;
            contentPosition.width /= fieldCount;

            using (var indent = new EditorGUI.IndentLevelScope(-EditorGUI.indentLevel))
            {
                for (int i = 0; i < fieldCount; i++)
                {
                    if (!property.NextVisible(true))
                    {
                        break;
                    }

                    label = EditorGUI.BeginProperty(contentPosition, new GUIContent(property.displayName), property);
                    EditorGUI.PropertyField(contentPosition, property, hideLabels ? GUIContent.none : label);
                    EditorGUI.EndProperty();

                    contentPosition.x += fieldWidth;
                }
            }
        }

        #endregion

        /// <summary>
        /// Gets the field count for the specified property.
        /// </summary>
        /// <param name="property">The property for which to get the field count.</param>
        /// <returns>The field count of the property.</returns>
        private static int GetFieldCount(SerializedProperty property)
        {
            int count;
            if (!_fieldCounts.TryGetValue(property.type, out count))
            {
                var children = property.Copy().GetEnumerator();
                while (children.MoveNext())
                {
                    count++;
                }

                _fieldCounts[property.type] = count;
            }

            return count;
        }
    }
}

#endif