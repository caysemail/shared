﻿#if UNITY_EDITOR

using Shared;
using UnityEditor;
using UnityEngine;

/*
[CustomPropertyDrawer(typeof(BoolVector3))]
public class PointEditor: PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.Vector3Field(position, label, property.vector3Value);
    }
}
//*/

[CustomPropertyDrawer(typeof(BoolVector3))]
public class PointDrawer : PropertyDrawer
{
    SerializedProperty X, Y, Z;
    string name;
    bool cache = false;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (!cache)
        {
            //get the name before it's gone
            name = property.displayName;

            //get the X and Y values
            property.Next(true);
            X = property.Copy();
            property.Next(true);
            Y = property.Copy();
            property.Next(true);
            Z = property.Copy();

            cache = true;
        }

        Rect contentPosition = EditorGUI.PrefixLabel(position, new GUIContent(name));

        //Check if there is enough space to put the name on the same line (to save space)
        if (position.height > 16f)
        {
            position.height = 16f;
            EditorGUI.indentLevel += 1;
            contentPosition = EditorGUI.IndentedRect(position);
            contentPosition.y += 18f;
        }

        float half = contentPosition.width / 3;
        GUI.skin.label.padding = new RectOffset(3, 3, 6, 6);

        //show the X and Y Z from the point
        EditorGUIUtility.labelWidth = 14f;
        contentPosition.width *= 0.33f;
        EditorGUI.indentLevel = 0;

        // Begin/end property & change check make each field
        // behave correctly when multi-object editing.
        EditorGUI.BeginProperty(contentPosition, label, X);
        {
            EditorGUI.BeginChangeCheck();
            bool newVal = EditorGUI.Toggle(contentPosition, new GUIContent("X"), X.boolValue);
            if (EditorGUI.EndChangeCheck())
                X.boolValue = newVal;
        }
        EditorGUI.EndProperty();

        contentPosition.x += half;
        EditorGUI.BeginProperty(contentPosition, label, Y);
        {
            EditorGUI.BeginChangeCheck();
            bool newVal = EditorGUI.Toggle(contentPosition, new GUIContent("Y"), Y.boolValue);
            if (EditorGUI.EndChangeCheck())
                Y.boolValue = newVal;
        }
        EditorGUI.EndProperty();

        contentPosition.x += half;
        EditorGUI.BeginProperty(contentPosition, label, Z);
        {
            EditorGUI.BeginChangeCheck();
            bool newVal = EditorGUI.Toggle(contentPosition, new GUIContent("Z"), Z.boolValue);
            if (EditorGUI.EndChangeCheck())
                Z.boolValue = newVal;
        }
        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return Screen.width < 333 ? (16f + 18f) : 16f;
    }
}

/*
[CustomEditor(typeof(BoolVector3))]
public class BoolVector3Editor : Editor
{
    private SerializedProperty x;
    private void OnEnable()
    {
        x = serializedObject.FindProperty("x"); 
        //s//criptA = (ScriptA)target;
    }

    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
        serializedObject.Update();

        GUI.enabled = false;
        EditorGUILayout.PropertyField(serializedObject.FindProperty("x"), true);
        GUI.enabled = true;

        EditorGUILayout.PropertyField(x, true);
        serializedObject.ApplyModifiedProperties();

        //BoolVector3? myTarget = target as BoolVector3?;


        //myTarget.id = EditorGUILayout.TextField("Emotion", myTarget.id);
        //myTarget.photo = (Sprite)EditorGUILayout.ObjectField("Sprite", myTarget.photo, typeof(Sprite), false);

    }
}*/


#endif