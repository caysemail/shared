﻿#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Shared
{
    [CustomPropertyDrawer(typeof(IdName))]
    /**
    * ReadOnlyAttributeDrawer - A class to make Read-Only inspector properties.
    **/
    public class IdNameEditor : PropertyDrawer
    {
        /// <summary>
        /// A dictionary lookup of field counts keyed by class type name.
        /// </summary>
        private static Dictionary<string, int> _fieldCounts = new Dictionary<string, int>();

        #region ================================ PUBLIC METHODS

        // Necessary since some properties tend to collapse smaller than their content
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            //Debug.LogWarning("EditorIntVector3: " + property.name + ", position=" + position);
            //EditorGUI.PropertyField(position, property, label, true); return;

            //if (position.height > 16) position.height = 16;

            var fieldCount = GetFieldCount(property);

            
            // property.NextVisible(true);
            // string result2 = property.intValue.ToString()+" ";
            // property.NextVisible(true);
            // result2 += property.stringValue.ToString();
            //
            // label.text += " " + result2;
            //
            
            Rect contentPosition = EditorGUI.PrefixLabel(position, label);
            //EditorGUI.LabelField(position, label);

            //return;

            EditorGUIUtility.labelWidth = 14f;
            //var width0 = 40f;
            //var width1 = contentPosition.width - width0;
            //bool hideLabels = false;// contentPosition.width < 185;
            //contentPosition.width = width0;
            
            using (var indent = new EditorGUI.IndentLevelScope(-EditorGUI.indentLevel))
            {
                property.NextVisible(true);
                string result = property.intValue.ToString()+" ";
                property.NextVisible(true);
                result += property.stringValue.ToString();
                
                EditorGUI.LabelField(contentPosition, result);
                
                // for (int i = 0; i < fieldCount; i++)
                // {
                //     if (!property.NextVisible(true))
                //     {
                //         break;
                //     }
                //
                //     var displayName = property.displayName;
                //     displayName = "";
                //     
                //     //label = EditorGUI.BeginProperty(contentPosition, new GUIContent(displayName), property);
                //     //EditorGUI.prope
                //
                //     var currentContentPosition = contentPosition;
                //     EditorGUI.PropertyField(currentContentPosition, property, GUIContent.none);
                //     // if (i == 0) ?
                //     //     EditorGUI.LabelField(currentContentPosition, property.intValue.ToString());
                //     // else if (i == 1)
                //     //     EditorGUI.LabelField(currentContentPosition, property.stringValue.ToString());
                //     //EditorGUI.EndProperty();
                //
                //     contentPosition.x += width0+2;
                //     contentPosition.width = width1-2;
                // }
            }
        }

        #endregion

        /// <summary>
        /// Gets the field count for the specified property.
        /// </summary>
        /// <param name="property">The property for which to get the field count.</param>
        /// <returns>The field count of the property.</returns>
        private static int GetFieldCount(SerializedProperty property)
        {
            int count;
            if (!_fieldCounts.TryGetValue(property.type, out count))
            {
                var children = property.Copy().GetEnumerator();
                while (children.MoveNext())
                {
                    count++;
                }

                _fieldCounts[property.type] = count;
            }

            return count;
        }
    }
}

#endif