using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Texture2DUtility
{
    private Dictionary<string, Texture2D> _textureToString = new Dictionary<string, Texture2D>();
    private string _resourceFolder = "";
    private bool _isInited = false;

    public Texture2DUtility Initialize(Dictionary<string, Texture2D> textureToString, string resourceFolder = "")
    {
        Assert.IsNotNull(textureToString);
        Assert.IsNotNull(resourceFolder);
        _textureToString = textureToString;
        _resourceFolder = resourceFolder;
        _isInited = true;
        return this;
    }
    
    public Texture2D GetTexture2D(string textureName)
    {
        Assert.IsNotNull(textureName);
        if (!_isInited)
            throw new UnityException("TextureUtility not initialized");
        bool isFound = _textureToString.TryGetValue(textureName, out var texture);
        if (isFound)
            return texture;
        var textureFromResource = Resources.Load<Texture2D>(_resourceFolder + textureName);
        if (textureFromResource == null)
            throw new UnityException("cannot load "+_resourceFolder+textureName);
        _textureToString.Add(textureName, textureFromResource);
        return textureFromResource;
    }
}
