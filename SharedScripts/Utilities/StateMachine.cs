﻿using System;


namespace Cayse.Utility
{
    public sealed class StateMachine<T>
    {
        #region ================================= PUBLIC FIELDS

        public Action<T> OnChange;
        public Action<T> OnChangeCompleted;
        //public readonly SmartEvent<T> OnChangeCompleted = new SmartEvent<T>();

        #endregion

        #region ===================== CONSTRUCTORS AND DESTRUCTOR

        public StateMachine()
        {
        }

        public StateMachine(T startState)
        {
            CurrentState = startState;
        }

        #endregion

        #region ========================= PROPERTIES AND INDEXERS

        public T CurrentState { get; private set; }

        public T PreviousState { get; private set; }

        #endregion

        #region ================================ PUBLIC METHODS

        public bool Change(T state)
        {
            if (state.Equals(CurrentState)) return false;
            PreviousState = CurrentState;
            CurrentState = state;
            OnChange.Invoke(CurrentState);
            OnChangeCompleted.Invoke(CurrentState);
            return true;
        }

        #endregion
    }
}