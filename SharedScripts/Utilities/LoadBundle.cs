﻿#if UNITY_EDITOR

using System.Collections.Generic;
using System.IO;
using System.Linq;
using Shared;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;


//[ExecuteInEditMode]
public class LoadBundle : MonoBehaviour
{
	private bool AddColliders = false;
	public enum OldMech
	{
		None,
		atlas,awesome,banshee,battlemaster,blackjack,blackknight,cataphract,catapult,centurion,cicada,
		commando,dragon,enforcer,firestarter,grasshopper,griffin,highlander_sldf,highlander,hunchback,
		jagermech,jenner,kingcrab,kintaro,locust,
		stalker,
		panther,
		trebuchet
	}

	public OldMech Mech = OldMech.None;
	
	private List<string> _oldNames = new List<string>();

	[Button]
	private void LoadOld()
	{
		_oldNames = new List<string>()
		{
			"chrprfmech_atlasbase-001",
			"chrprfmech_awesomebase-001",
			"chrprfmech_bansheebase-001",
			"chrprfmech_battlemasterbase-001",
			"chrprfmech_blackjackbase-001",
			"chrprfmech_blackknightbase-001",
			"chrprfmech_cataphractbase-001",
			"chrprfmech_catapultbase-001",
			"chrprfmech_centurionbase-001",
			"chrprfmech_cicadabase-001",
			"chrprfmech_commandobase-001",
			"chrprfmech_dragonbase-001",
			"chrprfmech_enforcerbase-001",
			"chrprfmech_firestarterbase-001",
			"chrprfmech_grasshopperbase-001",
			"chrprfmech_griffinbase-001",
			"chrprfmech_highlander_sldfbase-001",
			"chrprfmech_highlanderbase-001",
			"chrprfmech_hunchbackbase-001",
			"chrprfmech_jagermechbase-001",
			"chrprfmech_jennerbase-001",
			"chrprfmech_kingcrabbase-001",
			"chrprfmech_kintarobase-001",
			"chrprfmech_locustbase-001",
			"chrprfmech_stalkerbase-001",
			"chrprfmech_pantherbase-001",
			"chrprfmech_trebuchetbase-001"
			
		};
		Assert.IsNotNull(_oldNames);
		Assert.IsTrue(_oldNames.Count > 0);
		if ((int) Mech > _oldNames.Count)
		{
			Debug.LogError("mech="+(int) Mech+" > "+_oldNames.Count);
			return;
		}
		LoadPrefabFromBundle(@"d:\c\BATTLETECH_old1\BattleTech_Data\StreamingAssets\data\assetbundles",
			_oldNames[-1+(int)Mech]);
	}
	
	public Material DefaultMaterial;
	public bool UseDefMat = false;
    public string Path = @"C:\games\BATTLETECH\BattleTech_Data\StreamingAssets\data\assetbundles\";
    public string Name = "chrprfmech_atlasbase-001";
    public bool LoadPrefab = false;
    public bool SavePrefab = false;
    public GameObject Instance;
    private AssetBundle _lastBundle;

    public bool loadMarauder = false;
    public bool loadDragon = false;

    public Transform MaterialSourceParent;
    public Material MaterialSource;

    // Use this for initialization
    void LoadPrefabFromBundle (string path, string name)
    {
	    if (Instance) DestroyImmediate(Instance);
	    Resources.UnloadUnusedAssets();
        //string path = @"C:\games\BATTLETECH\Mods\GP Mad Cat MK II\assetbundles"+@"\";
        //string path = @"C:\games\BATTLETECH\BattleTech_Data\StreamingAssets\data\assetbundles\";
        var fullPath = path + @"\" + name;
        var prefabName = name + ".prefab";
        //var myLoadedAssetBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "myassetBundle"));
        if (!File.Exists(fullPath))
        {
	        Debug.LogError("file not exist at path="+fullPath);
	        return;
        }
        AssetBundle.UnloadAllAssetBundles(true); 
        _lastBundle = AssetBundle.LoadFromFile(fullPath);
        if (_lastBundle == null)
        {
            Debug.Log("Failed to load AssetBundle!");
            return;
        }

        var prefab = _lastBundle.LoadAsset<GameObject>(prefabName);
        if (prefab == null)
        {
            Debug.Log("Failed to load prefab="+ prefabName);
            return;
        }
        
        Instance = Instantiate(prefab, transform, false);
        // clean
        var t1 = Instance.transform.Find("Heraldry"); DestroyImmediate(t1.gameObject);
        t1 = Instance.transform.Find("Foot IK"); DestroyImmediate(t1.gameObject);
        t1 = Instance.transform.Find("Grounder IK"); DestroyImmediate(t1.gameObject);
        t1 = Instance.transform.Find("WindZone"); DestroyImmediate(t1.gameObject);
        t1 = Instance.transform.Find("VFXCollider_vert"); if (t1) DestroyImmediate(t1.gameObject);
        t1 = Instance.transform.Find("BlipObjectUnknown"); DestroyImmediate(t1.gameObject);
        t1 = Instance.transform.Find("BlipObjectIdentified"); DestroyImmediate(t1.gameObject);
        t1 = Instance.transform.Find("VFXCollider_parent"); if (t1) DestroyImmediate(t1.gameObject);

        var transforms = Instance.transform.GetComponentsInChildren<Transform>(true);
        if (transforms.Length == 0)
        Debug.LogWarning("no transforms found");
        else for (var index = transforms.Length-1; index >= 0; index--)
        {
            var transform = transforms[index];
            if (transform.name.Contains("_dmg"))
                DestroyImmediate(transform.gameObject);
        }

        _lastBundle.Unload(false);
        Resources.UnloadUnusedAssets();
		//*/
		MakeMeshCopy();
    }

    private void SavePrefabAsAsset()
    {
        PrefabUtility.SaveAsPrefabAsset(Instance, "Assets/Prefabs/" + Name + ".prefab");
    }

    [Button]
    private void LoadDragon()
    {
	    LoadPrefabFromBundle(Application.dataPath+@"\Bundles\", @"chrprfmech_dragonbase-001");
    }
    [Button]
    private void LoadAwesome()
    {
	    LoadPrefabFromBundle(Application.dataPath+@"\Bundles\", @"chrprfmech_awesomebase-001");
    }
    [Button]
    private void LoadCatapultOutside()
    {
	    LoadPrefabFromBundle(@"C:\games\BATTLETECH_old1\BattleTech_Data\StreamingAssets\data\assetbundles",
		    @"chrprfmech_catapultbase-001");
    }
    [Button]
    private void LoadCicadaOutside()
    {
	    LoadPrefabFromBundle(@"C:\games\BATTLETECH_old1\BattleTech_Data\StreamingAssets\data\assetbundles",
		    @"chrprfmech_cicadabase-001");
    }
    
    [Button]
    private void LoadMarauderFromBattletech()
    {
	    LoadPrefabFromBundle(@"C:\games\BATTLETECH\Mods\CommunityAssets\assetbundles", @"chrprfmech_marauderiibase-001");
    }
    
    
    void Update ()
	{
	    if (LoadPrefab)
	    {
	        LoadPrefabFromBundle(Path, Name);
	        LoadPrefab = false;
	    }
	    if (SavePrefab)
	    {
	        SavePrefabAsAsset();
            SavePrefab = false;
	    }

	    if (loadMarauder)
	    {
	        LoadPrefabFromBundle(@"C:\games\BATTLETECH\Mods\BD_Marauder II\assetbundles",
	            @"chrprfmech_marauderiibase-001");
	        loadMarauder = false;
	    }

	    if (loadDragon)
	    {
	        loadDragon = false;
            LoadPrefabFromBundle(Application.dataPath+@"\Bundles\", @"chrprfmech_dragonbase-001");
        }
	}

    private void MakeMeshCopy()
    {
	    var smrs = Instance.transform.GetComponentsInChildren<SkinnedMeshRenderer>(true);
	    if (smrs.Length > 0 && DefaultMaterial != null)
	    {
		    foreach (var smr in smrs)
		    {
			    if (UseDefMat) smr.sharedMaterial = DefaultMaterial;
			    smr.sharedMesh = (Mesh) Instantiate( smr.sharedMesh );
			    if (AddColliders)
			    {
				    var mc = smr.gameObject.AddComponent<MeshCollider>();
				    mc.sharedMesh = smr.sharedMesh;
			    }
		    }
	    }
    }

    [Button]
    private void SwitchLegs()
    {
	    var t = Instance.transform.FindDeepChild("mesh_LLeg").gameObject; t.SetActive(!t.activeSelf);
	    t = Instance.transform.FindDeepChild("mesh_RLeg").gameObject; t.SetActive(!t.activeSelf);
    }
    [Button]
    private void SwitchArms()
    {
	    var t = Instance.transform.FindDeepChild("mesh_LArm").gameObject; t.SetActive(!t.activeSelf);
	    t = Instance.transform.FindDeepChild("mesh_RArm").gameObject; t.SetActive(!t.activeSelf);
    }
    
    public Vector3 PelvisRotation = Vector3.zero;

    [Button]
    private void UpdatePelvisRotation()
    {
	    var t = Instance.transform.FindDeepChild("j_Pelvis");
	    t.localRotation = Quaternion.Euler(PelvisRotation);
	    return;
	    // var smrs = Instance.transform.GetComponentsInChildren<SkinnedMeshRenderer>(true);
	    // if (smrs.Length > 0)
	    // {
		   //  foreach (var smr in smrs)
		   //  {
			  //   var mc = smr.gameObject.GetComponent<MeshCollider>();
			  //   if (mc == null) mc = smr.gameObject.AddComponent<MeshCollider>();
			  //   mc.sharedMesh = null;
			  //   smr.BakeMesh(smr.sharedMesh);
			  //   mc.sharedMesh = smr.sharedMesh;
		   //  }
	    // }
    }

    [Button]
    private void GetMaterialFrom()
    {
	    if (MaterialSourceParent == null) return;
	    var childTransforms = MaterialSourceParent.GetComponentsInChildren<Transform>(true);
	    var pelvis = childTransforms.FirstOrDefault(t => t.name.Contains("centre_torso_pelvis"));
	    if (pelvis == null) return;
	    var smrBase = pelvis.GetComponent<SkinnedMeshRenderer>();
	    MaterialSource = smrBase.sharedMaterial;
	    var smrs = Instance.transform.GetComponentsInChildren<SkinnedMeshRenderer>(true);
	    if (smrs.Length > 0)
	    {
		    foreach (var smr in smrs)
		    {
			    smr.sharedMaterial.shader = MaterialSource.shader;
		    }
	    }

    }
}

#endif