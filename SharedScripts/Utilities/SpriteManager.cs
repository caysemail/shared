﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.U2D;
using UnityEngine.UI;

#if  UNITY_EDITOR
    using UnityEditor.Sprites;
#endif



namespace Shared
{
    /// <summary>
    /// Extensions for SpriteManager class
    /// </summary>
    public static class SpriteManagerExtensions
    {
        /// <summary>
        /// Load sprite by name into image.sprite
        /// </summary>
        /// <param name="image">Image</param>
        /// <param name="spriteName">sprite name w/o any folder</param>
        public static void SpriteLoadFromAtlas(this Image image, string spriteName)
        {
            SpriteManager.LoadFromAtlas(image, spriteName);
        }
        public static void SpriteLoadFromResource(this Image image, string spriteName)
        {
            SpriteManager.LoadFromResource(image, spriteName);
        }
    }

    /// <summary>
    /// Load and manages all sprites from atlases
    /// </summary>
    public class SpriteManager
    {
        private static SpriteManager _instance;
        /// <summary>
        /// Sprite name by actual Sprite collection
        /// </summary>
        private readonly Dictionary<string, Sprite> _sprites = new Dictionary<string, Sprite>();
        /// <summary>
        /// fast access sprite name to atlas collection
        /// </summary>
        private readonly Dictionary<string, SpriteAtlasInfo> _spritesToAtlases =
            new Dictionary<string, SpriteAtlasInfo>();

        // texture atlas name to texture2d
        private readonly Dictionary<string, Texture2D> _textures = new Dictionary<string, Texture2D>();
        private readonly Dictionary<string, Sprite> _sprites0 = new Dictionary<string, Sprite>();

        private SpriteAtlas[] _spriteAtlases;

        private bool _isInited;
        private string _folder;

        public Sprite SpriteTransparent;

        public int Count => _spritesToAtlases.Count;
        
        /// <summary>
        /// Singleton constructor
        /// </summary>
        public SpriteManager()
        {
            _instance = this;
        }

        /// <summary>
        /// Load sprite by name into image.sprite
        /// </summary>
        /// <param name="image">Image</param>
        /// <param name="spriteName">sprite name w/o any folder</param>
        public static void LoadFromAtlas(Image image, string spriteName)
        {
            Assert.IsNotNull(image);
            image.sprite = _instance.Get(spriteName);
        }
        public static void LoadFromResource(Image image, string spriteName)
        {
            image.sprite = Resources.Load<Sprite>(spriteName);
        }

        public static Sprite LoadFromAtlas(string spriteName)
        {
           return _instance.Get(spriteName);
        }

        /// <summary>
        /// Load all atlases from resources
        /// </summary>
        public SpriteManager Initialize(string folder)
        {
            if (_isInited)
            {
                Debug.LogError("SpriteManager already inited");
                return this;
            }
            _textures.Clear();
            _sprites0.Clear();
            var startTime = Time.realtimeSinceStartup;
            _folder = folder;
            Assert.IsFalse(string.IsNullOrEmpty(_folder));

            _spriteAtlases = Resources.LoadAll<SpriteAtlas>("");
            //string debug = "";
            foreach (SpriteAtlas spriteAtlas in _spriteAtlases)
            {
                CollectAtlasInfo(spriteAtlas);
            }

            Resources.UnloadUnusedAssets();

            _isInited = true;

            //Debug.Log("SpriteManager initialized with " + _spriteAtlases.Length + " atlases(sprites="+Count+"), inited in " + (Time.realtimeSinceStartup - startTime) + " seconds.");
            return this;
        }

        public Texture2D GetTexture(string atlasName)
        {
#if UNITY_EDITOR            
            if (!Application.isPlaying)
            {
                Packer.GetAtlasDataForSprite(_sprites0[atlasName], out var atlas, out var texture);
                return texture;
                //return SpriteUtility.GetSpriteTexture(_sprites0[atlasName], true);
            }
#endif            
            return _textures[atlasName];
        }
        
        // public Texture GetTexture(int atlasIndex = 0)
        // {
        //     SpriteAtlas first = _spriteAtlases[atlasIndex];
        //     var nameInfo = _spritesToAtlases.First(t => t.Value._atlas == first);
        //     var sprite = first.GetSprite(nameInfo.Key);
        //     // editor method - bad idea
        //     var texture2D = SpriteUtility.GetSpriteTexture(sprite, true);
        //     return texture2D;
        // }
        

        /// <summary>
        /// return sprite by sprite name from atlas
        /// </summary>
        /// <param name="spriteName">sprite name w/o any folder</param>
        /// <returns></returns>
        public Sprite Get(string spriteName)
        {
            if (string.IsNullOrEmpty(spriteName))
            {
                Debug.LogError("sprite is null or empty!");
                return default(Sprite);
            }
            SpriteAtlasInfo atlasInfo;
            Assert.IsNotNull(_spritesToAtlases);
            try
            {
                atlasInfo = _spritesToAtlases[spriteName];
            }
            catch (Exception e)
            {
                Debug.LogError("No atlas found for sprite \"" + spriteName + "\"! "+e);
                return default(Sprite);
            }

            Sprite sprite;

            if (!_sprites.TryGetValue(spriteName, out sprite))
            {
                sprite = atlasInfo.GetSprite(spriteName);
                _sprites.Add(spriteName, sprite);
            }

            return sprite;
        }

        /// <summary>
        /// Return array of Vector2[3] where
        /// [0] is position of sprite on atlas as percent
        /// [1] is size of sprite on atlas as percent
        /// [2] is sprite rect on atlas as pixels
        /// </summary>
        /// <param name="spriteName">sprite name w/o any folder</param>
        /// <returns></returns>
        public Vector2[] GetSpriteUV(string spriteName)
        {
            var sprite = Get(spriteName);
            var texture = sprite.texture;
            var spriteRect = sprite.textureRect;
            var textureSize = new Vector2(texture.width, texture.height);
            Assert.IsTrue(texture.width > 0 && texture.height > 0);
            if (!(spriteRect.size.x > 0 && spriteRect.size.y > 0))
                throw new UnityException("sprite size is 0, spriteName="+spriteName+", sprite="+sprite+", texture="+texture+", textureRect="+sprite.textureRect);
            
            var result = new[]
            {
                spriteRect.position / textureSize, 
                spriteRect.size / textureSize, 
                spriteRect.size
            };

            return result;
        }

        /// <summary>
        /// Clear atlases and sprites
        /// </summary>
        public void ClearCache()
        {
            var spritesBeforeCount = _sprites.Count;

            foreach (var info in _spritesToAtlases.Values)
                info.ClearAtlas();

            _sprites.Clear();

            Debug.Log("SpriteManager cleared " + spritesBeforeCount + " sprites.");
        }

        /// <summary>
        /// Fill collection by atlas sprites
        /// </summary>
        /// <param name="atlas"></param>
        private void CollectAtlasInfo(SpriteAtlas atlas)
        {
            var spriteClones = new Sprite[atlas.spriteCount];

            atlas.GetSprites(spriteClones);
            
            _textures.Add(atlas.name, spriteClones[0].texture);
            _sprites0.Add(atlas.name, spriteClones[0]);
                
            foreach (var spriteClone in spriteClones)
            {
                var spriteName = Regex.Replace(spriteClone.name, @"(\(Clone\))$", string.Empty);

                if (_spritesToAtlases.ContainsKey(spriteName))
                    Debug.LogError("SpriteManager already contains sprite named '" + spriteName +
                                   "'! Unique name required! Skipping it..., total="+ _spritesToAtlases.Count+", last="+_spritesToAtlases.Last().Key);
                else
                    _spritesToAtlases.Add(spriteName, new SpriteAtlasInfo(_folder+@"\"+atlas.name));
            }
        }

        /// <summary>
        /// atlas name to spriteatlas pair
        /// </summary>
        private class SpriteAtlasInfo
        {
            private readonly string _atlasName;
            public SpriteAtlas _atlas;
            
            /// <summary>
            /// init info w/o atlas loaded
            /// </summary>
            /// <param name="atlasName">atlas name</param>
            public SpriteAtlasInfo(string atlasName)
            {
                _atlasName = atlasName;
            }

            /// <summary>
            /// Return sprite by sprite name
            /// Load atlas on first call
            /// </summary>
            /// <param name="spriteName">sprite name w/o folders</param>
            /// <returns></returns>
            public Sprite GetSprite(string spriteName)
            {
                if (_atlas == null)
                {
                    _atlas = Resources.Load<SpriteAtlas>(_atlasName);
                    if (_atlas == null)
                    {
                        Debug.LogError("Cannot load atlas "+_atlasName+" when loading sprite "+spriteName);
                        return null;
                    }
                }
                Assert.IsNotNull(_atlas);
                return _atlas.GetSprite(spriteName);
            }

            /// <summary>
            /// Clear atlas ref
            /// </summary>
            public void ClearAtlas()
            {
                _atlas = null;
            }
        }
    }
}