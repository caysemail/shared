﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;

namespace Shared
{
    public class FindRandom
    {
        private readonly int _baseSeed;
        private int _localSeed = 0;

        public int LocalSeed => _localSeed;
        public int BaseSeed => _baseSeed;
        
        public int setLocalSeed
        {
            set => _localSeed = value;
        }
        
        public FindRandom(int baseSeedX, int baseSeedY)
        {
            _baseSeed = baseSeedX;
            setLocalSeed = RangeIncluded(0, baseSeedY);
        }
        
        public FindRandom(int baseSeed)
        {
            _baseSeed = baseSeed;
        }
        public FindRandom(long baseSeed)
        {
            _baseSeed = (int)baseSeed;
        }
        public FindRandom()
        {
            _baseSeed = (int)DateTime.Now.Ticks;
        }

        public bool GetBool()
        {
            UnityEngine.Random.InitState(_localSeed + _baseSeed);
            var seed2 = Random.Range(int.MinValue, int.MaxValue);
            Random.InitState(seed2);
            _localSeed++;
            return Random.value > 0.5f;
        }

        public float GetValue()
        {
            UnityEngine.Random.InitState(_localSeed + _baseSeed);
            var seed2 = Random.Range(int.MinValue, int.MaxValue);
            Random.InitState(seed2);
            _localSeed++;
            return Random.value;
        }

        public T GetElementFromEnum<T>()
        {
            var typeValues = Enum.GetValues(typeof(T));
            var typeValuesLength = typeValues.Length;
            var randomType = (T)typeValues.GetValue(Range(0,typeValuesLength));
            return randomType;
        }
        
        public Vector2 GetVector2Value()
        {
            return new Vector2(GetValue(), GetValue());
        }
        
        public Vector3 GetPosition(Bounds bounds)
        {
            return new Vector3(
                Range(bounds.min.x, bounds.max.x),
                Range(bounds.min.y, bounds.max.y),
                Range(bounds.min.z, bounds.max.z)
            );
        }
        
        public Vector2Int GetPosition(Vector2Int size)
        {
            return new Vector2Int(Range(0, size.x),Range(0, size.y));
        }
       
        public Vector2 GetPositionInCircle(float radiusIn, float radiusOut)
        {
            var v = GetValueInCircle(1f);
            return v.normalized * radiusIn + v*(radiusOut - radiusIn);
        }

        public Vector3 GetPositionFromMesh(MeshFilter mf)
        {
            var vertices = mf.sharedMesh.vertices;
            var point = GetElement(vertices);
            var t = mf.transform;
            return t.TransformPoint(point);
            //point = t.rotation* point;
            //return point + t.position;
        }
        
        public Vector3 GetPosition(MeshCollider collider)
        {
            var randomInBounds = GetPosition(collider.bounds);
            //var center = collider.transform.position;
            //var extents = collider.bounds.extents;
            //var radius = (extents.x + extents.y + extents.z);
            //var randomPointInSphere = GetValueInSphere(radius);
            var result = collider.ClosestPoint(randomInBounds);
            return result;
        }

        public Vector3 GetValueInRadius(float circleRadius)
        {
            var angle = Range(-Mathf.PI, Mathf.PI);
            return circleRadius * new Vector3(MathF.Sin(angle), 0, Mathf.Cos(angle));
        }
            
        public Vector3 GetValueInSphere(float sphereRadius)
        {
            UnityEngine.Random.InitState(_localSeed + _baseSeed);
            var seed2 = Random.Range(int.MinValue, int.MaxValue);
            Random.InitState(seed2);
            _localSeed++;
            return Random.insideUnitSphere*sphereRadius;
        }
        
        public Vector2 GetValueInCircle(float radius)
        {
            UnityEngine.Random.InitState(_localSeed + _baseSeed);
            var seed2 = Random.Range(int.MinValue, int.MaxValue);
            Random.InitState(seed2);
            _localSeed++;
            return Random.insideUnitCircle*radius;
        }

        public Vector3 GetDirectionXZ()
        {
            UnityEngine.Random.InitState(_localSeed + _baseSeed);
            var seed2 = Random.Range(int.MinValue, int.MaxValue);
            Random.InitState(seed2);
            _localSeed++;
            float x = (Random.value > 0.5f ? +1 : -1) * Random.Range(0.001f, 1f);
            float z = (Random.value > 0.5f ? +1 : -1) * Random.Range(0.001f, 1f);
            return new Vector2(x, z).normalized.ToVector3XZ();
        }

        public Vector2 GetDirection2d()
        {
            UnityEngine.Random.InitState(_localSeed + _baseSeed);
            var seed2 = Random.Range(int.MinValue, int.MaxValue);
            Random.InitState(seed2);
            _localSeed++;
            float x = (Random.value > 0.5f ? +1 : -1) * Random.Range(0.001f, 1f);
            float y = (Random.value > 0.5f ? +1 : -1) * Random.Range(0.001f, 1f);
            return new Vector2(x, y).normalized;
        }

        public T GetElement<T> (List<T> list)// where T : Object
        {
            Assert2.IsNotNull(list);
            Assert.IsTrue(list.Count > 0);
            return list[Range(0, list.Count)];
        }
        
        public T GetElementFast<T> (List<T> list)// where T : Object
        {
            return list[Random.Range(0, list.Count)];
        }

        public T GetElement<T>(List<(T, float)> list) // where T : Object
        {
            float maxValue = list.Sum(t => t.Item2);
            if (maxValue <= 0)
            {
                //Debug.LogError("maxValue="+maxValue);
                return default(T);
            }
            var randomValue = GetValue();
            float summChance = 0f;
            foreach (var tuple in list)
            {
                summChance += tuple.Item2 / maxValue;
                if (randomValue <= summChance)
                    return tuple.Item1;
            }
            Debug.LogError("maxValue="+maxValue+", randomValue="+randomValue+", summChance="+summChance);
            return default(T);
        }
        
        public T GetElementWeightened<T>(List<(T, float)> list) // where T : Object
        {
            var randomValue = GetValue();
            float summChance = 0f;
            foreach (var tuple in list)
            {
                summChance += tuple.Item2;
                if (randomValue <= summChance)
                    return tuple.Item1;
            }
            //Debug.LogError("randomValue="+randomValue+", summChance="+summChance);
            return default(T);
        }


        // https://stackoverflow.com/questions/273313/randomize-a-listt
        public void Randomize<T> (List<T> list)// where T : Object
        {
            Assert.IsNotNull(list);
            Assert.IsTrue(list.Count > 0);
            int n = list.Count;
            while (n > 1)
            {
                n--;
                //int k = ThreadSafeRandom.ThisThreadsRandom.Next(n + 1);
                int k = RangeIncluded(0, n);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
        
        public T GetElement<T>(T[] array)
        {
            Assert.IsNotNull(array);
            Assert.IsTrue(array.Length > 0);
            return array[Range(0, array.Length)];
        }

        public Vector2Int Range(Vector2Int size)
        {
            return new Vector2Int(Range(0, size.x), Range(0, size.y));
        }
        
        public int RangeIncluded(int minIncluded, int maxIncluded)
        {
            UnityEngine.Random.InitState(_localSeed + _baseSeed);
            var seed2 = Random.Range(int.MinValue, int.MaxValue);
            Random.InitState(seed2);
            //UnityEngine.Debug.LogError("Random _localSeed="+ _localSeed+ ", _baseSeed="+ _baseSeed+", seed2=" + seed2);
            _localSeed++;
            return Random.Range(minIncluded, maxIncluded+1);
        }
       
        public int RangeIncluded(Vector2Int minMax)
        {
            return RangeIncluded(minMax.x, minMax.y);
        }

        public float RangeIncluded(Vector2 minMax)
        {
            return Range(minMax.x, minMax.y);
        }

        public int Range(int minIncluded, int maxNotIncluded)
        {
            UnityEngine.Random.InitState(_localSeed + _baseSeed);
            var seed2 = Random.Range(int.MinValue, int.MaxValue);
            Random.InitState(seed2);
            //UnityEngine.Debug.LogError("Random _localSeed="+ _localSeed+ ", _baseSeed="+ _baseSeed+", seed2=" + seed2);
            _localSeed++;
            return Random.Range(minIncluded, maxNotIncluded);
        }
        
        public float Range(float minIncluded, float maxIncluded)
        {
            UnityEngine.Random.InitState(_localSeed + _baseSeed);
            var seed2 = Random.Range(int.MinValue, int.MaxValue);
            Random.InitState(seed2);
            _localSeed++;
            return Random.Range(minIncluded, maxIncluded);
        }

        public Color ColorMinMax(float min, float max)
        {
            //var max = 0.99f;
            //var min = 0.55f;
            float r = Range(min, max);  
            float g = Range(min, max);  
            float b = Range(min, max);
            return new Color(r, g, b, 1f);
        }
        
        // public Color ColorDark()
        // {
        //     var max = 0.45f;
        //     var min = 0.1f;
        //     float r = Range(min, max);  
        //     float g = Range(min, max);  
        //     float b = Range(min, max);
        //     return new Color(r, g, b, 1f);
        // }

        public Color RandomizeColor(Color color, float range)
        {
            color.r = color.r + Range(-range, +range);
            color.g = color.g + Range(-range, +range);
            color.b = color.b + Range(-range, +range);
            return color;
        }
        
        public Color IdToColorNotGray(int id)
        {
            UnityEngine.Random.InitState(id+_baseSeed);
            var seed2 = Random.Range(int.MinValue, int.MaxValue);
            Random.InitState(seed2);
            var max = 0.6f;
            var min = 0.4f;
            float r = Random.value > 0.5f ? Random.Range(max, 0.9f) : Random.Range(0.1f, min);  
            float g = Random.value > 0.5f ? Random.Range(max, 0.9f) : Random.Range(0.1f, min);  
            float b = Random.value > 0.5f ? Random.Range(max, 0.9f) : Random.Range(0.1f, min);  
            var color = new Color(r,g,b);
            return color;
        }
        
        public Color IdToColorNotGray2(int id)
        {
            UnityEngine.Random.InitState(id+_baseSeed);
            var seed2 = Random.Range(int.MinValue, int.MaxValue);
            Random.InitState(seed2);
            // 0-0.3 0.3-0.6 0.6-1
            List<float> values = new List<float>()
            {
                Random.Range(0, 0.3f), Random.Range(0.3f, 0.6f), Random.Range(0.6f, 1.0f),
            };
            Randomize(values);
            var color = new Color(values[0],values[1],values[2], 1f);
            return color;
        }

        public Color IdToColor(int id)
        {
            UnityEngine.Random.InitState(id+_baseSeed);
            var seed2 = Random.Range(int.MinValue, int.MaxValue);
            Random.InitState(seed2);
            var color = new Color(Random.Range(0.1f, 1f), Random.Range(0.1f, 1f), Random.Range(0.1f, 1f));
            var t = 0.3f;
            if (color.r < t && color.g < t && color.b < t)
                color = new Color(Random.Range(t, 1f), Random.Range(t, 1f), Random.Range(t, 1f));
            return color;
        }
    }
}