﻿
using System;
using UnityEngine;

namespace Shared
{
    [Serializable]
    public class FollowCamera
    {
        [SerializeField]
        private Camera _camera;
        [SerializeField]
        private Transform _target;
        [SerializeField]
        private float _speed;
        public FollowCamera SetCamera(Camera camera)
        {
            _camera = camera;
            return this;
        }

        public FollowCamera SetFollow(Transform target)
        {
            _target = target;
            return this;
        }

        public FollowCamera SetFollow(GameObject target)
        {
            _target = target.transform;
            return this;
        }

        public FollowCamera SetSpeed(float speed)
        {
            _speed = speed;
            return this;
        }

        public void FixedUpdate()
        {
            Vector3 desiredPosition = _target.position;// + offset;
            Vector3 smoothedPosition = Vector3.Lerp(_camera.transform.position, desiredPosition, _speed);
            _camera.transform.position = smoothedPosition;

            _camera.transform.LookAt(_target);
        }
    }
}