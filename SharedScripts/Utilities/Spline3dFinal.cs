﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    // https://github.com/riven8192/LibBase/blob/a70af645e9b35df824b9214b0ef2749bfb2b5df0/src/craterstudio/math/Spline3D.java
    public class Spline3dFinal
    {
        class CacheItem
        {
            public CacheItem(float xpos, float ypos, float zpos)
            {
                this.xpos = xpos;
                this.ypos = ypos;
                this.zpos = zpos;
            }

            public float position;
            public float xpos, ypos, zpos;
            public float travelled;
        }

        readonly int count;
        private readonly Cubic[] x, y, z;
        public float[] PosCoeff;

        public float GetCoeff(float pos)
        {
            return PosCoeff[(int) pos];
        }

        public Spline3dFinal(List<Vector3> points)
        {
            count = points.Count;

            float[] x = new float[count];
            float[] y = new float[count];
            float[] z = new float[count];

            PosCoeff = new float[count];

            for (int i = 0; i < count; i++)
            {
                x[i] = points[i].x;
                y[i] = points[i].y;
                z[i] = points[i].z;
                if (i < count-1)
                    PosCoeff[i] = 1f / Vector3.Distance(points[i], points[i+1]);
                else
                    PosCoeff[i] = PosCoeff[i-1];
            }

            this.x = Curve.calcCurve(count - 1, x);
            this.y = Curve.calcCurve(count - 1, y);
            this.z = Curve.calcCurve(count - 1, z);
        }

        public Spline3dFinal(List<Transform> points)
        {
            count = points.Count;

            float[] x = new float[count];
            float[] y = new float[count];
            float[] z = new float[count];

            for (int i = 0; i < count; i++)
            {
                x[i] = points[i].position.x;
                y[i] = points[i].position.y;
                z[i] = points[i].position.z;
            }

            this.x = Curve.calcCurve(count - 1, x);
            this.y = Curve.calcCurve(count - 1, y);
            this.z = Curve.calcCurve(count - 1, z);
        }

        public Spline3dFinal(float[][] points)
        {
            count = points.Length;

            float[] x = new float[count];
            float[] y = new float[count];
            float[] z = new float[count];

            for (int i = 0; i < count; i++)
            {
                x[i] = points[i][0];
                y[i] = points[i][1];
                z[i] = points[i][2];
            }

            this.x = Curve.calcCurve(count - 1, x);
            this.y = Curve.calcCurve(count - 1, y);
            this.z = Curve.calcCurve(count - 1, z);
        }

        /**
	    * POINT COUNT
	    */

        public int pointCount()
        {
            return count;
        }


        /**
         * POSITION
         */

        public float[] getPositionAt(float param)
        {
            float[] v = new float[3];
            this.getPositionAt(param, v);
            return v;
        }

        public Vector3 GetPositionAt(float param)
        {
            float[] v = new float[3];
            this.getPositionAt(param, v);
            return new Vector3(v[0], v[1], v[2]);
        }

        public static double MathDoubleUlp(double value)
        {
            // This is actually a constant in the same static class as this method, but 
            // we put it here for brevity of this example.
            const double MaxULP = 1.9958403095347198116563727130368E+292;

            if (Double.IsNaN(value))
            {
                return Double.NaN;
            }
            else if (Double.IsPositiveInfinity(value) || Double.IsNegativeInfinity(value))
            {
                return Double.PositiveInfinity;
            }
            else if (value == 0.0)
            {
                return Double.Epsilon;    // Equivalent of Double.MIN_VALUE in Java; Double.MinValue in C# is the actual minimum value a double can hold.
            }
            else if (Math.Abs(value) == Double.MaxValue)
            {
                return MaxULP;
            }

            // All you need to understand about DoubleInfo is that it's a helper struct 
            // that provides more functionality than is used here, but in this situation, 
            // we only use the `Bits` property, which is just the double converted into a 
            // long.
            //DoubleInfo info = new DoubleInfo(value);
            long bits = BitConverter.DoubleToInt64Bits(value);

            // This is safe because we already checked for value == Double.MaxValue.
            return Math.Abs(BitConverter.Int64BitsToDouble(bits + 1) - value);
        }

        public void getPositionAt(float param, float[] result)
        {
            // clamp
            if (param < 0.0f)
                param = 0.0f;
            if (param >= count - 1)
                param = (count - 1) - (float)MathDoubleUlp(count - 1);

            // split
            if (param >= x.Length)
                param = x.Length - 0.00001f;
            int ti = (int)param;
            float tf = param - ti;

            result[0] = 0;
            result[1] = 0;
            result[2] = 0;
            // eval
            if (ti < x.Length)
                result[0] = x[ti].eval(tf);
            else Debug.LogError("ti="+ti+" !< "+x.Length+", param="+param+", maxCount="+pointCount());
            if (ti < y.Length)
                result[1] = y[ti].eval(tf);
            else Debug.LogError("ti=" + ti + " !< " + y.Length + ", param=" + param+", maxCount="+pointCount());
            if (ti < z.Length)
                result[2] = z[ti].eval(tf);
            else Debug.LogError("ti=" + ti + " !< " + z.Length + ", param=" + param+", maxCount="+pointCount());
        }

        private List<CacheItem> travelCache;
        private float maxTravelStep;
        private float posStep;

        public void enabledTripCaching(float maxTravelStep, float posStep)
        {
            this.maxTravelStep = maxTravelStep;
            this.posStep = posStep;

            float x = this.x[0].eval(0.0f);
            float y = this.y[0].eval(0.0f);
            float z = this.z[0].eval(0.0f);

            this.travelCache = new List<CacheItem>();
            this.travelCache.Add(new CacheItem(x, y, z));
        }

        public float[] getTripPosition(float totalTrip)
        {
            CacheItem last = this.travelCache[this.travelCache.Count - 1];

            // build cache
            while (last.travelled < totalTrip)
            {
                if (totalTrip == 0.0f)
                {
                    // don't even bother
                    break;
                }

                float travel = Math.Min(totalTrip - last.travelled, maxTravelStep);

                CacheItem curr = this.getSteppingPosition(last.position, travel,
                        posStep);

                if (curr.position >= this.count)
                {
                    // reached end of spline
                    break;
                }

                // only cache if we travelled far enough
                if (curr.travelled > this.maxTravelStep * 0.95f)
                {
                    this.travelCache.Add(curr);
                }

                curr.travelled += last.travelled;

                last = curr;
            }

            // figure out position

            int lo = 0;
            int hi = this.travelCache.Count - 1;

            while (true)
            {
                int mid = (lo + hi) / 2;

                last = this.travelCache[mid];

                if (last.travelled < totalTrip)
                {
                    if (lo == mid)
                        break;
                    lo = mid;
                }
                else
                {
                    if (hi == mid)
                        break;
                    hi = mid;
                }
            }

            for (int i = lo; i <= hi; i++)
            {
                CacheItem item = this.travelCache[i];

                if (item.travelled <= totalTrip)
                {
                    last = item;
                }
                else
                {
                    break;
                }
            }

            float travelToLast = totalTrip - last.travelled;
            last = this.getSteppingPosition(last.position, travelToLast, posStep);
            return new float[] { last.xpos, last.ypos };
        }

        private CacheItem getSteppingPosition(float posOffset, float travel,
                float segmentStep)
        {
            float pos = posOffset;
            float[] last = this.getPositionAt(pos);

            float travelled = 0.0f;

            while (travelled < travel && pos < this.count)
            {
                float[] curr = this.getPositionAt(pos += segmentStep);
                travelled += dist(last, curr);
                last = curr;
            }

            CacheItem item = new CacheItem(last[0], last[1], last[2]);
            item.position = pos;
            item.travelled = travelled;
            return item;
        }

        private static float dist(float[] a, float[] b)
        {
            float dx = b[0] - a[0];
            float dy = b[1] - a[1];
            float dz = b[2] - a[2];

            return (float)Math.Sqrt(dx * dx + dy * dy + dz * dz);
        }

        /**
         * CURVE CLASS
         */
        private static class Curve
        {
            public static Cubic[] calcCurve(int n, float[] axis)
            {
                float[] gamma = new float[n + 1];
                float[] delta = new float[n + 1];
                float[] d = new float[n + 1];
                Cubic[] c = new Cubic[n];

                // gamma
                gamma[0] = 0.5F;
                for (int i = 1; i < n; i++)
                    gamma[i] = 1.0F / (4.0F - gamma[i - 1]);
                gamma[n] = 1.0F / (2.0F - gamma[n - 1]);

                // delta
                delta[0] = 3.0F * (axis[1] - axis[0]) * gamma[0];
                for (int i = 1; i < n; i++)
                    delta[i] = (3.0F * (axis[i + 1] - axis[i - 1]) - delta[i - 1])
                               * gamma[i];
                delta[n] = (3.0F * (axis[n] - axis[n - 1]) - delta[n - 1])
                           * gamma[n];

                // d
                d[n] = delta[n];
                for (int i = n - 1; i >= 0; i--)
                    d[i] = delta[i] - gamma[i] * d[i + 1];

                // c
                for (int i = 0; i < n; i++)
                {
                    float x0 = axis[i];
                    float x1 = axis[i + 1];
                    float d0 = d[i];
                    float d1 = d[i + 1];
                    c[i] = new Cubic(x0, d0, 3.0F * (x1 - x0) - 2.0F * d0 - d1,
                        2.0F * (x0 - x1) + d0 + d1);
                }
                return c;
            }
        }

        /**
        * CUBIC CLASS
        */
        class Cubic
        {
            private float a, b, c, d;

            public Cubic(float a, float b, float c, float d)
            {
                this.a = a;
                this.b = b;
                this.c = c;
                this.d = d;
            }

            public float eval(float u)
            {
                return (((d * u) + c) * u + b) * u + a;
            }
        }
    }
}