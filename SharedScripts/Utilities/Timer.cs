using System.Diagnostics;
using System.Globalization;
using UnityEngine;

namespace Shared
{
    public class FpsTimer
    {
        private Shared.Timer timer = new Timer();
        private Shared.Timer timerInput = new Timer();
        private Shared.Timer timerOutput = new Timer();
        public Vector2 fpsInputOutput;

        public bool InProgress(float targetFps)
        {
            fpsInputOutput.x = 1f / timerInput.Restart();
            
            float time = timer.GetElapsed();
            var fps = 1 / time;
            if (fps > targetFps)
                return true;
            timer.Restart();
            
            fpsInputOutput.y = 1f / timerOutput.Restart();

            return false;
        }
    }
    
    public class Timer
    {
        private double _timeStart;
        private Stopwatch _watch;
        public float elapsed;
        public float total;

        public static Timer StartTimer()
        {
            var timer = new Timer();
            return timer;
        }
        
        public Timer()
        {
            total = 0;
            Start();
        }

        public Timer Start()
        {
#if UNITY_EDITOR
            if (UnityEditor.EditorApplication.isPlaying)
            {
                _watch = new Stopwatch();
                _watch.Start();
                
            }
#endif
            else if (Application.isPlaying)
            {
                _watch = new Stopwatch();
                _watch.Start();
            }
            else
            {
#if UNITY_EDITOR
                _timeStart = UnityEditor.EditorApplication.timeSinceStartup;
#endif
            }

            return this;
        }

        public float Restart()
        {
            var value = Stop();
            Start();
            return value;
        }

        public float GetElapsed()
        {
            elapsed = 0;
            if (Application.isPlaying)
            {
                elapsed = _watch.ElapsedMilliseconds / 1000f;
            }
            else
            {
#if UNITY_EDITOR
                elapsed = (float) (UnityEditor.EditorApplication.timeSinceStartup - _timeStart);
#endif
            }

            return elapsed;
            
        }

        public string StopToString()
        {
            var stopTime = Stop();
            return "(timer: "+stopTime + " sec.)";
        }

        public float Stop()
        {
            elapsed = 0;
            if (Application.isPlaying)
            {
                _watch.Stop();
                elapsed = _watch.ElapsedMilliseconds / 1000f;
            }
            else
            {
#if UNITY_EDITOR
                elapsed = (float) (UnityEditor.EditorApplication.timeSinceStartup - _timeStart);
#endif
            }

            total += elapsed;

            return elapsed;
        }

        public override string ToString()
        {
            return elapsed.ToString(CultureInfo.InvariantCulture)+" sec.";
        }
    }
}