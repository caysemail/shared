using System;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Shared
{
    public static class AddressableExt
    {
        public static async UniTask<T> Instantiate<T>(Transform parent, string path)
        {
            T result = default;
            if (string.IsNullOrWhiteSpace(path)) return result;
            AsyncOperationHandle<T> handle = default;
            try
            {
                handle = Addressables.LoadAssetAsync<T>(path);
                await UniTask.WaitUntil(() => handle.IsDone);
                if (handle.Status == AsyncOperationStatus.Succeeded)
                {
                    result = handle.Result;
                }
                else throw new UnityException("handle.Status="+handle.Status);
            }
            catch (Exception e)
            {
                Debug.LogError("fail to load " + path + ", handle=" + handle+", exception="+e);
            }
            finally
            {
                Addressables.Release(handle);
            }

            return result;
        }
        
        public static async UniTask<GameObject> InstantiatePrefab(Transform parent, string path)
        {
            GameObject go = null;
            if (string.IsNullOrWhiteSpace(path)) return null;
            AsyncOperationHandle<GameObject> handle = default;
            try
            {
                handle = Addressables.LoadAssetAsync<GameObject>(path);
                await UniTask.WaitUntil(() => handle.IsDone);
                if (handle.Status == AsyncOperationStatus.Succeeded)
                {
                    go = GameObject.Instantiate(handle.Result, parent, false);
                    //material.mainTexture = handle.Result;
                }
                else throw new UnityException("handle.Status="+handle.Status);
            }
            catch (Exception e)
            {
                Debug.LogError("fail to load texture=" + path + ", handle=" + handle+", exception="+e);
            }
            finally
            {
                Addressables.Release(handle);
            }

            return go;
        }
        
        public static async UniTask LoadTexture(string path, Material material)
        {
            if (string.IsNullOrWhiteSpace(path)) return;
            
            AsyncOperationHandle<Texture2D> handle = default;
            try
            {
                handle = Addressables.LoadAssetAsync<Texture2D>(path);
                await UniTask.WaitUntil(() => handle.IsDone);
                if (handle.Status == AsyncOperationStatus.Succeeded)
                {
                    material.mainTexture = handle.Result;
                }
                else throw new UnityException("handle.Status="+handle.Status);
            }
            catch (Exception e)
            {
                Debug.LogError("fail to load texture=" + path + ", handle=" + handle+", exception="+e);
            }
            finally
            {
                Addressables.Release(handle);
            }
        }
        
        public static async UniTask<Texture2D> LoadTexture2D(string path)
        {
            Texture2D texture = null;
            if (string.IsNullOrWhiteSpace(path)) return null;

            // if (!Application.isPlaying)
            // {
            //     texture = AssetDatabase.LoadAssetAtPath<Texture2D>(path);
            //     return texture;
            // }
            
            AsyncOperationHandle<Texture2D> handle = default;
            try
            {
                handle = Addressables.LoadAssetAsync<Texture2D>(path);
                //await handle.Task;
                await UniTask.WaitUntil(() => handle.IsDone);
                if (handle.Status == AsyncOperationStatus.Succeeded)
                {
                    texture = handle.Result;
                }
                else throw new UnityException("handle.Status="+handle.Status);
            }
            catch (Exception e)
            {
                Debug.LogError("fail to load texture=" + path + ", handle=" + handle+", exception="+e);
            }
            finally
            {
                Addressables.Release(handle);
            }
            return texture;
        }
    }
}