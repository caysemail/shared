﻿using System;
using Shared;
using UnityEngine;
using UnityEngine.Rendering;

public class MeshVolumeBase : MonoBehaviour
{
    [NonSerialized] public Mesh _mesh;
    [ReadOnly] public MeshSection _section;
    public bool IsMeshDynamic;
    protected bool IsComplete = true;
    public bool IsCollider = false;

    public void Initialize()
    {
        //if (_mesh == null)
            _mesh = new Mesh();
        _mesh.Clear();
        if (IsMeshDynamic)
            _mesh.MarkDynamic();

        //if (_section == null)
        _section = new MeshSection();
        

        var meshFilter = GetComponent<MeshFilter>();
        if (meshFilter)
            meshFilter.mesh = _mesh;

        var collider = GetComponent<MeshCollider>();
        if (IsCollider && collider)
            collider.sharedMesh = _mesh;
        //_negZCounter = 0;
    }
    
    public void Apply(bool isQuads = true)
    {
        //IsUseSides = false;
        var log = new StopWatchLog("Clear=");

        _mesh.Clear();
        if (IsMeshDynamic)
            _mesh.MarkDynamic();
        _mesh.indexFormat = IndexFormat.UInt32;

        log.NewLog("SetVertices=");

        _mesh.SetVertices(_section.Vertices);

        log.NewLog("SetIndices=");

        if (_section.Indices.Count > 0)
            _mesh.SetIndices(_section.Indices.ToArray(),
                isQuads ? MeshTopology.Quads : MeshTopology.Triangles,
                0);

        if (_section.IndicesMore.Count > 0)
        {
            _mesh.subMeshCount = _section.IndicesMore.Count+1;
            for (var index = 0; index < _section.IndicesMore.Count; index++)
            {
                var indices = _section.IndicesMore[index];
                _mesh.SetIndices(indices.ToArray(),
                    isQuads ? MeshTopology.Quads : MeshTopology.Triangles,
                    index+1);
            }
        }

        log.NewLog("SetColors=");

        _mesh.SetColors(_section.Colors);

        if (_section.Uvs0V.Count > 0)
        {
            log.NewLog("Uvs0V+N=");
            _mesh.SetUVs(0, _section.Uvs0V);
            if (_section.Uvs1V.Count > 0 && _section.Uvs1V.Count != _section.Vertices.Count)
                Debug.LogError("uvs2="+_section.Uvs1V.Count+" vs vertices="+_section.Vertices.Count);
            if (_section.Uvs1V.Count > 0) _mesh.SetUVs(1, _section.Uvs1V);
            if (_section.Uvs2V.Count > 0) _mesh.SetUVs(2, _section.Uvs2V);
            if (_section.Uvs3V.Count > 0)
                if (_section.Uvs3V.Count == _section.Vertices.Count) _mesh.SetUVs(3, _section.Uvs3V);
            else Debug.LogError("Uvs3("+_section.Uvs3V.Count+") != Vertices("+_section.Vertices.Count+")");
            _mesh.RecalculateNormals();
            _mesh.RecalculateTangents();
        }
        else
        {
            bool isUv0 = _section.Uv0.Count > 0;
            bool isUv1 = _section.Uv1.Count > 0;
            log.NewLog(
                (isUv0?" UV0("+_section.Uv0.Count+")":"")
                +(isUv1?" UV1("+_section.Uv1.Count+")":"")
                +" + N/T=");
            if (_section.Uv0.Count != _section.Vertices.Count)
            {
                Debug.LogError("Uv0="+_section.Uv0.Count+" != Vertices="+ _section.Vertices.Count);
            }
            _mesh.SetUVs(0, _section.Uv0);
            if (_section.Uv1.Count > 0 && _section.Uv1.Count != _section.Vertices.Count)
                Debug.LogError("uvs2="+_section.Uv1.Count+" vs vertices="+_section.Vertices.Count);
            if (_section.Uv1.Count > 0) _mesh.SetUVs(1, _section.Uv1);
            //if (_section.Uvs3.Count > 0) _mesh.SetUVs(2, _section.Uvs3);
            _mesh.RecalculateNormals();
            _mesh.RecalculateTangents();
        }

        //var meshFilter = GetComponent<MeshFilter>();
        //if (meshFilter)
        //    meshFilter.mesh = _mesh;
        var collider = GetComponent<MeshCollider>();
        if (IsCollider && collider != null && collider.enabled)
        {
            log.NewLog("SetCollider=");
            collider.sharedMesh = _mesh;
        }

        var skin = GetComponent<SkinnedMeshRenderer>();
        if (skin && skin.enabled)
            skin.sharedMesh = _mesh;

        log.NewLog();
        Debug.Log(name+" MeshVolume.Apply: "+log);
    }
}

