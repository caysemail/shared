﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Rendering;
using Vector2 = UnityEngine.Vector2;

//public enum UVs { UV0, UV1, UV2, UV3 }
namespace Shared
{
    public class MeshSectionHex
    {
        private MeshSection _section;
        private bool _isInited;
        
        public static List<Vector2> edgesV2;
        public static List<Vector3> edges;
        
        public MeshSectionHex(MeshSection section)
        {
            _section = section;
            _isInited = false;
        }

        public void Initialize()
        {
            PrepareEdges();
            _isInited = true;
        }
        
        protected void PrepareEdges()
        {
            edgesV2 = HexagonGenerator.CreateHexagon(Vector2.zero, 1f).ToList();
            Assert.IsTrue(edgesV2.Count == 6, "edgesV2.Count="+edgesV2.Count);
            edges = new List<Vector3>();
            for (int i = 0; i < edgesV2.Count; i++)
            {
                edgesV2[i] = new Vector2(edgesV2[i].x.Cut3(), edgesV2[i].y.Cut3());
                edges.Add(new Vector3(edgesV2[i].x, 0, edgesV2[i].y));
            }
        }
        
        public void Create7(Vector3 worldPosition, float size)
        {
            if (!_isInited) Initialize();
            
            List<Vector3> points = new List<Vector3>();
            foreach (var edgePosition in edges)
            {
                var point = worldPosition + edgePosition * size;
                points.Add(point);
            }
            _section.Vertices.AddRange(points);

            List<int> indices = new List<int>();
            for (var index0 = 0; index0 < points.Count-1; index0++)
            {
                var index1 = index0 +1 >= points.Count-1 ? 0 : index0+1;
                var center = points.Count - 1;
                indices.Add(index0);
                indices.Add(index1);
                indices.Add(center);
            }
            foreach (var item in indices)
            {
                _section.Indices.Add(_section.indicesCount+item);
            }
            _section.indicesCount += points.Count;
        }
    }
    
    public class MeshSection
    {
        public int indicesCount;
        public int vertexVisibleCount;
        
        public List<Vector3> Vertices = new List<Vector3>();
        //public List<Vector3> VerticesUsed = new List<Vector3>();
        public List<Color32> Colors = new List<Color32>();
        public List<int> Indices = new List<int>();
        public List<List<int>> IndicesMore = new List<List<int>>();
        public List<Vector2> Uv0 = new List<Vector2>();
        public List<Vector2> Uv1 = new List<Vector2>();
        public List<Vector2> Uv2 = new List<Vector2>();
        public List<Vector2> Uvs3 = new List<Vector2>();

        public List<Vector4> Uvs0V = new List<Vector4>();
        public List<Vector4> Uvs1V = new List<Vector4>();
        public List<Vector4> Uvs2V = new List<Vector4>();
        public List<Vector4> Uvs3V = new List<Vector4>();

        private IntVector3 _debugCounter = new IntVector3(0, 0, 0);

        [NonSerialized] public Mesh mesh;
        [NonSerialized] public MeshFilter mf;
        [NonSerialized] public MeshRenderer mr;
        [NonSerialized] public Material material;
        [NonSerialized] public MeshSectionHex hex;
        [NonSerialized] public Color color;

        public MeshSection()
        {
            hex = new MeshSectionHex(this);
        }

        public MeshSection SetNewMesh()
        {
            mesh = new Mesh();
            return this;
        }

        public void Clear()
        {
            Vertices.Clear();
            Colors.Clear();
            Indices.Clear();
            IndicesMore.Clear();
            Uv0.Clear();
            Uv1.Clear();
            Uv2.Clear();
            indicesCount = 0;
        }

        public int GetOrSetIndexAndVertexAndUv0AndColor(Vector3 vertex)
        {
            for (var index = 0; index < Vertices.Count; index++)
            {
                var tempVertex = Vertices[index];
                if (tempVertex == vertex) return index;
            }
            Vertices.Add(vertex);
            Uv0.Add(Vector2.zero);
            Colors.Add(new Color32(255,255,255,255));
            return Vertices.Count - 1;
        }
        
        public MeshSection ReAsSelf(Transform transform, bool simpleMr = false)
        {
            mf = transform.GetOrAddComponent<MeshFilter>();
            mr = transform.GetOrAddComponent<MeshRenderer>();
            mesh = new Mesh();
            mf.mesh = mesh;
            if (simpleMr)
            {
                MakeSimpleMeshRenderer();
            }
            return this;
        }
        
        public MeshSection ReInstantiateAsChild(Transform transform, string childName, bool simpleMr = false)
        {
            //transform.DetachChild(childName);
            var child = transform.ReInstantiateChild(childName);
            mf = child.GetOrAddComponent<MeshFilter>();
            mr = child.GetOrAddComponent<MeshRenderer>();
            mesh = new Mesh();
            mf.mesh = mesh;
            if (simpleMr)
            {
                MakeSimpleMeshRenderer();
            }
            return this;
        }

        public void MakeSimpleMeshRenderer()
        {
            mr.shadowCastingMode = ShadowCastingMode.Off;
            mr.receiveShadows = false;
            mr.lightProbeUsage = LightProbeUsage.Off;
            mr.reflectionProbeUsage = ReflectionProbeUsage.Off;
            mr.allowOcclusionWhenDynamic = false;
            mr.motionVectorGenerationMode = MotionVectorGenerationMode.ForceNoMotion;
        }
            
        public void ApplyTriangles(Mesh mesh)
        {
            Apply(mesh, false);
        }

        public void ApplyQuads(Mesh mesh)
        {
            Apply(mesh, true);
        }
        
        public struct ApplyMask
        {
            public bool quads;
            public bool clear;
            public bool vertices;
            public bool indices;
            public bool indicesMore;
            public bool colors;
            public bool uv0;
            public bool uv1;
            public bool uv2;
            public bool normalsAndTangents;
            public bool log;
            public bool lines;
            public int submesh;
            public bool material;
            public bool test;
            public bool markDynamic;
        }

        public void Apply(ApplyMask mask)
        {
            Apply(mesh, mask);
        }

        public void Apply(Mesh mesh, ApplyMask mask)
        {
            Assert.IsNotNull(mesh);
            if (mask.log)
            {
                Debug.Log("Apply: vertices="+Vertices.Count+", indices="+Indices.Count+", submesh="+mask.submesh+", indicesMore="+mask.indicesMore
                +", IndicesMore="+IndicesMore.Count+", indices="+mask.indices+", colors="+Colors.Count);
            }

            if (mask.material)
                mr.material = material;

            if (mask.clear)
                mesh.Clear();

            if (mask.markDynamic)
                mesh.MarkDynamic();
            
            if (mask.vertices)
                mesh.SetVertices(Vertices);

            if (mask.indices)
            {
                mesh.indexFormat = IndexFormat.UInt32;
                if (mask.test)
                {
                    if (!mask.quads && !mask.lines && Indices.Count%3 != 0)
                        throw new UnityException("Indices=" + Indices.Count + " must /3("+(Indices.Count%3)+"), Vertices=" + Vertices.Count + ", indicesCount=" + indicesCount);
                    for (var i = 0; i < Indices.Count; i++)
                    {
                        var index = Indices[i];
                        if (index < 0 || index >= Vertices.Count)
                            throw new UnityException("Indices=" + Indices.Count + ", Vertices=" + Vertices.Count + ", indicesCount=" + indicesCount + ", Indices["+i+"]=" + index);
                    }
                }
                mesh.SetIndices(Indices.ToArray(),
                    mask.quads ? MeshTopology.Quads : (mask.lines ? MeshTopology.Lines : MeshTopology.Triangles),
                    mask.submesh);
            }

            if (mask.indicesMore && IndicesMore.Count > 0)
            {
                var topology = mask.quads ? MeshTopology.Quads : (mask.lines ? MeshTopology.Lines : MeshTopology.Triangles);
                mesh.subMeshCount = IndicesMore.Count+1;
                for (var index = 0; index < IndicesMore.Count; index++)
                {
                    var indices = IndicesMore[index];
                    if (mask.log)
                    {
                        Debug.Log("Apply: vertices="+Vertices.Count+", indices="+Indices.Count+", submesh="+mask.submesh+", indicesMore="+mask.indicesMore
                                  +", IndicesMore="+IndicesMore.Count+", indices="+mask.indices+", index="+index+", indices="+indices.Count);
                    }
                    
                    mesh.SetIndices(indices.ToArray(), topology, index+1);
                }
            }
            
            if (mask.colors)
            {
                if (Colors.Count != Vertices.Count)
                {
                    Debug.LogError("colors=" + Colors.Count + " != vertices=" + Vertices.Count);
                }
                else
                    mesh.SetColors(Colors);
            }

            if (mask.uv0)
            {
                if (Uv0.Count == 0)
                    Debug.LogError("Uv0 is empty");
                mesh.SetUVs(0, Uv0);
            }
            if (mask.uv1)
            {
                if (Uv1.Count == 0)
                    Debug.LogError("Uv1 is empty");
                mesh.SetUVs(1, Uv1);
            }
            if (mask.uv2)
            {
                if (Uv2.Count == 0)
                    Debug.LogError("Uv2 is empty");
                mesh.SetUVs(2, Uv2);
            }
            if (mask.normalsAndTangents)
            {
                mesh.RecalculateNormals();
                mesh.RecalculateTangents();
            }

            if (mask.vertices || mask.normalsAndTangents)
            {
                mesh.RecalculateBounds();
            }
        }

        public void MakeIndicesByArray(int size)
        {
            if (size * size != Vertices.Count)
            {
                throw new UnityException("size=" + size + ", size*size=" + size * size + ", Vertices=" + Vertices.Count);
            }
            
            Indices.Clear();
            Uv0.Clear();
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    int index = x + y * size;
                    
                    Uv0.Add(new Vector2(x/(float)(size-1),y/(float)(size-1)));

                    if (x + 1 < size && y + 1 < size)
                    {
                        var i0 = index;
                        var i1 = x + (y + 1) * size;
                        var i2 = (x + 1) + (y + 1) * size;
                        var i3 = (x + 1) + (y) * size;
                        Indices.Add(i0);
                        Indices.Add(i1);
                        Indices.Add(i2);
                        Indices.Add(i3);
                    }
                }
            }
        }
        
        public void IndicesAddByCount(int verticesAddedCount)
        {
            for (int i = 0; i < verticesAddedCount; i++)
            {
                Indices.Add(indicesCount++);
            }
        }
        
        public void RotateAroundYByDegree(float degree)
        {
            Quaternion rotation = Quaternion.Euler(0,degree,0);
            for (int n = 0; n < Vertices.Count; n++) {
             
                Vertices[n] = rotation * Vertices[n];
            }
        }
        
        public void AddSection(MeshSection section, Vector3 centerPosition, float scale = 1f, bool isSelfTest = false)
        {
            if (section.Vertices.Count > 0)
                foreach (var vertex in section.Vertices)
                {
                    Vertices.Add(vertex*scale + centerPosition);
                }
            
            if (section.Colors.Count > 0)
                Colors.AddRange(section.Colors);

            if (section.Indices.Count > 0)
            {
                Assert.IsTrue(section.Vertices.Count > 0);
                foreach (var index in section.Indices)
                {
                    var newIndex = index + indicesCount;
                    if (isSelfTest && newIndex < Vertices.Count - section.Vertices.Count || newIndex >= Vertices.Count)
                        throw new UnityException("section.Vertices=" + section.Vertices.Count + ", Vertices=" + Vertices.Count + ", section.Indices=" + section.Indices.Count
                                                 +", section.Indices[]="+newIndex+", indicesCount="+indicesCount);
                    Indices.Add(newIndex);
                }
                indicesCount+=section.Vertices.Count;
            }
            
            if (section.Uv0.Count > 0)
                Uv0.AddRange(section.Uv0);
        }

        public void AddPolygon(List<Vector3> points, Color color, Vector3 positionAdd)
        {
            Assert.IsTrue(points.Count >= 3);

            for (var index = 0; index < points.Count; index++)
            {
                Vertices.Add(points[index]+positionAdd);
                Colors.Add(color);
            }

            int indices = indicesCount;
            var indices0 = 0;
            for (var index = 0; index < points.Count-2; index++)
            {
                Indices.Add(indices + indices0);
                Indices.Add(indices + index+1);
                Indices.Add(indices + index+2);
            }

            indicesCount += points.Count;
        }

        public void Apply(Mesh _mesh, bool isQuads = true)
        {
            if (Colors.Count != Vertices.Count)
            {
                throw new UnityException("colors=" + Colors.Count + " != vertices=" + Vertices.Count);
            }

            //IsUseSides = false;
            var log = new StopWatchLog("Clear=");

            _mesh.Clear();
            //if (IsMeshDynamic) _mesh.MarkDynamic();
            _mesh.indexFormat = IndexFormat.UInt32;

            log.NewLog("SetVertices=");

            _mesh.SetVertices(Vertices);

            log.NewLog("SetIndices=");

            if (Indices.Count > 0)
                _mesh.SetIndices(Indices.ToArray(),
                    isQuads ? MeshTopology.Quads : MeshTopology.Triangles,
                    0);

            if (IndicesMore.Count > 0)
            {
                _mesh.subMeshCount = IndicesMore.Count + 1;
                for (var index = 0; index < IndicesMore.Count; index++)
                {
                    var indices = IndicesMore[index];
                    _mesh.SetIndices(indices.ToArray(),
                        isQuads ? MeshTopology.Quads : MeshTopology.Triangles,
                        index + 1);
                }
            }

            log.NewLog("SetColors=");

            _mesh.SetColors(Colors);

            if (Uvs0V.Count > 0)
            {
                log.NewLog("UV+N=");
                _mesh.SetUVs(0, Uvs0V);
                if (Uvs1V.Count > 0 && Uvs1V.Count != Vertices.Count)
                    Debug.LogError("uvs2=" + Uvs1V.Count + " vs vertices=" + Vertices.Count);
                if (Uvs1V.Count > 0) _mesh.SetUVs(1, Uvs1V);
                if (Uvs2V.Count > 0) _mesh.SetUVs(2, Uvs2V);
                if (Uvs3V.Count > 0)
                    if (Uvs3V.Count == Vertices.Count) _mesh.SetUVs(3, Uvs3V);
                    else Debug.LogError("Uvs3(" + Uvs3V.Count + ") != Vertices(" + Vertices.Count + ")");
            }
            else
            {
                log.NewLog("UV+N=");
                _mesh.SetUVs(0, Uv0);
                if (Uv1.Count > 0 && Uv1.Count != Vertices.Count)
                    Debug.LogError("uvs2=" + Uv1.Count + " vs vertices=" + Vertices.Count);
                if (Uv1.Count > 0) _mesh.SetUVs(1, Uv1);
                if (Uvs3.Count > 0) _mesh.SetUVs(2, Uvs3);
            }

            _mesh.RecalculateNormals();
            _mesh.RecalculateTangents();

            //var meshFilter = GetComponent<MeshFilter>();
            //if (meshFilter)
            //    meshFilter.mesh = _mesh;
            // var collider = GetComponent<MeshCollider>();
            // if (IsCollider && collider != null && collider.enabled)
            // {
            //     log.NewLog("SetCollider=");
            //     collider.sharedMesh = _mesh;
            // }

            //var skin = GetComponent<SkinnedMeshRenderer>();
            // if (skin && skin.enabled)
            //     skin.sharedMesh = _mesh;

            log.NewLog();
            //Debug.Log(name+" MeshVolume.Apply: "+log);
        }

        public void InitAs9Squares()
        {
            // 12 13 14 15
            //  8  9 10 11
            //  4  5  6  7
            //  0  1  2  3 

            // color - none
            // vertices
            for (int x = 0; x < 4; x++)
            {
                for (int y = 0; y < 4; y++)
                {
                    var pos = new Vector2(x / 3f, y / 3f);
                    Vertices.Add(pos);
                    Uv0.Add(pos);
                }
            }
            // indices - triangles
            // bottom
            Indices.AddRange(new[] {0, 4, 5, 1});
            Indices.AddRange(new[] {1, 5, 6, 2});
            Indices.AddRange(new[] {2, 6, 7, 3});
            // middle
            Indices.AddRange(new[] {4, 8, 9, 5});
            Indices.AddRange(new[] {5, 9, 10, 6});
            Indices.AddRange(new[] {6, 10, 11, 7});
            // top
            Indices.AddRange(new[] {8, 12, 13, 9});
            Indices.AddRange(new[] {9, 13, 14, 10});
            Indices.AddRange(new[] {10, 14, 15, 11});
        }

        public void InitAs9SquaresXZ()
        {
            // 12 13 14 15
            //  8  9 10 11
            //  4  5  6  7
            //  0  1  2  3 

            // color - none
            // vertices
            for (int y = 0; y < 4; y++)
            {
                for (int x = 0; x < 4; x++)
                {
                    var pos = new Vector2(x / 3f, y / 3f);
                    Vertices.Add(pos.ToXyZ());
                    Uv0.Add(pos);
                }
            }
            // indices - triangles
            // bottom
            Indices.AddRange(new[] {0, 4, 5, 1});
            Indices.AddRange(new[] {1, 5, 6, 2});
            Indices.AddRange(new[] {2, 6, 7, 3});
            // middle
            Indices.AddRange(new[] {4, 8, 9, 5});
            Indices.AddRange(new[] {5, 9, 10, 6});
            Indices.AddRange(new[] {6, 10, 11, 7});
            // top
            Indices.AddRange(new[] {8, 12, 13, 9});
            Indices.AddRange(new[] {9, 13, 14, 10});
            Indices.AddRange(new[] {10, 14, 15, 11});
        }
        
        public void InitAs4SquaresXZ()
        {
            // 6 7 8
            // 3 4 5
            // 0 1 2
            for (int y = 0; y < 3; y++)
            {
                for (int x = 0; x < 3; x++)                
                {
                    var pos = new Vector2(x / 2f, y / 2f);
                    Vertices.Add(pos.ToXyZ());
                    Uv0.Add(pos);
                }
            }
            
            List<int> Indices = new List<int>();
            Indices.AddRange(new[] {0, 3, 4, 1});
            Indices.AddRange(new[] {1, 4, 5, 2});
            Indices.AddRange(new[] {3, 6, 7, 4});
            Indices.AddRange(new[] {4, 7, 8, 5});
            foreach (var index in Indices)
            {
                this.Indices.Add(index + indicesCount);
            }
            indicesCount += 9;
        }
        
        public void InitAsSquareXZ()
        {
            // 2 3
            // 0 1
            for (int y = 0; y < 2; y++)
            {
                for (int x = 0; x < 2; x++)
                {
                    var pos = new Vector2(x, y);
                    Vertices.Add(pos.ToXyZ());
                    Uv0.Add(pos);
                }
            }
            Indices.AddRange(new[] {0+indicesCount, 2+indicesCount, 3+indicesCount, 1+indicesCount});
            indicesCount += 4;
        }
        
        public void AddSquareXZCentered()
        {
            // 2 3
            // 0 1
            Vertices.Add(new Vector3(-0.5f,0,-0.5f));
            Vertices.Add(new Vector3(-0.5f,0,+0.5f));
            Vertices.Add(new Vector3(+0.5f,0,+0.5f));
            Vertices.Add(new Vector3(+0.5f,0,-0.5f));
            
            Uv0.Add(new Vector2(0,0));
            Uv0.Add(new Vector2(0,1));
            Uv0.Add(new Vector2(1,1));
            Uv0.Add(new Vector2(1,0));
            
            Indices.AddRange(new[] {0+indicesCount, 1+indicesCount, 2+indicesCount, 3+indicesCount});
            indicesCount += 4;
        }


        public MeshSection InitAsSquareByTriangles(Color32 color, Vector3 posMult)
        {
            //  1  2
            //  0  3 
            for (int i = 0; i < 4; i++)
                Colors.Add(color);
            Vertices.Add(new Vector3(0.0f, 0, 0.0f).Mult(posMult));
            Vertices.Add(new Vector3(0.0f, 0, 1.0f).Mult(posMult));
            Vertices.Add(new Vector3(1.0f, 0, 1.0f).Mult(posMult));
            Vertices.Add(new Vector3(1.0f, 0, 0.0f).Mult(posMult));
            Uv0.Add(new Vector2(0.0f, 0.0f));
            Uv0.Add(new Vector2(0.0f, 1.0f));
            Uv0.Add(new Vector2(1.0f, 1.0f));
            Uv0.Add(new Vector2(1.0f, 0.0f));
            Indices.AddRange(new[] {0, 1, 2, 0, 2, 3});
            return this;
        }
        
        public MeshSection InitAsSquare(Color32 color, bool addUv1 = false, float size = 1, bool asTriangles = false)
        {
            //  1  2
            //  0  3 
            for (int i = 0; i < 4; i++)
                Colors.Add(color);
            Vertices.Add(new Vector2(0.0f, 0.0f) * size);
            Vertices.Add(new Vector2(0.0f, 1.0f) * size);
            Vertices.Add(new Vector2(1.0f, 1.0f) * size);
            Vertices.Add(new Vector2(1.0f, 0.0f) * size);
            Uv0.Add(new Vector2(0.0f, 0.0f));
            Uv0.Add(new Vector2(0.0f, 1.0f));
            Uv0.Add(new Vector2(1.0f, 1.0f));
            Uv0.Add(new Vector2(1.0f, 0.0f));
            
            if (asTriangles)
                Indices.AddRange(new[] {0, 1, 2, 0, 2, 3});
            else
                Indices.AddRange(new[] {0, 1, 2, 3});
            
            if (addUv1)
            {
                Uv1.Add(new Vector2(0.0f, 0.0f));
                Uv1.Add(new Vector2(0.0f, 1.0f));
                Uv1.Add(new Vector2(1.0f, 1.0f));
                Uv1.Add(new Vector2(1.0f, 0.0f));
            }
            return this;
        }
        
        public void InitAs012345678()
        {
            //  6  7  8
            //  3  4  5
            //  0  1  2 

            // color
            for (int j = 0; j < 9; j++)
                Colors.Add(Color.white);
            // vertices
            Vertices.Add(new Vector2(0.0f, 0.0f));
            Vertices.Add(new Vector2(0.5f, 0.0f));
            Vertices.Add(new Vector2(1.0f, 0.0f));
            Vertices.Add(new Vector2(0.0f, 0.5f));
            Vertices.Add(new Vector2(0.5f, 0.5f));
            Vertices.Add(new Vector2(1.0f, 0.5f));
            Vertices.Add(new Vector2(0.0f, 1.0f));
            Vertices.Add(new Vector2(0.5f, 1.0f));
            Vertices.Add(new Vector2(1.0f, 1.0f));
            // indices - triangles
            // left bottom inner
            Indices.AddRange(new[] {4, 1, 3});
            // left top inner
            Indices.AddRange(new[] {4, 3, 7});
            Indices.AddRange(new[] {4, 7, 5});
            Indices.AddRange(new[] {4, 5, 1});
            // left bottom outer
            Indices.AddRange(new[] {0, 3, 1});
            Indices.AddRange(new[] {6, 7, 3});
            Indices.AddRange(new[] {8, 5, 7});
            Indices.AddRange(new[] {2, 1, 5});
            // uv0
            Uv0.Add(new Vector2(0.0f, 0.0f));
            Uv0.Add(new Vector2(0.5f, 0.0f));
            Uv0.Add(new Vector2(1.0f, 0.0f));
            Uv0.Add(new Vector2(0.0f, 0.5f));
            Uv0.Add(new Vector2(0.5f, 0.5f));
            Uv0.Add(new Vector2(1.0f, 0.5f));
            Uv0.Add(new Vector2(0.0f, 1.0f));
            Uv0.Add(new Vector2(0.5f, 1.0f));
            Uv0.Add(new Vector2(1.0f, 1.0f));
        }

        public void AddSquareXZ(float scale = 1f, float y = 0f)
        {
            Vertices.Add(new Vector3(0,y,0)*scale);
            Vertices.Add(new Vector3(0,y,1)*scale);
            Vertices.Add(new Vector3(1,y,1)*scale);
            Vertices.Add(new Vector3(1,y,0)*scale);
            Indices.Add(0);
            Indices.Add(1);
            Indices.Add(2);
            Indices.Add(3);
            Uv0.Add(new Vector2(0,0));
            Uv0.Add(new Vector2(0,1));
            Uv0.Add(new Vector2(1,1));
            Uv0.Add(new Vector2(1,0));
        }

        public void AddSquare(List<Vector3> points, List<Vector2> uv, List<int> indices = null)
        {
            //Debug.LogWarning("AddSquare: "+points.Count+", uvMult="+uvMult);
            int count = Vertices.Count;

            foreach (var point in points)
                Vertices.Add(point);

            int submesh = 0;

            for (var index = 0; index < points.Count; index++)
                Colors.Add(Color.white);

            if (submesh == 0)
            {
                if (indices != null && indices.Count > 0)
                {
                    foreach (var index in indices)
                    {
                        Indices.Add(index + count);
                    }
                }
                else if (points.Count == 3)
                    for (int point = 0; point < points.Count; point++)
                        Indices.Add(count + point);
                else if (points.Count == 4)
                {
                    for (int point = 0; point < 3; point++)
                        Indices.Add(count + point);
                    Indices.Add(count);
                    Indices.Add(count + 2);
                    Indices.Add(count + 3);
                }
            }

            if (submesh > 0)
            {
                for (int i = 0; i < submesh - IndicesMore.Count; i++)
                    IndicesMore.Add(new List<int>());
                for (int point = 0; point < points.Count; point++)
                    IndicesMore[submesh - 1].Add(count + point);
            }

            Uv0.AddRange(uv);

            //Uvs.Add(new Vector2(0,0).Mult(uvMult));
            //Uvs.Add(new Vector2(0,1).Mult(uvMult));
            //Uvs.Add(new Vector2(1,1).Mult(uvMult));
            //Uvs.Add(new Vector2(1,0).Mult(uvMult));
        }

        public void AddSquare(List<Vector3> points)
        {
            //Debug.LogWarning("AddSquare: "+points.Count+", uvMult="+uvMult);
            int count = Vertices.Count;

            foreach (var point in points)
                Vertices.Add(point);

            int submesh = 0;

            foreach (var point in points)
                Colors.Add(new Color(point.x, point.y, point.z));

            if (submesh == 0)
            {
                // if (indices != null && indices.Count > 0)
                // {
                //     foreach (var index in indices)
                //     {
                //         Indices.Add(index + count);
                //     }
                // }
                // else if (points.Count == 3)
                //     for (int point = 0; point < points.Count; point++)
                //         Indices.Add(count + point);
                if (points.Count == 4)
                {
                    for (int point = 0; point < 4; point++)
                        Indices.Add(count + point);
                    //   Indices.Add(count);
                    //   Indices.Add(count+2);
                    //   Indices.Add(count+3);
                }
            }

            if (submesh > 0)
            {
                for (int i = 0; i < submesh - IndicesMore.Count; i++)
                    IndicesMore.Add(new List<int>());
                for (int point = 0; point < points.Count; point++)
                    IndicesMore[submesh - 1].Add(count + point);
            }

            // Uvs0V.AddRange(points);

            //Uvs.Add(new Vector2(0,0).Mult(uvMult));
            //Uvs.Add(new Vector2(0,1).Mult(uvMult));
            //Uvs.Add(new Vector2(1,1).Mult(uvMult));
            //Uvs.Add(new Vector2(1,0).Mult(uvMult));
        }


        public void AddSquare(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            int count = Vertices.Count;

            Vertices.Add(p0);
            Vertices.Add(p1);
            Vertices.Add(p2);
            Vertices.Add(p3);

            int submesh = 0;

            for (var index = 0; index < 4; index++)
                Colors.Add(Color.white);

            if (submesh == 0)
                for (int point = 0; point < 4; point++)
                    Indices.Add(count + point);

            if (submesh > 0)
            {
                for (int i = 0; i < submesh - IndicesMore.Count; i++)
                    IndicesMore.Add(new List<int>());
                for (int point = 0; point < 4; point++)
                    IndicesMore[submesh - 1].Add(count + point);
            }

            Uv0.Add(new Vector2(0, 0));
            Uv0.Add(new Vector2(0, 1));
            Uv0.Add(new Vector2(1, 1));
            Uv0.Add(new Vector2(1, 0));
        }

        public void AddRectangePosX(int x1, int y1, int z1, int x2, int y2, int z2, float cubeSize)
        {
            if (_debugCounter.x > 0)
            {
                _debugCounter.x--;
                Debug.LogError("AddRectangePosX: x1=" + x1 + ", y1=" + y1 + ", z1=" + z1 + ", x2=" + x2 + ", y2=" + y2 +
                               ", z2=" + z2);
            }

            Assert.IsTrue(x1 == x2);
            Assert.IsTrue(y1 == y2);
            Assert.IsTrue(z1 <= z2);

            x2 += 1;
            y2 += 1;
            z2 += 1;

            Vector3 posShift = -new Vector3(64f, 64f, 64f);
            posShift += new Vector3(1.5f, 0.5f, 0.5f);
            float posMult = cubeSize;

            var v1 = new Vector3(x1, y1, z1);
            var v2 = new Vector3(x1, y2, z1);
            var v3 = new Vector3(x1, y2, z2);
            var v4 = new Vector3(x1, y1, z2);

            AddVertices(v1, v2, v3, v4, posShift, posMult, 0);
        }

        public void AddRectangeNegX(int x1, int y1, int z1, int x2, int y2, int z2, float cubeSize)
        {
            if (_debugCounter.x > 0)
            {
                _debugCounter.x--;
                Debug.LogError("AddRectangeNegX: x1=" + x1 + ", y1=" + y1 + ", z1=" + z1 + ", x2=" + x2 + ", y2=" + y2 +
                               ", z2=" + z2);
            }

            Assert.IsTrue(x1 == x2);
            Assert.IsTrue(y1 == y2);
            Assert.IsTrue(z1 >= z2);

            x2 += 1;
            y2 += 1;
            z1 += 1;

            Vector3 posShift = -new Vector3(64f, 64f, 64f);
            posShift += new Vector3(0.5f, 0.5f, 0.5f);
            float posMult = cubeSize;

            var v1 = new Vector3(x1, y1, z1);
            var v2 = new Vector3(x1, y2, z1);
            var v3 = new Vector3(x1, y2, z2);
            var v4 = new Vector3(x1, y1, z2);

            AddVertices(v1, v2, v3, v4, posShift, posMult, 0);
        }

        private void AddVertices(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, Vector3 shift, float mult,
            int submesh = 0)
        {
            int count = Vertices.Count;
            Vertices.Add((v1 + shift) * mult);
            Vertices.Add((v2 + shift) * mult);
            Vertices.Add((v3 + shift) * mult);
            Vertices.Add((v4 + shift) * mult);

            for (var index = 0; index < 4; index++)
                Colors.Add(Color.white);

            if (submesh == 0)
                for (int point = 0; point < 4; point++)
                    Indices.Add(count + point);

            if (submesh > 0)
            {
                for (int i = 0; i < submesh - IndicesMore.Count; i++)
                    IndicesMore.Add(new List<int>());
                for (int point = 0; point < 4; point++)
                    IndicesMore[submesh - 1].Add(count + point);
            }

            Uvs3.Add(v1 / 128f);
            Uvs3.Add(v2 / 128f);
            Uvs3.Add(v3 / 128f);
            Uvs3.Add(v4 / 128f);
        }

        public void AddRectangePosZ(int x1, int y1, int z1, int x2, int y2, int z2, float cubeSize)
        {
            if (_debugCounter.z > 0)
            {
                _debugCounter.z--;
                Debug.LogError("AddRectangePosZ: x1=" + x1 + ", y1=" + y1 + ", z1=" + z1 + ", x2=" + x2 + ", y2=" + y2 +
                               ", z2=" + z2);
            }

            Assert.IsTrue(x1 <= x2);
            Assert.IsTrue(y1 == y2);
            Assert.IsTrue(z1 == z2);

            x2 += 1;
            y2 += 1;
            z2 += 1;

            Vector3 posShift = -new Vector3(64f, 64f, 64f);
            posShift += new Vector3(0.5f, 0.5f, 1.5f);
            float posMult = cubeSize;

            var v1 = new Vector3(x2, y1, z1);
            var v2 = new Vector3(x2, y2, z1);
            var v3 = new Vector3(x1, y2, z1);
            var v4 = new Vector3(x1, y1, z1);

            AddVertices(v1, v2, v3, v4, posShift, posMult);
        }

        public void AddRectangeNegZ(int x1, int y1, int z1, int x2, int y2, int z2, float cubeSize)
        {
            if (_debugCounter.z > 0)
            {
                _debugCounter.z--;
                Debug.LogError("AddRectangeNegZ: x1=" + x1 + ", y1=" + y1 + ", z1=" + z1 + ", x2=" + x2 + ", y2=" + y2 +
                               ", z2=" + z2);
            }

            Assert.IsTrue(x1 <= x2);
            Assert.IsTrue(y1 == y2);
            Assert.IsTrue(z1 == z2);

            x2 += 1;
            y2 += 1;
            z2 += 1;

            Vector3 posShift = -new Vector3(64f, 64f, 64f);
            posShift += new Vector3(0.5f, 0.5f, 0.5f);
            float posMult = cubeSize;

            var v1 = new Vector3(x1, y1, z1);
            var v2 = new Vector3(x1, y2, z1);
            var v3 = new Vector3(x2, y2, z1);
            var v4 = new Vector3(x2, y1, z1);

            AddVertices(v1, v2, v3, v4, posShift, posMult);
        }

        public void AddRectangePosY(int x1, int y1, int z1, int x2, int y2, int z2, float cubeSize)
        {
            if (_debugCounter.y > 0)
            {
                _debugCounter.y--;
                Debug.LogError("AddRectangePosY: x1=" + x1 + ", y1=" + y1 + ", z1=" + z1 + ", x2=" + x2 + ", y2=" + y2 +
                               ", z2=" + z2);
            }

            Assert.IsTrue(x1 >= x2);
            Assert.IsTrue(y1 == y2);
            Assert.IsTrue(z1 == z2);

            var t = x1;
            x1 = x2;
            x2 = t;

            x2 += 1;
            y2 += 1;
            z2 += 1;

            Vector3 posShift = -new Vector3(64f, 64f, 64f);
            posShift += new Vector3(0.5f, 1.5f, 0.5f);
            float posMult = cubeSize;

            var v1 = new Vector3(x2, y1, z2);
            var v2 = new Vector3(x2, y1, z1);
            var v3 = new Vector3(x1, y1, z1);
            var v4 = new Vector3(x1, y1, z2);

            if (_debugCounter.y > 0)
                Debug.LogError("AddRectangePosY: v1=" + v1 + ", v2=" + v2 + ", v3=" + v3 + ", v4=" + v4);

            AddVertices(v1, v2, v3, v4, posShift, posMult);
        }

        public void AddRectangeNegY(int x1, int y1, int z1, int x2, int y2, int z2, float cubeSize)
        {
            if (_debugCounter.y > 0)
            {
                _debugCounter.y--;
                Debug.LogError("AddRectangeNegY: x1=" + x1 + ", y1=" + y1 + ", z1=" + z1 + ", x2=" + x2 + ", y2=" + y2 +
                               ", z2=" + z2);
            }

            Assert.IsTrue(x1 >= x2);
            Assert.IsTrue(y1 == y2);
            Assert.IsTrue(z1 == z2);

            //var t = x1; x1 = x2; x2 = t;

            x1 += 1;
            y2 += 1;
            z2 += 1;

            Vector3 posShift = -new Vector3(64f, 64f, 64f);
            posShift += new Vector3(0.5f, 0.5f, 0.5f);
            float posMult = cubeSize;

            var v1 = new Vector3(x1, y1, z1);
            var v2 = new Vector3(x1, y1, z2);
            var v3 = new Vector3(x2, y1, z2);
            var v4 = new Vector3(x2, y1, z1);

            if (_debugCounter.y > 0)
                Debug.LogError("AddRectangeNegY: v1=" + v1 + ", v2=" + v2 + ", v3=" + v3 + ", v4=" + v4);

            AddVertices(v1, v2, v3, v4, posShift, posMult);
        }
    }
}