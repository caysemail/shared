﻿#if Newtonsoft

using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Shared
{

    public class ListVector2FromArray : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException("Not implemented yet");
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            //Debug.LogError("reader.TokenType="+reader.TokenType+", objectType="+objectType+", "+reader.Value);

            if (reader.TokenType != JsonToken.StartArray)
            {
                Debug.LogError("reader.TokenType="+reader.TokenType+", objectType="+objectType+", "+reader.Value);
                return serializer.Deserialize(reader, objectType);
            }
            
            //Debug.LogError("BoolVector3FromArray.ReadJson: TokenType="+reader.TokenType+", reader="+reader.ToString());
            var obj = JArray.Load(reader);
            var obj0 = obj[0];
            if (obj0.Type == JTokenType.Array)
            {
                List<Vector2> list = new List<Vector2>();
                // it is array of arrays
                foreach (var token in obj)
                {
                    list.Add(ParseFloatToken(token));
                }
                return list;
            }
            else if (obj0.Type == JTokenType.Float)
            {
                return ParseFloatToken(obj);
            }
            else
            {
                throw new UnityException("obj.Count=" + obj.Count + ", reader.TokenType=" + reader.TokenType +
                                         ", obj[0]=" + obj0+", obj0.Type="+obj0.Type);
            }

            Vector2 ParseFloatToken(JToken token)
            {
                float f = 0;
                Vector2 v = Vector2.zero;
                if (!float.TryParse((string) token[0], NumberStyles.Float, CultureInfo.InvariantCulture, out f))
                    throw new UnityException("cannot parse x?="+obj[0]+", y="+obj[1]);
                v.x = f;
            
                if (!float.TryParse((string) token[1], NumberStyles.Float, CultureInfo.InvariantCulture, out f))
                    throw new UnityException("cannot parse x="+obj[0]+", y?="+obj[1]);
                v.y = f;
                return v;
            }
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override bool CanConvert(Type objectType)
        {
            return false;
        }
    }
}

#endif