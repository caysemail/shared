﻿using System;
using System.Collections.Generic;
using System.Linq;
using Shared;
using UnityEngine;

public enum AnimationType
{
    None = 0,
    Aware = 1,
    Melee = 2,
    Idle = 3,
    Death = 4,
    Cast = 5,
    Walk = 6,
    Run = 7,
    Shoot = 8,
    Meditate = 9,
    Max = 9,
}

public class UnitAnimator : MonoBehaviour
{
    public Action OnEndState;
    
    [SerializeField] private bool _isDebug = false;
    //[SerializeField] [ReadOnly] private AnimationType _type = AnimationType.None;
    [SerializeField] private AnimationClipOverride[] ClipOverrides = new AnimationClipOverride[0];
    [SerializeField] [ReadOnly] private bool _isInited;
    private bool _isFail;
    [SerializeField] [ReadOnly] private AnimationSMBehaviour.StateInfo _stateInfo;

    public Animator _animator;
    [SerializeField] [ReadOnly] private AnimationType _lastAnimationType = AnimationType.None;
    private List<AnimationType> _aniTypes;
    private AnimationSMBehaviour[] _behaviours;

    private const string CLASSNAME = "UnitAnimator.";

    public AnimationType state => _lastAnimationType;
    
    [Serializable]
    private class AnimationClipOverride
    {
        public AnimationType clipType = default;
        public AnimationClip overrideWith = default;
        public float Speed = 1f;
    }

    [Button]
    private void Update_0()
    {
        AnimatorUpdate(0);
    }
    [Button]
    private void Update_002()
    {
        AnimatorUpdate(0.02f);
    }

    public void Initialize()
    {
        if (_isInited || _isFail) return;
        // animator
        _animator = GetComponent<Animator>();
        if (_animator == null)
        {
            //Debug.LogError(CLASSNAME + "Initialize: no animator on " + name, gameObject);
            _isFail = true;
            return;
        }

        // ani types
        _aniTypes = new List<AnimationType>();
        foreach (var value in Enum.GetValues(typeof(AnimationType)))
        {
            var aniType = (AnimationType) value;
            if (aniType == AnimationType.None) continue;
            _aniTypes.Add(aniType);
        }

        // override
        AnimatorOverrideController overrideController = new AnimatorOverrideController();
        overrideController.runtimeAnimatorController = _animator.runtimeAnimatorController;
        List<KeyValuePair<AnimationClip, AnimationClip>> overrides =
            new List<KeyValuePair<AnimationClip, AnimationClip>>();
        overrideController.GetOverrides(overrides);
        foreach (AnimationClipOverride clipOverride in ClipOverrides)
        {
            overrideController[clipOverride.clipType.ToString()] = clipOverride.overrideWith;
            //Debug.LogError("override "+clipOverride.clipType.ToString()+" with "+clipOverride.overrideWith.name);
            //var speedOverride = clipOverride.Speed;
            //if (speedOverride != 0 && speedOverride != 1)
            //    _animator.SetFloat(clipOverride.clipType+"Speed", clipOverride.Speed);
        }
        _animator.runtimeAnimatorController = overrideController;
        // initialize clips
        _behaviours = _animator.GetBehaviours<AnimationSMBehaviour>();
        foreach (var behaviour in _behaviours)
        {
            behaviour.Initialize(OnAnimationCallback);
        }

        UpdateAnimationsSpeed();

        _isInited = true;
    }

    [Button]
    private void UpdateAnimationsSpeed()
    {
        foreach (AnimationClipOverride clipOverride in ClipOverrides)
        {
            if (clipOverride.Speed > 0)
                _animator.SetFloat(clipOverride.clipType.ToString()+"Speed", clipOverride.Speed);
        }
    }
    
    private void OnAnimationCallback(AnimationSMBehaviour.StateInfo info)
    {
        //_stateInfo = info;
        //_lastType = info.Type;
        //_lastStateType = info.StateType stateType;
        if (_isDebug)
            Debug.Log(CLASSNAME+"OnAnimationCallback: "+info.StateType+" "+info.Type+", length="+info.Length+", time="+Time.realtimeSinceStartup+" seconds"+
                      ", prev StateType="+_stateInfo.StateType+" Type="+_stateInfo.Type);
        if (info.StateType == AnimationStateType.Start)
        {
            // OnHalfState?.Invoke();
            // OnHalfState = null;
            if (info.Type != _stateInfo.Type)
            {
                // new type
                OnEndState?.Invoke();
                //OnEndState = null;
            }
            _stateInfo = info;
            // OnHalfState?.Invoke();
            // OnHalfState = null;
        }
        //else Debug.LogError("? ");

        //if (info.StateType == AnimationStateType.End)
        //{
        //    OnEndState?.Invoke(info.StateType);
        ///    OnEndState = null;
        //}
    }

    public void Play(AnimationType state, float duration = -1)
    {
        if (_isFail) return;
        if (!_isInited)
            Initialize();
        PlayAnimation(state, true, duration);
    }

    private void PlayAnimation(AnimationType type, bool isRootMotion, float duration = -1f)
    {
        if (_isFail) return;
        if (!_isInited)
        {
            Debug.LogError("not inited", this);
            return;
        }

        _animator.enabled = true;
        //if (_stateInfo.Type == type) return;
        if (_lastAnimationType == type) return;
        if (_isDebug) Debug.Log(CLASSNAME + "PlayAnimation: " + type + ", previous=" + _lastAnimationType);
        _lastAnimationType = type;

        var custom = ClipOverrides.FirstOrDefault(t => t.clipType == type);
        if (custom == null)
        {
            Debug.LogError("not found clip type=" + type, gameObject);
            Debug.Break();
        }
        else if (custom.Speed != 0 && custom.Speed != 1)
        {
            //Debug.LogWarning("SetFloat: "+type+"Speed"+" "+custom.Speed+ " "+Find.ConfigPlayer.MoveSpeed);
            //_animator.SetFloat(type+"Speed", custom.Speed * MajestyFind.Find.ConfigPlayer.MoveSpeed);
        }

        if (duration > 0)
        {
            //var ani = _behaviours.FirstOrDefault(t => t.Type == type);
            var newSpeed = custom.overrideWith.length / duration; // 0.6f / 1f
            _animator.SetFloat(type + "Speed", newSpeed);
        }

        SetBool(type);
    }

    private void SetBool(AnimationType type)
    {
        foreach (var aniType in _aniTypes)
        {
            if (aniType == type)
                continue;
            _animator.SetBool(aniType.ToString(), false);
        }

        _animator.SetBool(type.ToString(), true);
    }

    [ReadOnly] [SerializeField] private float lastDeltaTime;
    [SerializeField] private int editorFrame;

    public void AnimatorUpdate(float deltaTime)
    {
        if (Application.isPlaying) return;
        //return;
        //if (_isFail || !_isInited) return;
        //Debug.Log("AnimatorUpdate="+deltaTime);
        _animator.updateMode = AnimatorUpdateMode.UnscaledTime;
        _animator.Update(deltaTime);
        //_animator.GetCurrentAnimatorStateInfo(0).
        lastDeltaTime = deltaTime;
        editorFrame++;
    }
}