﻿using System;
using System.Threading.Tasks;
using UnityEngine;

public enum AnimationStateType
{
    None = 0,
    Start = 1,
    End = 3,
}

public class AnimationSMBehaviour : StateMachineBehaviour
{
    [Serializable]
    public struct StateInfo
    {
        public AnimationStateType StateType;
        public AnimationType Type;
        public float Length;

        public override string ToString()
        {
            return JsonUtility.ToJson(this);
        }
    }

    public AnimationType Type;
    private Action<StateInfo> _onAnimationCallback;

    public bool ApplyRootMotion = false;
    //[SerializeField] [ReadOnly] private float _speed;
    //[SerializeField] private bool _debugLog = false;

    // private void SetSpeed(float speed)
    //{
    //     _speed = speed;
    // }

    public void Initialize(Action<StateInfo> onAnimationCallback)
    {
        _onAnimationCallback = onAnimationCallback;
    }

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // if (_debugLog)
        //    Debug.LogWarning("AnimationBehaviour.OnStateEnter: "+animator.name+", worldPos="+ animator.transform.position
        //                 +", length="+stateInfo.length);
        _onAnimationCallback?.Invoke(new StateInfo()
            {StateType = AnimationStateType.Start, Type = Type, Length = stateInfo.length});

        //TaskDelayCallback(stateInfo.length/2f, () =>
        //{
        //     _onAnimationCallback?.Invoke(new StateInfo() { StateType = AnimationStateType.Middle, Type = Type, Length = stateInfo.length });
        // });
    }

    private async void TaskDelayCallback(float delay, Action action)
    {
        await Task.Delay((int) (delay * 1000f));
        action?.Invoke();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //  todo state exit is currently bugged ! do not use it - it show not his state
        _onAnimationCallback?.Invoke(new StateInfo()
            {StateType = AnimationStateType.End, Type = Type, Length = stateInfo.length});
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    public override void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // if (_debugLog)
        //     Debug.LogWarning("MotionBehaviour.OnStateMove: " + animator.name + ", worldPos=" + animator.transform.position);
        if (ApplyRootMotion)
            animator.ApplyBuiltinRootMotion();
        // Implement code that processes and affects root motion
        //Debug.LogWarning("AnimationBehaviour.OnStateMove:  " + animator.name);
    }

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}