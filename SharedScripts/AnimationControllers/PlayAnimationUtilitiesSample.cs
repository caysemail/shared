﻿using System;
using Shared;
using UnityEngine;

using UnityEngine.Playables;

using UnityEngine.Animations;

//[ExecuteInEditMode]
[RequireComponent(typeof(Animator))]
[ExecuteAlways]
public class PlayAnimationUtilitiesSample : MonoBehaviour
{
    public AnimationClip clip;

    public PlayableGraph playableGraph;
    public bool playAnimatorInEditor;

    public int updateCount;
    public int animatorUpdateCount;

    [Button]
    void StartAnimation()
    {
        AnimationPlayableUtilities.PlayClip(GetComponent<Animator>(), clip, out playableGraph);
    }

    [Button]
    private void StopAnimation()
    {
        playableGraph.Destroy();
    }

    void OnDisable()
    {
        // Destroys all Playables and Outputs created by the graph.
        playableGraph.Destroy();
    }

    private void Update()
    {
        if (!playAnimatorInEditor)
            return;
        updateCount++;
        //Debug.LogError("Update: playAnimatorInEditor="+playAnimatorInEditor);
        //if (!playAnimatorInEditor)
        //    return;
        //GetComponent<UnitAnimator>().AnimatorUpdate(Time.deltaTime);
        var animator = GetComponent<Animator>();//.AnimatorUpdate(Time.deltaTime);
        animator.updateMode = AnimatorUpdateMode.UnscaledTime;
        animator.Update(Time.deltaTime);
    }

    public float _lastGizmoTime;
    public float _gizmoDeltaTime;
    private void OnDrawGizmos()
    {
        //return;
        //Debug.LogError("OnDrawGizmos");
        if (!playAnimatorInEditor)
            return;
        var animator = GetComponent<Animator>();//.AnimatorUpdate(Time.deltaTime);
        //animator.updateMode = AnimatorUpdateMode.UnscaledTime;
        _gizmoDeltaTime = Time.realtimeSinceStartup - _lastGizmoTime;
        animator.Update(_gizmoDeltaTime);
        animatorUpdateCount++;
        _lastGizmoTime = Time.realtimeSinceStartup;
    }
}