﻿#if Newtonsoft

using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Shared
{
    public class ListVector3FromArray : JsonConverter
    {

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException("Not implemented yet");
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            //Debug.LogError("reader.TokenType="+reader.TokenType+", objectType="+objectType+", "+reader.Value);

            if (reader.TokenType != JsonToken.StartArray)
            {
                Debug.LogError("reader.TokenType="+reader.TokenType+", objectType="+objectType+", "+reader.Value);
                return serializer.Deserialize(reader, objectType);
            }
            
            var obj = JArray.Load(reader);
            var obj0 = obj[0];
            //Debug.LogError("ListVector3FromArray.ReadJson: TokenType="+reader.TokenType+", obj0.Type="+obj0.Type);
            if (obj0.Type == JTokenType.Array)
            {
                List<Vector3> list = new List<Vector3>();
                // it is array of arrays
                foreach (var token in obj)
                {
                    list.Add(ParseFloatToken(token));
                }
                return list;
            }
            else if (obj0.Type == JTokenType.Float)
            {
                return ParseFloatToken(obj);
            }
            else
            {
                throw new UnityException("obj.Count=" + obj.Count + ", reader.TokenType=" + reader.TokenType +
                                         ", obj[0]=" + obj0+", obj0.Type="+obj0.Type);
            }

            Vector3 ParseFloatToken(JToken token)
            {
                float f = 0;
                Vector3 v = Vector3.zero;
                if (!float.TryParse((string) token[0], NumberStyles.Float, CultureInfo.InvariantCulture, out f))
                    throw new UnityException("cannot parse x?="+obj[0]+", y="+obj[1]);
                v.x = f;
            
                if (!float.TryParse((string) token[1], NumberStyles.Float, CultureInfo.InvariantCulture, out f))
                    throw new UnityException("cannot parse x="+obj[0]+", y?="+obj[1]);
                v.y = f;

                if (!float.TryParse((string) token[2], NumberStyles.Float, CultureInfo.InvariantCulture, out f))
                    throw new UnityException("cannot parse x="+obj[0]+", y?="+obj[1]+", z?="+obj[2]);
                v.z = f;
                return v;
            }
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override bool CanConvert(Type objectType)
        {
            return false;
        }
    }
}

#endif