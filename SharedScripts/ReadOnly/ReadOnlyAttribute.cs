﻿using UnityEditor;
using UnityEngine;

namespace Shared
{
    public class ReadOnlyAttribute : PropertyAttribute
    {
    }
}