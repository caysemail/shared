namespace Shared
{
    public interface IView
    {
        public void Initialize();
    }
}