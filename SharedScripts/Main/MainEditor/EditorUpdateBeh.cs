using System;
using Cysharp.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Shared
{
    [ExecuteInEditMode]
    public class EditorUpdateBeh : MonoBehaviour
    {
        [ReadOnly][SerializeField] private int _updateCount;
        [ReadOnly][SerializeField] private int _fps;

        [NonSerialized] private bool _isActive;

        [SerializeField] private bool editorRepaint = true;

        private const float delay = 1f / 60f;
        private Shared.Timer fpsTimer = null;
        
        private void OnEnable()
        {
            if (Application.isPlaying)
            {
                this.enabled = false;
                return;
            }
            _updateCount = 0;
            fpsTimer = new Timer();
            _fps = 0;
            _isActive = true;
            StartUpdateAsync().Forget();
        }

        private void OnDisable()
        {
            _isActive = false;
        }

        private void FpsMeter()
        {
            float fpsTime = fpsTimer.Stop();
            _fps = (int)(1f / fpsTime);
            fpsTimer.Start();
        }

        private async UniTask StartUpdateAsync()
        {
            var updateTimer = new Shared.Timer();
            float realFixedUpdateTime = 0f;
            var fixedDeltaTime = Time.fixedDeltaTime;
            bool isContinue = true;
            
            do
            {
                if (EditorApplication.isPaused)
                {
                    await UniTask.Delay((int)(delay*1000f));
                    continue;
                }
                
                realFixedUpdateTime = 0;
                updateTimer.Start();

                if (MainFind.events.update != null && !Application.isPlaying && _isActive)
                {
                    //int count0 = MainFind.Find.Events.fixedUpdate.GetInvocationList().Length;
                    try
                    {
                        MainFind.events.update?.Invoke();
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                        //Debug.LogError(e);
                    }
                    
                    //int count1 = MainFind.Find.Events.fixedUpdate.GetInvocationList().Length;
                    //Debug.LogError("count0=" + count0 + ", count1=" + count1);
                }

                if (editorRepaint && !Application.isPlaying)
                {
                    // Ensure continuous Update calls.
                    UnityEditor.EditorApplication.QueuePlayerLoopUpdate();
                    UnityEditor.SceneView.RepaintAll();
                }
                
                realFixedUpdateTime += updateTimer.Stop();
                _updateCount++;
                FpsMeter();

                if (fixedDeltaTime - realFixedUpdateTime > 0f) // 0.020 - 0.019 = 0.001 
                {
                    await UniTask.Delay((int)((fixedDeltaTime - realFixedUpdateTime) * 1000));
                }
                else
                {
                    if (realFixedUpdateTime < 0.03f)
                    {
                        //Debug.LogWarning("fixedDeltaTime=" + fixedDeltaTime + ", delay=" + realFixedUpdateTime);
                    }
                    else
                        Debug.LogError("fixedDeltaTime="+fixedDeltaTime+", delay="+realFixedUpdateTime);
                    await UniTask.Delay((int)(1));
                    
                    if (realFixedUpdateTime > 8)
                        break;
                }
            } while (isContinue && _isActive && !Application.isPlaying && enabled);

            _isActive = false;
        }
    }
}