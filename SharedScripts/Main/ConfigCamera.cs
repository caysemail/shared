﻿using UnityEngine;

namespace Shared
{
    [CreateAssetMenu(fileName = "Assets/Resources/Configs/Camera", menuName = "Configs/Camera", order = 1)]
    public class ConfigCamera : ScriptableObject
    {
        public bool useConfig = false;
        public float distance = 150;
        public Vector2 orbitAngles = new Vector2(50f, 240f);
        public Vector3 focusPointPosition = new Vector3(0, 0, 0);
        [SerializeField, Range(0.1f, 100f)] public float zoomSensitivity = 10f;
    }
}