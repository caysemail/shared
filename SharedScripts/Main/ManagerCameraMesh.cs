﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;

namespace Shared
{
    public class ManagerCameraMesh : Manager
    {
        [SerializeField]private bool isDebug;
        [SerializeField]private bool isEnableCollider;
        
        [SerializeField] private ConfigCamera _configCamera;
        //public bool ExecuteInEditMode = false;
        //public int _updateCounter;
        [ReadOnly]
        public Vector2 _mousePos;
        [ReadOnly]
        public Vector2 _screenSize;

        [SerializeField] private bool _isVerticalAngle90 = false;
        [SerializeField, Range(-89f, 90.0f)] private float _minVerticalAngle = 5f, _maxVerticalAngle = 89f;
        [SerializeField] private Vector2 _orbitAngles = new Vector2(50f, 240f);

        //[SerializeField] private Vector2 _distanceMinMax = new Vector2(10f, 300f);
        
        [SerializeField, Range(10f, 500f)] private float _distance = 145f;

        [SerializeField, Range(1f, 100f)] private float _rotationSpeed = 10f;

        [SerializeField] private Vector2 _zoomMinMax = new Vector2(10f, 500f);
        [SerializeField, Range(0.1f, 100f)] private float _zoomSensitivity = 10f;

        [SerializeField] private Vector2 _distanceToGroundMinMax = new Vector2(10f, 300f);
        [SerializeField, Range(1.0f, 100f)] private float _cameraSafeRadius = 10f;
        

        [SerializeField] [ReadOnly] private bool _isGrab;
        [SerializeField] [ReadOnly] private bool _isManualZoom;
        [SerializeField] [ReadOnly] private bool _isManualMove;

        [SerializeField] [ReadOnly] private bool _isManualRotation;
        [SerializeField] [ReadOnly] private float _distanceToGround;

        [SerializeField] private bool _stickToGround = false;
        [SerializeField] [ReadOnly] private bool _isCameraSphereHit;
        
        [SerializeField] private bool _debugMode = false;
        [SerializeField] private bool _debugLog = false;
        //[SerializeField][ReadOnly] private bool _isAutoRotation;

        private bool _inited = false;

        private Vector2 _prevMousePosition;
        private Vector2 _mouseDelta;

        [NonSerialized] private Camera _cameraMain;
        [NonSerialized] private Transform _cameraMainTransform;
        [NonSerialized] private Transform _focusPoint;
        [NonSerialized] private Transform _grabPoint;

        private Vector3 _grabDelta;
        private const float e = 0.001f;
        private Plane _groundPlane = new Plane(Vector3.up, Vector3.zero);
        private Vector2 _grabStartMousePos;
        private Vector3 _grabStartFocusPosition;

        private class DebugInfo
        {
            public Ray ray;
            public bool isHit;
            public Vector3 hitPos;
            public Ray ray0;
            public Ray ray1;
        }
        private List<DebugInfo> _focusPointList = new List<DebugInfo>();

        [Button]
        private void ClearDebug()
        {
            _focusPointList.Clear();
        }

        private const float PERSPECTIVE_CAMERA_NEAR_CLIPPLANE = 1f; // do not < 0.01
        private const float ORTHO_CAMERA_NEAR_CLIPPLANE = -50f;

        private void FindObjects()
        {
            _cameraMainTransform = transform.Find("Main Camera");
            Assert.IsNotNull(_cameraMainTransform);
            _focusPoint = transform.Find("FocusPoint");
            if (_focusPoint == null)
            {
                _focusPoint = new GameObject("FocusPoint").transform;
                _focusPoint.SetParent(transform, false);
            }
            _grabPoint = transform.Find("GrabPoint");
            if (_grabPoint == null)
            {
                _grabPoint = new GameObject("GrabPoint").transform;
                _grabPoint.SetParent(transform, false);
            }

            _grabPoint.position = _focusPoint.position;
            _cameraMain = _cameraMainTransform.GetComponent<Camera>();
        }
        
        private void InternalInit()
        {
            _isGrab = false;

            FindObjects();

            MainFind.state.cameraMain = _cameraMain;

            LoadFromConfig();
            
            // if (PhysUtility.TryRaycastGround(_focusPoint.position, out Vector3 groundPosition))
            // {
            //     _focusPoint.position = groundPosition;
            // }

            _cameraMainTransform.localRotation = Quaternion.Euler(_orbitAngles);
            _prevMousePosition = Input.mousePosition;
            ConstrainAngles(ref _orbitAngles);
            //SetCamera();

            _inited = true;
        }

        [Button]
        private void LoadFromConfig()
        {
            if (_configCamera != null && _configCamera.useConfig)
            {
                FindObjects();
                _focusPoint.position = _configCamera.focusPointPosition;
                _orbitAngles = _configCamera.orbitAngles;
                _distance = _configCamera.distance;
                _zoomSensitivity = _configCamera.zoomSensitivity;
            }
        }

        [Button]
        private void SaveToConfig()
        {
            if (_configCamera != null)
            {
                _focusPoint = transform.Find("FocusPoint");
                if (_focusPoint)
                    _configCamera.focusPointPosition = _focusPoint.position;
                _configCamera.orbitAngles = _orbitAngles;
                _configCamera.distance = _distance;
            }
        }

        protected override void Initialize()
        {
            base.Initialize();
            
            InternalInit();
        }

        public void SetFocusPoint(Vector3 position)
        {
            _focusPoint.position = position;
            if (PhysUtility.TryRaycastGround(_focusPoint.position, out Vector3 groundPosition))
            {
                _focusPoint.position = groundPosition;
            }
        }

        private void SetCamera()
        {
            //_cameraMain.orthographic = MainFind.Find.IsCameraOrthographic;
            _cameraMain.nearClipPlane = _cameraMain.orthographic
                ? ORTHO_CAMERA_NEAR_CLIPPLANE
                : PERSPECTIVE_CAMERA_NEAR_CLIPPLANE;
        }

        public void OnFixedUpdate()
        {
        }

        public void OnLateUpdate()
        {
        }

        [Button]
        void LateUpdate()
        {
            if (!_inited) InternalInit();
            //_updateCounter++;

            if (Application.isPlaying)
                _mousePos = Input.mousePosition;
            else
                _mousePos = Event.current.mousePosition;

            if (Application.isPlaying)
                _screenSize = new Vector2(Screen.width, Screen.height);
            else
                _screenSize = GetMainGameViewSize();

            if (_mousePos.x < 0 || _mousePos.x >= Screen.width) return;
            if (_mousePos.y < 0 || _mousePos.y >= Screen.height) return;

            _mouseDelta = _mousePos - _prevMousePosition;

            _isManualMove = false;
            
            _isManualZoom = ManualZoom();
            _isManualMove = ManualMove();
            _isManualRotation = ManualRotation();

            bool isChanged = false;
            isChanged |= _isManualZoom;
            isChanged |= _isManualMove;

            Quaternion lookRotation;
            if (_isManualRotation)
            {
                lookRotation = Quaternion.Euler(_orbitAngles);
            }
            else
            {
                lookRotation = _cameraMainTransform.localRotation;
            }

            Vector3 lookDirection = lookRotation * Vector3.forward;
            Vector3 lookPosition = _focusPoint.localPosition - lookDirection * _distance;

            // if (_debugMode)
            // {
            //     if (_isManualMove)
            //         _focusPointList.Add(_focusPoint.localPosition);
            // }
            
            //if (MainFind.Find.IsCameraOrthographic) _cameraMain.orthographicSize = _distance;

            //Vector3 castFrom = lookPosition;
            //var cameraHeight = castFrom.y;
            //castFrom.y = _zoomMinMax.y;

            //cubeCenter = castFrom;
            
            
            CheckDistanceToGround(lookPosition);

            float yDelta = 0f;
            if (_distanceToGround < _distanceToGroundMinMax.x)
                yDelta = _distanceToGroundMinMax.x - _distanceToGround;
            else if (_distanceToGround > _distanceToGroundMinMax.y)
                yDelta = - (_distanceToGround - _distanceToGroundMinMax.x);

            lookPosition.y += yDelta;

            if (yDelta != 0f)
            {
                _distance = Vector3.Distance(lookPosition, _focusPoint.localPosition);
                _cameraMainTransform.localPosition = lookPosition;
                _cameraMainTransform.LookAt(_focusPoint.localPosition);
                _orbitAngles = _cameraMainTransform.eulerAngles;
                //lookDirection = (lookPosition - _focusPoint.localPosition).normalized;

                // var ray = new Ray(lookPosition, lookDirection);
                // bool isHit = Physics.Raycast(ray, out var hit, float.MaxValue, MainFind.Find.LayerMaskGround);
                // if (isHit)
                // {
                //     _distance = hit.distance;
                //     _focusPoint.localPosition = hit.point;
                // }

                //bool isHit = PhysUtility.TryRaycastGround(centerScreenRay, out Vector3 hitPos);
                //lookDirection = (lookPosition - _focusPoint.localPosition).normalized;
                //_focusPoint.localPosition = 

            }
            else
            {
                _cameraMainTransform.localPosition = lookPosition;
                _cameraMainTransform.localRotation = lookRotation;
            }

            //_cameraMainTransform.SetPositionAndRotation(lookPosition, lookRotation);

            _prevMousePosition = _mousePos;
        }

        // private void OnDrawGizmos()
        // {
        //     if (ExecuteInEditMode)
        //         OnUpdate();
        // }

#if UNITY_EDITOR
        private void OnDrawGizmosSe()
        {
            if (_cameraMainTransform == null) return;

            if (isDebug)
            {
                Gizmos.color = Color.white;
                Gizmos.DrawLine(_focusPoint.position, _cameraMainTransform.position);
            }
            
            //Gizmos.color = Color.green;
            //Gizmos.DrawWireSphere(_cameraMainTransform.position, _cameraCastRadius);
            //Gizmos.color = Color.yellow;
            //Gizmos.DrawLine(_cameraMainTransform.position, _cameraMainTransform.position+cubeDir);

            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(_focusPoint.position, 1f);
            Gizmos.DrawLine(_focusPoint.position, _focusPoint.position + _grabDelta);

            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(_grabPoint.position, 1f);

            if (_debugMode)
            {
                Handles.color = Color.green;
                foreach (var info in _focusPointList)
                {
                    Handles.color = info.isHit ? Color.green : Color.red;
                    if (info.isHit)
                        Handles.DrawLine(info.hitPos, info.ray.origin);
                    else
                        Handles.DrawLine(info.ray.origin, info.ray.direction*1000f);
                    
                    Handles.color = Color.blue;
                    Handles.DrawLine(info.ray0.origin, info.ray0.direction*10000f);
                    Handles.color = Color.cyan;
                    Handles.DrawLine(info.ray1.origin, info.ray1.direction*10000f);
                }
            }
        }
#endif        

        private bool AutomaticRotation()
        {
            return false;
        }

        private bool ManualRotation()
        {
            Vector2 input = Vector2.zero;
            //input = new Vector2(Input.GetAxis("Vertical Camera"), Input.GetAxis("Horizontal Camera"));
            if (Input.GetMouseButton(1))
            {
                input.x = -_mouseDelta.y;
                input.y = _mouseDelta.x;
            }
            else return false;

            if (input.x < -e || input.x > e || input.y < -e || input.y > e)
            {
                var newOrbitAngles = _orbitAngles + _rotationSpeed * Time.unscaledDeltaTime * input;
                ConstrainAngles(ref newOrbitAngles);

                // if (false)
                // {
                //     var lookRotation = Quaternion.Euler(newOrbitAngles);
                //     var lookDirection = lookRotation * Vector3.forward;
                //     var lookPosition = _focusPoint.localPosition - lookDirection * _distance;
                //
                //     CheckDistanceToGround(lookPosition);
                //
                //     float yDelta = 0f;
                //     if (_distanceToGround < _distanceToGroundMinMax.x)
                //         yDelta = _distanceToGroundMinMax.x - _distanceToGround;
                //     else if (_distanceToGround > _distanceToGroundMinMax.y)
                //         yDelta = -(_distanceToGround - _distanceToGroundMinMax.x);
                //
                //     if (yDelta != 0f)
                //         return false;
                // }

                _orbitAngles = newOrbitAngles;
                //_lastManualRotationTime = Time.unscaledTime;
                return true;
            }

            return false;
        }

        private Ray CustomRay(Vector2 screenPos)
        {
            Vector3 worldPos =
                _cameraMain.ScreenToWorldPoint(new Vector3(screenPos.x, screenPos.y, Camera.main.nearClipPlane));
            Vector3 camPos = _cameraMain.transform.position;
            Vector3 dir = (worldPos - camPos).normalized;
            return new Ray(worldPos, dir);
        }

        private void CheckDistanceToGround(Vector3 cameraPosition)
        {
            if (!isEnableCollider)
            {
                _distanceToGround = Vector3.Distance(cameraPosition, _focusPoint.position);
                return;
            }

            Vector3 up = 1000 * Vector3.up;
            //var ray = new Ray(cameraPosition + up, Vector3.down);

            _isCameraSphereHit = Physics.SphereCast(cameraPosition+up, _cameraSafeRadius, Vector3.down, out var hitInfo, float.MaxValue, MainFind.state.LayerMaskGround);
            
            //bool isHit = PhysUtility.TryRaycastGround(ray, out Vector3 hitPos);
            if (_isCameraSphereHit)
                 _distanceToGround = cameraPosition.y - hitInfo.point.y;
            else
                 _distanceToGround = 0;
        }

        private bool ManualMove()
        {
            if (!Input.GetMouseButton(0))
            {
                _isGrab = false;
                return false;
            }

            if (!_isGrab)
            {
                // grab started
                _isGrab = true;
                _grabStartMousePos = _mousePos;
                _grabStartFocusPosition = _focusPoint.localPosition;
            }

            if (!(_mouseDelta.x < -e) && !(_mouseDelta.x > e) && !(_mouseDelta.y < -e) && !(_mouseDelta.y > e))
                return false;

            //if (_prevMousePosition == _mousePos) return false;

            _isGrab = true;
            Ray ray0 = _cameraMain.ScreenPointToRay(_grabStartMousePos);
            Ray ray1 = _cameraMain.ScreenPointToRay(_mousePos);
//            Ray ray0c = CustomRay(_grabStartMousePos);
//            Ray ray1c = CustomRay(_mousePos);
            if (ray0.origin == ray1.origin)
            {
                //Debug.LogError("rays is equals");
                //return false;
            }

            _groundPlane.Raycast(ray0, out var rayDistance0);
            _groundPlane.Raycast(ray1, out var rayDistance1);
            var planePoint0 = ray0.GetPoint(rayDistance0);
            var planePoint1 = ray1.GetPoint(rayDistance1);
            _grabDelta = planePoint1 - planePoint0;

            var newPosition = _grabStartFocusPosition - _grabDelta;

            Vector2 newPosScrPos = _cameraMain.WorldToScreenPoint(newPosition);
            //var centerScreenPos = _screenSize / 2f;
            Ray centerScreenRay = _cameraMain.ScreenPointToRay(newPosScrPos);
            bool isHit = PhysUtility.TryRaycastGround(centerScreenRay, out Vector3 hitPos);
            /*
            Debug.LogWarning("newPosition=" + newPosition + ", _grabDelta=" + _grabDelta + ", _mouseDelta=" +
                             _mouseDelta
                             + ", isHit=" + isHit + ", _isGrab=" + _isGrab
                             + ", newPosScrPos=" + newPosScrPos + ", hitPos=" + hitPos
                             + ", _prevMousePosition=" + _prevMousePosition + ", _mousePos=" + _mousePos
                             + ", pp0="+planePoint0+", pp1="+planePoint1
//                             + "\n, ray0=" + ray0 + ", ray1=" + ray1
//                             + "\n, ray0c=" + ray0c + ", ray1c=" + ray1c
                             );
            //*/
            
            if (_debugMode)
            {
                _focusPointList.Add(new DebugInfo()
                {
                    ray = centerScreenRay, isHit = isHit, hitPos = hitPos
                    , ray0 = ray0, ray1 = ray1,
                });
            }
            
            if (isHit)
            {
                if (_stickToGround)
                    newPosition = hitPos;
            }
            else
            {
                //_isGrab = false;
                return false;
            }

            //CheckDistanceToGround(_cameraMainTransform.localPosition);
            
            _focusPoint.localPosition = newPosition;


            return true;
        }

        private bool ManualZoom()
        {
            float d = _distance;
            //Mouse1 = Input.mouseScrollDelta;
            d += -Input.mouseScrollDelta.y * _zoomSensitivity;
            d = Mathf.Clamp(d, _zoomMinMax.x, _zoomMinMax.y);
            if (_distance == d) return false;
            _distance = d;

            return true;
        }

        void ConstrainAngles(ref Vector2 orbitAngles)
        {
            if (_isVerticalAngle90) 
                orbitAngles.x = 90;
            else
                orbitAngles.x = Mathf.Clamp(orbitAngles.x, _minVerticalAngle, _maxVerticalAngle);

            if (orbitAngles.y < 0f)
            {
                orbitAngles.y += 360f;
            }
            else if (orbitAngles.y >= 360f)
            {
                orbitAngles.y -= 360f;
            }
        }

        // C#
        public static Vector2 GetMainGameViewSize()
        {
            System.Type T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
            System.Reflection.MethodInfo GetSizeOfMainGameView = T.GetMethod("GetSizeOfMainGameView",
                System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
            System.Object Res = GetSizeOfMainGameView.Invoke(null, null);
            return (Vector2) Res;
        }
    }
}