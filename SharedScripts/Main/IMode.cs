﻿namespace Shared
{
    public interface IMode
    {
        public void Initialize();
    }
}