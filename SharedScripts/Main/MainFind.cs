﻿using System;
using UnityEngine;

namespace Shared
{
    public static class MainFind
    {
        public class FixedUpdateEvent : SimpleEvent
        {
        }

        public static class state
        {
            public static readonly int LayerGround = LayerMask.NameToLayer("Ground");
            public static readonly int LayerMaskGround = 1 << LayerMask.NameToLayer("Ground");
            public static readonly int LayerWater = LayerMask.NameToLayer("Water");
            public static readonly int LayerMaskWater = 1 << LayerMask.NameToLayer("Water");
            public static readonly int LayerMaskGroundAndWater = (1 << LayerMask.NameToLayer("Ground")) + (1 << LayerMask.NameToLayer("Water"));
            public static readonly int LayerMaskRockTree = 1 << LayerMask.NameToLayer("RockTree");
            public static readonly int LayerMaskPart = 1 << LayerMask.NameToLayer("Part");

            public static ManagerMain main;
            public static ILoader loader;
            //public static Events events;

            public static Camera cameraMain;
            //public static bool isCameraOrthographic;
            
            //public static Action initialize;
        }
        
        public static class events
        {
            public static TrackedEvent awake  =new TrackedEvent();
            public static TrackedEvent start  =new TrackedEvent();
            //public static Action awake;
            public static Action update;// = new SimpleEvent();
            public static Action lateUpdate;// = new SimpleEvent();
            public static Action fixedUpdate;// = new FixedUpdateEvent();
            public static Action<Transform> cameraFollow;// = new SimpleEvent<Transform>();

            public static void Reset()
            {
                awake = new TrackedEvent();
                update = null;
                lateUpdate = null;
                fixedUpdate = null;
                cameraFollow = null;
            }
        }

        public static void Reset()
        {
            events.Reset();
        }

        public static void ClearConsoleAndGraph()
        {
            ExeGraph.Clear();
            ClearConsole();
        }

        public static void ClearConsole()
        {
            var logEntries = System.Type.GetType("UnityEditor.LogEntries, UnityEditor.dll");

            var clearMethod = logEntries.GetMethod("Clear",
                System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);

            clearMethod.Invoke(null, null);
        }
        
        public static void EditorRepaintScene()
        {
#if UNITY_EDITOR
            UnityEditor.EditorWindow view = UnityEditor.EditorWindow.GetWindow<UnityEditor.SceneView>();
            view.Repaint();
#endif
        }
    }
}