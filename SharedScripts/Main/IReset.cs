namespace Shared
{
    public interface IReset
    {
        public void OnReset();
    }
}