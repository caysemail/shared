﻿using System;
using UnityEngine;

namespace Shared
{
    //[ExecuteAlways]
    //[ExecuteInEditMode]
    public class MainView : MonoBehaviour
    {
        [SerializeField] private int _targetFramerate = -1;
        [SerializeField] private bool IsFixedUpdate = false;
        [SerializeField] private bool IsUpdate = false;
        [SerializeField] private bool IsLateUpdate = false;
        [SerializeField] private bool PlayInEditor = false;
        [SerializeField] [ReadOnly] private int fixedCount = 0;
        [SerializeField] [ReadOnly] private int updateCount = 0;

        [NonSerialized] private bool _isInited;

        private void OnEnable()
        {
            //Debug.LogError("ManagerMain.OnEnable: enabled="+this.enabled+", playing="+Application.isPlaying);
            InitializeLocal();
        }
        private void OnDisable()
        {
            //Debug.LogError("ManagerMain.OnDisable: enabled="+this.enabled+", playing="+Application.isPlaying);
        }
        
        [Button]
        private void Awake()
        {
            if (!Application.isPlaying) MainFind.ClearConsole();

            ResetOnCompilation(transform);

            ExeGraph.Clear();
            Debug.Log("ManagerMain.Awake: enabled="+this.enabled+", playing="+Application.isPlaying);
            fixedCount = 0;
            if (!enabled) return;

            if (Camera.main)
            {
                Debug.Log("Camera(main scaledPixel): " + Camera.main.scaledPixelWidth + "x" + Camera.main.scaledPixelHeight);
            }
            else
            {
                Debug.LogWarning("Camera.main not found");
            }

            InitializeLocal();

            IViewExt.InitializeChildren(transform);

            MainFind.events.awake.Invoke();
        }

        private void Start()
        {
            MainFind.events.start.Invoke();
        }

        public static void ResetOnCompilation(Transform transform)
        {
#if UNITY_EDITOR
            transform.ResetChildren();
            
            UnityEditor.Compilation.CompilationPipeline.compilationStarted -= OnCompilationStarted;
            UnityEditor.Compilation.CompilationPipeline.compilationStarted += OnCompilationStarted;
            
            void OnCompilationStarted(object obj)
            {
                MainFind.Reset();
                transform.ResetChildren();
            }
#endif
        }

        private void InitializeLocal()
        {
            if (Application.isPlaying || PlayInEditor)
            {
                fixedCount = 0;
                updateCount = 0;
                Application.targetFrameRate = _targetFramerate;
                //MainFind.state.main = this;
                //MainFind.Find.events = new MainFind.Events();
                _isInited = true;
                //MainFind.Find.initialize?.Invoke();
            }
        }
        
        private void FixedUpdate()
        {
            //if (!Application.isPlaying) return;
            if (!_isInited) return;
            if (!IsFixedUpdate) return;

            MainFind.events.fixedUpdate?.Invoke();
            fixedCount++;
        }

        private void Update()
        {
            //if (!Application.isPlaying) return;
            if (!_isInited) return;
            if (!IsUpdate) return;

            MainFind.events.update?.Invoke();
            updateCount++;
        }

        private void LateUpdate()
        {
            //if (!Application.isPlaying) return;
            if (!_isInited) return;
            if (!IsLateUpdate) return;

            MainFind.events.lateUpdate?.Invoke();
        }
    }
}