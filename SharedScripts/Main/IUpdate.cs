namespace Shared
{
    public interface IUpdate
    {
        public void OnUpdate();
    }
}