﻿using System;
using UnityEngine;

namespace Shared
{
    //[ExecuteAlways]
    //[ExecuteInEditMode]
    public class ManagerMain : Manager
    {
        [SerializeField] private int _targetFramerate = -1;
        [SerializeField] private bool IsFixedUpdate = false;
        [SerializeField] private bool IsUpdate = false;
        [SerializeField] private bool IsLateUpdate = false;
        [SerializeField] private bool PlayInEditor = false;
        [SerializeField] [ReadOnly] private int fixedCount = 0;
        [SerializeField] [ReadOnly] private int updateCount = 0;

        [NonSerialized] private bool _isInited;

        private void OnEnable()
        {
            //Debug.LogError("ManagerMain.OnEnable: enabled="+this.enabled+", playing="+Application.isPlaying);
            InitializeLocal();
        }
        private void OnDisable()
        {
            //Debug.LogError("ManagerMain.OnDisable: enabled="+this.enabled+", playing="+Application.isPlaying);
        }
        
        [Button]
        private void Awake()
        {
            ExeGraph.Clear();
            if (!Application.isPlaying) MainFind.ClearConsole();
            Debug.Log("ManagerMain.Awake: enabled="+this.enabled+", playing="+Application.isPlaying);
            fixedCount = 0;
            if (!enabled) return;

            if (Camera.main)
            {
                Debug.Log("Camera: " + Camera.main.scaledPixelWidth + "x" + Camera.main.scaledPixelHeight);
            }
            else
            {
                Debug.LogWarning("Camera.main not found");
            }
            
            InitializeLocal();

            this.InitializeMaster();
            _isInited = true;
            this.InitializeChilds();
            
            MainFind.events.awake.Invoke();
        }

        protected override void Initialize()
        {
            Debug.Log("ManagerMain.Initialize");
            base.Initialize();
        }

        private void InitializeLocal()
        {
            if (Application.isPlaying || PlayInEditor)
            {
                fixedCount = 0;
                updateCount = 0;
                Application.targetFrameRate = _targetFramerate;
                MainFind.state.main = this;
                //MainFind.Find.events = new MainFind.Events();
                _isInited = true;
                //MainFind.Find.initialize?.Invoke();
            }
        }
        
        private void FixedUpdate()
        {
            //if (!Application.isPlaying) return;
            if (!_isInited) return;
            if (!IsFixedUpdate) return;

            MainFind.events.fixedUpdate?.Invoke();
            fixedCount++;
        }

        private void Update()
        {
            //if (!Application.isPlaying) return;
            if (!_isInited) return;
            if (!IsUpdate) return;

            MainFind.events.update?.Invoke();
            updateCount++;
        }

        private void LateUpdate()
        {
            //if (!Application.isPlaying) return;
            if (!_isInited) return;
            if (!IsLateUpdate) return;

            MainFind.events.lateUpdate?.Invoke();
        }
    }
}