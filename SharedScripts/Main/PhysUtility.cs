﻿using UnityEngine;
using static Shared.MainFind;

namespace Shared
{ 
    public static class PhysUtility
    {
        private static float _maxRaycastValue = 65000f;

        public static bool TryRaycastGround(Vector3 position, out float height)
        {
            height = 0;
            bool hitGround = Physics.Raycast(position + Vector3.up * _maxRaycastValue / 2f,
                Vector3.down, out var hit, _maxRaycastValue, MainFind.state.LayerMaskGround);
            if (hitGround)
            {
                //Debug.LogWarning("hit.point="+hit.point+", n="+hit.normal+", distance="+hit.distance+", layer="+Find.LayerMaskGround);
                height = Vector3.Distance(hit.point, position);
                if (hit.point.y > position.y)
                    height = -height;
            }

            return hitGround;
        }

        public static bool TryRaycastGround(Vector3 position, out Vector3 hitPosition)
        {
            hitPosition = position;
            bool hitGround = Physics.Raycast(position + Vector3.up * _maxRaycastValue / 2f,
                Vector3.down, out var hit, _maxRaycastValue, MainFind.state.LayerMaskGround);
            if (hitGround)
            {
                hitPosition = hit.point;
                return true;
            }

            return false;
        }
        
        public static bool TryRaycastDown(Vector3 position, int layerMask)
        {
            bool hitGround = Physics.Raycast(position + Vector3.up * _maxRaycastValue / 2f,
                Vector3.down, out var hit, _maxRaycastValue, layerMask);
            return hitGround;
        }

        public static bool TryRaycastGround(Vector3 position, out RaycastHit hit)
        {
            bool hitGround = Physics.Raycast(position + Vector3.up * _maxRaycastValue / 2f,
                Vector3.down, out hit, _maxRaycastValue, MainFind.state.LayerMaskGround);
            return hitGround;
        }

        public static bool TryRaycastWaterAgainstGround(Vector3 position, out Vector3 hitPosition)
        {
            hitPosition = position;
            bool hitGround = Physics.Raycast(position + Vector3.up * _maxRaycastValue / 2f,
                Vector3.down, out var hit, _maxRaycastValue, MainFind.state.LayerMaskGroundAndWater);
            if (hitGround)
            {
                if (hit.transform.gameObject.layer == MainFind.state.LayerGround)
                    return false;
                hitPosition = hit.point;
                return true;
            }

            return false;
        }

        public static bool TryRaycastGround(Ray ray, out Vector3 hitPosition)
        {
            hitPosition = Vector3.zero;
            bool hitGround = Physics.Raycast(ray, out var hit, float.PositiveInfinity, MainFind.state.LayerMaskGround);
            if (hitGround)
            {
                hitPosition = hit.point;
                return true;
            }

            return false;
        }

        public static float GetDeltaAngleX(Quaternion rotation)
        {
            var angle = Quaternion.Angle(rotation, Quaternion.identity);
            if (rotation.eulerAngles.x > 180)
                angle = -angle;
            return angle;
        }
    }
}