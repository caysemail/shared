﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Shared
{
    public class Camera2dView : MonoBehaviour, IView
    {
        [Serializable]
        public class CameraProvider
        {
            public bool followTarget;
            public Vector2 targetPosition;
            public Camera cameraMain;
            public Transform target;
        }
        
        [SerializeField] private GraphicRaycaster m_Raycaster;
        [SerializeField] private bool _enableZoom = true;
        //[SerializeField] private Vector2 _distanceMinMax = new Vector2(10f, 300f);
        [SerializeField] private float _currentDistance;
        [SerializeField] private float _distance = 40f;

        [SerializeField] private Vector2 _zoomMinMax = new Vector2(2, 82);
        [SerializeField, Range(0.1f, 100f)] private float _zoomSensitivity = 10f;
        [SerializeField, Range(0.1f, 100f)] private float _zoomSpeed = 10f;

        [SerializeField, Range(0.01f, 1f)] private float _moveSensitivity = 0.1f;
        [SerializeField, Range(0.01f, 0.9f)] private float _followSpeed = 0.3f;

        private const bool isCameraOrthographic = true;
        
        private const float PERSPECTIVE_CAMERA_NEAR_CLIPPLANE = 1f; // do not < 0.01
        private const float ORTHO_CAMERA_NEAR_CLIPPLANE = -2f;

        public Vector2 moveDelta;
        public Vector2 lastMousePos;
        public Vector3 mapPos;
        public Vector2 newMousePos;

        private bool _isOutOfScreen = false;
        private bool _isMoveActive = false;
        private bool _isPositionChangedAtPreviousLastFrame = false;

        private Camera _bgCamera;
        //private Camera _liquidCamera;
        //private Camera _fgCamera;

        [ReadOnly]
        public Vector2 mousePosition;
        public CameraProvider cameraProvider = null;

        public void Initialize()
        {
            //if (!Application.isPlaying) return;
            
            cameraProvider = new CameraProvider();
            MainFind.events.update += OnUpdate;
            MainFind.events.cameraFollow += OnCameraFollow;
            
            cameraProvider.cameraMain = transform.GetComponentInChildren<Camera>(true);
            _bgCamera = cameraProvider.cameraMain;
            //_liquidCamera = transform.Find("Liquid Camera").GetComponent<Camera>();
            //_fgCamera = transform.Find("Foreground Camera").GetComponent<Camera>();
            
            MainFind.state.cameraMain = cameraProvider.cameraMain;
            MainFind.state.cameraMain.orthographic = isCameraOrthographic;
            MainFind.state.cameraMain.nearClipPlane = isCameraOrthographic ? ORTHO_CAMERA_NEAR_CLIPPLANE : PERSPECTIVE_CAMERA_NEAR_CLIPPLANE;
            _currentDistance = _distance;
            _bgCamera.orthographicSize = _currentDistance;
            // init start pos
            //transform.localPosition = Find.configs.main.mapSize.ToVector3 / 2f;
        }

        private void OnCameraFollow(Transform target)
        {
            cameraProvider.target = target;
            cameraProvider.followTarget = true;
        }
            
        private void OnUpdate()
        {
            ProcessFollowTarget();

            _isPositionChangedAtPreviousLastFrame = false;
            var mousePos = Input.mousePosition;
            if (mousePos.x < 0 || mousePos.y < 0 || mousePos.x > Screen.width || mousePos.y > Screen.height)
            {
                _isOutOfScreen = true;
                return;
            }

            if (_isOutOfScreen)
            {
                _isOutOfScreen = false;
                this.lastMousePos = Input.mousePosition;
                return;
            }

            mapPos = MainFind.state.cameraMain.ScreenToWorldPoint(mousePos);
             
            var isZoom = ManualZoom();
            var isMove = ManualMove();
            
            if (_currentDistance != _distance)
                isZoom = true;

            if (!_enableZoom && isZoom)
            {
                isZoom = false;
                _currentDistance = _distance;
            }

            bool isPositionChanged = lastMousePos != (Vector2) mousePos;
            if (isPositionChanged)
            {
                mousePosition = mousePos;
                //  Find.data.cursorPosition = mousePos;
                // Find.data.cursorWorldPosition = mapPos;
                //  Find.data.cursorMapPosition = mapPos.ToVector2Int();
                //  Find.events.cursorUpdated.Invoke();
            }

            if (Input.GetMouseButtonUp(0))
            {
              //  Find.data.clicks.select = !IsInputOnUi();
            }
            if (Input.GetMouseButtonUp(1) && !isPositionChanged && !_isMoveActive)
            {
               // Find.data.clicks.deselect = true;
            }

            this.lastMousePos = Input.mousePosition;

            if (Input.GetMouseButtonUp(1))
            {
                _isMoveActive = false;
                //Debug.Log("OnUpdate: _isMoveActive="+_isMoveActive);
            }

            if (!isZoom && !isMove)
                return;

            var position = transform.localPosition;
            
            if (isZoom)
            {
                _currentDistance = Mathf.Lerp(_currentDistance, _distance, Time.deltaTime*_zoomSpeed);
                var delta = _distance - _currentDistance;
                if (delta > -1 && delta < 1)
                    _currentDistance = _distance;
                //Find.cameraMain.orthographicSize = _currentDistance;

                _bgCamera.orthographicSize = _currentDistance;
                //_liquidCamera.orthographicSize = _currentDistance;
                //_fgCamera.orthographicSize = _currentDistance;

                newMousePos = MainFind.state.cameraMain.WorldToScreenPoint(mapPos);
                //transform.localPosition = newMousePos;
            }
            else if (isMove)
            {
                position += (Vector3) (moveDelta * _currentDistance / 20f );
            }

            if (position.x < 0) position.x = 0;
            if (position.y < 0) position.y = 0;

            transform.localPosition = position;
            _isPositionChangedAtPreviousLastFrame = true;
        }

        private void ProcessFollowTarget()
        {
            if (!cameraProvider.followTarget)
                return;
            if (_isPositionChangedAtPreviousLastFrame)
                return;
            if (Input.GetMouseButton(1))
                return;
            
            Vector2 position = transform.localPosition;
            cameraProvider.targetPosition = cameraProvider.target.position;
            if ((Vector2)position == cameraProvider.targetPosition)
                return;

            Vector2 dir = cameraProvider.targetPosition - position;
            position += dir * _followSpeed;
            transform.localPosition = position;
        }

        private bool IsInputOnUi()
        {
            //Set up the new Pointer Event
            PointerEventData m_PointerEventData = new PointerEventData(null);
            //Set the Pointer Event Position to that of the mouse position
            m_PointerEventData.position = Input.mousePosition;

            //Create a list of Raycast Results
            List<RaycastResult> results = new List<RaycastResult>();

            //Raycast using the Graphics Raycaster and mouse click position
            m_Raycaster.Raycast(m_PointerEventData, results);

            return results.Count > 0;

            //For every result returned, output the name of the GameObject on the Canvas hit by the Ray
            //foreach (RaycastResult result in results)
                //Debug.Log("Hit " + result.gameObject.name);
        }

        private bool ManualMove()
        {
            if (!Input.GetMouseButton(1))
            {
                return false;
            }
            
            var mouseMove = (Vector2) Input.mousePosition - lastMousePos;
            if (mouseMove == Vector2.zero)
                return false;

            //Debug.Log("ManualMove: _isMoveActive="+_isMoveActive);
            _isMoveActive = true;

            moveDelta = -(Vector3) (mouseMove * _moveSensitivity);
            return true;
        }

        private bool ManualZoom()
        {
            float d = _distance;
            //Mouse1 = Input.mouseScrollDelta;
            d += -Input.mouseScrollDelta.y * _zoomSensitivity * (_currentDistance / 20f);
            d = Mathf.Clamp(d, _zoomMinMax.x, _zoomMinMax.y);
            if (_distance == d) return false;
            _distance = d;
        
            return true;
        }
    }
}