using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public class TextureToMeshBeh : MonoBehaviour
    {
        public Texture2D Texture;
        public float Height = 256;
        [SerializeField]
        private int _antialiasCount = 0;
        [SerializeField]
        private int _antialiasSize = 1;

        
        [Button]
        private void MakeAntiAlias1()
        {
            _antialiasCount = 1;
            CreateMesh();
        }
        
        [Button]
        private void MakeAntiAlias0()
        {
            _antialiasCount = 0;
            CreateMesh();
        }

        [Button]
        private void CreateMesh()
        {
            var width = Texture.width;
            var height = Texture.height;

            var pixels = Texture.GetPixels();

            var pixels2 = new Color[pixels.Length];

            MeshSection section = new MeshSection();

            List<int> deltaIndices = new List<int>()
            {
                0, 1, 1 * width, 1 * width + 1
            };

            int aaSize = _antialiasSize;
            for (int i = 0; i < _antialiasCount; i++)
            {
                for (int y = 0; y < height - aaSize; y++)
                {
                    for (int x = 0; x < width - aaSize; x++)
                    {
                        var index = x + y * width;
                        //var pixel = pixels[x + y * width];
                        Color color = Color.black;
                        for (int ax = 0; ax < aaSize; ax++)
                        {
                            for (int ay = 0; ay < aaSize; ay++)
                            {
                                var aaIndex = ax + ay * width;
                                var pixel = pixels[index + aaIndex];
                                color += pixel;
                            }
                        }
                        color /= aaSize * aaSize;
                        pixels2[index] = color;
                    }
                }
                pixels = pixels2;
                pixels2 = new Color[pixels.Length];
            }
            
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    var pixel = pixels[width-x-1 + (height-y-1) * width];
                    //var pixel = Texture.GetPixel(width-x-1, height-y-1);
                    var grayscale = (pixel.r + pixel.g + pixel.b) / 3f;
                    grayscale = pixel.grayscale;
                    var grayPixel = grayscale;

                    section.Vertices.Add(new Vector3(x, grayPixel*Height, y));
                    section.Colors.Add(pixel);
                }
            }
            
            for (int y = 0; y < height-1; y++)
            {
                for (int x = 0; x < width-1; x++)
                {
                    //if (x == 0 || y == 0) continue;
                    
                    // section.Indices.Add(width * x + y); //Top right
                    // section.Indices.Add(width * x + y - 1); //Bottom right
                    // section.Indices.Add(width * (x - 1) + y - 1); //Bottom left - First triangle
                    // section.Indices.Add(width * (x - 1) + y - 1); //Bottom left 
                    // section.Indices.Add(width * (x - 1) + y); //Top left
                    // section.Indices.Add(width * x + y); //Top right - Second triangle

                    section.Indices.Add((x ) + (y)*width ); //Bottom left - First triangle
                    section.Indices.Add((x) + (y+1)*width); //Top left
                    section.Indices.Add((x+1) + (y+1)*width); //Top right
                    section.Indices.Add((x+1) + (y)*width); //Bottom right
                    
                    //break;
                }

                //break;
            }
            
            //Vector2[] uvs = new Vector2[section.Vertices.Count];
            //for (var i = 0; i < uvs.Length; i++) //Give UV coords X,Z world coords
            //    uvs[i] = new Vector2(verts[i].x, verts[i].z);
            
            var meshFilter = transform.GetOrAddComponent<MeshFilter>();
            var meshRenderer = transform.GetOrAddComponent<MeshRenderer>();
            
            var mesh = new Mesh();
            meshFilter.mesh = mesh;
            
            section.Apply(mesh, new MeshSection.ApplyMask() {normalsAndTangents = true, indices = true, vertices = true, quads = true, colors = true});
        }
    }
}