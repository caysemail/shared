﻿using UnityEngine;

namespace Shared
{
    public class StaticBatcher : MonoBehaviour
    {
        [Button]
        private void Bake()
        {
            var renderer = GetComponent<MeshRenderer>();
            if (renderer == null)
                renderer = gameObject.AddComponent<MeshRenderer>();
            var meshFilter = GetComponent<MeshFilter>();
            if (meshFilter == null)
                meshFilter = gameObject.AddComponent<MeshFilter>();
            StaticBatchingUtility.Combine(this.gameObject);
        }
    }
}