using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;

namespace Shared
{
    public class SimpleAsyncEvent<T>
    {
        private readonly List<Func<T, UniTask>> _callbacks = new List<Func<T, UniTask>>();

        public void AddListener(Func<T, UniTask> callback)
        {
            _callbacks.Add(callback);
        }

        public void RemoveListener(Func<T, UniTask> callback)
        {
            _callbacks.Remove(callback);
        }

        public async UniTask InvokeAsync(T arg)
        {
            var callbacks = new List<Func<T, UniTask>>(_callbacks);
            var maxCount = callbacks.Count;
            for (var index = 0; index < maxCount; index++)
            {
                await callbacks[index].Invoke(arg);
            }
        }
    }
}