﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class FpsMeter : MonoBehaviour
{
    private float deltaTime = 0.0f;
    private float _updateTimer;
    private Text _text;
    private List<float> _values = new List<float>();

    private void Awake()
    {
        _text = GetComponent<Text>();
    }

    void Update()
    {
        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
        _updateTimer += Time.deltaTime;
        if (_updateTimer > 0.1f)
        {
            _updateTimer = 0;
            float msec = deltaTime * 1000.0f;
            float fps = 1.0f / deltaTime;
            _values.Add(fps);
            if (_values.Count > 50)
                _values.RemoveAt(0);
            var average = _values.Sum() / _values.Count;
            _text.text = $"{msec:0.0} ms ({fps:0.} fps) (~ {average:0.} fps)";
        }
    }
}