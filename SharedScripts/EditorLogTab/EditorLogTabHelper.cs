﻿using UnityEngine.Events;

namespace Shared
{
    public static class EditorLogTabHelper
    {
        public class EditorMessageEvent : UnityEvent<string, string> { }

        public static EditorMessageEvent MessageEvent = new EditorMessageEvent();

//        public class EditorMessage
//        {
//            public string Tab;
//            public string Message;
//
//            public EditorMessage(string tab, string message)
//            {
//                Tab = tab;
//                Message = message;
//            }
//        }
//
//        public static UniRx.ReactiveProperty<EditorMessage> Message = new ReactiveProperty<EditorMessage>();
    }
}
