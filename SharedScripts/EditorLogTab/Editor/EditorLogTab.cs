﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;

using UnityEditor;
using UnityEngine;

namespace Shared
{
    public class EditorLogTab : EditorWindow //SearchableEditorWindow
    {
        //private string myString = "Hello World";
        private string tab = "";
        private string message = "";
        bool groupEnabled;
        //bool myBool = true;
        //float myFloat = 1.23f;
        private int _selectedTab = 0;

        private List<Tuple<string, string>> _messages = new List<Tuple<string, string>>();
        private List<string> _tabsClean = new List<string>();
        private List<string> _tabs = new List<string>();
        private List<string> _tabsMessage = new List<string>();
        private static bool _isActive = false;
        private Dictionary<string, List<string>> _tabSubTabs = new Dictionary<string, List<string>>();

        private static EditorLogTab Instance;// => GetWindow<EditorLogTab>();
        //private static IDisposable _subscription;

        //        // Add menu item named "My Window" to the Window menu
        //        [MenuItem("Robot Warfare/Test Editor log tab")]
        //        public static void ShowWindow()
        //        {
        //            //Show existing window instance. If one doesn't exist, make one.
        //            EditorWindow.GetWindow(typeof(EditorLogTab));
        //            _subscription = EditorLogTabHelper.Message.Subscribe(UniRx.Observer.Create<EditorMessage>(AddMessage));
        //        }

        private void Awake()
        {
            //_isActive = true;
            //Debug.LogWarning("EditorLogTab.Awake: _isActive="+ _isActive + ", Instance=" + Instance);
        }

        private void TestMessages()
        {
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
            ProcessMessage("Test Awake", "1 fdslk fjsdlkf jsdfj sdlkf jlksdfj lksdjf lksdlkf jlksdf sdf sdfjlsdjflksdjfljsdlf lksdflksdjlfksldkjf jklsdjflk sdlkf sd");
            ProcessMessage("Test Awake", "2 fsd sfd\n sdf sdf \n sdf sdf sdf sdf\n sdfddddddddddddddddddddddddddddddddfsfsdfsdfsdkjhfjasdhfkjasdkjfhksjafhkjsadkjfhksjdafkjsdkjfhsdkjfkjsdfkjsdkjfkjsdhkjfsdakjfh");
        }

        private void Clear()
        {
            _messages.Clear();
            _tabs.Clear();
            _tabsMessage.Clear();
            _tabSubTabs.Clear();
            _tabsClean.Clear();
        }

        private void ClearCurrentTab()
        {
        }

        private void OnEnable()
        {
            Clear();
            Instance = this;
            _isActive = true;
            EditorLogTabHelper.MessageEvent.AddListener(AddMessage);
            //Debug.LogWarning("EditorLogTab.OnEnable: _isActive=" + _isActive + ", Instance=" + Instance);
        }

        private void OnDisable()
        {
            Instance = null;
            _isActive = false;
            EditorLogTabHelper.MessageEvent.RemoveListener(AddMessage);
            //Debug.LogWarning("EditorLogTab.OnDisable: _isActive=" + _isActive + ", Instance=" + Instance);
        }

        private void OnDestroy()
        { EditorLogTabHelper.MessageEvent.RemoveListener(AddMessage); }

        private static void AddMessage(string tab, string message)
        {
            if (_isActive)
            Instance.ProcessMessage(tab, message);
        }

//        private static void AddMessage(EditorMessage newMessage)
//        {
//            Debug.LogWarning("EditorLogTab.AddMessage: ");
//            Instance.ProcessMessage(newMessage);
//        }

        private void ProcessMessage(string tab, string message)
        {
            if (tab == null) tab = "";
            if (message == null) message = "";
            _messages.Add(new Tuple<string, string>(tab, message));

            if (!_tabs.Contains(tab)) _tabs.Add(tab);
            var index = _tabs.IndexOf(tab);
            if (index >= _tabsMessage.Count)
                _tabsMessage.Add(""+tab);
            _tabsMessage[index] += "\n" + message;
            //Debug.LogWarning("EditorLogTab.ProcessMessage: tab=" + tab + ", message=" + message+", index="+index + ", tabsMessages=" + _tabsMessage.Count + ", _tabs=" + _tabs.Count);
            //bool isNull = newMessage == null;
            //Debug.LogWarning("EditorLogTab.ProcessMessage: "+(isNull?"is null":""));
            //if (isNull)
            //return;
            this.tab = tab;
            this.message = message;
        }

        // Add menu named "My Window" to the Window menu
        [MenuItem("Shared/Editor log tabs")]
        private static void Init()
        {
        // Get existing open window or if none, make a new one:
            EditorLogTab window = (EditorLogTab)EditorWindow.GetWindow(typeof(EditorLogTab));
            window.Show();
            Instance = window;
            _isActive = true;
            EditorLogTabHelper.MessageEvent.AddListener(AddMessage);
            //_subscription = EditorLogTabHelper.Message.Subscribe(UniRx.Observer.Create<EditorMessage>(AddMessage));
        }

//        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
//        private static void OnBeforeSceneLoadRuntimeMethod()
//        {
//            Debug.LogWarning("EditorLogTab.OnBeforeSceneLoadRuntimeMethod: _isActive=" + _isActive+ ", Instance=" + Instance);
//            if (!_isActive) return;
//            Instance._messages.Clear();
//            EditorLogTabHelper.MessageEvent.RemoveListener(AddMessage);
//            EditorLogTabHelper.MessageEvent.AddListener(AddMessage);
//        }

        Vector2 scrollPosition;
        //private float subTubIndex = 0;

        void OnGUI()
        {
            //GUILayout.Label("Tabs:", EditorStyles.boldLabel);
//            string[] options = new string[]
//            {
//                "Option1", "Option2", "Option3",
//            };
            _selectedTab = EditorGUILayout.Popup("", _selectedTab, _tabs.ToArray());
            //myString = EditorGUILayout.TextField("Tab=", _selectedTab>=_tabs.Count?"":_tabs[_selectedTab]);

            if (_selectedTab < 0 || _selectedTab >= _tabsMessage.Count)
            {
                //EditorGUILayout.LabelField("Wrong index="+_selectedTab+", tabsMessages="+_tabsMessage.Count+ ", _tabs="+ _tabs.Count);
                return;
            }

            string tab = _tabs[_selectedTab];
            List<string> subTabs = null;
            if (_tabSubTabs.TryGetValue(tab, out subTabs))
            {
                //subTubIndex = EditorGUILayout.IntSlider(subTubIndex, 1, subTabs.Count);
            }
            //subTubIndex = EditorGUILayout.IntSlider(subTubIndex, 1, 10);

            // Begin a scroll view. All rects are calculated automatically -
            // it will use up any available screen space and make sure contents flow correctly.
            // This is kept small with the last two parameters to force scrollbars to appear.
            //scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

            //myString = EditorGUILayout.TextField("Text Field", myString);
            EditorGUILayout.LabelField(_tabsMessage[_selectedTab], GUI.skin.textArea);
            //Debug.LogWarning("_tabsMessage[_selectedTab]="+ _tabsMessage[_selectedTab]);
            // End the scrollview we began above.
            GUILayout.EndScrollView();

            //myString = EditorGUILayout.TextField("Text Field", myString);
            //tab = EditorGUILayout.TextField("Tab", tab);
            //message = EditorGUILayout.TextField("Message", message);

            if (GUILayout.Button("Clear this tab"))
            {
                ClearCurrentTab();
            }

//            groupEnabled = EditorGUILayout.BeginToggleGroup("Optional Settings", groupEnabled);
//            myBool = EditorGUILayout.Toggle("Toggle", myBool);
//            myFloat = EditorGUILayout.Slider("Slider", myFloat, -3, 3);
//            EditorGUILayout.EndToggleGroup();
        }
    }
}

#endif