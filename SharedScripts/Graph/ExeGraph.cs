using System;
using System.Diagnostics;
using System.Linq;
using Cysharp.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using XNode;
using Debug = UnityEngine.Debug;

namespace Shared
{
    [CreateAssetMenu]
    public class ExeGraph : XNode.NodeGraph
    {
        private static ExeGraph _singleton;

        private static string TARGET_NAMESPACE = "MechFleet";
        private static int NODE_WIDTH = 240;
        private static int NODE_HEIGHT = 80;

        public static bool LogEvents = false;
        
        private static ExeGraph GetSingleton()
        {
            if (_singleton == null)
            {
                string path = "Assets/Graph/ExeGraph.asset";
                _singleton = (ExeGraph)AssetDatabase.LoadAssetAtPath(path, typeof(ExeGraph));
                if (_singleton == null)
                    throw new UnityException("not found asset at path=" + path);
            }

            return _singleton;
        }

        public new static void Clear()
        {
            var graph = GetSingleton();
            ((NodeGraph)graph).Clear();
        }

        private static void AddObject(string classFrom, string methodFrom, string classTo, string methodTo)
        {
            var graph = GetSingleton();
            var nodes = graph.nodes;
            if (nodes == null)
            {
                Debug.LogError("nodes is null");
                return;
            }
            var fromName = classFrom + "." + methodFrom;
            var toName = classTo + "." + methodTo;
            
            var nodeFrom = (ExeNode) nodes.FirstOrDefault(t => t.name == classFrom);
             if (LogEvents)
                 Debug.LogWarning("AddObject: classFrom="+classFrom+", nodeFrom="+nodeFrom+", nodes="+nodes.Count+", fromName="+fromName);
             if (nodeFrom == null)
             {
                 //Debug.LogWarning("node not found with fromName="+fromName+" - add as new, nodes="+nodes.Count);
                 AddObject(classFrom, methodFrom, 0);
                 //return;
                 nodeFrom = (ExeNode) nodes.FirstOrDefault(t => t.name == classFrom);
             }
             if (nodeFrom == null)
             {
                 Debug.LogError("node not found with fromName="+fromName+", nodes="+nodes.Count);
                 //return;
             }

            // var nodeFrom = (ExeNode) nodes.FirstOrDefault(t => t.name == fromName);
            // if (nodeFrom == null)
            // {
            //      AddObject(classFrom, methodFrom, 0);
            //      nodeFrom = (ExeNode) nodes.FirstOrDefault(t => t.name == fromName);
            // }
            if (nodeFrom == null)
                Debug.LogError("AddObject: node not found name="+fromName+", nodes="+nodes?.Count);
            else
                AddObject(classTo, methodTo, nodeFrom.xPositionIndex+1);
        }

        private static void AddObject(string className, string methodName, int positionIndex = 0)
        {
#if !UNITY_EDITOR
    return;
#endif
            var graph = GetSingleton();
            
            var nodeAlreadyExist = graph.nodes.Any(t => t.name == className);
            if (LogEvents)
                Debug.Log("AddObject: className="+className+", positionIndex="+positionIndex+", nodeAlreadyExist="+nodeAlreadyExist);
            if (nodeAlreadyExist)
                return;
            
            var addedNode = graph.AddNode<ExeNode>();
            addedNode.xPositionIndex = positionIndex;
            //var classObjectType = classObject.GetType().Name;
            addedNode.name = className;//+"."+methodName;

            var nodeIndex = graph.nodes.IndexOf(addedNode);
            addedNode.position = new Vector2(positionIndex * NODE_WIDTH,  nodeIndex * NODE_HEIGHT);
        }

        private static void Connect(string classFrom, string methodFrom, string classTo, string methodTo)
        {
            //Debug.LogWarning("Connect: classFrom="+classFrom+", methodFrom="+methodFrom+", classTo="+classTo+", methodTo="+methodTo);
            var graph = GetSingleton();
            var nodes = graph.nodes;
            // find from
            var fromName = classFrom + "." + methodFrom;
            var nodeFrom = nodes.FirstOrDefault(t => t.name == classFrom);
            // find to
            var toName = classTo + "." + methodTo;
            var nodeTo = nodes.FirstOrDefault(t => t.name == classTo);

            var nodeFromExe = (ExeNode)nodeFrom;
            var nodeToExe = (ExeNode)nodeTo;
            if (nodeFromExe == null || nodeToExe == null)
            {
                Debug.LogError("Connect: nodeFromExe="+nodeFromExe+", nodeToExe="+nodeToExe);
                return;
            }

            //var nodePort = nodeTo.AddDynamicInput(typeof(ExeNode), Node.ConnectionType.Override, Node.TypeConstraint.None, fromName);
            //nodePort.


            string outMethodName = "out " + methodFrom;
            var fromPort = nodeFromExe.DynamicInputs.FirstOrDefault(t=>t.fieldName == outMethodName);
            //var fromPort = nodeFromExe.DynamicOutputs.FirstOrDefault();
            if (fromPort == null)
            {
                fromPort = nodeFromExe.AddDynamicOutput(typeof(XNode.NodePort), Node.ConnectionType.Multiple,
                    Node.TypeConstraint.None, outMethodName);
            }

            string toMethodName = "in " + methodTo;
            var toPort = nodeToExe.DynamicInputs.FirstOrDefault(t=>t.fieldName == toMethodName);
            //var toPort = nodeToExe.DynamicInputs.FirstOrDefault();
            if (toPort == null)
            {
                toPort = nodeToExe.AddDynamicInput(typeof(XNode.NodePort), Node.ConnectionType.Multiple,
                    Node.TypeConstraint.None, toMethodName);
            }
            
            //nodeToExe.input = new NodePort();

            // if (nodeFromExe.output == null)
            // {
            //     Debug.LogError("nodeFromExe.output is null");
            //     throw new UnityException("nodeFromExe.output is null");
            // }
            
            //Assert2.IsNotNull(fromPort);
            //Assert2.IsNotNull(toPort);
            
            fromPort.Connect(toPort);
            
            //Debug.LogWarning("Connect: from="+classFrom+"."+methodFrom+", to="+classTo+"."+methodTo);
        }

        public static void AddSelf()
        {
            System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace();
            //LogCallStack(st);
            var frame = st.GetFrame(1);
            GetFrameClassMethod(frame, out var className, out var methodName);
            //Debug.LogWarning("AddSelf: self="+className+"."+methodName);
            AddObject(className, methodName);
        }

        public static string GetNameFromAction(Func<UniTask> callback)
        {
            string name = "";
            
            var method = callback.Method;
            var target = callback.Target;

            if (method.DeclaringType.Name.Contains("c__DisplayClass"))
            {
                Debug.LogError("incorrect method name="+method.DeclaringType.Name);
                throw new UnityException();
            }

            name = method.DeclaringType.Name + "." + method.Name;
            
            //Debug.LogError("GetNameFromAction: name="+name);
            
            return name;
        }

        public static string GetNameFromAction(Action callback)
        {
            string name = "";

            var method = callback.Method;
            var target = callback.Target;

            if (method.DeclaringType.Name.Contains("c__DisplayClass"))
            {
                Debug.LogError("incorrect method name="+method.DeclaringType.Name);
                throw new UnityException();
            }

            name = method.DeclaringType.Name + "." + method.Name;
            
            //Debug.LogError("GetNameFromAction: name="+name);
            
            return name;
        }

        public static string GetNameFromStack()
        {
            string name = "";
            System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace();
            GetFirstNormalName(st, out int frameIndex);
            
            var frame = st.GetFrame(frameIndex);

            GetFrameClassMethod(frame, out var classFrom, out var methodFrom);
            name = classFrom + "." + methodFrom;

            return name;

            void GetFirstNormalName(StackTrace st, out int frameIndex)
            {
                //Debug.LogError("GetNameFromStack");
                //LogCallStack(st);
                
                var max = st.FrameCount > 9 ? 9 : st.FrameCount;
                for (int i = 0; i < max; i++)
                {
                    GetFrame(st, i, out var className, out var methodName, out string nameSpace);
                    //var frame = st.GetFrame(i);
                    //var method = frame.GetMethod();
                    //var decType = method.DeclaringType;
                    //var methodName = method.Name;
                    //var className = method.DeclaringType.Name;
                    //var decTypeNamespace = decType.Namespace;
                    //if (decTypeNamespace != TARGET_NAMESPACE) continue;

                    string info = "class="+className+", method="+methodName+", nameSpace="+nameSpace;
                    //Debug.LogError("  GetNameFromStack: i="+i+" "+info);

                    if (className == "ManagerMain" || className == "MainView")
                    {
                        frameIndex = i;
                        return;
                    }
                    
                    if (nameSpace == "Shared") continue;
                    if (nameSpace == "System.Reflection") continue;
                    if (className.Contains("UniTask"))
                        continue;
                    if (methodName == "MoveNext") continue;
                    //if (methodName.Contains("<")) continue;
                    if (className == "AwaiterActions") continue;
                    
                    frameIndex = i;
                    return;
                }

                LogCallStack(st);
                throw new UnityException("frameIndex not found");
            }
        }
        
        public static void AddInvokeTo(string toName)
        {
            var fromName = GetNameFromStack();
            var classFrom = fromName.Substring(0, fromName.IndexOf('.'));
            var methodFrom = fromName.Substring(fromName.IndexOf('.')+1, fromName.Length-fromName.IndexOf('.')-1);
            var classTo = toName.Substring(0, toName.IndexOf('.'));
            var methodTo = toName.Substring(toName.IndexOf('.')+1, toName.Length-toName.IndexOf('.')-1);

            if (classFrom == classTo)
            {
                //Debug.LogError("ExeGraph.AddInvokeTo: (same class) from=" + fromName + ", to=" + toName + ", (from=" + classFrom + "." + methodFrom + ", to=" + classTo + "." +methodTo + ")");
            }
            else if (LogEvents)
                Debug.Log("ExeGraph.AddInvokeTo: from="+fromName+", to="+toName+", (from="+classFrom+"."+methodFrom+", to="+classTo+"."+methodTo+")");
            
            AddObject(classFrom, methodFrom, classTo, methodTo);
            
            Connect(classFrom, methodFrom, classTo, methodTo);
        }
        
        // public static void AddCall()
        // {
        //     return;
        //     GetCallStack(out var classFrom, out var methodFrom, out var classTo, out var methodTo);
        //     
        //     var graph = GetSingleton();
        //     var nodes = graph.nodes;
        //     Debug.LogError("[Node] CallStack: from="+classFrom+"."+methodFrom+", to="+classTo+"."+methodTo+" (nodes="+graph.nodes.Count+")");
        //     
        //     AddObject(classFrom, methodFrom, classTo, methodTo);
        //     
        //     Connect(classFrom, methodFrom, classTo, methodTo);
        // }

        private static void LogCallStack(StackTrace st)
        {
            var max = st.FrameCount > 7 ? 7 : st.FrameCount;
            for (int i = 0; i < max; i++)
            {
                GetFrame(st, i, out var className, out var methodName, out string nameSpace);
                string info = "class="+className+", method="+methodName+", nameSpace="+nameSpace;
                Debug.LogWarning("  i="+i+" "+info);
            }
        }

        private static void GetFrame(StackTrace st, int i, out string className, out string methodName, out string nameSpace)
        {
            var frame = st.GetFrame(i);
            var method = frame.GetMethod();
            var decType = method.DeclaringType;
            nameSpace = decType.Namespace;
            className = method.DeclaringType.Name;
            methodName = method.Name;
            if (methodName == "MoveNext")
            {
                var decType1 = decType?.DeclaringType;
                className = decType1?.Name;
                methodName = decType?.Name;
            }
        }
        
        private static void GetCallStack(out string classFrom, out string methodFrom, out string classTo, out string methodTo)
        {
            classFrom=""; methodFrom="";
            classTo = ""; methodTo="";
            System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace();

            GetFirstNonAsyncCall(st, out int frameIndex);
            
            //Debug.LogWarning("frameIndex="+frameIndex);
            
            var stTo = st.GetFrame(frameIndex);
            var stFrom = st.GetFrame(frameIndex+1);

            GetFrameClassMethod(stFrom, out classFrom, out methodFrom);
            GetFrameClassMethod(stTo, out classTo, out methodTo);

            void GetFirstNonAsyncCall(StackTrace st, out int frameIndex)
            {
                LogCallStack(st);
                var max = st.FrameCount > 7 ? 7 : st.FrameCount;
                for (int i = 0; i < max; i++)
                {
                    var frame = st.GetFrame(i);
                    var method = frame.GetMethod();
                    var decType = method.DeclaringType;
                    var decTypeNamespace = decType.Namespace;
                    if (decTypeNamespace != TARGET_NAMESPACE) continue;
                    
                    //var decTypeName = decType.Name;
                    //if (decTypeName == "ExeGraph") continue;
                    
                    
                    var methodName = method.Name;
                    if (methodName == "MoveNext") continue;
                    if (methodName.Contains("<"))
                        continue;
                    //var className = method.DeclaringType.Name;
                    //if (className == "AsyncUniTaskMethodBuilder") continue;

                    frameIndex = i;
                    return;
                }

                LogCallStack(st);
                throw new UnityException("frameIndex not found");
            }
        }
        
        private static void GetFrameClassMethod(StackFrame frame, out string className, out string methodName)
        {
            var method = frame.GetMethod();
            className = method.DeclaringType.Name;
            methodName = method.Name;
            if (methodName == "MoveNext")
            {
                // async
                var decType = method.DeclaringType;
                //name0 = decType?.Name;
                var decType1 = decType.DeclaringType;
                //name1 = decType1?.Name;

                className = decType1.Name;
                methodName = decType.Name;
                if (methodName.Contains("<"))
                {
                    methodName = methodName.Substring(methodName.IndexOf("<")+1,
                        methodName.IndexOf(">")-methodName.IndexOf("<")-1);
                }
            }
        }
    }
}