﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;

namespace Shared
{
    public class TrackedEventASync
    {
        private readonly List<Func<UniTask>> _callbacks = new List<Func<UniTask>>();
        private readonly List<string> _callbacksName = new List<string>();

        public void ReAdd(Func<UniTask> callback)
        {
            _callbacks.Remove(callback);
            _callbacks.Add(callback);
            var callbackName = ExeGraph.GetNameFromAction(callback);
            _callbacksName.Add(callbackName);
        }

        public void Remove(Func<UniTask> callback)
        {
            var index = _callbacks.IndexOf(callback);
            _callbacks.RemoveAt(index);
            _callbacksName.RemoveAt(index);
        }

        public async UniTask InvokeAsync()
        {
            var callbacks = new List<Func<UniTask>>(_callbacks);
            var maxCount = callbacks.Count;
            for (var index = 0; index < maxCount; index++)
            {
                ExeGraph.AddInvokeTo(_callbacksName[index]);
                await callbacks[index].Invoke();
            }
        }
    }
}