using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shared
{
    public class TrackedEvent
    {
        //private static bool _isFirstTime;
        
        private readonly List<Action> _callbacks = new List<Action>();
        private readonly List<string> _callbacksName = new List<string>();

        // public TrackedEvent()
        // {
        //     if (_isFirstTime == false)
        //         ExeGraph.Clear();
        //     _isFirstTime = true;
        // }

        public void ReAdd(Action callback)
        {
            _callbacks.Remove(callback);
            _callbacks.Add(callback);
            var callbackName = ExeGraph.GetNameFromAction(callback);
            _callbacksName.Add(callbackName);
        }
        
        public void Remove(Action callback)
        {
            var index = _callbacks.IndexOf(callback);
            _callbacks.RemoveAt(index);
            _callbacksName.RemoveAt(index);
        }

        public void Invoke()
        {
            List<Action> callbacks = new List<Action>(_callbacks);
            var maxCount = callbacks.Count;
            for (var index = 0; index < maxCount; index++)
            {
                ExeGraph.AddInvokeTo(_callbacksName[index]);
                callbacks[index].Invoke();
            }
        }
        
        public async Task Wait(int msDelay = 10)
        {
            bool isInvoked = false;
            void Set()  {  isInvoked = true; }
            ReAdd(Set);
            while (!isInvoked)
            {
                await Task.Delay(msDelay); // ms
            }
            Remove(Set);
        }
    }
}