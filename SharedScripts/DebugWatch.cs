﻿using System.Diagnostics;
using System.IO;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Shared
{
    public class DebugWatch
    {
        private Stopwatch _stopwatch;

        public void Start()
        {
            //StopWatchLog f = new StopWatchLog("asds");
            _stopwatch = new Stopwatch();
            _stopwatch.Start();
        }

        public void Stop()
        {
            _stopwatch.Stop();
            //var est = UnityEngine.StackTraceUtility.ExtractStackTrace ();
            var st = new StackTrace(1, true);
            var st0 = st.GetFrame(0);
            var className = Path.GetFileName(st0.GetFileName()).Replace(".cs", "");
            Debug.Log("DebugWatch(frame="+Time.frameCount+"): "
                      +className+"."+st0.GetMethod().Name+": "+_stopwatch.Elapsed.TotalSeconds+" seconds");
        }
    }
}